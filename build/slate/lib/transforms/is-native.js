'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

/**
 * Transforms.
 *
 * @type {Object}
 */

var Transforms = {};

/**
 * Set the `isNative` flag on the underlying state to prevent re-renders.
 *
 * @param {Transform} transform
 * @param {Boolean} value
 */

Transforms.isNative = function (transform, value) {
  var state = transform.state;

  state = state.set('isNative', value);
  transform.state = state;
};

/**
 * Export.
 *
 * @type {Object}
 */

exports.default = Transforms;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy90cmFuc2Zvcm1zL2lzLW5hdGl2ZS5qcyJdLCJuYW1lcyI6WyJUcmFuc2Zvcm1zIiwiaXNOYXRpdmUiLCJ0cmFuc2Zvcm0iLCJ2YWx1ZSIsInN0YXRlIiwic2V0Il0sIm1hcHBpbmdzIjoiOzs7Ozs7QUFDQTs7Ozs7O0FBTUEsSUFBTUEsYUFBYSxFQUFuQjs7QUFFQTs7Ozs7OztBQU9BQSxXQUFXQyxRQUFYLEdBQXNCLFVBQUNDLFNBQUQsRUFBWUMsS0FBWixFQUFzQjtBQUFBLE1BQ3BDQyxLQURvQyxHQUMxQkYsU0FEMEIsQ0FDcENFLEtBRG9DOztBQUUxQ0EsVUFBUUEsTUFBTUMsR0FBTixDQUFVLFVBQVYsRUFBc0JGLEtBQXRCLENBQVI7QUFDQUQsWUFBVUUsS0FBVixHQUFrQkEsS0FBbEI7QUFDRCxDQUpEOztBQU1BOzs7Ozs7a0JBTWVKLFUiLCJmaWxlIjoiaXMtbmF0aXZlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiXG4vKipcbiAqIFRyYW5zZm9ybXMuXG4gKlxuICogQHR5cGUge09iamVjdH1cbiAqL1xuXG5jb25zdCBUcmFuc2Zvcm1zID0ge31cblxuLyoqXG4gKiBTZXQgdGhlIGBpc05hdGl2ZWAgZmxhZyBvbiB0aGUgdW5kZXJseWluZyBzdGF0ZSB0byBwcmV2ZW50IHJlLXJlbmRlcnMuXG4gKlxuICogQHBhcmFtIHtUcmFuc2Zvcm19IHRyYW5zZm9ybVxuICogQHBhcmFtIHtCb29sZWFufSB2YWx1ZVxuICovXG5cblRyYW5zZm9ybXMuaXNOYXRpdmUgPSAodHJhbnNmb3JtLCB2YWx1ZSkgPT4ge1xuICBsZXQgeyBzdGF0ZSB9ID0gdHJhbnNmb3JtXG4gIHN0YXRlID0gc3RhdGUuc2V0KCdpc05hdGl2ZScsIHZhbHVlKVxuICB0cmFuc2Zvcm0uc3RhdGUgPSBzdGF0ZVxufVxuXG4vKipcbiAqIEV4cG9ydC5cbiAqXG4gKiBAdHlwZSB7T2JqZWN0fVxuICovXG5cbmV4cG9ydCBkZWZhdWx0IFRyYW5zZm9ybXNcbiJdfQ==