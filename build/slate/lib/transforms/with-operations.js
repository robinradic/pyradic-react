'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _normalize = require('../utils/normalize');

var _normalize2 = _interopRequireDefault(_normalize);

var _apply = require('../operations/apply');

var _apply2 = _interopRequireDefault(_apply);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Transforms.
 *
 * @type {Object}
 */

var Transforms = {};

/**
 * Apply an `operation` to the current state.
 *
 * @param {Transform} transform
 * @param {Object} operation
 */

Transforms.applyOperation = function (transform, operation) {
  var state = transform.state,
      operations = transform.operations;

  transform.state = (0, _apply2.default)(state, operation);
  transform.operations = operations.concat([operation]);
};

/**
 * Add mark to text at `offset` and `length` in node by `path`.
 *
 * @param {Transform} transform
 * @param {Array} path
 * @param {Number} offset
 * @param {Number} length
 * @param {Mixed} mark
 */

Transforms.addMarkOperation = function (transform, path, offset, length, mark) {
  var inverse = [{
    type: 'remove_mark',
    path: path,
    offset: offset,
    length: length,
    mark: mark
  }];

  var operation = {
    type: 'add_mark',
    path: path,
    offset: offset,
    length: length,
    mark: mark,
    inverse: inverse
  };

  transform.applyOperation(operation);
};

/**
 * Insert a `node` at `path`.
 *
 * @param {Transform} transform
 * @param {Array} path
 * @param {Node} node
 */

Transforms.insertNodeOperation = function (transform, path, node) {
  var inverse = [{
    type: 'remove_node',
    path: path
  }];

  var operation = {
    type: 'insert_node',
    path: path,
    node: node,
    inverse: inverse
  };

  transform.applyOperation(operation);
};

/**
 * Insert `text` at `offset` in node by `path`.
 *
 * @param {Transform} transform
 * @param {Array} path
 * @param {Number} offset
 * @param {String} text
 * @param {Set<Mark>} marks (optional)
 */

Transforms.insertTextOperation = function (transform, path, offset, text, marks) {
  var inverseLength = text.length;
  var inverse = [{
    type: 'remove_text',
    path: path,
    offset: offset,
    length: inverseLength
  }];

  var operation = {
    type: 'insert_text',
    path: path,
    offset: offset,
    text: text,
    marks: marks,
    inverse: inverse
  };

  transform.applyOperation(operation);
};

/**
 * Join a node by `path` with a node `withPath`.
 *
 * @param {Transform} transform
 * @param {Array} path
 * @param {Array} withPath
 */

Transforms.joinNodeOperation = function (transform, path, withPath) {
  var state = transform.state;
  var document = state.document;

  var node = document.assertPath(withPath);

  var inverse = [{
    type: 'split_node',
    path: withPath,
    position: node.kind == 'text' ? node.length : node.nodes.size
  }];

  var operation = {
    type: 'join_node',
    path: path,
    withPath: withPath,
    inverse: inverse
  };

  transform.applyOperation(operation);
};

/**
 * Move a node by `path` to a `newPath` and `newIndex`.
 *
 * @param {Transform} transform
 * @param {Array} path
 * @param {Array} newPath
 * @param {Number} newIndex
 */

Transforms.moveNodeOperation = function (transform, path, newPath) {
  var inverse = [{
    type: 'move_node',
    path: newPath,
    newPath: path
  }];

  var operation = {
    type: 'move_node',
    path: path,
    newPath: newPath,
    inverse: inverse
  };

  transform.applyOperation(operation);
};

/**
 * Remove mark from text at `offset` and `length` in node by `path`.
 *
 * @param {Transform} transform
 * @param {Array} path
 * @param {Number} offset
 * @param {Number} length
 * @param {Mark} mark
 */

Transforms.removeMarkOperation = function (transform, path, offset, length, mark) {
  var inverse = [{
    type: 'add_mark',
    path: path,
    offset: offset,
    length: length,
    mark: mark
  }];

  var operation = {
    type: 'remove_mark',
    path: path,
    offset: offset,
    length: length,
    mark: mark,
    inverse: inverse
  };

  transform.applyOperation(operation);
};

/**
 * Remove a node by `path`.
 *
 * @param {Transform} transform
 * @param {Array} path
 */

Transforms.removeNodeOperation = function (transform, path) {
  var state = transform.state;
  var document = state.document;

  var node = document.assertPath(path);

  var inverse = [{
    type: 'insert_node',
    path: path,
    node: node
  }];

  var operation = {
    type: 'remove_node',
    path: path,
    inverse: inverse
  };

  transform.applyOperation(operation);
};

/**
 * Remove text at `offset` and `length` in node by `path`.
 *
 * @param {Transform} transform
 * @param {Array} path
 * @param {Number} offset
 * @param {Number} length
 */

Transforms.removeTextOperation = function (transform, path, offset, length) {
  var state = transform.state;
  var document = state.document;

  var node = document.assertPath(path);
  var ranges = node.getRanges();
  var inverse = [];

  // Loop the ranges of text in the node, creating inverse insert operations for
  // each of the ranges that overlap with the remove operation. This is
  // necessary because insert's can only have a single set of marks associated
  // with them, but removes can remove many.
  ranges.reduce(function (start, range) {
    var text = range.text,
        marks = range.marks;

    var end = start + text.length;
    if (start > offset + length) return end;
    if (end <= offset) return end;

    var endOffset = Math.min(end, offset + length);
    var string = text.slice(offset - start, endOffset - start);

    inverse.push({
      type: 'insert_text',
      path: path,
      offset: offset,
      text: string,
      marks: marks
    });

    return end;
  }, 0);

  var operation = {
    type: 'remove_text',
    path: path,
    offset: offset,
    length: length,
    inverse: inverse
  };

  transform.applyOperation(operation);
};

/**
 * Merge `properties` into state `data`.
 *
 * @param {Transform} transform
 * @param {Object} properties
 */

Transforms.setDataOperation = function (transform, properties) {
  var state = transform.state;
  var data = state.data;

  var inverseProps = {};

  for (var k in properties) {
    inverseProps[k] = data[k];
  }

  var inverse = [{
    type: 'set_data',
    properties: inverseProps
  }];

  var operation = {
    type: 'set_data',
    properties: properties,
    inverse: inverse
  };

  transform.applyOperation(operation);
};

/**
 * Set `properties` on mark on text at `offset` and `length` in node by `path`.
 *
 * @param {Transform} transform
 * @param {Array} path
 * @param {Number} offset
 * @param {Number} length
 * @param {Mark} mark
 * @param {Mark} newMark
 */

Transforms.setMarkOperation = function (transform, path, offset, length, mark, newMark) {
  var inverse = [{
    type: 'set_mark',
    path: path,
    offset: offset,
    length: length,
    mark: newMark,
    newMark: mark
  }];

  var operation = {
    type: 'set_mark',
    path: path,
    offset: offset,
    length: length,
    mark: mark,
    newMark: newMark,
    inverse: inverse
  };

  transform.applyOperation(operation);
};

/**
 * Set `properties` on a node by `path`.
 *
 * @param {Transform} transform
 * @param {Array} path
 * @param {Object} properties
 */

Transforms.setNodeOperation = function (transform, path, properties) {
  var state = transform.state;
  var document = state.document;

  var node = document.assertPath(path);
  var inverseProps = {};

  for (var k in properties) {
    inverseProps[k] = node[k];
  }

  var inverse = [{
    type: 'set_node',
    path: path,
    properties: inverseProps
  }];

  var operation = {
    type: 'set_node',
    path: path,
    properties: properties,
    inverse: inverse
  };

  transform.applyOperation(operation);
};

/**
 * Set the selection to a new `selection`.
 *
 * @param {Transform} transform
 * @param {Mixed} selection
 */

Transforms.setSelectionOperation = function (transform, properties) {
  var options = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};

  properties = _normalize2.default.selectionProperties(properties);

  var state = transform.state;
  var document = state.document,
      selection = state.selection;

  var prevProps = {};
  var props = {};

  // Remove any properties that are already equal to the current selection. And
  // create a dictionary of the previous values for all of the properties that
  // are being changed, for the inverse operation.
  for (var k in properties) {
    if (!options.snapshot && properties[k] == selection[k]) continue;
    props[k] = properties[k];
    prevProps[k] = selection[k];
  }

  // If the selection moves, clear any marks, unless the new selection
  // does change the marks in some way
  var moved = ['anchorKey', 'anchorOffset', 'focusKey', 'focusOffset'].some(function (p) {
    return props.hasOwnProperty(p);
  });

  if (selection.marks && properties.marks == selection.marks && moved) {
    props.marks = null;
  }

  // Resolve the selection keys into paths.
  if (props.anchorKey) {
    props.anchorPath = document.getPath(props.anchorKey);
    delete props.anchorKey;
  }

  if (prevProps.anchorKey) {
    prevProps.anchorPath = document.getPath(prevProps.anchorKey);
    delete prevProps.anchorKey;
  }

  if (props.focusKey) {
    props.focusPath = document.getPath(props.focusKey);
    delete props.focusKey;
  }

  if (prevProps.focusKey) {
    prevProps.focusPath = document.getPath(prevProps.focusKey);
    delete prevProps.focusKey;
  }

  // Define an inverse of the operation for undoing.
  var inverse = [{
    type: 'set_selection',
    properties: prevProps
  }];

  // Define the operation.
  var operation = {
    type: 'set_selection',
    properties: props,
    inverse: inverse
  };

  // Apply the operation.
  transform.applyOperation(operation);
};

/**
 * Split a node by `path` at `position`.
 *
 * @param {Transform} transform
 * @param {Array} path
 * @param {Number} position
 */

Transforms.splitNodeOperation = function (transform, path, position) {
  var inversePath = path.slice();
  inversePath[path.length - 1] += 1;

  var inverse = [{
    type: 'join_node',
    path: inversePath,
    withPath: path
  }];

  var operation = {
    type: 'split_node',
    path: path,
    position: position,
    inverse: inverse
  };

  transform.applyOperation(operation);
};

/**
 * Export.
 *
 * @type {Object}
 */

exports.default = Transforms;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy90cmFuc2Zvcm1zL3dpdGgtb3BlcmF0aW9ucy5qcyJdLCJuYW1lcyI6WyJUcmFuc2Zvcm1zIiwiYXBwbHlPcGVyYXRpb24iLCJ0cmFuc2Zvcm0iLCJvcGVyYXRpb24iLCJzdGF0ZSIsIm9wZXJhdGlvbnMiLCJjb25jYXQiLCJhZGRNYXJrT3BlcmF0aW9uIiwicGF0aCIsIm9mZnNldCIsImxlbmd0aCIsIm1hcmsiLCJpbnZlcnNlIiwidHlwZSIsImluc2VydE5vZGVPcGVyYXRpb24iLCJub2RlIiwiaW5zZXJ0VGV4dE9wZXJhdGlvbiIsInRleHQiLCJtYXJrcyIsImludmVyc2VMZW5ndGgiLCJqb2luTm9kZU9wZXJhdGlvbiIsIndpdGhQYXRoIiwiZG9jdW1lbnQiLCJhc3NlcnRQYXRoIiwicG9zaXRpb24iLCJraW5kIiwibm9kZXMiLCJzaXplIiwibW92ZU5vZGVPcGVyYXRpb24iLCJuZXdQYXRoIiwicmVtb3ZlTWFya09wZXJhdGlvbiIsInJlbW92ZU5vZGVPcGVyYXRpb24iLCJyZW1vdmVUZXh0T3BlcmF0aW9uIiwicmFuZ2VzIiwiZ2V0UmFuZ2VzIiwicmVkdWNlIiwic3RhcnQiLCJyYW5nZSIsImVuZCIsImVuZE9mZnNldCIsIk1hdGgiLCJtaW4iLCJzdHJpbmciLCJzbGljZSIsInB1c2giLCJzZXREYXRhT3BlcmF0aW9uIiwicHJvcGVydGllcyIsImRhdGEiLCJpbnZlcnNlUHJvcHMiLCJrIiwic2V0TWFya09wZXJhdGlvbiIsIm5ld01hcmsiLCJzZXROb2RlT3BlcmF0aW9uIiwic2V0U2VsZWN0aW9uT3BlcmF0aW9uIiwib3B0aW9ucyIsInNlbGVjdGlvblByb3BlcnRpZXMiLCJzZWxlY3Rpb24iLCJwcmV2UHJvcHMiLCJwcm9wcyIsInNuYXBzaG90IiwibW92ZWQiLCJzb21lIiwiaGFzT3duUHJvcGVydHkiLCJwIiwiYW5jaG9yS2V5IiwiYW5jaG9yUGF0aCIsImdldFBhdGgiLCJmb2N1c0tleSIsImZvY3VzUGF0aCIsInNwbGl0Tm9kZU9wZXJhdGlvbiIsImludmVyc2VQYXRoIl0sIm1hcHBpbmdzIjoiOzs7Ozs7QUFDQTs7OztBQUNBOzs7Ozs7QUFFQTs7Ozs7O0FBTUEsSUFBTUEsYUFBYSxFQUFuQjs7QUFFQTs7Ozs7OztBQU9BQSxXQUFXQyxjQUFYLEdBQTRCLFVBQUNDLFNBQUQsRUFBWUMsU0FBWixFQUEwQjtBQUFBLE1BQzVDQyxLQUQ0QyxHQUN0QkYsU0FEc0IsQ0FDNUNFLEtBRDRDO0FBQUEsTUFDckNDLFVBRHFDLEdBQ3RCSCxTQURzQixDQUNyQ0csVUFEcUM7O0FBRXBESCxZQUFVRSxLQUFWLEdBQWtCLHFCQUFNQSxLQUFOLEVBQWFELFNBQWIsQ0FBbEI7QUFDQUQsWUFBVUcsVUFBVixHQUF1QkEsV0FBV0MsTUFBWCxDQUFrQixDQUFDSCxTQUFELENBQWxCLENBQXZCO0FBQ0QsQ0FKRDs7QUFNQTs7Ozs7Ozs7OztBQVVBSCxXQUFXTyxnQkFBWCxHQUE4QixVQUFDTCxTQUFELEVBQVlNLElBQVosRUFBa0JDLE1BQWxCLEVBQTBCQyxNQUExQixFQUFrQ0MsSUFBbEMsRUFBMkM7QUFDdkUsTUFBTUMsVUFBVSxDQUFDO0FBQ2ZDLFVBQU0sYUFEUztBQUVmTCxjQUZlO0FBR2ZDLGtCQUhlO0FBSWZDLGtCQUplO0FBS2ZDO0FBTGUsR0FBRCxDQUFoQjs7QUFRQSxNQUFNUixZQUFZO0FBQ2hCVSxVQUFNLFVBRFU7QUFFaEJMLGNBRmdCO0FBR2hCQyxrQkFIZ0I7QUFJaEJDLGtCQUpnQjtBQUtoQkMsY0FMZ0I7QUFNaEJDO0FBTmdCLEdBQWxCOztBQVNBVixZQUFVRCxjQUFWLENBQXlCRSxTQUF6QjtBQUNELENBbkJEOztBQXFCQTs7Ozs7Ozs7QUFRQUgsV0FBV2MsbUJBQVgsR0FBaUMsVUFBQ1osU0FBRCxFQUFZTSxJQUFaLEVBQWtCTyxJQUFsQixFQUEyQjtBQUMxRCxNQUFNSCxVQUFVLENBQUM7QUFDZkMsVUFBTSxhQURTO0FBRWZMO0FBRmUsR0FBRCxDQUFoQjs7QUFLQSxNQUFNTCxZQUFZO0FBQ2hCVSxVQUFNLGFBRFU7QUFFaEJMLGNBRmdCO0FBR2hCTyxjQUhnQjtBQUloQkg7QUFKZ0IsR0FBbEI7O0FBT0FWLFlBQVVELGNBQVYsQ0FBeUJFLFNBQXpCO0FBQ0QsQ0FkRDs7QUFnQkE7Ozs7Ozs7Ozs7QUFVQUgsV0FBV2dCLG1CQUFYLEdBQWlDLFVBQUNkLFNBQUQsRUFBWU0sSUFBWixFQUFrQkMsTUFBbEIsRUFBMEJRLElBQTFCLEVBQWdDQyxLQUFoQyxFQUEwQztBQUN6RSxNQUFNQyxnQkFBZ0JGLEtBQUtQLE1BQTNCO0FBQ0EsTUFBTUUsVUFBVSxDQUFDO0FBQ2ZDLFVBQU0sYUFEUztBQUVmTCxjQUZlO0FBR2ZDLGtCQUhlO0FBSWZDLFlBQVFTO0FBSk8sR0FBRCxDQUFoQjs7QUFPQSxNQUFNaEIsWUFBWTtBQUNoQlUsVUFBTSxhQURVO0FBRWhCTCxjQUZnQjtBQUdoQkMsa0JBSGdCO0FBSWhCUSxjQUpnQjtBQUtoQkMsZ0JBTGdCO0FBTWhCTjtBQU5nQixHQUFsQjs7QUFTQVYsWUFBVUQsY0FBVixDQUF5QkUsU0FBekI7QUFDRCxDQW5CRDs7QUFxQkE7Ozs7Ozs7O0FBUUFILFdBQVdvQixpQkFBWCxHQUErQixVQUFDbEIsU0FBRCxFQUFZTSxJQUFaLEVBQWtCYSxRQUFsQixFQUErQjtBQUFBLE1BQ3BEakIsS0FEb0QsR0FDMUNGLFNBRDBDLENBQ3BERSxLQURvRDtBQUFBLE1BRXBEa0IsUUFGb0QsR0FFdkNsQixLQUZ1QyxDQUVwRGtCLFFBRm9EOztBQUc1RCxNQUFNUCxPQUFPTyxTQUFTQyxVQUFULENBQW9CRixRQUFwQixDQUFiOztBQUVBLE1BQU1ULFVBQVUsQ0FBQztBQUNmQyxVQUFNLFlBRFM7QUFFZkwsVUFBTWEsUUFGUztBQUdmRyxjQUFVVCxLQUFLVSxJQUFMLElBQWEsTUFBYixHQUFzQlYsS0FBS0wsTUFBM0IsR0FBb0NLLEtBQUtXLEtBQUwsQ0FBV0M7QUFIMUMsR0FBRCxDQUFoQjs7QUFNQSxNQUFNeEIsWUFBWTtBQUNoQlUsVUFBTSxXQURVO0FBRWhCTCxjQUZnQjtBQUdoQmEsc0JBSGdCO0FBSWhCVDtBQUpnQixHQUFsQjs7QUFPQVYsWUFBVUQsY0FBVixDQUF5QkUsU0FBekI7QUFDRCxDQW5CRDs7QUFxQkE7Ozs7Ozs7OztBQVNBSCxXQUFXNEIsaUJBQVgsR0FBK0IsVUFBQzFCLFNBQUQsRUFBWU0sSUFBWixFQUFrQnFCLE9BQWxCLEVBQThCO0FBQzNELE1BQU1qQixVQUFVLENBQUM7QUFDZkMsVUFBTSxXQURTO0FBRWZMLFVBQU1xQixPQUZTO0FBR2ZBLGFBQVNyQjtBQUhNLEdBQUQsQ0FBaEI7O0FBTUEsTUFBTUwsWUFBWTtBQUNoQlUsVUFBTSxXQURVO0FBRWhCTCxjQUZnQjtBQUdoQnFCLG9CQUhnQjtBQUloQmpCO0FBSmdCLEdBQWxCOztBQU9BVixZQUFVRCxjQUFWLENBQXlCRSxTQUF6QjtBQUNELENBZkQ7O0FBaUJBOzs7Ozs7Ozs7O0FBVUFILFdBQVc4QixtQkFBWCxHQUFpQyxVQUFDNUIsU0FBRCxFQUFZTSxJQUFaLEVBQWtCQyxNQUFsQixFQUEwQkMsTUFBMUIsRUFBa0NDLElBQWxDLEVBQTJDO0FBQzFFLE1BQU1DLFVBQVUsQ0FBQztBQUNmQyxVQUFNLFVBRFM7QUFFZkwsY0FGZTtBQUdmQyxrQkFIZTtBQUlmQyxrQkFKZTtBQUtmQztBQUxlLEdBQUQsQ0FBaEI7O0FBUUEsTUFBTVIsWUFBWTtBQUNoQlUsVUFBTSxhQURVO0FBRWhCTCxjQUZnQjtBQUdoQkMsa0JBSGdCO0FBSWhCQyxrQkFKZ0I7QUFLaEJDLGNBTGdCO0FBTWhCQztBQU5nQixHQUFsQjs7QUFTQVYsWUFBVUQsY0FBVixDQUF5QkUsU0FBekI7QUFDRCxDQW5CRDs7QUFxQkE7Ozs7Ozs7QUFPQUgsV0FBVytCLG1CQUFYLEdBQWlDLFVBQUM3QixTQUFELEVBQVlNLElBQVosRUFBcUI7QUFBQSxNQUM1Q0osS0FENEMsR0FDbENGLFNBRGtDLENBQzVDRSxLQUQ0QztBQUFBLE1BRTVDa0IsUUFGNEMsR0FFL0JsQixLQUYrQixDQUU1Q2tCLFFBRjRDOztBQUdwRCxNQUFNUCxPQUFPTyxTQUFTQyxVQUFULENBQW9CZixJQUFwQixDQUFiOztBQUVBLE1BQU1JLFVBQVUsQ0FBQztBQUNmQyxVQUFNLGFBRFM7QUFFZkwsY0FGZTtBQUdmTztBQUhlLEdBQUQsQ0FBaEI7O0FBTUEsTUFBTVosWUFBWTtBQUNoQlUsVUFBTSxhQURVO0FBRWhCTCxjQUZnQjtBQUdoQkk7QUFIZ0IsR0FBbEI7O0FBTUFWLFlBQVVELGNBQVYsQ0FBeUJFLFNBQXpCO0FBQ0QsQ0FsQkQ7O0FBb0JBOzs7Ozs7Ozs7QUFTQUgsV0FBV2dDLG1CQUFYLEdBQWlDLFVBQUM5QixTQUFELEVBQVlNLElBQVosRUFBa0JDLE1BQWxCLEVBQTBCQyxNQUExQixFQUFxQztBQUFBLE1BQzVETixLQUQ0RCxHQUNsREYsU0FEa0QsQ0FDNURFLEtBRDREO0FBQUEsTUFFNURrQixRQUY0RCxHQUUvQ2xCLEtBRitDLENBRTVEa0IsUUFGNEQ7O0FBR3BFLE1BQU1QLE9BQU9PLFNBQVNDLFVBQVQsQ0FBb0JmLElBQXBCLENBQWI7QUFDQSxNQUFNeUIsU0FBU2xCLEtBQUttQixTQUFMLEVBQWY7QUFDQSxNQUFNdEIsVUFBVSxFQUFoQjs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBcUIsU0FBT0UsTUFBUCxDQUFjLFVBQUNDLEtBQUQsRUFBUUMsS0FBUixFQUFrQjtBQUFBLFFBQ3RCcEIsSUFEc0IsR0FDTm9CLEtBRE0sQ0FDdEJwQixJQURzQjtBQUFBLFFBQ2hCQyxLQURnQixHQUNObUIsS0FETSxDQUNoQm5CLEtBRGdCOztBQUU5QixRQUFNb0IsTUFBTUYsUUFBUW5CLEtBQUtQLE1BQXpCO0FBQ0EsUUFBSTBCLFFBQVEzQixTQUFTQyxNQUFyQixFQUE2QixPQUFPNEIsR0FBUDtBQUM3QixRQUFJQSxPQUFPN0IsTUFBWCxFQUFtQixPQUFPNkIsR0FBUDs7QUFFbkIsUUFBTUMsWUFBWUMsS0FBS0MsR0FBTCxDQUFTSCxHQUFULEVBQWM3QixTQUFTQyxNQUF2QixDQUFsQjtBQUNBLFFBQU1nQyxTQUFTekIsS0FBSzBCLEtBQUwsQ0FBV2xDLFNBQVMyQixLQUFwQixFQUEyQkcsWUFBWUgsS0FBdkMsQ0FBZjs7QUFFQXhCLFlBQVFnQyxJQUFSLENBQWE7QUFDWC9CLFlBQU0sYUFESztBQUVYTCxnQkFGVztBQUdYQyxvQkFIVztBQUlYUSxZQUFNeUIsTUFKSztBQUtYeEI7QUFMVyxLQUFiOztBQVFBLFdBQU9vQixHQUFQO0FBQ0QsR0FsQkQsRUFrQkcsQ0FsQkg7O0FBb0JBLE1BQU1uQyxZQUFZO0FBQ2hCVSxVQUFNLGFBRFU7QUFFaEJMLGNBRmdCO0FBR2hCQyxrQkFIZ0I7QUFJaEJDLGtCQUpnQjtBQUtoQkU7QUFMZ0IsR0FBbEI7O0FBUUFWLFlBQVVELGNBQVYsQ0FBeUJFLFNBQXpCO0FBQ0QsQ0F4Q0Q7O0FBMENBOzs7Ozs7O0FBT0FILFdBQVc2QyxnQkFBWCxHQUE4QixVQUFDM0MsU0FBRCxFQUFZNEMsVUFBWixFQUEyQjtBQUFBLE1BQy9DMUMsS0FEK0MsR0FDckNGLFNBRHFDLENBQy9DRSxLQUQrQztBQUFBLE1BRS9DMkMsSUFGK0MsR0FFdEMzQyxLQUZzQyxDQUUvQzJDLElBRitDOztBQUd2RCxNQUFNQyxlQUFlLEVBQXJCOztBQUVBLE9BQUssSUFBTUMsQ0FBWCxJQUFnQkgsVUFBaEIsRUFBNEI7QUFDMUJFLGlCQUFhQyxDQUFiLElBQWtCRixLQUFLRSxDQUFMLENBQWxCO0FBQ0Q7O0FBRUQsTUFBTXJDLFVBQVUsQ0FBQztBQUNmQyxVQUFNLFVBRFM7QUFFZmlDLGdCQUFZRTtBQUZHLEdBQUQsQ0FBaEI7O0FBS0EsTUFBTTdDLFlBQVk7QUFDaEJVLFVBQU0sVUFEVTtBQUVoQmlDLDBCQUZnQjtBQUdoQmxDO0FBSGdCLEdBQWxCOztBQU1BVixZQUFVRCxjQUFWLENBQXlCRSxTQUF6QjtBQUNELENBckJEOztBQXVCQTs7Ozs7Ozs7Ozs7QUFXQUgsV0FBV2tELGdCQUFYLEdBQThCLFVBQUNoRCxTQUFELEVBQVlNLElBQVosRUFBa0JDLE1BQWxCLEVBQTBCQyxNQUExQixFQUFrQ0MsSUFBbEMsRUFBd0N3QyxPQUF4QyxFQUFvRDtBQUNoRixNQUFNdkMsVUFBVSxDQUFDO0FBQ2ZDLFVBQU0sVUFEUztBQUVmTCxjQUZlO0FBR2ZDLGtCQUhlO0FBSWZDLGtCQUplO0FBS2ZDLFVBQU13QyxPQUxTO0FBTWZBLGFBQVN4QztBQU5NLEdBQUQsQ0FBaEI7O0FBU0EsTUFBTVIsWUFBWTtBQUNoQlUsVUFBTSxVQURVO0FBRWhCTCxjQUZnQjtBQUdoQkMsa0JBSGdCO0FBSWhCQyxrQkFKZ0I7QUFLaEJDLGNBTGdCO0FBTWhCd0Msb0JBTmdCO0FBT2hCdkM7QUFQZ0IsR0FBbEI7O0FBVUFWLFlBQVVELGNBQVYsQ0FBeUJFLFNBQXpCO0FBQ0QsQ0FyQkQ7O0FBdUJBOzs7Ozs7OztBQVFBSCxXQUFXb0QsZ0JBQVgsR0FBOEIsVUFBQ2xELFNBQUQsRUFBWU0sSUFBWixFQUFrQnNDLFVBQWxCLEVBQWlDO0FBQUEsTUFDckQxQyxLQURxRCxHQUMzQ0YsU0FEMkMsQ0FDckRFLEtBRHFEO0FBQUEsTUFFckRrQixRQUZxRCxHQUV4Q2xCLEtBRndDLENBRXJEa0IsUUFGcUQ7O0FBRzdELE1BQU1QLE9BQU9PLFNBQVNDLFVBQVQsQ0FBb0JmLElBQXBCLENBQWI7QUFDQSxNQUFNd0MsZUFBZSxFQUFyQjs7QUFFQSxPQUFLLElBQU1DLENBQVgsSUFBZ0JILFVBQWhCLEVBQTRCO0FBQzFCRSxpQkFBYUMsQ0FBYixJQUFrQmxDLEtBQUtrQyxDQUFMLENBQWxCO0FBQ0Q7O0FBRUQsTUFBTXJDLFVBQVUsQ0FBQztBQUNmQyxVQUFNLFVBRFM7QUFFZkwsY0FGZTtBQUdmc0MsZ0JBQVlFO0FBSEcsR0FBRCxDQUFoQjs7QUFNQSxNQUFNN0MsWUFBWTtBQUNoQlUsVUFBTSxVQURVO0FBRWhCTCxjQUZnQjtBQUdoQnNDLDBCQUhnQjtBQUloQmxDO0FBSmdCLEdBQWxCOztBQU9BVixZQUFVRCxjQUFWLENBQXlCRSxTQUF6QjtBQUNELENBeEJEOztBQTBCQTs7Ozs7OztBQU9BSCxXQUFXcUQscUJBQVgsR0FBbUMsVUFBQ25ELFNBQUQsRUFBWTRDLFVBQVosRUFBeUM7QUFBQSxNQUFqQlEsT0FBaUIsdUVBQVAsRUFBTzs7QUFDMUVSLGVBQWEsb0JBQVVTLG1CQUFWLENBQThCVCxVQUE5QixDQUFiOztBQUQwRSxNQUdsRTFDLEtBSGtFLEdBR3hERixTQUh3RCxDQUdsRUUsS0FIa0U7QUFBQSxNQUlsRWtCLFFBSmtFLEdBSTFDbEIsS0FKMEMsQ0FJbEVrQixRQUprRTtBQUFBLE1BSXhEa0MsU0FKd0QsR0FJMUNwRCxLQUowQyxDQUl4RG9ELFNBSndEOztBQUsxRSxNQUFNQyxZQUFZLEVBQWxCO0FBQ0EsTUFBTUMsUUFBUSxFQUFkOztBQUVBO0FBQ0E7QUFDQTtBQUNBLE9BQUssSUFBTVQsQ0FBWCxJQUFnQkgsVUFBaEIsRUFBNEI7QUFDMUIsUUFBSSxDQUFDUSxRQUFRSyxRQUFULElBQXFCYixXQUFXRyxDQUFYLEtBQWlCTyxVQUFVUCxDQUFWLENBQTFDLEVBQXdEO0FBQ3hEUyxVQUFNVCxDQUFOLElBQVdILFdBQVdHLENBQVgsQ0FBWDtBQUNBUSxjQUFVUixDQUFWLElBQWVPLFVBQVVQLENBQVYsQ0FBZjtBQUNEOztBQUVEO0FBQ0E7QUFDQSxNQUFNVyxRQUFRLENBQ1osV0FEWSxFQUVaLGNBRlksRUFHWixVQUhZLEVBSVosYUFKWSxFQUtaQyxJQUxZLENBS1A7QUFBQSxXQUFLSCxNQUFNSSxjQUFOLENBQXFCQyxDQUFyQixDQUFMO0FBQUEsR0FMTyxDQUFkOztBQU9BLE1BQ0VQLFVBQVV0QyxLQUFWLElBQ0E0QixXQUFXNUIsS0FBWCxJQUFvQnNDLFVBQVV0QyxLQUQ5QixJQUVBMEMsS0FIRixFQUlFO0FBQ0FGLFVBQU14QyxLQUFOLEdBQWMsSUFBZDtBQUNEOztBQUVEO0FBQ0EsTUFBSXdDLE1BQU1NLFNBQVYsRUFBcUI7QUFDbkJOLFVBQU1PLFVBQU4sR0FBbUIzQyxTQUFTNEMsT0FBVCxDQUFpQlIsTUFBTU0sU0FBdkIsQ0FBbkI7QUFDQSxXQUFPTixNQUFNTSxTQUFiO0FBQ0Q7O0FBRUQsTUFBSVAsVUFBVU8sU0FBZCxFQUF5QjtBQUN2QlAsY0FBVVEsVUFBVixHQUF1QjNDLFNBQVM0QyxPQUFULENBQWlCVCxVQUFVTyxTQUEzQixDQUF2QjtBQUNBLFdBQU9QLFVBQVVPLFNBQWpCO0FBQ0Q7O0FBRUQsTUFBSU4sTUFBTVMsUUFBVixFQUFvQjtBQUNsQlQsVUFBTVUsU0FBTixHQUFrQjlDLFNBQVM0QyxPQUFULENBQWlCUixNQUFNUyxRQUF2QixDQUFsQjtBQUNBLFdBQU9ULE1BQU1TLFFBQWI7QUFDRDs7QUFFRCxNQUFJVixVQUFVVSxRQUFkLEVBQXdCO0FBQ3RCVixjQUFVVyxTQUFWLEdBQXNCOUMsU0FBUzRDLE9BQVQsQ0FBaUJULFVBQVVVLFFBQTNCLENBQXRCO0FBQ0EsV0FBT1YsVUFBVVUsUUFBakI7QUFDRDs7QUFFRDtBQUNBLE1BQU12RCxVQUFVLENBQUM7QUFDZkMsVUFBTSxlQURTO0FBRWZpQyxnQkFBWVc7QUFGRyxHQUFELENBQWhCOztBQUtBO0FBQ0EsTUFBTXRELFlBQVk7QUFDaEJVLFVBQU0sZUFEVTtBQUVoQmlDLGdCQUFZWSxLQUZJO0FBR2hCOUM7QUFIZ0IsR0FBbEI7O0FBTUE7QUFDQVYsWUFBVUQsY0FBVixDQUF5QkUsU0FBekI7QUFDRCxDQXRFRDs7QUF3RUE7Ozs7Ozs7O0FBUUFILFdBQVdxRSxrQkFBWCxHQUFnQyxVQUFDbkUsU0FBRCxFQUFZTSxJQUFaLEVBQWtCZ0IsUUFBbEIsRUFBK0I7QUFDN0QsTUFBTThDLGNBQWM5RCxLQUFLbUMsS0FBTCxFQUFwQjtBQUNBMkIsY0FBWTlELEtBQUtFLE1BQUwsR0FBYyxDQUExQixLQUFnQyxDQUFoQzs7QUFFQSxNQUFNRSxVQUFVLENBQUM7QUFDZkMsVUFBTSxXQURTO0FBRWZMLFVBQU04RCxXQUZTO0FBR2ZqRCxjQUFVYjtBQUhLLEdBQUQsQ0FBaEI7O0FBTUEsTUFBTUwsWUFBWTtBQUNoQlUsVUFBTSxZQURVO0FBRWhCTCxjQUZnQjtBQUdoQmdCLHNCQUhnQjtBQUloQlo7QUFKZ0IsR0FBbEI7O0FBT0FWLFlBQVVELGNBQVYsQ0FBeUJFLFNBQXpCO0FBQ0QsQ0FsQkQ7O0FBb0JBOzs7Ozs7a0JBTWVILFUiLCJmaWxlIjoid2l0aC1vcGVyYXRpb25zLmpzIiwic291cmNlc0NvbnRlbnQiOlsiXG5pbXBvcnQgTm9ybWFsaXplIGZyb20gJy4uL3V0aWxzL25vcm1hbGl6ZSdcbmltcG9ydCBhcHBseSBmcm9tICcuLi9vcGVyYXRpb25zL2FwcGx5J1xuXG4vKipcbiAqIFRyYW5zZm9ybXMuXG4gKlxuICogQHR5cGUge09iamVjdH1cbiAqL1xuXG5jb25zdCBUcmFuc2Zvcm1zID0ge31cblxuLyoqXG4gKiBBcHBseSBhbiBgb3BlcmF0aW9uYCB0byB0aGUgY3VycmVudCBzdGF0ZS5cbiAqXG4gKiBAcGFyYW0ge1RyYW5zZm9ybX0gdHJhbnNmb3JtXG4gKiBAcGFyYW0ge09iamVjdH0gb3BlcmF0aW9uXG4gKi9cblxuVHJhbnNmb3Jtcy5hcHBseU9wZXJhdGlvbiA9ICh0cmFuc2Zvcm0sIG9wZXJhdGlvbikgPT4ge1xuICBjb25zdCB7IHN0YXRlLCBvcGVyYXRpb25zIH0gPSB0cmFuc2Zvcm1cbiAgdHJhbnNmb3JtLnN0YXRlID0gYXBwbHkoc3RhdGUsIG9wZXJhdGlvbilcbiAgdHJhbnNmb3JtLm9wZXJhdGlvbnMgPSBvcGVyYXRpb25zLmNvbmNhdChbb3BlcmF0aW9uXSlcbn1cblxuLyoqXG4gKiBBZGQgbWFyayB0byB0ZXh0IGF0IGBvZmZzZXRgIGFuZCBgbGVuZ3RoYCBpbiBub2RlIGJ5IGBwYXRoYC5cbiAqXG4gKiBAcGFyYW0ge1RyYW5zZm9ybX0gdHJhbnNmb3JtXG4gKiBAcGFyYW0ge0FycmF5fSBwYXRoXG4gKiBAcGFyYW0ge051bWJlcn0gb2Zmc2V0XG4gKiBAcGFyYW0ge051bWJlcn0gbGVuZ3RoXG4gKiBAcGFyYW0ge01peGVkfSBtYXJrXG4gKi9cblxuVHJhbnNmb3Jtcy5hZGRNYXJrT3BlcmF0aW9uID0gKHRyYW5zZm9ybSwgcGF0aCwgb2Zmc2V0LCBsZW5ndGgsIG1hcmspID0+IHtcbiAgY29uc3QgaW52ZXJzZSA9IFt7XG4gICAgdHlwZTogJ3JlbW92ZV9tYXJrJyxcbiAgICBwYXRoLFxuICAgIG9mZnNldCxcbiAgICBsZW5ndGgsXG4gICAgbWFyayxcbiAgfV1cblxuICBjb25zdCBvcGVyYXRpb24gPSB7XG4gICAgdHlwZTogJ2FkZF9tYXJrJyxcbiAgICBwYXRoLFxuICAgIG9mZnNldCxcbiAgICBsZW5ndGgsXG4gICAgbWFyayxcbiAgICBpbnZlcnNlLFxuICB9XG5cbiAgdHJhbnNmb3JtLmFwcGx5T3BlcmF0aW9uKG9wZXJhdGlvbilcbn1cblxuLyoqXG4gKiBJbnNlcnQgYSBgbm9kZWAgYXQgYHBhdGhgLlxuICpcbiAqIEBwYXJhbSB7VHJhbnNmb3JtfSB0cmFuc2Zvcm1cbiAqIEBwYXJhbSB7QXJyYXl9IHBhdGhcbiAqIEBwYXJhbSB7Tm9kZX0gbm9kZVxuICovXG5cblRyYW5zZm9ybXMuaW5zZXJ0Tm9kZU9wZXJhdGlvbiA9ICh0cmFuc2Zvcm0sIHBhdGgsIG5vZGUpID0+IHtcbiAgY29uc3QgaW52ZXJzZSA9IFt7XG4gICAgdHlwZTogJ3JlbW92ZV9ub2RlJyxcbiAgICBwYXRoLFxuICB9XVxuXG4gIGNvbnN0IG9wZXJhdGlvbiA9IHtcbiAgICB0eXBlOiAnaW5zZXJ0X25vZGUnLFxuICAgIHBhdGgsXG4gICAgbm9kZSxcbiAgICBpbnZlcnNlLFxuICB9XG5cbiAgdHJhbnNmb3JtLmFwcGx5T3BlcmF0aW9uKG9wZXJhdGlvbilcbn1cblxuLyoqXG4gKiBJbnNlcnQgYHRleHRgIGF0IGBvZmZzZXRgIGluIG5vZGUgYnkgYHBhdGhgLlxuICpcbiAqIEBwYXJhbSB7VHJhbnNmb3JtfSB0cmFuc2Zvcm1cbiAqIEBwYXJhbSB7QXJyYXl9IHBhdGhcbiAqIEBwYXJhbSB7TnVtYmVyfSBvZmZzZXRcbiAqIEBwYXJhbSB7U3RyaW5nfSB0ZXh0XG4gKiBAcGFyYW0ge1NldDxNYXJrPn0gbWFya3MgKG9wdGlvbmFsKVxuICovXG5cblRyYW5zZm9ybXMuaW5zZXJ0VGV4dE9wZXJhdGlvbiA9ICh0cmFuc2Zvcm0sIHBhdGgsIG9mZnNldCwgdGV4dCwgbWFya3MpID0+IHtcbiAgY29uc3QgaW52ZXJzZUxlbmd0aCA9IHRleHQubGVuZ3RoXG4gIGNvbnN0IGludmVyc2UgPSBbe1xuICAgIHR5cGU6ICdyZW1vdmVfdGV4dCcsXG4gICAgcGF0aCxcbiAgICBvZmZzZXQsXG4gICAgbGVuZ3RoOiBpbnZlcnNlTGVuZ3RoLFxuICB9XVxuXG4gIGNvbnN0IG9wZXJhdGlvbiA9IHtcbiAgICB0eXBlOiAnaW5zZXJ0X3RleHQnLFxuICAgIHBhdGgsXG4gICAgb2Zmc2V0LFxuICAgIHRleHQsXG4gICAgbWFya3MsXG4gICAgaW52ZXJzZSxcbiAgfVxuXG4gIHRyYW5zZm9ybS5hcHBseU9wZXJhdGlvbihvcGVyYXRpb24pXG59XG5cbi8qKlxuICogSm9pbiBhIG5vZGUgYnkgYHBhdGhgIHdpdGggYSBub2RlIGB3aXRoUGF0aGAuXG4gKlxuICogQHBhcmFtIHtUcmFuc2Zvcm19IHRyYW5zZm9ybVxuICogQHBhcmFtIHtBcnJheX0gcGF0aFxuICogQHBhcmFtIHtBcnJheX0gd2l0aFBhdGhcbiAqL1xuXG5UcmFuc2Zvcm1zLmpvaW5Ob2RlT3BlcmF0aW9uID0gKHRyYW5zZm9ybSwgcGF0aCwgd2l0aFBhdGgpID0+IHtcbiAgY29uc3QgeyBzdGF0ZSB9ID0gdHJhbnNmb3JtXG4gIGNvbnN0IHsgZG9jdW1lbnQgfSA9IHN0YXRlXG4gIGNvbnN0IG5vZGUgPSBkb2N1bWVudC5hc3NlcnRQYXRoKHdpdGhQYXRoKVxuXG4gIGNvbnN0IGludmVyc2UgPSBbe1xuICAgIHR5cGU6ICdzcGxpdF9ub2RlJyxcbiAgICBwYXRoOiB3aXRoUGF0aCxcbiAgICBwb3NpdGlvbjogbm9kZS5raW5kID09ICd0ZXh0JyA/IG5vZGUubGVuZ3RoIDogbm9kZS5ub2Rlcy5zaXplLFxuICB9XVxuXG4gIGNvbnN0IG9wZXJhdGlvbiA9IHtcbiAgICB0eXBlOiAnam9pbl9ub2RlJyxcbiAgICBwYXRoLFxuICAgIHdpdGhQYXRoLFxuICAgIGludmVyc2UsXG4gIH1cblxuICB0cmFuc2Zvcm0uYXBwbHlPcGVyYXRpb24ob3BlcmF0aW9uKVxufVxuXG4vKipcbiAqIE1vdmUgYSBub2RlIGJ5IGBwYXRoYCB0byBhIGBuZXdQYXRoYCBhbmQgYG5ld0luZGV4YC5cbiAqXG4gKiBAcGFyYW0ge1RyYW5zZm9ybX0gdHJhbnNmb3JtXG4gKiBAcGFyYW0ge0FycmF5fSBwYXRoXG4gKiBAcGFyYW0ge0FycmF5fSBuZXdQYXRoXG4gKiBAcGFyYW0ge051bWJlcn0gbmV3SW5kZXhcbiAqL1xuXG5UcmFuc2Zvcm1zLm1vdmVOb2RlT3BlcmF0aW9uID0gKHRyYW5zZm9ybSwgcGF0aCwgbmV3UGF0aCkgPT4ge1xuICBjb25zdCBpbnZlcnNlID0gW3tcbiAgICB0eXBlOiAnbW92ZV9ub2RlJyxcbiAgICBwYXRoOiBuZXdQYXRoLFxuICAgIG5ld1BhdGg6IHBhdGgsXG4gIH1dXG5cbiAgY29uc3Qgb3BlcmF0aW9uID0ge1xuICAgIHR5cGU6ICdtb3ZlX25vZGUnLFxuICAgIHBhdGgsXG4gICAgbmV3UGF0aCxcbiAgICBpbnZlcnNlLFxuICB9XG5cbiAgdHJhbnNmb3JtLmFwcGx5T3BlcmF0aW9uKG9wZXJhdGlvbilcbn1cblxuLyoqXG4gKiBSZW1vdmUgbWFyayBmcm9tIHRleHQgYXQgYG9mZnNldGAgYW5kIGBsZW5ndGhgIGluIG5vZGUgYnkgYHBhdGhgLlxuICpcbiAqIEBwYXJhbSB7VHJhbnNmb3JtfSB0cmFuc2Zvcm1cbiAqIEBwYXJhbSB7QXJyYXl9IHBhdGhcbiAqIEBwYXJhbSB7TnVtYmVyfSBvZmZzZXRcbiAqIEBwYXJhbSB7TnVtYmVyfSBsZW5ndGhcbiAqIEBwYXJhbSB7TWFya30gbWFya1xuICovXG5cblRyYW5zZm9ybXMucmVtb3ZlTWFya09wZXJhdGlvbiA9ICh0cmFuc2Zvcm0sIHBhdGgsIG9mZnNldCwgbGVuZ3RoLCBtYXJrKSA9PiB7XG4gIGNvbnN0IGludmVyc2UgPSBbe1xuICAgIHR5cGU6ICdhZGRfbWFyaycsXG4gICAgcGF0aCxcbiAgICBvZmZzZXQsXG4gICAgbGVuZ3RoLFxuICAgIG1hcmssXG4gIH1dXG5cbiAgY29uc3Qgb3BlcmF0aW9uID0ge1xuICAgIHR5cGU6ICdyZW1vdmVfbWFyaycsXG4gICAgcGF0aCxcbiAgICBvZmZzZXQsXG4gICAgbGVuZ3RoLFxuICAgIG1hcmssXG4gICAgaW52ZXJzZSxcbiAgfVxuXG4gIHRyYW5zZm9ybS5hcHBseU9wZXJhdGlvbihvcGVyYXRpb24pXG59XG5cbi8qKlxuICogUmVtb3ZlIGEgbm9kZSBieSBgcGF0aGAuXG4gKlxuICogQHBhcmFtIHtUcmFuc2Zvcm19IHRyYW5zZm9ybVxuICogQHBhcmFtIHtBcnJheX0gcGF0aFxuICovXG5cblRyYW5zZm9ybXMucmVtb3ZlTm9kZU9wZXJhdGlvbiA9ICh0cmFuc2Zvcm0sIHBhdGgpID0+IHtcbiAgY29uc3QgeyBzdGF0ZSB9ID0gdHJhbnNmb3JtXG4gIGNvbnN0IHsgZG9jdW1lbnQgfSA9IHN0YXRlXG4gIGNvbnN0IG5vZGUgPSBkb2N1bWVudC5hc3NlcnRQYXRoKHBhdGgpXG5cbiAgY29uc3QgaW52ZXJzZSA9IFt7XG4gICAgdHlwZTogJ2luc2VydF9ub2RlJyxcbiAgICBwYXRoLFxuICAgIG5vZGUsXG4gIH1dXG5cbiAgY29uc3Qgb3BlcmF0aW9uID0ge1xuICAgIHR5cGU6ICdyZW1vdmVfbm9kZScsXG4gICAgcGF0aCxcbiAgICBpbnZlcnNlLFxuICB9XG5cbiAgdHJhbnNmb3JtLmFwcGx5T3BlcmF0aW9uKG9wZXJhdGlvbilcbn1cblxuLyoqXG4gKiBSZW1vdmUgdGV4dCBhdCBgb2Zmc2V0YCBhbmQgYGxlbmd0aGAgaW4gbm9kZSBieSBgcGF0aGAuXG4gKlxuICogQHBhcmFtIHtUcmFuc2Zvcm19IHRyYW5zZm9ybVxuICogQHBhcmFtIHtBcnJheX0gcGF0aFxuICogQHBhcmFtIHtOdW1iZXJ9IG9mZnNldFxuICogQHBhcmFtIHtOdW1iZXJ9IGxlbmd0aFxuICovXG5cblRyYW5zZm9ybXMucmVtb3ZlVGV4dE9wZXJhdGlvbiA9ICh0cmFuc2Zvcm0sIHBhdGgsIG9mZnNldCwgbGVuZ3RoKSA9PiB7XG4gIGNvbnN0IHsgc3RhdGUgfSA9IHRyYW5zZm9ybVxuICBjb25zdCB7IGRvY3VtZW50IH0gPSBzdGF0ZVxuICBjb25zdCBub2RlID0gZG9jdW1lbnQuYXNzZXJ0UGF0aChwYXRoKVxuICBjb25zdCByYW5nZXMgPSBub2RlLmdldFJhbmdlcygpXG4gIGNvbnN0IGludmVyc2UgPSBbXVxuXG4gIC8vIExvb3AgdGhlIHJhbmdlcyBvZiB0ZXh0IGluIHRoZSBub2RlLCBjcmVhdGluZyBpbnZlcnNlIGluc2VydCBvcGVyYXRpb25zIGZvclxuICAvLyBlYWNoIG9mIHRoZSByYW5nZXMgdGhhdCBvdmVybGFwIHdpdGggdGhlIHJlbW92ZSBvcGVyYXRpb24uIFRoaXMgaXNcbiAgLy8gbmVjZXNzYXJ5IGJlY2F1c2UgaW5zZXJ0J3MgY2FuIG9ubHkgaGF2ZSBhIHNpbmdsZSBzZXQgb2YgbWFya3MgYXNzb2NpYXRlZFxuICAvLyB3aXRoIHRoZW0sIGJ1dCByZW1vdmVzIGNhbiByZW1vdmUgbWFueS5cbiAgcmFuZ2VzLnJlZHVjZSgoc3RhcnQsIHJhbmdlKSA9PiB7XG4gICAgY29uc3QgeyB0ZXh0LCBtYXJrcyB9ID0gcmFuZ2VcbiAgICBjb25zdCBlbmQgPSBzdGFydCArIHRleHQubGVuZ3RoXG4gICAgaWYgKHN0YXJ0ID4gb2Zmc2V0ICsgbGVuZ3RoKSByZXR1cm4gZW5kXG4gICAgaWYgKGVuZCA8PSBvZmZzZXQpIHJldHVybiBlbmRcblxuICAgIGNvbnN0IGVuZE9mZnNldCA9IE1hdGgubWluKGVuZCwgb2Zmc2V0ICsgbGVuZ3RoKVxuICAgIGNvbnN0IHN0cmluZyA9IHRleHQuc2xpY2Uob2Zmc2V0IC0gc3RhcnQsIGVuZE9mZnNldCAtIHN0YXJ0KVxuXG4gICAgaW52ZXJzZS5wdXNoKHtcbiAgICAgIHR5cGU6ICdpbnNlcnRfdGV4dCcsXG4gICAgICBwYXRoLFxuICAgICAgb2Zmc2V0LFxuICAgICAgdGV4dDogc3RyaW5nLFxuICAgICAgbWFya3MsXG4gICAgfSlcblxuICAgIHJldHVybiBlbmRcbiAgfSwgMClcblxuICBjb25zdCBvcGVyYXRpb24gPSB7XG4gICAgdHlwZTogJ3JlbW92ZV90ZXh0JyxcbiAgICBwYXRoLFxuICAgIG9mZnNldCxcbiAgICBsZW5ndGgsXG4gICAgaW52ZXJzZSxcbiAgfVxuXG4gIHRyYW5zZm9ybS5hcHBseU9wZXJhdGlvbihvcGVyYXRpb24pXG59XG5cbi8qKlxuICogTWVyZ2UgYHByb3BlcnRpZXNgIGludG8gc3RhdGUgYGRhdGFgLlxuICpcbiAqIEBwYXJhbSB7VHJhbnNmb3JtfSB0cmFuc2Zvcm1cbiAqIEBwYXJhbSB7T2JqZWN0fSBwcm9wZXJ0aWVzXG4gKi9cblxuVHJhbnNmb3Jtcy5zZXREYXRhT3BlcmF0aW9uID0gKHRyYW5zZm9ybSwgcHJvcGVydGllcykgPT4ge1xuICBjb25zdCB7IHN0YXRlIH0gPSB0cmFuc2Zvcm1cbiAgY29uc3QgeyBkYXRhIH0gPSBzdGF0ZVxuICBjb25zdCBpbnZlcnNlUHJvcHMgPSB7fVxuXG4gIGZvciAoY29uc3QgayBpbiBwcm9wZXJ0aWVzKSB7XG4gICAgaW52ZXJzZVByb3BzW2tdID0gZGF0YVtrXVxuICB9XG5cbiAgY29uc3QgaW52ZXJzZSA9IFt7XG4gICAgdHlwZTogJ3NldF9kYXRhJyxcbiAgICBwcm9wZXJ0aWVzOiBpbnZlcnNlUHJvcHNcbiAgfV1cblxuICBjb25zdCBvcGVyYXRpb24gPSB7XG4gICAgdHlwZTogJ3NldF9kYXRhJyxcbiAgICBwcm9wZXJ0aWVzLFxuICAgIGludmVyc2UsXG4gIH1cblxuICB0cmFuc2Zvcm0uYXBwbHlPcGVyYXRpb24ob3BlcmF0aW9uKVxufVxuXG4vKipcbiAqIFNldCBgcHJvcGVydGllc2Agb24gbWFyayBvbiB0ZXh0IGF0IGBvZmZzZXRgIGFuZCBgbGVuZ3RoYCBpbiBub2RlIGJ5IGBwYXRoYC5cbiAqXG4gKiBAcGFyYW0ge1RyYW5zZm9ybX0gdHJhbnNmb3JtXG4gKiBAcGFyYW0ge0FycmF5fSBwYXRoXG4gKiBAcGFyYW0ge051bWJlcn0gb2Zmc2V0XG4gKiBAcGFyYW0ge051bWJlcn0gbGVuZ3RoXG4gKiBAcGFyYW0ge01hcmt9IG1hcmtcbiAqIEBwYXJhbSB7TWFya30gbmV3TWFya1xuICovXG5cblRyYW5zZm9ybXMuc2V0TWFya09wZXJhdGlvbiA9ICh0cmFuc2Zvcm0sIHBhdGgsIG9mZnNldCwgbGVuZ3RoLCBtYXJrLCBuZXdNYXJrKSA9PiB7XG4gIGNvbnN0IGludmVyc2UgPSBbe1xuICAgIHR5cGU6ICdzZXRfbWFyaycsXG4gICAgcGF0aCxcbiAgICBvZmZzZXQsXG4gICAgbGVuZ3RoLFxuICAgIG1hcms6IG5ld01hcmssXG4gICAgbmV3TWFyazogbWFya1xuICB9XVxuXG4gIGNvbnN0IG9wZXJhdGlvbiA9IHtcbiAgICB0eXBlOiAnc2V0X21hcmsnLFxuICAgIHBhdGgsXG4gICAgb2Zmc2V0LFxuICAgIGxlbmd0aCxcbiAgICBtYXJrLFxuICAgIG5ld01hcmssXG4gICAgaW52ZXJzZSxcbiAgfVxuXG4gIHRyYW5zZm9ybS5hcHBseU9wZXJhdGlvbihvcGVyYXRpb24pXG59XG5cbi8qKlxuICogU2V0IGBwcm9wZXJ0aWVzYCBvbiBhIG5vZGUgYnkgYHBhdGhgLlxuICpcbiAqIEBwYXJhbSB7VHJhbnNmb3JtfSB0cmFuc2Zvcm1cbiAqIEBwYXJhbSB7QXJyYXl9IHBhdGhcbiAqIEBwYXJhbSB7T2JqZWN0fSBwcm9wZXJ0aWVzXG4gKi9cblxuVHJhbnNmb3Jtcy5zZXROb2RlT3BlcmF0aW9uID0gKHRyYW5zZm9ybSwgcGF0aCwgcHJvcGVydGllcykgPT4ge1xuICBjb25zdCB7IHN0YXRlIH0gPSB0cmFuc2Zvcm1cbiAgY29uc3QgeyBkb2N1bWVudCB9ID0gc3RhdGVcbiAgY29uc3Qgbm9kZSA9IGRvY3VtZW50LmFzc2VydFBhdGgocGF0aClcbiAgY29uc3QgaW52ZXJzZVByb3BzID0ge31cblxuICBmb3IgKGNvbnN0IGsgaW4gcHJvcGVydGllcykge1xuICAgIGludmVyc2VQcm9wc1trXSA9IG5vZGVba11cbiAgfVxuXG4gIGNvbnN0IGludmVyc2UgPSBbe1xuICAgIHR5cGU6ICdzZXRfbm9kZScsXG4gICAgcGF0aCxcbiAgICBwcm9wZXJ0aWVzOiBpbnZlcnNlUHJvcHNcbiAgfV1cblxuICBjb25zdCBvcGVyYXRpb24gPSB7XG4gICAgdHlwZTogJ3NldF9ub2RlJyxcbiAgICBwYXRoLFxuICAgIHByb3BlcnRpZXMsXG4gICAgaW52ZXJzZSxcbiAgfVxuXG4gIHRyYW5zZm9ybS5hcHBseU9wZXJhdGlvbihvcGVyYXRpb24pXG59XG5cbi8qKlxuICogU2V0IHRoZSBzZWxlY3Rpb24gdG8gYSBuZXcgYHNlbGVjdGlvbmAuXG4gKlxuICogQHBhcmFtIHtUcmFuc2Zvcm19IHRyYW5zZm9ybVxuICogQHBhcmFtIHtNaXhlZH0gc2VsZWN0aW9uXG4gKi9cblxuVHJhbnNmb3Jtcy5zZXRTZWxlY3Rpb25PcGVyYXRpb24gPSAodHJhbnNmb3JtLCBwcm9wZXJ0aWVzLCBvcHRpb25zID0ge30pID0+IHtcbiAgcHJvcGVydGllcyA9IE5vcm1hbGl6ZS5zZWxlY3Rpb25Qcm9wZXJ0aWVzKHByb3BlcnRpZXMpXG5cbiAgY29uc3QgeyBzdGF0ZSB9ID0gdHJhbnNmb3JtXG4gIGNvbnN0IHsgZG9jdW1lbnQsIHNlbGVjdGlvbiB9ID0gc3RhdGVcbiAgY29uc3QgcHJldlByb3BzID0ge31cbiAgY29uc3QgcHJvcHMgPSB7fVxuXG4gIC8vIFJlbW92ZSBhbnkgcHJvcGVydGllcyB0aGF0IGFyZSBhbHJlYWR5IGVxdWFsIHRvIHRoZSBjdXJyZW50IHNlbGVjdGlvbi4gQW5kXG4gIC8vIGNyZWF0ZSBhIGRpY3Rpb25hcnkgb2YgdGhlIHByZXZpb3VzIHZhbHVlcyBmb3IgYWxsIG9mIHRoZSBwcm9wZXJ0aWVzIHRoYXRcbiAgLy8gYXJlIGJlaW5nIGNoYW5nZWQsIGZvciB0aGUgaW52ZXJzZSBvcGVyYXRpb24uXG4gIGZvciAoY29uc3QgayBpbiBwcm9wZXJ0aWVzKSB7XG4gICAgaWYgKCFvcHRpb25zLnNuYXBzaG90ICYmIHByb3BlcnRpZXNba10gPT0gc2VsZWN0aW9uW2tdKSBjb250aW51ZVxuICAgIHByb3BzW2tdID0gcHJvcGVydGllc1trXVxuICAgIHByZXZQcm9wc1trXSA9IHNlbGVjdGlvbltrXVxuICB9XG5cbiAgLy8gSWYgdGhlIHNlbGVjdGlvbiBtb3ZlcywgY2xlYXIgYW55IG1hcmtzLCB1bmxlc3MgdGhlIG5ldyBzZWxlY3Rpb25cbiAgLy8gZG9lcyBjaGFuZ2UgdGhlIG1hcmtzIGluIHNvbWUgd2F5XG4gIGNvbnN0IG1vdmVkID0gW1xuICAgICdhbmNob3JLZXknLFxuICAgICdhbmNob3JPZmZzZXQnLFxuICAgICdmb2N1c0tleScsXG4gICAgJ2ZvY3VzT2Zmc2V0JyxcbiAgXS5zb21lKHAgPT4gcHJvcHMuaGFzT3duUHJvcGVydHkocCkpXG5cbiAgaWYgKFxuICAgIHNlbGVjdGlvbi5tYXJrcyAmJlxuICAgIHByb3BlcnRpZXMubWFya3MgPT0gc2VsZWN0aW9uLm1hcmtzICYmXG4gICAgbW92ZWRcbiAgKSB7XG4gICAgcHJvcHMubWFya3MgPSBudWxsXG4gIH1cblxuICAvLyBSZXNvbHZlIHRoZSBzZWxlY3Rpb24ga2V5cyBpbnRvIHBhdGhzLlxuICBpZiAocHJvcHMuYW5jaG9yS2V5KSB7XG4gICAgcHJvcHMuYW5jaG9yUGF0aCA9IGRvY3VtZW50LmdldFBhdGgocHJvcHMuYW5jaG9yS2V5KVxuICAgIGRlbGV0ZSBwcm9wcy5hbmNob3JLZXlcbiAgfVxuXG4gIGlmIChwcmV2UHJvcHMuYW5jaG9yS2V5KSB7XG4gICAgcHJldlByb3BzLmFuY2hvclBhdGggPSBkb2N1bWVudC5nZXRQYXRoKHByZXZQcm9wcy5hbmNob3JLZXkpXG4gICAgZGVsZXRlIHByZXZQcm9wcy5hbmNob3JLZXlcbiAgfVxuXG4gIGlmIChwcm9wcy5mb2N1c0tleSkge1xuICAgIHByb3BzLmZvY3VzUGF0aCA9IGRvY3VtZW50LmdldFBhdGgocHJvcHMuZm9jdXNLZXkpXG4gICAgZGVsZXRlIHByb3BzLmZvY3VzS2V5XG4gIH1cblxuICBpZiAocHJldlByb3BzLmZvY3VzS2V5KSB7XG4gICAgcHJldlByb3BzLmZvY3VzUGF0aCA9IGRvY3VtZW50LmdldFBhdGgocHJldlByb3BzLmZvY3VzS2V5KVxuICAgIGRlbGV0ZSBwcmV2UHJvcHMuZm9jdXNLZXlcbiAgfVxuXG4gIC8vIERlZmluZSBhbiBpbnZlcnNlIG9mIHRoZSBvcGVyYXRpb24gZm9yIHVuZG9pbmcuXG4gIGNvbnN0IGludmVyc2UgPSBbe1xuICAgIHR5cGU6ICdzZXRfc2VsZWN0aW9uJyxcbiAgICBwcm9wZXJ0aWVzOiBwcmV2UHJvcHNcbiAgfV1cblxuICAvLyBEZWZpbmUgdGhlIG9wZXJhdGlvbi5cbiAgY29uc3Qgb3BlcmF0aW9uID0ge1xuICAgIHR5cGU6ICdzZXRfc2VsZWN0aW9uJyxcbiAgICBwcm9wZXJ0aWVzOiBwcm9wcyxcbiAgICBpbnZlcnNlLFxuICB9XG5cbiAgLy8gQXBwbHkgdGhlIG9wZXJhdGlvbi5cbiAgdHJhbnNmb3JtLmFwcGx5T3BlcmF0aW9uKG9wZXJhdGlvbilcbn1cblxuLyoqXG4gKiBTcGxpdCBhIG5vZGUgYnkgYHBhdGhgIGF0IGBwb3NpdGlvbmAuXG4gKlxuICogQHBhcmFtIHtUcmFuc2Zvcm19IHRyYW5zZm9ybVxuICogQHBhcmFtIHtBcnJheX0gcGF0aFxuICogQHBhcmFtIHtOdW1iZXJ9IHBvc2l0aW9uXG4gKi9cblxuVHJhbnNmb3Jtcy5zcGxpdE5vZGVPcGVyYXRpb24gPSAodHJhbnNmb3JtLCBwYXRoLCBwb3NpdGlvbikgPT4ge1xuICBjb25zdCBpbnZlcnNlUGF0aCA9IHBhdGguc2xpY2UoKVxuICBpbnZlcnNlUGF0aFtwYXRoLmxlbmd0aCAtIDFdICs9IDFcblxuICBjb25zdCBpbnZlcnNlID0gW3tcbiAgICB0eXBlOiAnam9pbl9ub2RlJyxcbiAgICBwYXRoOiBpbnZlcnNlUGF0aCxcbiAgICB3aXRoUGF0aDogcGF0aCxcbiAgfV1cblxuICBjb25zdCBvcGVyYXRpb24gPSB7XG4gICAgdHlwZTogJ3NwbGl0X25vZGUnLFxuICAgIHBhdGgsXG4gICAgcG9zaXRpb24sXG4gICAgaW52ZXJzZSxcbiAgfVxuXG4gIHRyYW5zZm9ybS5hcHBseU9wZXJhdGlvbihvcGVyYXRpb24pXG59XG5cbi8qKlxuICogRXhwb3J0LlxuICpcbiAqIEB0eXBlIHtPYmplY3R9XG4gKi9cblxuZXhwb3J0IGRlZmF1bHQgVHJhbnNmb3Jtc1xuIl19