'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _apply = require('../operations/apply');

var _apply2 = _interopRequireDefault(_apply);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Transforms.
 *
 * @type {Object}
 */

var Transforms = {};

/**
 * Apply an `operation` to the current state.
 *
 * @param {Transform} transform
 * @param {Object} operation
 */

Transforms.applyOperation = function (transform, operation) {
  var state = transform.state,
      operations = transform.operations;

  transform.state = (0, _apply2.default)(state, operation);
  transform.operations = operations.concat([operation]);
};

/**
 * Apply a series of `operations` to the current state.
 *
 * @param {Transform} transform
 * @param {Array} operations
 */

Transforms.applyOperations = function (transform) {
  var operations = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : [];
  var _iteratorNormalCompletion = true;
  var _didIteratorError = false;
  var _iteratorError = undefined;

  try {
    for (var _iterator = operations[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
      var op = _step.value;

      transform.applyOperation(op);
    }
  } catch (err) {
    _didIteratorError = true;
    _iteratorError = err;
  } finally {
    try {
      if (!_iteratorNormalCompletion && _iterator.return) {
        _iterator.return();
      }
    } finally {
      if (_didIteratorError) {
        throw _iteratorError;
      }
    }
  }
};

/**
 * Call a `fn` as if it was a core transform. This is a convenience method to
 * make using non-core transforms easier to read and chain.
 *
 * @param {Transform} transform
 * @param {Function} fn
 * @param {Mixed} ...args
 */

Transforms.call = function (transform, fn) {
  for (var _len = arguments.length, args = Array(_len > 2 ? _len - 2 : 0), _key = 2; _key < _len; _key++) {
    args[_key - 2] = arguments[_key];
  }

  fn.apply(undefined, [transform].concat(args));
  return;
};

/**
 * Set the `isNative` flag on the underlying state to prevent re-renders.
 *
 * @param {Transform} transform
 * @param {Boolean} value
 */

Transforms.setIsNative = function (transform, value) {
  var state = transform.state;

  state = state.set('isNative', value);
  transform.state = state;
};

/**
 * Set `properties` on the top-level state's data.
 *
 * @param {Transform} transform
 * @param {Object} properties
 */

Transforms.setData = function (transform, properties) {
  var state = transform.state;
  var data = state.data;


  transform.applyOperation({
    type: 'set_data',
    properties: properties,
    data: data
  });
};

/**
 * Export.
 *
 * @type {Object}
 */

exports.default = Transforms;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy90cmFuc2Zvcm1zL2dlbmVyYWwuanMiXSwibmFtZXMiOlsiVHJhbnNmb3JtcyIsImFwcGx5T3BlcmF0aW9uIiwidHJhbnNmb3JtIiwib3BlcmF0aW9uIiwic3RhdGUiLCJvcGVyYXRpb25zIiwiY29uY2F0IiwiYXBwbHlPcGVyYXRpb25zIiwib3AiLCJjYWxsIiwiZm4iLCJhcmdzIiwic2V0SXNOYXRpdmUiLCJ2YWx1ZSIsInNldCIsInNldERhdGEiLCJwcm9wZXJ0aWVzIiwiZGF0YSIsInR5cGUiXSwibWFwcGluZ3MiOiI7Ozs7OztBQUNBOzs7Ozs7QUFFQTs7Ozs7O0FBTUEsSUFBTUEsYUFBYSxFQUFuQjs7QUFFQTs7Ozs7OztBQU9BQSxXQUFXQyxjQUFYLEdBQTRCLFVBQUNDLFNBQUQsRUFBWUMsU0FBWixFQUEwQjtBQUFBLE1BQzVDQyxLQUQ0QyxHQUN0QkYsU0FEc0IsQ0FDNUNFLEtBRDRDO0FBQUEsTUFDckNDLFVBRHFDLEdBQ3RCSCxTQURzQixDQUNyQ0csVUFEcUM7O0FBRXBESCxZQUFVRSxLQUFWLEdBQWtCLHFCQUFNQSxLQUFOLEVBQWFELFNBQWIsQ0FBbEI7QUFDQUQsWUFBVUcsVUFBVixHQUF1QkEsV0FBV0MsTUFBWCxDQUFrQixDQUFDSCxTQUFELENBQWxCLENBQXZCO0FBQ0QsQ0FKRDs7QUFNQTs7Ozs7OztBQU9BSCxXQUFXTyxlQUFYLEdBQTZCLFVBQUNMLFNBQUQsRUFBZ0M7QUFBQSxNQUFwQkcsVUFBb0IsdUVBQVAsRUFBTztBQUFBO0FBQUE7QUFBQTs7QUFBQTtBQUMzRCx5QkFBaUJBLFVBQWpCLDhIQUE2QjtBQUFBLFVBQWxCRyxFQUFrQjs7QUFDM0JOLGdCQUFVRCxjQUFWLENBQXlCTyxFQUF6QjtBQUNEO0FBSDBEO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFJNUQsQ0FKRDs7QUFNQTs7Ozs7Ozs7O0FBU0FSLFdBQVdTLElBQVgsR0FBa0IsVUFBQ1AsU0FBRCxFQUFZUSxFQUFaLEVBQTRCO0FBQUEsb0NBQVRDLElBQVM7QUFBVEEsUUFBUztBQUFBOztBQUM1Q0QsdUJBQUdSLFNBQUgsU0FBaUJTLElBQWpCO0FBQ0E7QUFDRCxDQUhEOztBQUtBOzs7Ozs7O0FBT0FYLFdBQVdZLFdBQVgsR0FBeUIsVUFBQ1YsU0FBRCxFQUFZVyxLQUFaLEVBQXNCO0FBQUEsTUFDdkNULEtBRHVDLEdBQzdCRixTQUQ2QixDQUN2Q0UsS0FEdUM7O0FBRTdDQSxVQUFRQSxNQUFNVSxHQUFOLENBQVUsVUFBVixFQUFzQkQsS0FBdEIsQ0FBUjtBQUNBWCxZQUFVRSxLQUFWLEdBQWtCQSxLQUFsQjtBQUNELENBSkQ7O0FBTUE7Ozs7Ozs7QUFPQUosV0FBV2UsT0FBWCxHQUFxQixVQUFDYixTQUFELEVBQVljLFVBQVosRUFBMkI7QUFBQSxNQUN0Q1osS0FEc0MsR0FDNUJGLFNBRDRCLENBQ3RDRSxLQURzQztBQUFBLE1BRXRDYSxJQUZzQyxHQUU3QmIsS0FGNkIsQ0FFdENhLElBRnNDOzs7QUFJOUNmLFlBQVVELGNBQVYsQ0FBeUI7QUFDdkJpQixVQUFNLFVBRGlCO0FBRXZCRiwwQkFGdUI7QUFHdkJDO0FBSHVCLEdBQXpCO0FBS0QsQ0FURDs7QUFXQTs7Ozs7O2tCQU1lakIsVSIsImZpbGUiOiJnZW5lcmFsLmpzIiwic291cmNlc0NvbnRlbnQiOlsiXG5pbXBvcnQgYXBwbHkgZnJvbSAnLi4vb3BlcmF0aW9ucy9hcHBseSdcblxuLyoqXG4gKiBUcmFuc2Zvcm1zLlxuICpcbiAqIEB0eXBlIHtPYmplY3R9XG4gKi9cblxuY29uc3QgVHJhbnNmb3JtcyA9IHt9XG5cbi8qKlxuICogQXBwbHkgYW4gYG9wZXJhdGlvbmAgdG8gdGhlIGN1cnJlbnQgc3RhdGUuXG4gKlxuICogQHBhcmFtIHtUcmFuc2Zvcm19IHRyYW5zZm9ybVxuICogQHBhcmFtIHtPYmplY3R9IG9wZXJhdGlvblxuICovXG5cblRyYW5zZm9ybXMuYXBwbHlPcGVyYXRpb24gPSAodHJhbnNmb3JtLCBvcGVyYXRpb24pID0+IHtcbiAgY29uc3QgeyBzdGF0ZSwgb3BlcmF0aW9ucyB9ID0gdHJhbnNmb3JtXG4gIHRyYW5zZm9ybS5zdGF0ZSA9IGFwcGx5KHN0YXRlLCBvcGVyYXRpb24pXG4gIHRyYW5zZm9ybS5vcGVyYXRpb25zID0gb3BlcmF0aW9ucy5jb25jYXQoW29wZXJhdGlvbl0pXG59XG5cbi8qKlxuICogQXBwbHkgYSBzZXJpZXMgb2YgYG9wZXJhdGlvbnNgIHRvIHRoZSBjdXJyZW50IHN0YXRlLlxuICpcbiAqIEBwYXJhbSB7VHJhbnNmb3JtfSB0cmFuc2Zvcm1cbiAqIEBwYXJhbSB7QXJyYXl9IG9wZXJhdGlvbnNcbiAqL1xuXG5UcmFuc2Zvcm1zLmFwcGx5T3BlcmF0aW9ucyA9ICh0cmFuc2Zvcm0sIG9wZXJhdGlvbnMgPSBbXSkgPT4ge1xuICBmb3IgKGNvbnN0IG9wIG9mIG9wZXJhdGlvbnMpIHtcbiAgICB0cmFuc2Zvcm0uYXBwbHlPcGVyYXRpb24ob3ApXG4gIH1cbn1cblxuLyoqXG4gKiBDYWxsIGEgYGZuYCBhcyBpZiBpdCB3YXMgYSBjb3JlIHRyYW5zZm9ybS4gVGhpcyBpcyBhIGNvbnZlbmllbmNlIG1ldGhvZCB0b1xuICogbWFrZSB1c2luZyBub24tY29yZSB0cmFuc2Zvcm1zIGVhc2llciB0byByZWFkIGFuZCBjaGFpbi5cbiAqXG4gKiBAcGFyYW0ge1RyYW5zZm9ybX0gdHJhbnNmb3JtXG4gKiBAcGFyYW0ge0Z1bmN0aW9ufSBmblxuICogQHBhcmFtIHtNaXhlZH0gLi4uYXJnc1xuICovXG5cblRyYW5zZm9ybXMuY2FsbCA9ICh0cmFuc2Zvcm0sIGZuLCAuLi5hcmdzKSA9PiB7XG4gIGZuKHRyYW5zZm9ybSwgLi4uYXJncylcbiAgcmV0dXJuXG59XG5cbi8qKlxuICogU2V0IHRoZSBgaXNOYXRpdmVgIGZsYWcgb24gdGhlIHVuZGVybHlpbmcgc3RhdGUgdG8gcHJldmVudCByZS1yZW5kZXJzLlxuICpcbiAqIEBwYXJhbSB7VHJhbnNmb3JtfSB0cmFuc2Zvcm1cbiAqIEBwYXJhbSB7Qm9vbGVhbn0gdmFsdWVcbiAqL1xuXG5UcmFuc2Zvcm1zLnNldElzTmF0aXZlID0gKHRyYW5zZm9ybSwgdmFsdWUpID0+IHtcbiAgbGV0IHsgc3RhdGUgfSA9IHRyYW5zZm9ybVxuICBzdGF0ZSA9IHN0YXRlLnNldCgnaXNOYXRpdmUnLCB2YWx1ZSlcbiAgdHJhbnNmb3JtLnN0YXRlID0gc3RhdGVcbn1cblxuLyoqXG4gKiBTZXQgYHByb3BlcnRpZXNgIG9uIHRoZSB0b3AtbGV2ZWwgc3RhdGUncyBkYXRhLlxuICpcbiAqIEBwYXJhbSB7VHJhbnNmb3JtfSB0cmFuc2Zvcm1cbiAqIEBwYXJhbSB7T2JqZWN0fSBwcm9wZXJ0aWVzXG4gKi9cblxuVHJhbnNmb3Jtcy5zZXREYXRhID0gKHRyYW5zZm9ybSwgcHJvcGVydGllcykgPT4ge1xuICBjb25zdCB7IHN0YXRlIH0gPSB0cmFuc2Zvcm1cbiAgY29uc3QgeyBkYXRhIH0gPSBzdGF0ZVxuXG4gIHRyYW5zZm9ybS5hcHBseU9wZXJhdGlvbih7XG4gICAgdHlwZTogJ3NldF9kYXRhJyxcbiAgICBwcm9wZXJ0aWVzLFxuICAgIGRhdGEsXG4gIH0pXG59XG5cbi8qKlxuICogRXhwb3J0LlxuICpcbiAqIEB0eXBlIHtPYmplY3R9XG4gKi9cblxuZXhwb3J0IGRlZmF1bHQgVHJhbnNmb3Jtc1xuIl19