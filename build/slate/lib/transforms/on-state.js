'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

/**
 * Transforms.
 *
 * @type {Object}
 */

var Transforms = {};

/**
 * Set the `isNative` flag on the underlying state to prevent re-renders.
 *
 * @param {Transform} transform
 * @param {Boolean} value
 */

Transforms.setIsNative = function (transform, value) {
  var state = transform.state;

  state = state.set('isNative', value);
  transform.state = state;
};

/**
 * Set `properties` on the top-level state's data.
 *
 * @param {Transform} transform
 * @param {Object} properties
 */

Transforms.setData = function (transform, properties) {
  var state = transform.state;
  var data = state.data;


  transform.applyOperation({
    type: 'set_data',
    properties: properties,
    data: data
  });
};

/**
 * Export.
 *
 * @type {Object}
 */

exports.default = Transforms;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy90cmFuc2Zvcm1zL29uLXN0YXRlLmpzIl0sIm5hbWVzIjpbIlRyYW5zZm9ybXMiLCJzZXRJc05hdGl2ZSIsInRyYW5zZm9ybSIsInZhbHVlIiwic3RhdGUiLCJzZXQiLCJzZXREYXRhIiwicHJvcGVydGllcyIsImRhdGEiLCJhcHBseU9wZXJhdGlvbiIsInR5cGUiXSwibWFwcGluZ3MiOiI7Ozs7OztBQUNBOzs7Ozs7QUFNQSxJQUFNQSxhQUFhLEVBQW5COztBQUVBOzs7Ozs7O0FBT0FBLFdBQVdDLFdBQVgsR0FBeUIsVUFBQ0MsU0FBRCxFQUFZQyxLQUFaLEVBQXNCO0FBQUEsTUFDdkNDLEtBRHVDLEdBQzdCRixTQUQ2QixDQUN2Q0UsS0FEdUM7O0FBRTdDQSxVQUFRQSxNQUFNQyxHQUFOLENBQVUsVUFBVixFQUFzQkYsS0FBdEIsQ0FBUjtBQUNBRCxZQUFVRSxLQUFWLEdBQWtCQSxLQUFsQjtBQUNELENBSkQ7O0FBTUE7Ozs7Ozs7QUFPQUosV0FBV00sT0FBWCxHQUFxQixVQUFDSixTQUFELEVBQVlLLFVBQVosRUFBMkI7QUFBQSxNQUN0Q0gsS0FEc0MsR0FDNUJGLFNBRDRCLENBQ3RDRSxLQURzQztBQUFBLE1BRXRDSSxJQUZzQyxHQUU3QkosS0FGNkIsQ0FFdENJLElBRnNDOzs7QUFJOUNOLFlBQVVPLGNBQVYsQ0FBeUI7QUFDdkJDLFVBQU0sVUFEaUI7QUFFdkJILDBCQUZ1QjtBQUd2QkM7QUFIdUIsR0FBekI7QUFLRCxDQVREOztBQVdBOzs7Ozs7a0JBTWVSLFUiLCJmaWxlIjoib24tc3RhdGUuanMiLCJzb3VyY2VzQ29udGVudCI6WyJcbi8qKlxuICogVHJhbnNmb3Jtcy5cbiAqXG4gKiBAdHlwZSB7T2JqZWN0fVxuICovXG5cbmNvbnN0IFRyYW5zZm9ybXMgPSB7fVxuXG4vKipcbiAqIFNldCB0aGUgYGlzTmF0aXZlYCBmbGFnIG9uIHRoZSB1bmRlcmx5aW5nIHN0YXRlIHRvIHByZXZlbnQgcmUtcmVuZGVycy5cbiAqXG4gKiBAcGFyYW0ge1RyYW5zZm9ybX0gdHJhbnNmb3JtXG4gKiBAcGFyYW0ge0Jvb2xlYW59IHZhbHVlXG4gKi9cblxuVHJhbnNmb3Jtcy5zZXRJc05hdGl2ZSA9ICh0cmFuc2Zvcm0sIHZhbHVlKSA9PiB7XG4gIGxldCB7IHN0YXRlIH0gPSB0cmFuc2Zvcm1cbiAgc3RhdGUgPSBzdGF0ZS5zZXQoJ2lzTmF0aXZlJywgdmFsdWUpXG4gIHRyYW5zZm9ybS5zdGF0ZSA9IHN0YXRlXG59XG5cbi8qKlxuICogU2V0IGBwcm9wZXJ0aWVzYCBvbiB0aGUgdG9wLWxldmVsIHN0YXRlJ3MgZGF0YS5cbiAqXG4gKiBAcGFyYW0ge1RyYW5zZm9ybX0gdHJhbnNmb3JtXG4gKiBAcGFyYW0ge09iamVjdH0gcHJvcGVydGllc1xuICovXG5cblRyYW5zZm9ybXMuc2V0RGF0YSA9ICh0cmFuc2Zvcm0sIHByb3BlcnRpZXMpID0+IHtcbiAgY29uc3QgeyBzdGF0ZSB9ID0gdHJhbnNmb3JtXG4gIGNvbnN0IHsgZGF0YSB9ID0gc3RhdGVcblxuICB0cmFuc2Zvcm0uYXBwbHlPcGVyYXRpb24oe1xuICAgIHR5cGU6ICdzZXRfZGF0YScsXG4gICAgcHJvcGVydGllcyxcbiAgICBkYXRhLFxuICB9KVxufVxuXG4vKipcbiAqIEV4cG9ydC5cbiAqXG4gKiBAdHlwZSB7T2JqZWN0fVxuICovXG5cbmV4cG9ydCBkZWZhdWx0IFRyYW5zZm9ybXNcbiJdfQ==