'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

/**
 * Slate-specific data transfer types.
 *
 * @type {Object}
 */

var TYPES = {
  FRAGMENT: 'application/x-slate-fragment',
  NODE: 'application/x-slate-node'
};

/**
 * Export.
 *
 * @type {Object}
 */

exports.default = TYPES;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9jb25zdGFudHMvdHlwZXMuanMiXSwibmFtZXMiOlsiVFlQRVMiLCJGUkFHTUVOVCIsIk5PREUiXSwibWFwcGluZ3MiOiI7Ozs7OztBQUNBOzs7Ozs7QUFNQSxJQUFNQSxRQUFRO0FBQ1pDLFlBQVUsOEJBREU7QUFFWkMsUUFBTTtBQUZNLENBQWQ7O0FBS0E7Ozs7OztrQkFNZUYsSyIsImZpbGUiOiJ0eXBlcy5qcyIsInNvdXJjZXNDb250ZW50IjpbIlxuLyoqXG4gKiBTbGF0ZS1zcGVjaWZpYyBkYXRhIHRyYW5zZmVyIHR5cGVzLlxuICpcbiAqIEB0eXBlIHtPYmplY3R9XG4gKi9cblxuY29uc3QgVFlQRVMgPSB7XG4gIEZSQUdNRU5UOiAnYXBwbGljYXRpb24veC1zbGF0ZS1mcmFnbWVudCcsXG4gIE5PREU6ICdhcHBsaWNhdGlvbi94LXNsYXRlLW5vZGUnLFxufVxuXG4vKipcbiAqIEV4cG9ydC5cbiAqXG4gKiBAdHlwZSB7T2JqZWN0fVxuICovXG5cbmV4cG9ydCBkZWZhdWx0IFRZUEVTXG4iXX0=