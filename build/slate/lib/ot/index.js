'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _debug = require('debug');

var _debug2 = _interopRequireDefault(_debug);

var _raw = require('../serializers/raw');

var _raw2 = _interopRequireDefault(_raw);

var _state = require('../models/state');

var _state2 = _interopRequireDefault(_state);

var _transform = require('./transform');

var _transform2 = _interopRequireDefault(_transform);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Debug.
 *
 * @type {Debug}
 */

var debug = (0, _debug2.default)('slate:ot');

/**
 * Create a new snapshot from a `raw` representation.
 *
 * @param {Object} raw
 * @return {Object}
 */

function create(raw) {
  debug('create', { raw: raw });

  if (!raw) {
    raw = {
      kind: 'state',
      document: {
        kind: 'document',
        nodes: [],
        data: {}
      }
    };
  }

  return raw;
}

/**
 * Serialize a `state` snapshot.
 *
 * @param {State} state
 * @return {Object}
 */

function serialize(state) {
  debug('serialize', { state: state });
  if (state instanceof _state2.default) {
    return _raw2.default.serialize(state);
  } else {
    return state;
  }
}

/**
 * Deserialize an array of `operations` to a state.
 *
 * @param {Object} raw
 * @return {State}
 */

function deserialize(raw) {
  debug('deserialize', { raw: raw });
  if (raw instanceof _state2.default) {
    return raw;
  } else {
    return _raw2.default.deserialize(raw, { normalize: false });
  }
}

/**
 * Apply `operations` to a `snapshot`.
 *
 * @param {State|Object} snapshot
 * @param {Array} operations
 * @return {State|Object}
 */

function apply(snapshot, operations) {
  debug('apply', { snapshot: snapshot, operations: operations });
  var isRaw = !(snapshot instanceof _state2.default);
  var before = isRaw ? _raw2.default.deserialize(snapshot, { normalize: false }) : snapshot;
  var next = before.change().applyOperations(operations).state;
  var after = isRaw ? _raw2.default.serialize(next) : next;
  return after;
}

/**
 * OT-type, compatible with: https://github.com/ottypes
 *
 * @type {Object}
 */

var type = {
  name: 'slate',
  uri: 'http://slatejs.org/types/slate/v1',
  apply: apply,
  create: create,
  deserialize: deserialize,
  serialize: serialize,
  transform: _transform2.default
};

/**
 * Export.
 *
 * @type {Object}
 */

exports.default = {
  type: type
};
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9vdC9pbmRleC5qcyJdLCJuYW1lcyI6WyJkZWJ1ZyIsImNyZWF0ZSIsInJhdyIsImtpbmQiLCJkb2N1bWVudCIsIm5vZGVzIiwiZGF0YSIsInNlcmlhbGl6ZSIsInN0YXRlIiwiZGVzZXJpYWxpemUiLCJub3JtYWxpemUiLCJhcHBseSIsInNuYXBzaG90Iiwib3BlcmF0aW9ucyIsImlzUmF3IiwiYmVmb3JlIiwibmV4dCIsImNoYW5nZSIsImFwcGx5T3BlcmF0aW9ucyIsImFmdGVyIiwidHlwZSIsIm5hbWUiLCJ1cmkiLCJ0cmFuc2Zvcm0iXSwibWFwcGluZ3MiOiI7Ozs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUNBOzs7Ozs7QUFFQTs7Ozs7O0FBTUEsSUFBTUEsUUFBUSxxQkFBTSxVQUFOLENBQWQ7O0FBRUE7Ozs7Ozs7QUFPQSxTQUFTQyxNQUFULENBQWdCQyxHQUFoQixFQUFxQjtBQUNuQkYsUUFBTSxRQUFOLEVBQWdCLEVBQUVFLFFBQUYsRUFBaEI7O0FBRUEsTUFBSSxDQUFDQSxHQUFMLEVBQVU7QUFDUkEsVUFBTTtBQUNKQyxZQUFNLE9BREY7QUFFSkMsZ0JBQVU7QUFDUkQsY0FBTSxVQURFO0FBRVJFLGVBQU8sRUFGQztBQUdSQyxjQUFNO0FBSEU7QUFGTixLQUFOO0FBUUQ7O0FBRUQsU0FBT0osR0FBUDtBQUNEOztBQUVEOzs7Ozs7O0FBT0EsU0FBU0ssU0FBVCxDQUFtQkMsS0FBbkIsRUFBMEI7QUFDeEJSLFFBQU0sV0FBTixFQUFtQixFQUFFUSxZQUFGLEVBQW5CO0FBQ0EsTUFBSUEsZ0NBQUosRUFBNEI7QUFDMUIsV0FBTyxjQUFJRCxTQUFKLENBQWNDLEtBQWQsQ0FBUDtBQUNELEdBRkQsTUFFTztBQUNMLFdBQU9BLEtBQVA7QUFDRDtBQUNGOztBQUVEOzs7Ozs7O0FBT0EsU0FBU0MsV0FBVCxDQUFxQlAsR0FBckIsRUFBMEI7QUFDeEJGLFFBQU0sYUFBTixFQUFxQixFQUFFRSxRQUFGLEVBQXJCO0FBQ0EsTUFBSUEsOEJBQUosRUFBMEI7QUFDeEIsV0FBT0EsR0FBUDtBQUNELEdBRkQsTUFFTztBQUNMLFdBQU8sY0FBSU8sV0FBSixDQUFnQlAsR0FBaEIsRUFBcUIsRUFBRVEsV0FBVyxLQUFiLEVBQXJCLENBQVA7QUFDRDtBQUNGOztBQUVEOzs7Ozs7OztBQVFBLFNBQVNDLEtBQVQsQ0FBZUMsUUFBZixFQUF5QkMsVUFBekIsRUFBcUM7QUFDbkNiLFFBQU0sT0FBTixFQUFlLEVBQUVZLGtCQUFGLEVBQVlDLHNCQUFaLEVBQWY7QUFDQSxNQUFNQyxRQUFRLEVBQUVGLG1DQUFGLENBQWQ7QUFDQSxNQUFNRyxTQUFTRCxRQUFRLGNBQUlMLFdBQUosQ0FBZ0JHLFFBQWhCLEVBQTBCLEVBQUVGLFdBQVcsS0FBYixFQUExQixDQUFSLEdBQTBERSxRQUF6RTtBQUNBLE1BQU1JLE9BQU9ELE9BQU9FLE1BQVAsR0FBZ0JDLGVBQWhCLENBQWdDTCxVQUFoQyxFQUE0Q0wsS0FBekQ7QUFDQSxNQUFNVyxRQUFRTCxRQUFRLGNBQUlQLFNBQUosQ0FBY1MsSUFBZCxDQUFSLEdBQThCQSxJQUE1QztBQUNBLFNBQU9HLEtBQVA7QUFDRDs7QUFFRDs7Ozs7O0FBTUEsSUFBTUMsT0FBTztBQUNYQyxRQUFNLE9BREs7QUFFWEMsT0FBSyxtQ0FGTTtBQUdYWCxjQUhXO0FBSVhWLGdCQUpXO0FBS1hRLDBCQUxXO0FBTVhGLHNCQU5XO0FBT1hnQjtBQVBXLENBQWI7O0FBVUE7Ozs7OztrQkFNZTtBQUNiSDtBQURhLEMiLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VzQ29udGVudCI6WyJcbmltcG9ydCBEZWJ1ZyBmcm9tICdkZWJ1ZydcbmltcG9ydCBSYXcgZnJvbSAnLi4vc2VyaWFsaXplcnMvcmF3J1xuaW1wb3J0IFN0YXRlIGZyb20gJy4uL21vZGVscy9zdGF0ZSdcbmltcG9ydCB0cmFuc2Zvcm0gZnJvbSAnLi90cmFuc2Zvcm0nXG5cbi8qKlxuICogRGVidWcuXG4gKlxuICogQHR5cGUge0RlYnVnfVxuICovXG5cbmNvbnN0IGRlYnVnID0gRGVidWcoJ3NsYXRlOm90JylcblxuLyoqXG4gKiBDcmVhdGUgYSBuZXcgc25hcHNob3QgZnJvbSBhIGByYXdgIHJlcHJlc2VudGF0aW9uLlxuICpcbiAqIEBwYXJhbSB7T2JqZWN0fSByYXdcbiAqIEByZXR1cm4ge09iamVjdH1cbiAqL1xuXG5mdW5jdGlvbiBjcmVhdGUocmF3KSB7XG4gIGRlYnVnKCdjcmVhdGUnLCB7IHJhdyB9KVxuXG4gIGlmICghcmF3KSB7XG4gICAgcmF3ID0ge1xuICAgICAga2luZDogJ3N0YXRlJyxcbiAgICAgIGRvY3VtZW50OiB7XG4gICAgICAgIGtpbmQ6ICdkb2N1bWVudCcsXG4gICAgICAgIG5vZGVzOiBbXSxcbiAgICAgICAgZGF0YToge30sXG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgcmV0dXJuIHJhd1xufVxuXG4vKipcbiAqIFNlcmlhbGl6ZSBhIGBzdGF0ZWAgc25hcHNob3QuXG4gKlxuICogQHBhcmFtIHtTdGF0ZX0gc3RhdGVcbiAqIEByZXR1cm4ge09iamVjdH1cbiAqL1xuXG5mdW5jdGlvbiBzZXJpYWxpemUoc3RhdGUpIHtcbiAgZGVidWcoJ3NlcmlhbGl6ZScsIHsgc3RhdGUgfSlcbiAgaWYgKHN0YXRlIGluc3RhbmNlb2YgU3RhdGUpIHtcbiAgICByZXR1cm4gUmF3LnNlcmlhbGl6ZShzdGF0ZSlcbiAgfSBlbHNlIHtcbiAgICByZXR1cm4gc3RhdGVcbiAgfVxufVxuXG4vKipcbiAqIERlc2VyaWFsaXplIGFuIGFycmF5IG9mIGBvcGVyYXRpb25zYCB0byBhIHN0YXRlLlxuICpcbiAqIEBwYXJhbSB7T2JqZWN0fSByYXdcbiAqIEByZXR1cm4ge1N0YXRlfVxuICovXG5cbmZ1bmN0aW9uIGRlc2VyaWFsaXplKHJhdykge1xuICBkZWJ1ZygnZGVzZXJpYWxpemUnLCB7IHJhdyB9KVxuICBpZiAocmF3IGluc3RhbmNlb2YgU3RhdGUpIHtcbiAgICByZXR1cm4gcmF3XG4gIH0gZWxzZSB7XG4gICAgcmV0dXJuIFJhdy5kZXNlcmlhbGl6ZShyYXcsIHsgbm9ybWFsaXplOiBmYWxzZSB9KVxuICB9XG59XG5cbi8qKlxuICogQXBwbHkgYG9wZXJhdGlvbnNgIHRvIGEgYHNuYXBzaG90YC5cbiAqXG4gKiBAcGFyYW0ge1N0YXRlfE9iamVjdH0gc25hcHNob3RcbiAqIEBwYXJhbSB7QXJyYXl9IG9wZXJhdGlvbnNcbiAqIEByZXR1cm4ge1N0YXRlfE9iamVjdH1cbiAqL1xuXG5mdW5jdGlvbiBhcHBseShzbmFwc2hvdCwgb3BlcmF0aW9ucykge1xuICBkZWJ1ZygnYXBwbHknLCB7IHNuYXBzaG90LCBvcGVyYXRpb25zIH0pXG4gIGNvbnN0IGlzUmF3ID0gIShzbmFwc2hvdCBpbnN0YW5jZW9mIFN0YXRlKVxuICBjb25zdCBiZWZvcmUgPSBpc1JhdyA/IFJhdy5kZXNlcmlhbGl6ZShzbmFwc2hvdCwgeyBub3JtYWxpemU6IGZhbHNlIH0pIDogc25hcHNob3RcbiAgY29uc3QgbmV4dCA9IGJlZm9yZS5jaGFuZ2UoKS5hcHBseU9wZXJhdGlvbnMob3BlcmF0aW9ucykuc3RhdGVcbiAgY29uc3QgYWZ0ZXIgPSBpc1JhdyA/IFJhdy5zZXJpYWxpemUobmV4dCkgOiBuZXh0XG4gIHJldHVybiBhZnRlclxufVxuXG4vKipcbiAqIE9ULXR5cGUsIGNvbXBhdGlibGUgd2l0aDogaHR0cHM6Ly9naXRodWIuY29tL290dHlwZXNcbiAqXG4gKiBAdHlwZSB7T2JqZWN0fVxuICovXG5cbmNvbnN0IHR5cGUgPSB7XG4gIG5hbWU6ICdzbGF0ZScsXG4gIHVyaTogJ2h0dHA6Ly9zbGF0ZWpzLm9yZy90eXBlcy9zbGF0ZS92MScsXG4gIGFwcGx5LFxuICBjcmVhdGUsXG4gIGRlc2VyaWFsaXplLFxuICBzZXJpYWxpemUsXG4gIHRyYW5zZm9ybSxcbn1cblxuLyoqXG4gKiBFeHBvcnQuXG4gKlxuICogQHR5cGUge09iamVjdH1cbiAqL1xuXG5leHBvcnQgZGVmYXVsdCB7XG4gIHR5cGUsXG59XG4iXX0=