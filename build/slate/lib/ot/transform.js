'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _debug = require('debug');

var _debug2 = _interopRequireDefault(_debug);

var _cloneDeep = require('lodash/cloneDeep');

var _cloneDeep2 = _interopRequireDefault(_cloneDeep);

var _difference = require('lodash/difference');

var _difference2 = _interopRequireDefault(_difference);

var _invert = require('../operations/invert');

var _invert2 = _interopRequireDefault(_invert);

var _isEqual = require('lodash/isEqual');

var _isEqual2 = _interopRequireDefault(_isEqual);

var _pick = require('lodash/pick');

var _pick2 = _interopRequireDefault(_pick);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

/**
 * Debug.
 *
 * @type {Debug}
 */

var debug = (0, _debug2.default)('slate:ot:transform');

/**
 * Transform operations `a` by operations `b` with `priority`.
 *
 * @param {Array} a
 * @param {Array} b
 * @param {String} priority
 */

function transform(a, b, priority) {
  // Deeply clone A so that the transformers can edit in place without worry.
  a = (0, _cloneDeep2.default)(a);

  var _iteratorNormalCompletion = true;
  var _didIteratorError = false;
  var _iteratorError = undefined;

  try {
    for (var _iterator = b[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
      var bo = _step.value;

      // Create a temporary array to store the new A operations, because sometimes
      // the transformers need to return `null` to remove an operation, or return
      // an array to add operations.
      var tmp = [];

      var _iteratorNormalCompletion2 = true;
      var _didIteratorError2 = false;
      var _iteratorError2 = undefined;

      try {
        for (var _iterator2 = a[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
          var ao = _step2.value;

          debug('a', _extends({}, ao));
          debug('b', _extends({}, bo));

          var ret = t(ao, bo, priority);

          if (ret === null) {
            debug('a\'', _extends({}, ao));
            continue;
          } else if (Array.isArray(ret)) {
            debug('a\'', [].concat(_toConsumableArray(ret)));
            tmp = tmp.concat(ret);
          } else {
            debug('a\'', _extends({}, ret));
            tmp.push(ao);
          }
        }

        // Update A for the next iteration.
      } catch (err) {
        _didIteratorError2 = true;
        _iteratorError2 = err;
      } finally {
        try {
          if (!_iteratorNormalCompletion2 && _iterator2.return) {
            _iterator2.return();
          }
        } finally {
          if (_didIteratorError2) {
            throw _iteratorError2;
          }
        }
      }

      a = tmp;
    }

    // Return the transformed A operations.
  } catch (err) {
    _didIteratorError = true;
    _iteratorError = err;
  } finally {
    try {
      if (!_iteratorNormalCompletion && _iterator.return) {
        _iterator.return();
      }
    } finally {
      if (_didIteratorError) {
        throw _iteratorError;
      }
    }
  }

  return a;
}

/**
 * Find the place where path `a` is intersected by path `b`, if it does.
 *
 * @param {Array} a
 * @param {Array} b
 * @return {String|Null}
 */

function intersectPath(a, b) {
  if (a == null || b == null) return null;

  for (var i = 0; i < b.length; i++) {
    var bv = b[i];
    var av = a[i];
    var al = i + 1 === a.length;
    var bl = i + 1 === b.length;
    // If ever `bv > av`, `b` doesn't intersect `a`.
    if (bv > av) return 'none';
    // If we're not at the end of `b`, and `bv < av`, `b` doesn't intersect `a`.
    if (!bl && bv < av) return 'none';
    // If at the end of both, and `bv == av`, `b` is exactly equal to `a`.
    if (al && bl && bv == av) return 'exact';
    // If only at the end of `b`, and `bv == av`, `b` is above `a`.
    if (bl && bv == av) return 'above';
    // If at the end of `b`, and `bv < av`, `b` is before `a`.
    if (bl && bv < av) return 'before';
  }

  return 'none';
}

/**
 * Find the place where `a`'s offset `a` is intersected by operation B's
 * offset `b`, if it does at all.
 *
 * @param {Number} a
 * @param {Number} b
 * @return {String}
 */

function intersectOffset(a, b) {
  if (a == null || b == null || isNaN(a) || isNaN(b)) return null;
  if (b < a) return 'before';
  if (b == a) return 'exact';
  return 'after';
}

/**
 * Transform operation `a` in place based on operation `b` and priority `p`.
 *
 * @param {Object} a
 * @param {Object} b
 * @param {String} p
 */

function t(a, b, p) {
  var ai = a.path.length - 1;
  var bi = b.path.length - 1;
  var av = a.path[ai];
  var bv = b.path[bi];
  var ani = a.newPath ? a.newPath.length - 1 : null;
  var bni = b.newPath ? b.newPath.length - 1 : null;
  var bnv = b.newPath ? b.newPath[bni] : null;

  var ax = 'offset' in a ? a.offset : null;
  var ay = 'offset' in a && 'length' in a ? a.offset + a.length : null;
  var bx = 'offset' in b ? b.offset : null;
  var by = 'offset' in b && 'length' in b ? b.offset + b.length : null;

  var ap_bp = intersectPath(a.path, b.path);
  var ap_bnp = intersectPath(a.path, b.newPath);
  var anp_bp = intersectPath(a.newPath, b.path);
  var anp_bnp = intersectPath(a.newPath, b.newPath);

  // TODO: get rid of these I think, easy to calculate?
  var ao_bo = intersectOffset(a.offset, b.offset);
  var aol_bol = intersectOffset(a.offset + a.length, b.offset + b.length);

  // TODO: or get rid of these because they're very hard to remember
  var abi_bpo = intersectOffset(a.path[bi + 1], b.position);
  var ao_bpo = intersectOffset(a.offset, b.position);
  var apo_bo = intersectOffset(a.position, b.offset);
  var apo_bpo = intersectOffset(a.position, b.position);
  var aol_bpo = intersectOffset(a.offset + a.length, b.position);
  var aot_bpo = 'text' in a ? intersectOffset(a.offset + a.text.length, b.position) : null;

  /**
   * Insert node.
   */

  if (b.type == 'insert_node') {
    if (ap_bp == 'exact' && a.type == 'merge_node') {
      var newPath = a.path.slice(0, ai).concat([a.path[ai] + 1]);
      var move = { type: 'move_node', path: a.path.slice(), newPath: newPath };
      return [move, a];
    }

    if (ap_bp == 'before' || ap_bp == 'above' || ap_bp == 'exact' && a.type != 'insert_node' || ap_bp == 'exact' && a.type == 'insert_node' && p == 'right') {
      a.path[bi]++;
    }

    if (anp_bp == 'before' || anp_bp == 'above' || anp_bp == 'exact' && p == 'right') {
      a.newPath[bi]++;
    }
  }

  /**
   * Remove node.
   */

  if (b.type == 'remove_node') {
    if (ap_bp == 'before' && a.type == 'merge_node' && av == bv + 1) {
      // TODO: remove `node:null`
      var remove = { type: 'remove_node', path: a.path.slice() };
      return [remove];
    }

    if (ap_bp == 'before') {
      a.path[bi]--;
    }

    if (anp_bp == 'before') {
      a.newPath[bi]--;
    }

    if (ap_bp == 'exact' && a.type == 'merge_node') {
      var path = a.path.slice(0, ai).concat([a.path[ai] - 1]);
      // TODO: remove `node:null`
      var _remove = { type: 'remove_node', path: path };
      return [_remove];
    }

    if (ap_bp == 'above' || ap_bp == 'exact' && a.type != 'insert_node') {
      return null;
    }
  }

  /**
   * Set node.
   */

  if (b.type == 'set_node') {
    if (a.type == 'set_node' && ap_bp == 'exact' && p == 'right') {
      var ak = Object.keys(a.properties);
      var bk = Object.keys(b.properties);
      var diff = (0, _difference2.default)(ak, bk);
      if (diff.length == 0) return null;
      a.properties = (0, _pick2.default)(a.properties, diff);
    }
  }

  /**
   * Move node.
   */

  if (b.type == 'move_node') {
    if (ap_bp == 'exact' && a.type == 'move_node' && p == 'right') {
      return null;
    }

    if (ap_bp == 'exact' && a.type == 'merge_node') {
      var inverse = (0, _invert2.default)(b);
      var _path = b.path.slice(0, bi).concat([b.path[bi] - 1]);
      var _newPath = b.newPath.slice();

      if (intersectPath(b.newPath, a.path) == 'before') {
        _newPath = b.newPath.slice(0, bni).concat(b.newPath[bni] - 1);
      }

      var _move = _extends({}, b, { path: _path, newPath: _newPath });
      return [inverse, a, _move];
    }

    if (ap_bp == 'before' && a.type == 'merge_node' && av == bv + 1) {
      var _inverse = (0, _invert2.default)(b);
      var _path2 = b.path.slice();
      var _newPath2 = b.newPath.slice();

      if (intersectPath(b.newPath, a.path) == 'before') {
        _newPath2 = b.newPath.slice(0, bni).concat(b.newPath[bni] - 1);
      }

      var _move2 = _extends({}, b, { path: _path2, newPath: _newPath2 });
      return [_inverse, a, _move2];
    }

    if (ap_bnp == 'exact' && ap_bp != 'before' && a.type == 'merge_node' || ap_bp == 'before' && a.type == 'merge_node' && ap_bnp == 'before' && av == bnv + 1) {
      var _inverse2 = (0, _invert2.default)(b);
      return [_inverse2, a, b];
    }

    if (ap_bnp == 'before' && ap_bp != 'before' && a.type == 'merge_node' && av == bv + 1) {
      var _inverse3 = (0, _invert2.default)(b);
      var _path3 = b.path.slice();
      var _newPath3 = b.newPath.slice(0, bni).concat(b.newPath[bni] - 1);
      var _move3 = _extends({}, b, { path: _path3, newPath: _newPath3 });
      return [_inverse3, a, _move3];
    }

    if (ap_bp == 'exact') {
      a.path = b.newPath.slice();
    }

    if (anp_bp == 'exact') {
      a.newPath = b.newPath.slice();
    }

    if (ap_bp == 'above') {
      a.path = b.newPath.concat(a.path.slice(b.newPath.length));
    }

    if (anp_bp == 'above') {
      a.newPath = b.newPath.concat(a.newPath.slice(b.newPath.length));
    }

    if (ap_bp == 'before' && ap_bnp != 'before') {
      a.path[bi]--;
    }

    if (ap_bp != 'exact' && anp_bp == 'before' && anp_bnp != 'before') {
      a.newPath[bi]--;
    }

    if (ap_bp == 'before' && ap_bnp == 'above' || ap_bp == 'none' && ap_bnp != 'none') {
      a.path[bni]++;
    }

    if (ap_bp != 'exact' && anp_bp == 'before' && anp_bnp == 'above' || ap_bp != 'exact' && anp_bp == 'none' && anp_bnp != 'none') {
      a.newPath[bni]++;
    }
  }

  /**
   * Merge node.
   */

  if (b.type == 'merge_node') {
    if (ap_bp == 'before') {
      a.path[bi]--;
    }

    if (ap_bp == 'before' && a.type == 'merge_node' && av == bv + 1) {
      a.position += b.position;
    }

    if (anp_bp == 'before') {
      a.newPath[bi]--;
    }

    if (ap_bp == 'above') {
      a.path[bi]--;
      a.path[bi + 1] += b.position;
    }

    if (anp_bp == 'above') {
      a.newPath[bi]--;
      a.newPath[bi + 1] += b.position;
    }

    if (ap_bp == 'exact' && a.type == 'merge_node') {
      return null;
    }

    if (ap_bp == 'exact') {
      a.path = a.path.slice(0, ai).concat([a.path[ai] - 1]);

      if (a.type == 'split_node') {
        a.position += b.position;
      }

      if (a.offset != null) {
        a.offset += b.position;
      }
    }
  }

  /**
   * Split node.
   */

  if (b.type == 'split_node') {
    if (ap_bp == 'above' && a.type == 'merge_node' && av == b.position) {
      var _path4 = a.path.slice(0, bi).concat([a.path[bi] + 1]).concat([0]);
      var _newPath4 = a.path.slice();
      var _move4 = { type: 'move_node', path: _path4, newPath: _newPath4 };
      return [_move4, a];
    }

    if (ap_bp == 'exact' && a.type == 'split_node' && apo_bpo == 'before') {
      a.path[bi]++;
      a.position -= b.position;
    }

    if (ap_bp == 'exact' && a.type == 'remove_node') {
      // TODO: remove `node:null`
      var _remove2 = _extends({}, a, { path: a.path.slice() });
      return [a, _remove2];
    }

    if (ap_bp == 'before') {
      a.path[bi]++;

      if (a.type == 'merge_node' && av == bv + 1) {
        a.position -= b.position;
      }
    }

    if (ap_bp != 'exact' && anp_bp == 'before') {
      a.newPath[bi]++;
    }

    if (ap_bp == 'above' && abi_bpo != 'after') {
      a.path[bi]++;
      a.path[bi + 1] -= b.position;
    }

    if (anp_bp == 'above' && abi_bpo != 'after') {
      a.newPath[bi]++;
      a.newPath[bi + 1] -= b.position;
    }

    if (ap_bp == 'exact' && a.type == 'move_node') {
      var _path5 = a.path.slice(0, ai).concat([a.path[ai] + 1]);
      var _newPath5 = a.newPath.slice();
      var _move5 = _extends({}, a, { path: _path5, newPath: _newPath5 });
      return [_move5, a];
    }

    if (ap_bp == 'exact' && a.offset != null && ao_bpo == 'before' && (aol_bpo == 'before' || aot_bpo == 'before')) {
      a.path[bi]++;
      a.offset -= b.position;
    }

    if (ap_bp == 'exact' && a.offset != null && ao_bpo == 'after' && (aol_bpo == 'before' || aot_bpo == 'before')) {
      var twoPath = a.path.slice(0, ai).concat([a.path[ai] + 1]);
      var one = _extends({}, a, { path: a.path.slice() });
      var two = _extends({}, a, { path: twoPath, offset: 0 });

      if (a.text == null) {
        one.length = b.position - a.offset;
        two.length = a.length - one.length;
      } else {
        one.text = a.text.slice(0, b.position - a.offset);
        two.text = a.text.slice(one.text.length);
      }

      return [one, two];
    }
  }

  /**
   * Insert text.
   */

  if (b.type == 'insert_text') {
    if (ap_bp == 'before' && a.type == 'merge_node' && av == bv + 1) {
      a.position += b.text.length;
    }

    if (ap_bp == 'exact' && a.type == 'split_node' && (apo_bo == 'before' || apo_bo == 'exact')) {
      a.position += b.text.length;
    }

    if (ap_bp != 'exact' || a.offset == null) {
      return;
    }

    if (ao_bo == 'after' && a.type == 'remove_text' && b.offset < a.offset + a.text.length) {
      var _one = _extends({}, a, { path: a.path.slice(), text: a.text.substring(0, b.offset - a.offset) });
      var _two = _extends({}, a, { path: a.path.slice(), offset: b.offset + b.text.length - _one.text.length, text: a.text.substring(_one.text.length) });
      return [_one, _two];
    }

    if (ao_bo == 'after' && a.length != null && b.offset < a.offset + a.length) {
      var _one2 = _extends({}, a, { path: a.path.slice(), length: b.offset - a.offset });
      var _two2 = _extends({}, a, { path: a.path.slice(), offset: b.offset + b.text.length, length: a.length - _one2.length });
      return [_one2, _two2];
    }

    if (ao_bo == 'before' || ao_bo == 'exact' && p == 'right' || ao_bo == 'exact' && a.type != 'insert_text') {
      a.offset += b.text.length;
    }
  }

  /**
   * Remove text.
   */

  if (b.type == 'remove_text') {
    if (ap_bp == 'before' && a.type == 'merge_node' && av == bv + 1) {
      a.position -= b.text.length;
    }

    if (ap_bp == 'exact' && a.type == 'split_node' && apo_bo == 'before') {
      var decrement = Math.min(a.position, b.text.length);
      a.position -= decrement;
    }

    if (ap_bp != 'exact' || a.offset == null) {
      return;
    }

    if (ao_bo == 'before' && a.type == 'insert_text') {
      var _remove3 = Math.min(a.offset - b.offset, b.text.length);
      a.offset -= _remove3;
    }

    if (ao_bo == 'before' && a.type == 'remove_text') {
      var _decrement = Math.min(b.text.length, a.offset - b.offset);
      var _remove4 = Math.min(a.text.length, b.text.length - _decrement);
      a.offset -= _decrement;
      a.text = a.text.substring(_remove4);
    }

    if (ao_bo == 'before' && a.length != null) {
      var _decrement2 = Math.min(b.text.length, a.offset - b.offset);
      var _remove5 = Math.min(a.length, b.text.length - _decrement2);
      a.offset -= _decrement2;
      a.length -= _remove5;
    }

    if (ao_bo == 'exact' && a.type == 'remove_text') {
      var _remove6 = Math.min(a.text.length, b.text.length);
      a.text = a.text.substring(_remove6);
    }

    if (ao_bo == 'exact' && a.length != null) {
      var _remove7 = Math.min(a.length, b.text.length);
      a.length -= _remove7;
    }

    if (ao_bo == 'after' && a.type == 'remove_text') {
      var _diff = b.offset - a.offset;
      var overlap = Math.max(0, Math.min(b.text.length, a.text.length - _diff));
      a.text = a.text.substring(0, _diff) + a.text.substring(_diff + overlap);
    }

    if (ao_bo == 'after' && a.length != null) {
      var _diff2 = b.offset - a.offset;
      var _overlap = Math.max(0, Math.min(b.text.length, a.length - _diff2));
      var _remove8 = Math.min(a.length, _overlap);
      a.length -= _remove8;
    }

    if (a.text === '' || a.length === 0) {
      return null;
    }
  }

  /**
   * Add mark.
   */

  if (b.type == 'add_mark') {
    if (a.type != 'remove_mark' || ap_bp != 'exact' || ao_bo == null || p == 'left' || !(0, _isEqual2.default)(a.mark, b.mark)) {
      return;
    }

    if (ao_bo == 'before' && a.offset < b.offset + b.length) {
      var skip = Math.min(b.length, a.offset - b.offset);
      var omit = Math.min(a.length, b.length - skip);
      a.offset += skip;
      a.length -= omit;
    }

    if (ao_bo == 'exact') {
      var _omit = Math.min(a.length, b.length);
      a.offset += _omit;
      a.length -= _omit;
    }

    if (ao_bo == 'after' && aol_bol == 'before') {
      var before = b.offset - a.offset;
      var after = a.length - before - b.length;
      var middle = a.length - before - after;
      var _one3 = _extends({}, a, { path: a.path.slice(), length: before });
      var _two3 = _extends({}, a, { path: a.path.slice(), offset: a.offset + before + middle, length: after });
      return [_one3, _two3];
    }

    if (ao_bo == 'after') {
      var _diff3 = b.offset - a.offset;
      var _overlap2 = Math.max(0, Math.min(b.length, a.length - _diff3));
      var _remove9 = Math.min(a.length, _overlap2);
      a.length -= _remove9;
    }

    if (a.length === 0) {
      return null;
    }
  }

  /**
   * Remove mark.
   */

  if (b.type == 'remove_mark') {
    if (ap_bp != 'exact' || ao_bo == null || a.type != 'add_mark' && a.type != 'set_mark' || a.type == 'add_mark' && p == 'left' || !(0, _isEqual2.default)(a.mark, b.mark)) {
      return;
    }

    if (ao_bo == 'before' && a.offset < b.offset + b.length) {
      var _skip = Math.min(b.length, a.offset - b.offset);
      var _omit2 = Math.min(a.length, b.length - _skip);
      a.offset += _skip;
      a.length -= _omit2;
    }

    if (ao_bo == 'exact') {
      var _omit3 = Math.min(a.length, b.length);
      a.offset += _omit3;
      a.length -= _omit3;
    }

    if (ao_bo == 'after' && aol_bol == 'before') {
      var _before = b.offset - a.offset;
      var _after = a.length - _before - b.length;
      var _middle = a.length - _before - _after;
      var _one4 = _extends({}, a, { path: a.path.slice(), length: _before });
      var _two4 = _extends({}, a, { path: a.path.slice(), offset: a.offset + _before + _middle, length: _after });
      return [_one4, _two4];
    }

    if (ao_bo == 'after') {
      var _diff4 = b.offset - a.offset;
      var _overlap3 = Math.max(0, Math.min(b.length, a.length - _diff4));
      var _remove10 = Math.min(a.length, _overlap3);
      a.length -= _remove10;
    }

    if (a.length === 0) {
      return null;
    }
  }

  /**
   * Set mark.
   */

  if (b.type == 'set_mark') {
    if (ap_bp != 'exact' || ao_bo == null || !(0, _isEqual2.default)(a.mark, b.mark)) {
      return;
    }

    if (a.type == 'set_mark' && p == 'left') {
      var overlaps = ay >= bx && by >= ax;
      if (!overlaps) return;

      var x = Math.max(ax, bx);
      var y = Math.min(ay, by);
      var offset = x;
      var length = y - x;
      if (length == 0) return;

      // TODO: can be replaced with `invert(B)`?
      var mark = _extends({}, (0, _cloneDeep2.default)(b.mark), (0, _cloneDeep2.default)(b.properties));
      var properties = (0, _pick2.default)(b.mark, Object.keys(b.properties));
      var _inverse4 = _extends({}, b, { path: b.path.slice(), offset: offset, length: length, mark: mark, properties: properties });
      return [_inverse4, a];
    }
  }
}

/**
 * Export.
 *
 * @type {Function}
 */

exports.default = transform;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9vdC90cmFuc2Zvcm0uanMiXSwibmFtZXMiOlsiZGVidWciLCJ0cmFuc2Zvcm0iLCJhIiwiYiIsInByaW9yaXR5IiwiYm8iLCJ0bXAiLCJhbyIsInJldCIsInQiLCJBcnJheSIsImlzQXJyYXkiLCJjb25jYXQiLCJwdXNoIiwiaW50ZXJzZWN0UGF0aCIsImkiLCJsZW5ndGgiLCJidiIsImF2IiwiYWwiLCJibCIsImludGVyc2VjdE9mZnNldCIsImlzTmFOIiwicCIsImFpIiwicGF0aCIsImJpIiwiYW5pIiwibmV3UGF0aCIsImJuaSIsImJudiIsImF4Iiwib2Zmc2V0IiwiYXkiLCJieCIsImJ5IiwiYXBfYnAiLCJhcF9ibnAiLCJhbnBfYnAiLCJhbnBfYm5wIiwiYW9fYm8iLCJhb2xfYm9sIiwiYWJpX2JwbyIsInBvc2l0aW9uIiwiYW9fYnBvIiwiYXBvX2JvIiwiYXBvX2JwbyIsImFvbF9icG8iLCJhb3RfYnBvIiwidGV4dCIsInR5cGUiLCJzbGljZSIsIm1vdmUiLCJyZW1vdmUiLCJhayIsIk9iamVjdCIsImtleXMiLCJwcm9wZXJ0aWVzIiwiYmsiLCJkaWZmIiwiaW52ZXJzZSIsInR3b1BhdGgiLCJvbmUiLCJ0d28iLCJzdWJzdHJpbmciLCJkZWNyZW1lbnQiLCJNYXRoIiwibWluIiwib3ZlcmxhcCIsIm1heCIsIm1hcmsiLCJza2lwIiwib21pdCIsImJlZm9yZSIsImFmdGVyIiwibWlkZGxlIiwib3ZlcmxhcHMiLCJ4IiwieSJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7QUFDQTs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUNBOzs7O0FBQ0E7Ozs7Ozs7O0FBRUE7Ozs7OztBQU1BLElBQU1BLFFBQVEscUJBQU0sb0JBQU4sQ0FBZDs7QUFFQTs7Ozs7Ozs7QUFRQSxTQUFTQyxTQUFULENBQW1CQyxDQUFuQixFQUFzQkMsQ0FBdEIsRUFBeUJDLFFBQXpCLEVBQW1DO0FBQ2pDO0FBQ0FGLE1BQUkseUJBQU1BLENBQU4sQ0FBSjs7QUFGaUM7QUFBQTtBQUFBOztBQUFBO0FBSWpDLHlCQUFpQkMsQ0FBakIsOEhBQW9CO0FBQUEsVUFBVEUsRUFBUzs7QUFDbEI7QUFDQTtBQUNBO0FBQ0EsVUFBSUMsTUFBTSxFQUFWOztBQUprQjtBQUFBO0FBQUE7O0FBQUE7QUFNbEIsOEJBQWlCSixDQUFqQixtSUFBb0I7QUFBQSxjQUFUSyxFQUFTOztBQUNsQlAsZ0JBQU0sR0FBTixlQUFnQk8sRUFBaEI7QUFDQVAsZ0JBQU0sR0FBTixlQUFnQkssRUFBaEI7O0FBRUEsY0FBTUcsTUFBTUMsRUFBRUYsRUFBRixFQUFNRixFQUFOLEVBQVVELFFBQVYsQ0FBWjs7QUFFQSxjQUFJSSxRQUFRLElBQVosRUFBa0I7QUFDaEJSLGtCQUFNLEtBQU4sZUFBa0JPLEVBQWxCO0FBQ0E7QUFDRCxXQUhELE1BR08sSUFBSUcsTUFBTUMsT0FBTixDQUFjSCxHQUFkLENBQUosRUFBd0I7QUFDN0JSLGtCQUFNLEtBQU4sK0JBQWtCUSxHQUFsQjtBQUNBRixrQkFBTUEsSUFBSU0sTUFBSixDQUFXSixHQUFYLENBQU47QUFDRCxXQUhNLE1BR0E7QUFDTFIsa0JBQU0sS0FBTixlQUFrQlEsR0FBbEI7QUFDQUYsZ0JBQUlPLElBQUosQ0FBU04sRUFBVDtBQUNEO0FBQ0Y7O0FBRUQ7QUF4QmtCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7O0FBeUJsQkwsVUFBSUksR0FBSjtBQUNEOztBQUVEO0FBaENpQztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBOztBQWlDakMsU0FBT0osQ0FBUDtBQUNEOztBQUVEOzs7Ozs7OztBQVFBLFNBQVNZLGFBQVQsQ0FBdUJaLENBQXZCLEVBQTBCQyxDQUExQixFQUE2QjtBQUMzQixNQUFJRCxLQUFLLElBQUwsSUFBYUMsS0FBSyxJQUF0QixFQUE0QixPQUFPLElBQVA7O0FBRTVCLE9BQUssSUFBSVksSUFBSSxDQUFiLEVBQWdCQSxJQUFJWixFQUFFYSxNQUF0QixFQUE4QkQsR0FBOUIsRUFBbUM7QUFDakMsUUFBTUUsS0FBS2QsRUFBRVksQ0FBRixDQUFYO0FBQ0EsUUFBTUcsS0FBS2hCLEVBQUVhLENBQUYsQ0FBWDtBQUNBLFFBQU1JLEtBQUtKLElBQUksQ0FBSixLQUFVYixFQUFFYyxNQUF2QjtBQUNBLFFBQU1JLEtBQUtMLElBQUksQ0FBSixLQUFVWixFQUFFYSxNQUF2QjtBQUNBO0FBQ0EsUUFBSUMsS0FBS0MsRUFBVCxFQUFhLE9BQU8sTUFBUDtBQUNiO0FBQ0EsUUFBSSxDQUFDRSxFQUFELElBQU9ILEtBQUtDLEVBQWhCLEVBQW9CLE9BQU8sTUFBUDtBQUNwQjtBQUNBLFFBQUlDLE1BQU1DLEVBQU4sSUFBWUgsTUFBTUMsRUFBdEIsRUFBMEIsT0FBTyxPQUFQO0FBQzFCO0FBQ0EsUUFBSUUsTUFBTUgsTUFBTUMsRUFBaEIsRUFBb0IsT0FBTyxPQUFQO0FBQ3BCO0FBQ0EsUUFBSUUsTUFBTUgsS0FBS0MsRUFBZixFQUFtQixPQUFPLFFBQVA7QUFDcEI7O0FBRUQsU0FBTyxNQUFQO0FBQ0Q7O0FBRUQ7Ozs7Ozs7OztBQVNBLFNBQVNHLGVBQVQsQ0FBeUJuQixDQUF6QixFQUE0QkMsQ0FBNUIsRUFBK0I7QUFDN0IsTUFBSUQsS0FBSyxJQUFMLElBQWFDLEtBQUssSUFBbEIsSUFBMEJtQixNQUFNcEIsQ0FBTixDQUExQixJQUFzQ29CLE1BQU1uQixDQUFOLENBQTFDLEVBQW9ELE9BQU8sSUFBUDtBQUNwRCxNQUFJQSxJQUFJRCxDQUFSLEVBQVcsT0FBTyxRQUFQO0FBQ1gsTUFBSUMsS0FBS0QsQ0FBVCxFQUFZLE9BQU8sT0FBUDtBQUNaLFNBQU8sT0FBUDtBQUNEOztBQUVEOzs7Ozs7OztBQVFBLFNBQVNPLENBQVQsQ0FBV1AsQ0FBWCxFQUFjQyxDQUFkLEVBQWlCb0IsQ0FBakIsRUFBb0I7QUFDbEIsTUFBTUMsS0FBS3RCLEVBQUV1QixJQUFGLENBQU9ULE1BQVAsR0FBZ0IsQ0FBM0I7QUFDQSxNQUFNVSxLQUFLdkIsRUFBRXNCLElBQUYsQ0FBT1QsTUFBUCxHQUFnQixDQUEzQjtBQUNBLE1BQU1FLEtBQUtoQixFQUFFdUIsSUFBRixDQUFPRCxFQUFQLENBQVg7QUFDQSxNQUFNUCxLQUFLZCxFQUFFc0IsSUFBRixDQUFPQyxFQUFQLENBQVg7QUFDQSxNQUFNQyxNQUFNekIsRUFBRTBCLE9BQUYsR0FBWTFCLEVBQUUwQixPQUFGLENBQVVaLE1BQVYsR0FBbUIsQ0FBL0IsR0FBbUMsSUFBL0M7QUFDQSxNQUFNYSxNQUFNMUIsRUFBRXlCLE9BQUYsR0FBWXpCLEVBQUV5QixPQUFGLENBQVVaLE1BQVYsR0FBbUIsQ0FBL0IsR0FBbUMsSUFBL0M7QUFDQSxNQUFNYyxNQUFNM0IsRUFBRXlCLE9BQUYsR0FBWXpCLEVBQUV5QixPQUFGLENBQVVDLEdBQVYsQ0FBWixHQUE2QixJQUF6Qzs7QUFFQSxNQUFNRSxLQUFLLFlBQVk3QixDQUFaLEdBQWdCQSxFQUFFOEIsTUFBbEIsR0FBMkIsSUFBdEM7QUFDQSxNQUFNQyxLQUFLLFlBQVkvQixDQUFaLElBQWlCLFlBQVlBLENBQTdCLEdBQWlDQSxFQUFFOEIsTUFBRixHQUFXOUIsRUFBRWMsTUFBOUMsR0FBdUQsSUFBbEU7QUFDQSxNQUFNa0IsS0FBSyxZQUFZL0IsQ0FBWixHQUFnQkEsRUFBRTZCLE1BQWxCLEdBQTJCLElBQXRDO0FBQ0EsTUFBTUcsS0FBSyxZQUFZaEMsQ0FBWixJQUFpQixZQUFZQSxDQUE3QixHQUFpQ0EsRUFBRTZCLE1BQUYsR0FBVzdCLEVBQUVhLE1BQTlDLEdBQXVELElBQWxFOztBQUVBLE1BQU1vQixRQUFRdEIsY0FBY1osRUFBRXVCLElBQWhCLEVBQXNCdEIsRUFBRXNCLElBQXhCLENBQWQ7QUFDQSxNQUFNWSxTQUFTdkIsY0FBY1osRUFBRXVCLElBQWhCLEVBQXNCdEIsRUFBRXlCLE9BQXhCLENBQWY7QUFDQSxNQUFNVSxTQUFTeEIsY0FBY1osRUFBRTBCLE9BQWhCLEVBQXlCekIsRUFBRXNCLElBQTNCLENBQWY7QUFDQSxNQUFNYyxVQUFVekIsY0FBY1osRUFBRTBCLE9BQWhCLEVBQXlCekIsRUFBRXlCLE9BQTNCLENBQWhCOztBQUVBO0FBQ0EsTUFBTVksUUFBUW5CLGdCQUFnQm5CLEVBQUU4QixNQUFsQixFQUEwQjdCLEVBQUU2QixNQUE1QixDQUFkO0FBQ0EsTUFBTVMsVUFBVXBCLGdCQUFnQm5CLEVBQUU4QixNQUFGLEdBQVc5QixFQUFFYyxNQUE3QixFQUFxQ2IsRUFBRTZCLE1BQUYsR0FBVzdCLEVBQUVhLE1BQWxELENBQWhCOztBQUVBO0FBQ0EsTUFBTTBCLFVBQVVyQixnQkFBZ0JuQixFQUFFdUIsSUFBRixDQUFPQyxLQUFLLENBQVosQ0FBaEIsRUFBZ0N2QixFQUFFd0MsUUFBbEMsQ0FBaEI7QUFDQSxNQUFNQyxTQUFTdkIsZ0JBQWdCbkIsRUFBRThCLE1BQWxCLEVBQTBCN0IsRUFBRXdDLFFBQTVCLENBQWY7QUFDQSxNQUFNRSxTQUFTeEIsZ0JBQWdCbkIsRUFBRXlDLFFBQWxCLEVBQTRCeEMsRUFBRTZCLE1BQTlCLENBQWY7QUFDQSxNQUFNYyxVQUFVekIsZ0JBQWdCbkIsRUFBRXlDLFFBQWxCLEVBQTRCeEMsRUFBRXdDLFFBQTlCLENBQWhCO0FBQ0EsTUFBTUksVUFBVTFCLGdCQUFnQm5CLEVBQUU4QixNQUFGLEdBQVc5QixFQUFFYyxNQUE3QixFQUFxQ2IsRUFBRXdDLFFBQXZDLENBQWhCO0FBQ0EsTUFBTUssVUFBVSxVQUFVOUMsQ0FBVixHQUFjbUIsZ0JBQWdCbkIsRUFBRThCLE1BQUYsR0FBVzlCLEVBQUUrQyxJQUFGLENBQU9qQyxNQUFsQyxFQUEwQ2IsRUFBRXdDLFFBQTVDLENBQWQsR0FBc0UsSUFBdEY7O0FBRUE7Ozs7QUFJQSxNQUFJeEMsRUFBRStDLElBQUYsSUFBVSxhQUFkLEVBQTZCO0FBQzNCLFFBQUlkLFNBQVMsT0FBVCxJQUFvQmxDLEVBQUVnRCxJQUFGLElBQVUsWUFBbEMsRUFBZ0Q7QUFDOUMsVUFBTXRCLFVBQVUxQixFQUFFdUIsSUFBRixDQUFPMEIsS0FBUCxDQUFhLENBQWIsRUFBZ0IzQixFQUFoQixFQUFvQlosTUFBcEIsQ0FBMkIsQ0FBQ1YsRUFBRXVCLElBQUYsQ0FBT0QsRUFBUCxJQUFhLENBQWQsQ0FBM0IsQ0FBaEI7QUFDQSxVQUFNNEIsT0FBTyxFQUFFRixNQUFNLFdBQVIsRUFBcUJ6QixNQUFNdkIsRUFBRXVCLElBQUYsQ0FBTzBCLEtBQVAsRUFBM0IsRUFBMkN2QixnQkFBM0MsRUFBYjtBQUNBLGFBQU8sQ0FBQ3dCLElBQUQsRUFBT2xELENBQVAsQ0FBUDtBQUNEOztBQUVELFFBQ0drQyxTQUFTLFFBQVYsSUFDQ0EsU0FBUyxPQURWLElBRUNBLFNBQVMsT0FBVCxJQUFvQmxDLEVBQUVnRCxJQUFGLElBQVUsYUFGL0IsSUFHQ2QsU0FBUyxPQUFULElBQW9CbEMsRUFBRWdELElBQUYsSUFBVSxhQUE5QixJQUErQzNCLEtBQUssT0FKdkQsRUFLRTtBQUNBckIsUUFBRXVCLElBQUYsQ0FBT0MsRUFBUDtBQUNEOztBQUVELFFBQ0dZLFVBQVUsUUFBWCxJQUNDQSxVQUFVLE9BRFgsSUFFQ0EsVUFBVSxPQUFWLElBQXFCZixLQUFLLE9BSDdCLEVBSUU7QUFDQXJCLFFBQUUwQixPQUFGLENBQVVGLEVBQVY7QUFDRDtBQUNGOztBQUVEOzs7O0FBSUEsTUFBSXZCLEVBQUUrQyxJQUFGLElBQVUsYUFBZCxFQUE2QjtBQUMzQixRQUFJZCxTQUFTLFFBQVQsSUFBcUJsQyxFQUFFZ0QsSUFBRixJQUFVLFlBQS9CLElBQStDaEMsTUFBTUQsS0FBSyxDQUE5RCxFQUFpRTtBQUMvRDtBQUNBLFVBQU1vQyxTQUFTLEVBQUVILE1BQU0sYUFBUixFQUF1QnpCLE1BQU12QixFQUFFdUIsSUFBRixDQUFPMEIsS0FBUCxFQUE3QixFQUFmO0FBQ0EsYUFBTyxDQUFDRSxNQUFELENBQVA7QUFDRDs7QUFFRCxRQUFJakIsU0FBUyxRQUFiLEVBQXVCO0FBQ3JCbEMsUUFBRXVCLElBQUYsQ0FBT0MsRUFBUDtBQUNEOztBQUVELFFBQUlZLFVBQVUsUUFBZCxFQUF3QjtBQUN0QnBDLFFBQUUwQixPQUFGLENBQVVGLEVBQVY7QUFDRDs7QUFFRCxRQUFJVSxTQUFTLE9BQVQsSUFBb0JsQyxFQUFFZ0QsSUFBRixJQUFVLFlBQWxDLEVBQWdEO0FBQzlDLFVBQU16QixPQUFPdkIsRUFBRXVCLElBQUYsQ0FBTzBCLEtBQVAsQ0FBYSxDQUFiLEVBQWdCM0IsRUFBaEIsRUFBb0JaLE1BQXBCLENBQTJCLENBQUNWLEVBQUV1QixJQUFGLENBQU9ELEVBQVAsSUFBYSxDQUFkLENBQTNCLENBQWI7QUFDQTtBQUNBLFVBQU02QixVQUFTLEVBQUVILE1BQU0sYUFBUixFQUF1QnpCLFVBQXZCLEVBQWY7QUFDQSxhQUFPLENBQUM0QixPQUFELENBQVA7QUFDRDs7QUFFRCxRQUNHakIsU0FBUyxPQUFWLElBQ0NBLFNBQVMsT0FBVCxJQUFvQmxDLEVBQUVnRCxJQUFGLElBQVUsYUFGakMsRUFHRTtBQUNBLGFBQU8sSUFBUDtBQUNEO0FBQ0Y7O0FBRUQ7Ozs7QUFJQSxNQUFJL0MsRUFBRStDLElBQUYsSUFBVSxVQUFkLEVBQTBCO0FBQ3hCLFFBQUloRCxFQUFFZ0QsSUFBRixJQUFVLFVBQVYsSUFBd0JkLFNBQVMsT0FBakMsSUFBNENiLEtBQUssT0FBckQsRUFBOEQ7QUFDNUQsVUFBTStCLEtBQUtDLE9BQU9DLElBQVAsQ0FBWXRELEVBQUV1RCxVQUFkLENBQVg7QUFDQSxVQUFNQyxLQUFLSCxPQUFPQyxJQUFQLENBQVlyRCxFQUFFc0QsVUFBZCxDQUFYO0FBQ0EsVUFBTUUsT0FBTywwQkFBV0wsRUFBWCxFQUFlSSxFQUFmLENBQWI7QUFDQSxVQUFJQyxLQUFLM0MsTUFBTCxJQUFlLENBQW5CLEVBQXNCLE9BQU8sSUFBUDtBQUN0QmQsUUFBRXVELFVBQUYsR0FBZSxvQkFBS3ZELEVBQUV1RCxVQUFQLEVBQW1CRSxJQUFuQixDQUFmO0FBQ0Q7QUFDRjs7QUFFRDs7OztBQUlBLE1BQUl4RCxFQUFFK0MsSUFBRixJQUFVLFdBQWQsRUFBMkI7QUFDekIsUUFBSWQsU0FBUyxPQUFULElBQW9CbEMsRUFBRWdELElBQUYsSUFBVSxXQUE5QixJQUE2QzNCLEtBQUssT0FBdEQsRUFBK0Q7QUFDN0QsYUFBTyxJQUFQO0FBQ0Q7O0FBRUQsUUFBSWEsU0FBUyxPQUFULElBQW9CbEMsRUFBRWdELElBQUYsSUFBVSxZQUFsQyxFQUFnRDtBQUM5QyxVQUFNVSxVQUFVLHNCQUFPekQsQ0FBUCxDQUFoQjtBQUNBLFVBQU1zQixRQUFPdEIsRUFBRXNCLElBQUYsQ0FBTzBCLEtBQVAsQ0FBYSxDQUFiLEVBQWdCekIsRUFBaEIsRUFBb0JkLE1BQXBCLENBQTJCLENBQUNULEVBQUVzQixJQUFGLENBQU9DLEVBQVAsSUFBYSxDQUFkLENBQTNCLENBQWI7QUFDQSxVQUFJRSxXQUFVekIsRUFBRXlCLE9BQUYsQ0FBVXVCLEtBQVYsRUFBZDs7QUFFQSxVQUFJckMsY0FBY1gsRUFBRXlCLE9BQWhCLEVBQXlCMUIsRUFBRXVCLElBQTNCLEtBQW9DLFFBQXhDLEVBQWtEO0FBQ2hERyxtQkFBVXpCLEVBQUV5QixPQUFGLENBQVV1QixLQUFWLENBQWdCLENBQWhCLEVBQW1CdEIsR0FBbkIsRUFBd0JqQixNQUF4QixDQUErQlQsRUFBRXlCLE9BQUYsQ0FBVUMsR0FBVixJQUFpQixDQUFoRCxDQUFWO0FBQ0Q7O0FBRUQsVUFBTXVCLHFCQUFZakQsQ0FBWixJQUFlc0IsV0FBZixFQUFxQkcsaUJBQXJCLEdBQU47QUFDQSxhQUFPLENBQUNnQyxPQUFELEVBQVUxRCxDQUFWLEVBQWFrRCxLQUFiLENBQVA7QUFDRDs7QUFFRCxRQUFJaEIsU0FBUyxRQUFULElBQXFCbEMsRUFBRWdELElBQUYsSUFBVSxZQUEvQixJQUErQ2hDLE1BQU1ELEtBQUssQ0FBOUQsRUFBaUU7QUFDL0QsVUFBTTJDLFdBQVUsc0JBQU96RCxDQUFQLENBQWhCO0FBQ0EsVUFBTXNCLFNBQU90QixFQUFFc0IsSUFBRixDQUFPMEIsS0FBUCxFQUFiO0FBQ0EsVUFBSXZCLFlBQVV6QixFQUFFeUIsT0FBRixDQUFVdUIsS0FBVixFQUFkOztBQUVBLFVBQUlyQyxjQUFjWCxFQUFFeUIsT0FBaEIsRUFBeUIxQixFQUFFdUIsSUFBM0IsS0FBb0MsUUFBeEMsRUFBa0Q7QUFDaERHLG9CQUFVekIsRUFBRXlCLE9BQUYsQ0FBVXVCLEtBQVYsQ0FBZ0IsQ0FBaEIsRUFBbUJ0QixHQUFuQixFQUF3QmpCLE1BQXhCLENBQStCVCxFQUFFeUIsT0FBRixDQUFVQyxHQUFWLElBQWlCLENBQWhELENBQVY7QUFDRDs7QUFFRCxVQUFNdUIsc0JBQVlqRCxDQUFaLElBQWVzQixZQUFmLEVBQXFCRyxrQkFBckIsR0FBTjtBQUNBLGFBQU8sQ0FBQ2dDLFFBQUQsRUFBVTFELENBQVYsRUFBYWtELE1BQWIsQ0FBUDtBQUNEOztBQUVELFFBQ0dmLFVBQVUsT0FBVixJQUFxQkQsU0FBUyxRQUE5QixJQUEwQ2xDLEVBQUVnRCxJQUFGLElBQVUsWUFBckQsSUFDQ2QsU0FBUyxRQUFULElBQXFCbEMsRUFBRWdELElBQUYsSUFBVSxZQUEvQixJQUErQ2IsVUFBVSxRQUF6RCxJQUFxRW5CLE1BQU1ZLE1BQU0sQ0FGcEYsRUFHRTtBQUNBLFVBQU04QixZQUFVLHNCQUFPekQsQ0FBUCxDQUFoQjtBQUNBLGFBQU8sQ0FBQ3lELFNBQUQsRUFBVTFELENBQVYsRUFBYUMsQ0FBYixDQUFQO0FBQ0Q7O0FBRUQsUUFBSWtDLFVBQVUsUUFBVixJQUFzQkQsU0FBUyxRQUEvQixJQUEyQ2xDLEVBQUVnRCxJQUFGLElBQVUsWUFBckQsSUFBcUVoQyxNQUFNRCxLQUFLLENBQXBGLEVBQXVGO0FBQ3JGLFVBQU0yQyxZQUFVLHNCQUFPekQsQ0FBUCxDQUFoQjtBQUNBLFVBQU1zQixTQUFPdEIsRUFBRXNCLElBQUYsQ0FBTzBCLEtBQVAsRUFBYjtBQUNBLFVBQU12QixZQUFVekIsRUFBRXlCLE9BQUYsQ0FBVXVCLEtBQVYsQ0FBZ0IsQ0FBaEIsRUFBbUJ0QixHQUFuQixFQUF3QmpCLE1BQXhCLENBQStCVCxFQUFFeUIsT0FBRixDQUFVQyxHQUFWLElBQWlCLENBQWhELENBQWhCO0FBQ0EsVUFBTXVCLHNCQUFZakQsQ0FBWixJQUFlc0IsWUFBZixFQUFxQkcsa0JBQXJCLEdBQU47QUFDQSxhQUFPLENBQUNnQyxTQUFELEVBQVUxRCxDQUFWLEVBQWFrRCxNQUFiLENBQVA7QUFDRDs7QUFFRCxRQUFJaEIsU0FBUyxPQUFiLEVBQXNCO0FBQ3BCbEMsUUFBRXVCLElBQUYsR0FBU3RCLEVBQUV5QixPQUFGLENBQVV1QixLQUFWLEVBQVQ7QUFDRDs7QUFFRCxRQUFJYixVQUFVLE9BQWQsRUFBdUI7QUFDckJwQyxRQUFFMEIsT0FBRixHQUFZekIsRUFBRXlCLE9BQUYsQ0FBVXVCLEtBQVYsRUFBWjtBQUNEOztBQUVELFFBQUlmLFNBQVMsT0FBYixFQUFzQjtBQUNwQmxDLFFBQUV1QixJQUFGLEdBQVN0QixFQUFFeUIsT0FBRixDQUFVaEIsTUFBVixDQUFpQlYsRUFBRXVCLElBQUYsQ0FBTzBCLEtBQVAsQ0FBYWhELEVBQUV5QixPQUFGLENBQVVaLE1BQXZCLENBQWpCLENBQVQ7QUFDRDs7QUFFRCxRQUFJc0IsVUFBVSxPQUFkLEVBQXVCO0FBQ3JCcEMsUUFBRTBCLE9BQUYsR0FBWXpCLEVBQUV5QixPQUFGLENBQVVoQixNQUFWLENBQWlCVixFQUFFMEIsT0FBRixDQUFVdUIsS0FBVixDQUFnQmhELEVBQUV5QixPQUFGLENBQVVaLE1BQTFCLENBQWpCLENBQVo7QUFDRDs7QUFFRCxRQUFJb0IsU0FBUyxRQUFULElBQXFCQyxVQUFVLFFBQW5DLEVBQTZDO0FBQzNDbkMsUUFBRXVCLElBQUYsQ0FBT0MsRUFBUDtBQUNEOztBQUVELFFBQUlVLFNBQVMsT0FBVCxJQUFvQkUsVUFBVSxRQUE5QixJQUEwQ0MsV0FBVyxRQUF6RCxFQUFtRTtBQUNqRXJDLFFBQUUwQixPQUFGLENBQVVGLEVBQVY7QUFDRDs7QUFFRCxRQUNHVSxTQUFTLFFBQVQsSUFBcUJDLFVBQVUsT0FBaEMsSUFDQ0QsU0FBUyxNQUFULElBQW1CQyxVQUFVLE1BRmhDLEVBR0U7QUFDQW5DLFFBQUV1QixJQUFGLENBQU9JLEdBQVA7QUFDRDs7QUFFRCxRQUNHTyxTQUFTLE9BQVQsSUFBb0JFLFVBQVUsUUFBOUIsSUFBMENDLFdBQVcsT0FBdEQsSUFDQ0gsU0FBUyxPQUFULElBQW9CRSxVQUFVLE1BQTlCLElBQXdDQyxXQUFXLE1BRnRELEVBR0U7QUFDQXJDLFFBQUUwQixPQUFGLENBQVVDLEdBQVY7QUFDRDtBQUNGOztBQUVEOzs7O0FBSUEsTUFBSTFCLEVBQUUrQyxJQUFGLElBQVUsWUFBZCxFQUE0QjtBQUMxQixRQUFJZCxTQUFTLFFBQWIsRUFBdUI7QUFDckJsQyxRQUFFdUIsSUFBRixDQUFPQyxFQUFQO0FBQ0Q7O0FBRUQsUUFBSVUsU0FBUyxRQUFULElBQXFCbEMsRUFBRWdELElBQUYsSUFBVSxZQUEvQixJQUErQ2hDLE1BQU1ELEtBQUssQ0FBOUQsRUFBaUU7QUFDL0RmLFFBQUV5QyxRQUFGLElBQWN4QyxFQUFFd0MsUUFBaEI7QUFDRDs7QUFFRCxRQUFJTCxVQUFVLFFBQWQsRUFBd0I7QUFDdEJwQyxRQUFFMEIsT0FBRixDQUFVRixFQUFWO0FBQ0Q7O0FBRUQsUUFBSVUsU0FBUyxPQUFiLEVBQXNCO0FBQ3BCbEMsUUFBRXVCLElBQUYsQ0FBT0MsRUFBUDtBQUNBeEIsUUFBRXVCLElBQUYsQ0FBT0MsS0FBSyxDQUFaLEtBQWtCdkIsRUFBRXdDLFFBQXBCO0FBQ0Q7O0FBRUQsUUFBSUwsVUFBVSxPQUFkLEVBQXVCO0FBQ3JCcEMsUUFBRTBCLE9BQUYsQ0FBVUYsRUFBVjtBQUNBeEIsUUFBRTBCLE9BQUYsQ0FBVUYsS0FBSyxDQUFmLEtBQXFCdkIsRUFBRXdDLFFBQXZCO0FBQ0Q7O0FBRUQsUUFBSVAsU0FBUyxPQUFULElBQW9CbEMsRUFBRWdELElBQUYsSUFBVSxZQUFsQyxFQUFnRDtBQUM5QyxhQUFPLElBQVA7QUFDRDs7QUFFRCxRQUFJZCxTQUFTLE9BQWIsRUFBc0I7QUFDcEJsQyxRQUFFdUIsSUFBRixHQUFTdkIsRUFBRXVCLElBQUYsQ0FBTzBCLEtBQVAsQ0FBYSxDQUFiLEVBQWdCM0IsRUFBaEIsRUFBb0JaLE1BQXBCLENBQTJCLENBQUNWLEVBQUV1QixJQUFGLENBQU9ELEVBQVAsSUFBYSxDQUFkLENBQTNCLENBQVQ7O0FBRUEsVUFBSXRCLEVBQUVnRCxJQUFGLElBQVUsWUFBZCxFQUE0QjtBQUMxQmhELFVBQUV5QyxRQUFGLElBQWN4QyxFQUFFd0MsUUFBaEI7QUFDRDs7QUFFRCxVQUFJekMsRUFBRThCLE1BQUYsSUFBWSxJQUFoQixFQUFzQjtBQUNwQjlCLFVBQUU4QixNQUFGLElBQVk3QixFQUFFd0MsUUFBZDtBQUNEO0FBQ0Y7QUFDRjs7QUFFRDs7OztBQUlBLE1BQUl4QyxFQUFFK0MsSUFBRixJQUFVLFlBQWQsRUFBNEI7QUFDMUIsUUFBSWQsU0FBUyxPQUFULElBQW9CbEMsRUFBRWdELElBQUYsSUFBVSxZQUE5QixJQUE4Q2hDLE1BQU1mLEVBQUV3QyxRQUExRCxFQUFvRTtBQUNsRSxVQUFNbEIsU0FBT3ZCLEVBQUV1QixJQUFGLENBQU8wQixLQUFQLENBQWEsQ0FBYixFQUFnQnpCLEVBQWhCLEVBQW9CZCxNQUFwQixDQUEyQixDQUFDVixFQUFFdUIsSUFBRixDQUFPQyxFQUFQLElBQWEsQ0FBZCxDQUEzQixFQUE2Q2QsTUFBN0MsQ0FBb0QsQ0FBQyxDQUFELENBQXBELENBQWI7QUFDQSxVQUFNZ0IsWUFBVTFCLEVBQUV1QixJQUFGLENBQU8wQixLQUFQLEVBQWhCO0FBQ0EsVUFBTUMsU0FBTyxFQUFFRixNQUFNLFdBQVIsRUFBcUJ6QixZQUFyQixFQUEyQkcsa0JBQTNCLEVBQWI7QUFDQSxhQUFPLENBQUN3QixNQUFELEVBQU9sRCxDQUFQLENBQVA7QUFDRDs7QUFFRCxRQUFJa0MsU0FBUyxPQUFULElBQW9CbEMsRUFBRWdELElBQUYsSUFBVSxZQUE5QixJQUE4Q0osV0FBVyxRQUE3RCxFQUF1RTtBQUNyRTVDLFFBQUV1QixJQUFGLENBQU9DLEVBQVA7QUFDQXhCLFFBQUV5QyxRQUFGLElBQWN4QyxFQUFFd0MsUUFBaEI7QUFDRDs7QUFFRCxRQUFJUCxTQUFTLE9BQVQsSUFBb0JsQyxFQUFFZ0QsSUFBRixJQUFVLGFBQWxDLEVBQWlEO0FBQy9DO0FBQ0EsVUFBTUcsd0JBQWNuRCxDQUFkLElBQWlCdUIsTUFBTXZCLEVBQUV1QixJQUFGLENBQU8wQixLQUFQLEVBQXZCLEdBQU47QUFDQSxhQUFPLENBQUNqRCxDQUFELEVBQUltRCxRQUFKLENBQVA7QUFDRDs7QUFFRCxRQUFJakIsU0FBUyxRQUFiLEVBQXVCO0FBQ3JCbEMsUUFBRXVCLElBQUYsQ0FBT0MsRUFBUDs7QUFFQSxVQUFJeEIsRUFBRWdELElBQUYsSUFBVSxZQUFWLElBQTBCaEMsTUFBTUQsS0FBSyxDQUF6QyxFQUE0QztBQUMxQ2YsVUFBRXlDLFFBQUYsSUFBY3hDLEVBQUV3QyxRQUFoQjtBQUNEO0FBQ0Y7O0FBRUQsUUFBSVAsU0FBUyxPQUFULElBQW9CRSxVQUFVLFFBQWxDLEVBQTRDO0FBQzFDcEMsUUFBRTBCLE9BQUYsQ0FBVUYsRUFBVjtBQUNEOztBQUVELFFBQUlVLFNBQVMsT0FBVCxJQUFvQk0sV0FBVyxPQUFuQyxFQUE0QztBQUMxQ3hDLFFBQUV1QixJQUFGLENBQU9DLEVBQVA7QUFDQXhCLFFBQUV1QixJQUFGLENBQU9DLEtBQUssQ0FBWixLQUFrQnZCLEVBQUV3QyxRQUFwQjtBQUNEOztBQUVELFFBQUlMLFVBQVUsT0FBVixJQUFxQkksV0FBVyxPQUFwQyxFQUE2QztBQUMzQ3hDLFFBQUUwQixPQUFGLENBQVVGLEVBQVY7QUFDQXhCLFFBQUUwQixPQUFGLENBQVVGLEtBQUssQ0FBZixLQUFxQnZCLEVBQUV3QyxRQUF2QjtBQUNEOztBQUVELFFBQUlQLFNBQVMsT0FBVCxJQUFvQmxDLEVBQUVnRCxJQUFGLElBQVUsV0FBbEMsRUFBK0M7QUFDN0MsVUFBTXpCLFNBQU92QixFQUFFdUIsSUFBRixDQUFPMEIsS0FBUCxDQUFhLENBQWIsRUFBZ0IzQixFQUFoQixFQUFvQlosTUFBcEIsQ0FBMkIsQ0FBQ1YsRUFBRXVCLElBQUYsQ0FBT0QsRUFBUCxJQUFhLENBQWQsQ0FBM0IsQ0FBYjtBQUNBLFVBQU1JLFlBQVUxQixFQUFFMEIsT0FBRixDQUFVdUIsS0FBVixFQUFoQjtBQUNBLFVBQU1DLHNCQUFZbEQsQ0FBWixJQUFldUIsWUFBZixFQUFxQkcsa0JBQXJCLEdBQU47QUFDQSxhQUFPLENBQUN3QixNQUFELEVBQU9sRCxDQUFQLENBQVA7QUFDRDs7QUFFRCxRQUNHa0MsU0FBUyxPQUFWLElBQ0NsQyxFQUFFOEIsTUFBRixJQUFZLElBRGIsSUFFQ1ksVUFBVSxRQUZYLEtBR0NHLFdBQVcsUUFBWCxJQUF1QkMsV0FBVyxRQUhuQyxDQURGLEVBS0U7QUFDQTlDLFFBQUV1QixJQUFGLENBQU9DLEVBQVA7QUFDQXhCLFFBQUU4QixNQUFGLElBQVk3QixFQUFFd0MsUUFBZDtBQUNEOztBQUVELFFBQ0dQLFNBQVMsT0FBVixJQUNDbEMsRUFBRThCLE1BQUYsSUFBWSxJQURiLElBRUNZLFVBQVUsT0FGWCxLQUdDRyxXQUFXLFFBQVgsSUFBdUJDLFdBQVcsUUFIbkMsQ0FERixFQUtFO0FBQ0EsVUFBTWEsVUFBVTNELEVBQUV1QixJQUFGLENBQU8wQixLQUFQLENBQWEsQ0FBYixFQUFnQjNCLEVBQWhCLEVBQW9CWixNQUFwQixDQUEyQixDQUFDVixFQUFFdUIsSUFBRixDQUFPRCxFQUFQLElBQWEsQ0FBZCxDQUEzQixDQUFoQjtBQUNBLFVBQU1zQyxtQkFBVzVELENBQVgsSUFBY3VCLE1BQU12QixFQUFFdUIsSUFBRixDQUFPMEIsS0FBUCxFQUFwQixHQUFOO0FBQ0EsVUFBTVksbUJBQVc3RCxDQUFYLElBQWN1QixNQUFNb0MsT0FBcEIsRUFBNkI3QixRQUFRLENBQXJDLEdBQU47O0FBRUEsVUFBSTlCLEVBQUUrQyxJQUFGLElBQVUsSUFBZCxFQUFvQjtBQUNsQmEsWUFBSTlDLE1BQUosR0FBYWIsRUFBRXdDLFFBQUYsR0FBYXpDLEVBQUU4QixNQUE1QjtBQUNBK0IsWUFBSS9DLE1BQUosR0FBYWQsRUFBRWMsTUFBRixHQUFXOEMsSUFBSTlDLE1BQTVCO0FBQ0QsT0FIRCxNQUdPO0FBQ0w4QyxZQUFJYixJQUFKLEdBQVcvQyxFQUFFK0MsSUFBRixDQUFPRSxLQUFQLENBQWEsQ0FBYixFQUFnQmhELEVBQUV3QyxRQUFGLEdBQWF6QyxFQUFFOEIsTUFBL0IsQ0FBWDtBQUNBK0IsWUFBSWQsSUFBSixHQUFXL0MsRUFBRStDLElBQUYsQ0FBT0UsS0FBUCxDQUFhVyxJQUFJYixJQUFKLENBQVNqQyxNQUF0QixDQUFYO0FBQ0Q7O0FBRUQsYUFBTyxDQUFDOEMsR0FBRCxFQUFNQyxHQUFOLENBQVA7QUFDRDtBQUNGOztBQUVEOzs7O0FBSUEsTUFBSTVELEVBQUUrQyxJQUFGLElBQVUsYUFBZCxFQUE2QjtBQUMzQixRQUFJZCxTQUFTLFFBQVQsSUFBcUJsQyxFQUFFZ0QsSUFBRixJQUFVLFlBQS9CLElBQStDaEMsTUFBTUQsS0FBSyxDQUE5RCxFQUFpRTtBQUMvRGYsUUFBRXlDLFFBQUYsSUFBY3hDLEVBQUU4QyxJQUFGLENBQU9qQyxNQUFyQjtBQUNEOztBQUVELFFBQUlvQixTQUFTLE9BQVQsSUFBb0JsQyxFQUFFZ0QsSUFBRixJQUFVLFlBQTlCLEtBQStDTCxVQUFVLFFBQVYsSUFBc0JBLFVBQVUsT0FBL0UsQ0FBSixFQUE2RjtBQUMzRjNDLFFBQUV5QyxRQUFGLElBQWN4QyxFQUFFOEMsSUFBRixDQUFPakMsTUFBckI7QUFDRDs7QUFFRCxRQUFJb0IsU0FBUyxPQUFULElBQW9CbEMsRUFBRThCLE1BQUYsSUFBWSxJQUFwQyxFQUEwQztBQUN4QztBQUNEOztBQUVELFFBQUlRLFNBQVMsT0FBVCxJQUFvQnRDLEVBQUVnRCxJQUFGLElBQVUsYUFBOUIsSUFBK0MvQyxFQUFFNkIsTUFBRixHQUFXOUIsRUFBRThCLE1BQUYsR0FBVzlCLEVBQUUrQyxJQUFGLENBQU9qQyxNQUFoRixFQUF3RjtBQUN0RixVQUFNOEMsb0JBQVc1RCxDQUFYLElBQWN1QixNQUFNdkIsRUFBRXVCLElBQUYsQ0FBTzBCLEtBQVAsRUFBcEIsRUFBb0NGLE1BQU0vQyxFQUFFK0MsSUFBRixDQUFPZSxTQUFQLENBQWlCLENBQWpCLEVBQW9CN0QsRUFBRTZCLE1BQUYsR0FBVzlCLEVBQUU4QixNQUFqQyxDQUExQyxHQUFOO0FBQ0EsVUFBTStCLG9CQUFXN0QsQ0FBWCxJQUFjdUIsTUFBTXZCLEVBQUV1QixJQUFGLENBQU8wQixLQUFQLEVBQXBCLEVBQW9DbkIsUUFBUTdCLEVBQUU2QixNQUFGLEdBQVc3QixFQUFFOEMsSUFBRixDQUFPakMsTUFBbEIsR0FBMkI4QyxLQUFJYixJQUFKLENBQVNqQyxNQUFoRixFQUF3RmlDLE1BQU0vQyxFQUFFK0MsSUFBRixDQUFPZSxTQUFQLENBQWlCRixLQUFJYixJQUFKLENBQVNqQyxNQUExQixDQUE5RixHQUFOO0FBQ0EsYUFBTyxDQUFDOEMsSUFBRCxFQUFNQyxJQUFOLENBQVA7QUFDRDs7QUFFRCxRQUFJdkIsU0FBUyxPQUFULElBQW9CdEMsRUFBRWMsTUFBRixJQUFZLElBQWhDLElBQXdDYixFQUFFNkIsTUFBRixHQUFXOUIsRUFBRThCLE1BQUYsR0FBVzlCLEVBQUVjLE1BQXBFLEVBQTRFO0FBQzFFLFVBQU04QyxxQkFBVzVELENBQVgsSUFBY3VCLE1BQU12QixFQUFFdUIsSUFBRixDQUFPMEIsS0FBUCxFQUFwQixFQUFvQ25DLFFBQVFiLEVBQUU2QixNQUFGLEdBQVc5QixFQUFFOEIsTUFBekQsR0FBTjtBQUNBLFVBQU0rQixxQkFBVzdELENBQVgsSUFBY3VCLE1BQU12QixFQUFFdUIsSUFBRixDQUFPMEIsS0FBUCxFQUFwQixFQUFvQ25CLFFBQVE3QixFQUFFNkIsTUFBRixHQUFXN0IsRUFBRThDLElBQUYsQ0FBT2pDLE1BQTlELEVBQXNFQSxRQUFRZCxFQUFFYyxNQUFGLEdBQVc4QyxNQUFJOUMsTUFBN0YsR0FBTjtBQUNBLGFBQU8sQ0FBQzhDLEtBQUQsRUFBTUMsS0FBTixDQUFQO0FBQ0Q7O0FBRUQsUUFDR3ZCLFNBQVMsUUFBVixJQUNDQSxTQUFTLE9BQVQsSUFBb0JqQixLQUFLLE9BRDFCLElBRUNpQixTQUFTLE9BQVQsSUFBb0J0QyxFQUFFZ0QsSUFBRixJQUFVLGFBSGpDLEVBSUU7QUFDQWhELFFBQUU4QixNQUFGLElBQVk3QixFQUFFOEMsSUFBRixDQUFPakMsTUFBbkI7QUFDRDtBQUNGOztBQUVEOzs7O0FBSUEsTUFBSWIsRUFBRStDLElBQUYsSUFBVSxhQUFkLEVBQTZCO0FBQzNCLFFBQUlkLFNBQVMsUUFBVCxJQUFxQmxDLEVBQUVnRCxJQUFGLElBQVUsWUFBL0IsSUFBK0NoQyxNQUFNRCxLQUFLLENBQTlELEVBQWlFO0FBQy9EZixRQUFFeUMsUUFBRixJQUFjeEMsRUFBRThDLElBQUYsQ0FBT2pDLE1BQXJCO0FBQ0Q7O0FBRUQsUUFBSW9CLFNBQVMsT0FBVCxJQUFvQmxDLEVBQUVnRCxJQUFGLElBQVUsWUFBOUIsSUFBK0NMLFVBQVUsUUFBN0QsRUFBd0U7QUFDdEUsVUFBTW9CLFlBQVlDLEtBQUtDLEdBQUwsQ0FBU2pFLEVBQUV5QyxRQUFYLEVBQXFCeEMsRUFBRThDLElBQUYsQ0FBT2pDLE1BQTVCLENBQWxCO0FBQ0FkLFFBQUV5QyxRQUFGLElBQWNzQixTQUFkO0FBQ0Q7O0FBRUQsUUFBSTdCLFNBQVMsT0FBVCxJQUFvQmxDLEVBQUU4QixNQUFGLElBQVksSUFBcEMsRUFBMEM7QUFDeEM7QUFDRDs7QUFFRCxRQUFJUSxTQUFTLFFBQVQsSUFBcUJ0QyxFQUFFZ0QsSUFBRixJQUFVLGFBQW5DLEVBQWtEO0FBQ2hELFVBQU1HLFdBQVNhLEtBQUtDLEdBQUwsQ0FBU2pFLEVBQUU4QixNQUFGLEdBQVc3QixFQUFFNkIsTUFBdEIsRUFBOEI3QixFQUFFOEMsSUFBRixDQUFPakMsTUFBckMsQ0FBZjtBQUNBZCxRQUFFOEIsTUFBRixJQUFZcUIsUUFBWjtBQUNEOztBQUVELFFBQUliLFNBQVMsUUFBVCxJQUFxQnRDLEVBQUVnRCxJQUFGLElBQVUsYUFBbkMsRUFBa0Q7QUFDaEQsVUFBTWUsYUFBWUMsS0FBS0MsR0FBTCxDQUFTaEUsRUFBRThDLElBQUYsQ0FBT2pDLE1BQWhCLEVBQXdCZCxFQUFFOEIsTUFBRixHQUFXN0IsRUFBRTZCLE1BQXJDLENBQWxCO0FBQ0EsVUFBTXFCLFdBQVNhLEtBQUtDLEdBQUwsQ0FBU2pFLEVBQUUrQyxJQUFGLENBQU9qQyxNQUFoQixFQUF3QmIsRUFBRThDLElBQUYsQ0FBT2pDLE1BQVAsR0FBZ0JpRCxVQUF4QyxDQUFmO0FBQ0EvRCxRQUFFOEIsTUFBRixJQUFZaUMsVUFBWjtBQUNBL0QsUUFBRStDLElBQUYsR0FBUy9DLEVBQUUrQyxJQUFGLENBQU9lLFNBQVAsQ0FBaUJYLFFBQWpCLENBQVQ7QUFDRDs7QUFFRCxRQUFJYixTQUFTLFFBQVQsSUFBcUJ0QyxFQUFFYyxNQUFGLElBQVksSUFBckMsRUFBMkM7QUFDekMsVUFBTWlELGNBQVlDLEtBQUtDLEdBQUwsQ0FBU2hFLEVBQUU4QyxJQUFGLENBQU9qQyxNQUFoQixFQUF3QmQsRUFBRThCLE1BQUYsR0FBVzdCLEVBQUU2QixNQUFyQyxDQUFsQjtBQUNBLFVBQU1xQixXQUFTYSxLQUFLQyxHQUFMLENBQVNqRSxFQUFFYyxNQUFYLEVBQW1CYixFQUFFOEMsSUFBRixDQUFPakMsTUFBUCxHQUFnQmlELFdBQW5DLENBQWY7QUFDQS9ELFFBQUU4QixNQUFGLElBQVlpQyxXQUFaO0FBQ0EvRCxRQUFFYyxNQUFGLElBQVlxQyxRQUFaO0FBQ0Q7O0FBRUQsUUFBSWIsU0FBUyxPQUFULElBQW9CdEMsRUFBRWdELElBQUYsSUFBVSxhQUFsQyxFQUFpRDtBQUMvQyxVQUFNRyxXQUFTYSxLQUFLQyxHQUFMLENBQVNqRSxFQUFFK0MsSUFBRixDQUFPakMsTUFBaEIsRUFBd0JiLEVBQUU4QyxJQUFGLENBQU9qQyxNQUEvQixDQUFmO0FBQ0FkLFFBQUUrQyxJQUFGLEdBQVMvQyxFQUFFK0MsSUFBRixDQUFPZSxTQUFQLENBQWlCWCxRQUFqQixDQUFUO0FBQ0Q7O0FBRUQsUUFBSWIsU0FBUyxPQUFULElBQW9CdEMsRUFBRWMsTUFBRixJQUFZLElBQXBDLEVBQTBDO0FBQ3hDLFVBQU1xQyxXQUFTYSxLQUFLQyxHQUFMLENBQVNqRSxFQUFFYyxNQUFYLEVBQW1CYixFQUFFOEMsSUFBRixDQUFPakMsTUFBMUIsQ0FBZjtBQUNBZCxRQUFFYyxNQUFGLElBQVlxQyxRQUFaO0FBQ0Q7O0FBRUQsUUFBSWIsU0FBUyxPQUFULElBQW9CdEMsRUFBRWdELElBQUYsSUFBVSxhQUFsQyxFQUFpRDtBQUMvQyxVQUFNUyxRQUFPeEQsRUFBRTZCLE1BQUYsR0FBVzlCLEVBQUU4QixNQUExQjtBQUNBLFVBQU1vQyxVQUFVRixLQUFLRyxHQUFMLENBQVMsQ0FBVCxFQUFZSCxLQUFLQyxHQUFMLENBQVNoRSxFQUFFOEMsSUFBRixDQUFPakMsTUFBaEIsRUFBd0JkLEVBQUUrQyxJQUFGLENBQU9qQyxNQUFQLEdBQWdCMkMsS0FBeEMsQ0FBWixDQUFoQjtBQUNBekQsUUFBRStDLElBQUYsR0FBUy9DLEVBQUUrQyxJQUFGLENBQU9lLFNBQVAsQ0FBaUIsQ0FBakIsRUFBb0JMLEtBQXBCLElBQTRCekQsRUFBRStDLElBQUYsQ0FBT2UsU0FBUCxDQUFpQkwsUUFBT1MsT0FBeEIsQ0FBckM7QUFDRDs7QUFFRCxRQUFJNUIsU0FBUyxPQUFULElBQW9CdEMsRUFBRWMsTUFBRixJQUFZLElBQXBDLEVBQTBDO0FBQ3hDLFVBQU0yQyxTQUFPeEQsRUFBRTZCLE1BQUYsR0FBVzlCLEVBQUU4QixNQUExQjtBQUNBLFVBQU1vQyxXQUFVRixLQUFLRyxHQUFMLENBQVMsQ0FBVCxFQUFZSCxLQUFLQyxHQUFMLENBQVNoRSxFQUFFOEMsSUFBRixDQUFPakMsTUFBaEIsRUFBd0JkLEVBQUVjLE1BQUYsR0FBVzJDLE1BQW5DLENBQVosQ0FBaEI7QUFDQSxVQUFNTixXQUFTYSxLQUFLQyxHQUFMLENBQVNqRSxFQUFFYyxNQUFYLEVBQW1Cb0QsUUFBbkIsQ0FBZjtBQUNBbEUsUUFBRWMsTUFBRixJQUFZcUMsUUFBWjtBQUNEOztBQUVELFFBQUluRCxFQUFFK0MsSUFBRixLQUFXLEVBQVgsSUFBaUIvQyxFQUFFYyxNQUFGLEtBQWEsQ0FBbEMsRUFBcUM7QUFDbkMsYUFBTyxJQUFQO0FBQ0Q7QUFDRjs7QUFFRDs7OztBQUlBLE1BQUliLEVBQUUrQyxJQUFGLElBQVUsVUFBZCxFQUEwQjtBQUN4QixRQUNHaEQsRUFBRWdELElBQUYsSUFBVSxhQUFYLElBQ0NkLFNBQVMsT0FEVixJQUVDSSxTQUFTLElBRlYsSUFHQ2pCLEtBQUssTUFITixJQUlDLENBQUMsdUJBQVFyQixFQUFFb0UsSUFBVixFQUFnQm5FLEVBQUVtRSxJQUFsQixDQUxKLEVBTUU7QUFDQTtBQUNEOztBQUVELFFBQUk5QixTQUFTLFFBQVQsSUFBcUJ0QyxFQUFFOEIsTUFBRixHQUFXN0IsRUFBRTZCLE1BQUYsR0FBVzdCLEVBQUVhLE1BQWpELEVBQXlEO0FBQ3ZELFVBQU11RCxPQUFPTCxLQUFLQyxHQUFMLENBQVNoRSxFQUFFYSxNQUFYLEVBQW1CZCxFQUFFOEIsTUFBRixHQUFXN0IsRUFBRTZCLE1BQWhDLENBQWI7QUFDQSxVQUFNd0MsT0FBT04sS0FBS0MsR0FBTCxDQUFTakUsRUFBRWMsTUFBWCxFQUFtQmIsRUFBRWEsTUFBRixHQUFXdUQsSUFBOUIsQ0FBYjtBQUNBckUsUUFBRThCLE1BQUYsSUFBWXVDLElBQVo7QUFDQXJFLFFBQUVjLE1BQUYsSUFBWXdELElBQVo7QUFDRDs7QUFFRCxRQUFJaEMsU0FBUyxPQUFiLEVBQXNCO0FBQ3BCLFVBQU1nQyxRQUFPTixLQUFLQyxHQUFMLENBQVNqRSxFQUFFYyxNQUFYLEVBQW1CYixFQUFFYSxNQUFyQixDQUFiO0FBQ0FkLFFBQUU4QixNQUFGLElBQVl3QyxLQUFaO0FBQ0F0RSxRQUFFYyxNQUFGLElBQVl3RCxLQUFaO0FBQ0Q7O0FBRUQsUUFBSWhDLFNBQVMsT0FBVCxJQUFvQkMsV0FBVyxRQUFuQyxFQUE2QztBQUMzQyxVQUFNZ0MsU0FBU3RFLEVBQUU2QixNQUFGLEdBQVc5QixFQUFFOEIsTUFBNUI7QUFDQSxVQUFNMEMsUUFBUXhFLEVBQUVjLE1BQUYsR0FBV3lELE1BQVgsR0FBb0J0RSxFQUFFYSxNQUFwQztBQUNBLFVBQU0yRCxTQUFTekUsRUFBRWMsTUFBRixHQUFXeUQsTUFBWCxHQUFvQkMsS0FBbkM7QUFDQSxVQUFNWixxQkFBVzVELENBQVgsSUFBY3VCLE1BQU12QixFQUFFdUIsSUFBRixDQUFPMEIsS0FBUCxFQUFwQixFQUFvQ25DLFFBQVF5RCxNQUE1QyxHQUFOO0FBQ0EsVUFBTVYscUJBQVc3RCxDQUFYLElBQWN1QixNQUFNdkIsRUFBRXVCLElBQUYsQ0FBTzBCLEtBQVAsRUFBcEIsRUFBb0NuQixRQUFROUIsRUFBRThCLE1BQUYsR0FBV3lDLE1BQVgsR0FBb0JFLE1BQWhFLEVBQXdFM0QsUUFBUTBELEtBQWhGLEdBQU47QUFDQSxhQUFPLENBQUNaLEtBQUQsRUFBTUMsS0FBTixDQUFQO0FBQ0Q7O0FBRUQsUUFBSXZCLFNBQVMsT0FBYixFQUFzQjtBQUNwQixVQUFNbUIsU0FBT3hELEVBQUU2QixNQUFGLEdBQVc5QixFQUFFOEIsTUFBMUI7QUFDQSxVQUFNb0MsWUFBVUYsS0FBS0csR0FBTCxDQUFTLENBQVQsRUFBWUgsS0FBS0MsR0FBTCxDQUFTaEUsRUFBRWEsTUFBWCxFQUFtQmQsRUFBRWMsTUFBRixHQUFXMkMsTUFBOUIsQ0FBWixDQUFoQjtBQUNBLFVBQU1OLFdBQVNhLEtBQUtDLEdBQUwsQ0FBU2pFLEVBQUVjLE1BQVgsRUFBbUJvRCxTQUFuQixDQUFmO0FBQ0FsRSxRQUFFYyxNQUFGLElBQVlxQyxRQUFaO0FBQ0Q7O0FBRUQsUUFBSW5ELEVBQUVjLE1BQUYsS0FBYSxDQUFqQixFQUFvQjtBQUNsQixhQUFPLElBQVA7QUFDRDtBQUNGOztBQUVEOzs7O0FBSUEsTUFBSWIsRUFBRStDLElBQUYsSUFBVSxhQUFkLEVBQTZCO0FBQzNCLFFBQ0dkLFNBQVMsT0FBVixJQUNDSSxTQUFTLElBRFYsSUFFQ3RDLEVBQUVnRCxJQUFGLElBQVUsVUFBVixJQUF3QmhELEVBQUVnRCxJQUFGLElBQVUsVUFGbkMsSUFHQ2hELEVBQUVnRCxJQUFGLElBQVUsVUFBVixJQUF3QjNCLEtBQUssTUFIOUIsSUFJQyxDQUFDLHVCQUFRckIsRUFBRW9FLElBQVYsRUFBZ0JuRSxFQUFFbUUsSUFBbEIsQ0FMSixFQU1FO0FBQ0E7QUFDRDs7QUFFRCxRQUFJOUIsU0FBUyxRQUFULElBQXFCdEMsRUFBRThCLE1BQUYsR0FBVzdCLEVBQUU2QixNQUFGLEdBQVc3QixFQUFFYSxNQUFqRCxFQUF5RDtBQUN2RCxVQUFNdUQsUUFBT0wsS0FBS0MsR0FBTCxDQUFTaEUsRUFBRWEsTUFBWCxFQUFtQmQsRUFBRThCLE1BQUYsR0FBVzdCLEVBQUU2QixNQUFoQyxDQUFiO0FBQ0EsVUFBTXdDLFNBQU9OLEtBQUtDLEdBQUwsQ0FBU2pFLEVBQUVjLE1BQVgsRUFBbUJiLEVBQUVhLE1BQUYsR0FBV3VELEtBQTlCLENBQWI7QUFDQXJFLFFBQUU4QixNQUFGLElBQVl1QyxLQUFaO0FBQ0FyRSxRQUFFYyxNQUFGLElBQVl3RCxNQUFaO0FBQ0Q7O0FBRUQsUUFBSWhDLFNBQVMsT0FBYixFQUFzQjtBQUNwQixVQUFNZ0MsU0FBT04sS0FBS0MsR0FBTCxDQUFTakUsRUFBRWMsTUFBWCxFQUFtQmIsRUFBRWEsTUFBckIsQ0FBYjtBQUNBZCxRQUFFOEIsTUFBRixJQUFZd0MsTUFBWjtBQUNBdEUsUUFBRWMsTUFBRixJQUFZd0QsTUFBWjtBQUNEOztBQUVELFFBQUloQyxTQUFTLE9BQVQsSUFBb0JDLFdBQVcsUUFBbkMsRUFBNkM7QUFDM0MsVUFBTWdDLFVBQVN0RSxFQUFFNkIsTUFBRixHQUFXOUIsRUFBRThCLE1BQTVCO0FBQ0EsVUFBTTBDLFNBQVF4RSxFQUFFYyxNQUFGLEdBQVd5RCxPQUFYLEdBQW9CdEUsRUFBRWEsTUFBcEM7QUFDQSxVQUFNMkQsVUFBU3pFLEVBQUVjLE1BQUYsR0FBV3lELE9BQVgsR0FBb0JDLE1BQW5DO0FBQ0EsVUFBTVoscUJBQVc1RCxDQUFYLElBQWN1QixNQUFNdkIsRUFBRXVCLElBQUYsQ0FBTzBCLEtBQVAsRUFBcEIsRUFBb0NuQyxRQUFReUQsT0FBNUMsR0FBTjtBQUNBLFVBQU1WLHFCQUFXN0QsQ0FBWCxJQUFjdUIsTUFBTXZCLEVBQUV1QixJQUFGLENBQU8wQixLQUFQLEVBQXBCLEVBQW9DbkIsUUFBUTlCLEVBQUU4QixNQUFGLEdBQVd5QyxPQUFYLEdBQW9CRSxPQUFoRSxFQUF3RTNELFFBQVEwRCxNQUFoRixHQUFOO0FBQ0EsYUFBTyxDQUFDWixLQUFELEVBQU1DLEtBQU4sQ0FBUDtBQUNEOztBQUVELFFBQUl2QixTQUFTLE9BQWIsRUFBc0I7QUFDcEIsVUFBTW1CLFNBQU94RCxFQUFFNkIsTUFBRixHQUFXOUIsRUFBRThCLE1BQTFCO0FBQ0EsVUFBTW9DLFlBQVVGLEtBQUtHLEdBQUwsQ0FBUyxDQUFULEVBQVlILEtBQUtDLEdBQUwsQ0FBU2hFLEVBQUVhLE1BQVgsRUFBbUJkLEVBQUVjLE1BQUYsR0FBVzJDLE1BQTlCLENBQVosQ0FBaEI7QUFDQSxVQUFNTixZQUFTYSxLQUFLQyxHQUFMLENBQVNqRSxFQUFFYyxNQUFYLEVBQW1Cb0QsU0FBbkIsQ0FBZjtBQUNBbEUsUUFBRWMsTUFBRixJQUFZcUMsU0FBWjtBQUNEOztBQUVELFFBQUluRCxFQUFFYyxNQUFGLEtBQWEsQ0FBakIsRUFBb0I7QUFDbEIsYUFBTyxJQUFQO0FBQ0Q7QUFDRjs7QUFFRDs7OztBQUlBLE1BQUliLEVBQUUrQyxJQUFGLElBQVUsVUFBZCxFQUEwQjtBQUN4QixRQUNFZCxTQUFTLE9BQVQsSUFDQUksU0FBUyxJQURULElBRUEsQ0FBQyx1QkFBUXRDLEVBQUVvRSxJQUFWLEVBQWdCbkUsRUFBRW1FLElBQWxCLENBSEgsRUFJRTtBQUNBO0FBQ0Q7O0FBRUQsUUFBSXBFLEVBQUVnRCxJQUFGLElBQVUsVUFBVixJQUF3QjNCLEtBQUssTUFBakMsRUFBeUM7QUFDdkMsVUFBTXFELFdBQVkzQyxNQUFNQyxFQUFOLElBQVlDLE1BQU1KLEVBQXBDO0FBQ0EsVUFBSSxDQUFDNkMsUUFBTCxFQUFlOztBQUVmLFVBQU1DLElBQUlYLEtBQUtHLEdBQUwsQ0FBU3RDLEVBQVQsRUFBYUcsRUFBYixDQUFWO0FBQ0EsVUFBTTRDLElBQUlaLEtBQUtDLEdBQUwsQ0FBU2xDLEVBQVQsRUFBYUUsRUFBYixDQUFWO0FBQ0EsVUFBTUgsU0FBUzZDLENBQWY7QUFDQSxVQUFNN0QsU0FBUzhELElBQUlELENBQW5CO0FBQ0EsVUFBSTdELFVBQVUsQ0FBZCxFQUFpQjs7QUFFakI7QUFDQSxVQUFNc0Qsb0JBQVkseUJBQU1uRSxFQUFFbUUsSUFBUixDQUFaLEVBQThCLHlCQUFNbkUsRUFBRXNELFVBQVIsQ0FBOUIsQ0FBTjtBQUNBLFVBQU1BLGFBQWEsb0JBQUt0RCxFQUFFbUUsSUFBUCxFQUFhZixPQUFPQyxJQUFQLENBQVlyRCxFQUFFc0QsVUFBZCxDQUFiLENBQW5CO0FBQ0EsVUFBTUcseUJBQWV6RCxDQUFmLElBQWtCc0IsTUFBTXRCLEVBQUVzQixJQUFGLENBQU8wQixLQUFQLEVBQXhCLEVBQXdDbkIsY0FBeEMsRUFBZ0RoQixjQUFoRCxFQUF3RHNELFVBQXhELEVBQThEYixzQkFBOUQsR0FBTjtBQUNBLGFBQU8sQ0FBQ0csU0FBRCxFQUFVMUQsQ0FBVixDQUFQO0FBQ0Q7QUFDRjtBQUNGOztBQUVEOzs7Ozs7a0JBTWVELFMiLCJmaWxlIjoidHJhbnNmb3JtLmpzIiwic291cmNlc0NvbnRlbnQiOlsiXG5pbXBvcnQgRGVidWcgZnJvbSAnZGVidWcnXG5pbXBvcnQgY2xvbmUgZnJvbSAnbG9kYXNoL2Nsb25lRGVlcCdcbmltcG9ydCBkaWZmZXJlbmNlIGZyb20gJ2xvZGFzaC9kaWZmZXJlbmNlJ1xuaW1wb3J0IGludmVydCBmcm9tICcuLi9vcGVyYXRpb25zL2ludmVydCdcbmltcG9ydCBpc0VxdWFsIGZyb20gJ2xvZGFzaC9pc0VxdWFsJ1xuaW1wb3J0IHBpY2sgZnJvbSAnbG9kYXNoL3BpY2snXG5cbi8qKlxuICogRGVidWcuXG4gKlxuICogQHR5cGUge0RlYnVnfVxuICovXG5cbmNvbnN0IGRlYnVnID0gRGVidWcoJ3NsYXRlOm90OnRyYW5zZm9ybScpXG5cbi8qKlxuICogVHJhbnNmb3JtIG9wZXJhdGlvbnMgYGFgIGJ5IG9wZXJhdGlvbnMgYGJgIHdpdGggYHByaW9yaXR5YC5cbiAqXG4gKiBAcGFyYW0ge0FycmF5fSBhXG4gKiBAcGFyYW0ge0FycmF5fSBiXG4gKiBAcGFyYW0ge1N0cmluZ30gcHJpb3JpdHlcbiAqL1xuXG5mdW5jdGlvbiB0cmFuc2Zvcm0oYSwgYiwgcHJpb3JpdHkpIHtcbiAgLy8gRGVlcGx5IGNsb25lIEEgc28gdGhhdCB0aGUgdHJhbnNmb3JtZXJzIGNhbiBlZGl0IGluIHBsYWNlIHdpdGhvdXQgd29ycnkuXG4gIGEgPSBjbG9uZShhKVxuXG4gIGZvciAoY29uc3QgYm8gb2YgYikge1xuICAgIC8vIENyZWF0ZSBhIHRlbXBvcmFyeSBhcnJheSB0byBzdG9yZSB0aGUgbmV3IEEgb3BlcmF0aW9ucywgYmVjYXVzZSBzb21ldGltZXNcbiAgICAvLyB0aGUgdHJhbnNmb3JtZXJzIG5lZWQgdG8gcmV0dXJuIGBudWxsYCB0byByZW1vdmUgYW4gb3BlcmF0aW9uLCBvciByZXR1cm5cbiAgICAvLyBhbiBhcnJheSB0byBhZGQgb3BlcmF0aW9ucy5cbiAgICBsZXQgdG1wID0gW11cblxuICAgIGZvciAoY29uc3QgYW8gb2YgYSkge1xuICAgICAgZGVidWcoJ2EnLCB7IC4uLmFvIH0pXG4gICAgICBkZWJ1ZygnYicsIHsgLi4uYm8gfSlcblxuICAgICAgY29uc3QgcmV0ID0gdChhbywgYm8sIHByaW9yaXR5KVxuXG4gICAgICBpZiAocmV0ID09PSBudWxsKSB7XG4gICAgICAgIGRlYnVnKCdhXFwnJywgeyAuLi5hbyB9KVxuICAgICAgICBjb250aW51ZVxuICAgICAgfSBlbHNlIGlmIChBcnJheS5pc0FycmF5KHJldCkpIHtcbiAgICAgICAgZGVidWcoJ2FcXCcnLCBbIC4uLnJldCBdKVxuICAgICAgICB0bXAgPSB0bXAuY29uY2F0KHJldClcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIGRlYnVnKCdhXFwnJywgeyAuLi5yZXQgfSlcbiAgICAgICAgdG1wLnB1c2goYW8pXG4gICAgICB9XG4gICAgfVxuXG4gICAgLy8gVXBkYXRlIEEgZm9yIHRoZSBuZXh0IGl0ZXJhdGlvbi5cbiAgICBhID0gdG1wXG4gIH1cblxuICAvLyBSZXR1cm4gdGhlIHRyYW5zZm9ybWVkIEEgb3BlcmF0aW9ucy5cbiAgcmV0dXJuIGFcbn1cblxuLyoqXG4gKiBGaW5kIHRoZSBwbGFjZSB3aGVyZSBwYXRoIGBhYCBpcyBpbnRlcnNlY3RlZCBieSBwYXRoIGBiYCwgaWYgaXQgZG9lcy5cbiAqXG4gKiBAcGFyYW0ge0FycmF5fSBhXG4gKiBAcGFyYW0ge0FycmF5fSBiXG4gKiBAcmV0dXJuIHtTdHJpbmd8TnVsbH1cbiAqL1xuXG5mdW5jdGlvbiBpbnRlcnNlY3RQYXRoKGEsIGIpIHtcbiAgaWYgKGEgPT0gbnVsbCB8fCBiID09IG51bGwpIHJldHVybiBudWxsXG5cbiAgZm9yIChsZXQgaSA9IDA7IGkgPCBiLmxlbmd0aDsgaSsrKSB7XG4gICAgY29uc3QgYnYgPSBiW2ldXG4gICAgY29uc3QgYXYgPSBhW2ldXG4gICAgY29uc3QgYWwgPSBpICsgMSA9PT0gYS5sZW5ndGhcbiAgICBjb25zdCBibCA9IGkgKyAxID09PSBiLmxlbmd0aFxuICAgIC8vIElmIGV2ZXIgYGJ2ID4gYXZgLCBgYmAgZG9lc24ndCBpbnRlcnNlY3QgYGFgLlxuICAgIGlmIChidiA+IGF2KSByZXR1cm4gJ25vbmUnXG4gICAgLy8gSWYgd2UncmUgbm90IGF0IHRoZSBlbmQgb2YgYGJgLCBhbmQgYGJ2IDwgYXZgLCBgYmAgZG9lc24ndCBpbnRlcnNlY3QgYGFgLlxuICAgIGlmICghYmwgJiYgYnYgPCBhdikgcmV0dXJuICdub25lJ1xuICAgIC8vIElmIGF0IHRoZSBlbmQgb2YgYm90aCwgYW5kIGBidiA9PSBhdmAsIGBiYCBpcyBleGFjdGx5IGVxdWFsIHRvIGBhYC5cbiAgICBpZiAoYWwgJiYgYmwgJiYgYnYgPT0gYXYpIHJldHVybiAnZXhhY3QnXG4gICAgLy8gSWYgb25seSBhdCB0aGUgZW5kIG9mIGBiYCwgYW5kIGBidiA9PSBhdmAsIGBiYCBpcyBhYm92ZSBgYWAuXG4gICAgaWYgKGJsICYmIGJ2ID09IGF2KSByZXR1cm4gJ2Fib3ZlJ1xuICAgIC8vIElmIGF0IHRoZSBlbmQgb2YgYGJgLCBhbmQgYGJ2IDwgYXZgLCBgYmAgaXMgYmVmb3JlIGBhYC5cbiAgICBpZiAoYmwgJiYgYnYgPCBhdikgcmV0dXJuICdiZWZvcmUnXG4gIH1cblxuICByZXR1cm4gJ25vbmUnXG59XG5cbi8qKlxuICogRmluZCB0aGUgcGxhY2Ugd2hlcmUgYGFgJ3Mgb2Zmc2V0IGBhYCBpcyBpbnRlcnNlY3RlZCBieSBvcGVyYXRpb24gQidzXG4gKiBvZmZzZXQgYGJgLCBpZiBpdCBkb2VzIGF0IGFsbC5cbiAqXG4gKiBAcGFyYW0ge051bWJlcn0gYVxuICogQHBhcmFtIHtOdW1iZXJ9IGJcbiAqIEByZXR1cm4ge1N0cmluZ31cbiAqL1xuXG5mdW5jdGlvbiBpbnRlcnNlY3RPZmZzZXQoYSwgYikge1xuICBpZiAoYSA9PSBudWxsIHx8IGIgPT0gbnVsbCB8fCBpc05hTihhKSB8fCBpc05hTihiKSkgcmV0dXJuIG51bGxcbiAgaWYgKGIgPCBhKSByZXR1cm4gJ2JlZm9yZSdcbiAgaWYgKGIgPT0gYSkgcmV0dXJuICdleGFjdCdcbiAgcmV0dXJuICdhZnRlcidcbn1cblxuLyoqXG4gKiBUcmFuc2Zvcm0gb3BlcmF0aW9uIGBhYCBpbiBwbGFjZSBiYXNlZCBvbiBvcGVyYXRpb24gYGJgIGFuZCBwcmlvcml0eSBgcGAuXG4gKlxuICogQHBhcmFtIHtPYmplY3R9IGFcbiAqIEBwYXJhbSB7T2JqZWN0fSBiXG4gKiBAcGFyYW0ge1N0cmluZ30gcFxuICovXG5cbmZ1bmN0aW9uIHQoYSwgYiwgcCkge1xuICBjb25zdCBhaSA9IGEucGF0aC5sZW5ndGggLSAxXG4gIGNvbnN0IGJpID0gYi5wYXRoLmxlbmd0aCAtIDFcbiAgY29uc3QgYXYgPSBhLnBhdGhbYWldXG4gIGNvbnN0IGJ2ID0gYi5wYXRoW2JpXVxuICBjb25zdCBhbmkgPSBhLm5ld1BhdGggPyBhLm5ld1BhdGgubGVuZ3RoIC0gMSA6IG51bGxcbiAgY29uc3QgYm5pID0gYi5uZXdQYXRoID8gYi5uZXdQYXRoLmxlbmd0aCAtIDEgOiBudWxsXG4gIGNvbnN0IGJudiA9IGIubmV3UGF0aCA/IGIubmV3UGF0aFtibmldIDogbnVsbFxuXG4gIGNvbnN0IGF4ID0gJ29mZnNldCcgaW4gYSA/IGEub2Zmc2V0IDogbnVsbFxuICBjb25zdCBheSA9ICdvZmZzZXQnIGluIGEgJiYgJ2xlbmd0aCcgaW4gYSA/IGEub2Zmc2V0ICsgYS5sZW5ndGggOiBudWxsXG4gIGNvbnN0IGJ4ID0gJ29mZnNldCcgaW4gYiA/IGIub2Zmc2V0IDogbnVsbFxuICBjb25zdCBieSA9ICdvZmZzZXQnIGluIGIgJiYgJ2xlbmd0aCcgaW4gYiA/IGIub2Zmc2V0ICsgYi5sZW5ndGggOiBudWxsXG5cbiAgY29uc3QgYXBfYnAgPSBpbnRlcnNlY3RQYXRoKGEucGF0aCwgYi5wYXRoKVxuICBjb25zdCBhcF9ibnAgPSBpbnRlcnNlY3RQYXRoKGEucGF0aCwgYi5uZXdQYXRoKVxuICBjb25zdCBhbnBfYnAgPSBpbnRlcnNlY3RQYXRoKGEubmV3UGF0aCwgYi5wYXRoKVxuICBjb25zdCBhbnBfYm5wID0gaW50ZXJzZWN0UGF0aChhLm5ld1BhdGgsIGIubmV3UGF0aClcblxuICAvLyBUT0RPOiBnZXQgcmlkIG9mIHRoZXNlIEkgdGhpbmssIGVhc3kgdG8gY2FsY3VsYXRlP1xuICBjb25zdCBhb19ibyA9IGludGVyc2VjdE9mZnNldChhLm9mZnNldCwgYi5vZmZzZXQpXG4gIGNvbnN0IGFvbF9ib2wgPSBpbnRlcnNlY3RPZmZzZXQoYS5vZmZzZXQgKyBhLmxlbmd0aCwgYi5vZmZzZXQgKyBiLmxlbmd0aClcblxuICAvLyBUT0RPOiBvciBnZXQgcmlkIG9mIHRoZXNlIGJlY2F1c2UgdGhleSdyZSB2ZXJ5IGhhcmQgdG8gcmVtZW1iZXJcbiAgY29uc3QgYWJpX2JwbyA9IGludGVyc2VjdE9mZnNldChhLnBhdGhbYmkgKyAxXSwgYi5wb3NpdGlvbilcbiAgY29uc3QgYW9fYnBvID0gaW50ZXJzZWN0T2Zmc2V0KGEub2Zmc2V0LCBiLnBvc2l0aW9uKVxuICBjb25zdCBhcG9fYm8gPSBpbnRlcnNlY3RPZmZzZXQoYS5wb3NpdGlvbiwgYi5vZmZzZXQpXG4gIGNvbnN0IGFwb19icG8gPSBpbnRlcnNlY3RPZmZzZXQoYS5wb3NpdGlvbiwgYi5wb3NpdGlvbilcbiAgY29uc3QgYW9sX2JwbyA9IGludGVyc2VjdE9mZnNldChhLm9mZnNldCArIGEubGVuZ3RoLCBiLnBvc2l0aW9uKVxuICBjb25zdCBhb3RfYnBvID0gJ3RleHQnIGluIGEgPyBpbnRlcnNlY3RPZmZzZXQoYS5vZmZzZXQgKyBhLnRleHQubGVuZ3RoLCBiLnBvc2l0aW9uKSA6IG51bGxcblxuICAvKipcbiAgICogSW5zZXJ0IG5vZGUuXG4gICAqL1xuXG4gIGlmIChiLnR5cGUgPT0gJ2luc2VydF9ub2RlJykge1xuICAgIGlmIChhcF9icCA9PSAnZXhhY3QnICYmIGEudHlwZSA9PSAnbWVyZ2Vfbm9kZScpIHtcbiAgICAgIGNvbnN0IG5ld1BhdGggPSBhLnBhdGguc2xpY2UoMCwgYWkpLmNvbmNhdChbYS5wYXRoW2FpXSArIDFdKVxuICAgICAgY29uc3QgbW92ZSA9IHsgdHlwZTogJ21vdmVfbm9kZScsIHBhdGg6IGEucGF0aC5zbGljZSgpLCBuZXdQYXRoIH1cbiAgICAgIHJldHVybiBbbW92ZSwgYV1cbiAgICB9XG5cbiAgICBpZiAoXG4gICAgICAoYXBfYnAgPT0gJ2JlZm9yZScpIHx8XG4gICAgICAoYXBfYnAgPT0gJ2Fib3ZlJykgfHxcbiAgICAgIChhcF9icCA9PSAnZXhhY3QnICYmIGEudHlwZSAhPSAnaW5zZXJ0X25vZGUnKSB8fFxuICAgICAgKGFwX2JwID09ICdleGFjdCcgJiYgYS50eXBlID09ICdpbnNlcnRfbm9kZScgJiYgcCA9PSAncmlnaHQnKVxuICAgICkge1xuICAgICAgYS5wYXRoW2JpXSsrXG4gICAgfVxuXG4gICAgaWYgKFxuICAgICAgKGFucF9icCA9PSAnYmVmb3JlJykgfHxcbiAgICAgIChhbnBfYnAgPT0gJ2Fib3ZlJykgfHxcbiAgICAgIChhbnBfYnAgPT0gJ2V4YWN0JyAmJiBwID09ICdyaWdodCcpXG4gICAgKSB7XG4gICAgICBhLm5ld1BhdGhbYmldKytcbiAgICB9XG4gIH1cblxuICAvKipcbiAgICogUmVtb3ZlIG5vZGUuXG4gICAqL1xuXG4gIGlmIChiLnR5cGUgPT0gJ3JlbW92ZV9ub2RlJykge1xuICAgIGlmIChhcF9icCA9PSAnYmVmb3JlJyAmJiBhLnR5cGUgPT0gJ21lcmdlX25vZGUnICYmIGF2ID09IGJ2ICsgMSkge1xuICAgICAgLy8gVE9ETzogcmVtb3ZlIGBub2RlOm51bGxgXG4gICAgICBjb25zdCByZW1vdmUgPSB7IHR5cGU6ICdyZW1vdmVfbm9kZScsIHBhdGg6IGEucGF0aC5zbGljZSgpIH1cbiAgICAgIHJldHVybiBbcmVtb3ZlXVxuICAgIH1cblxuICAgIGlmIChhcF9icCA9PSAnYmVmb3JlJykge1xuICAgICAgYS5wYXRoW2JpXS0tXG4gICAgfVxuXG4gICAgaWYgKGFucF9icCA9PSAnYmVmb3JlJykge1xuICAgICAgYS5uZXdQYXRoW2JpXS0tXG4gICAgfVxuXG4gICAgaWYgKGFwX2JwID09ICdleGFjdCcgJiYgYS50eXBlID09ICdtZXJnZV9ub2RlJykge1xuICAgICAgY29uc3QgcGF0aCA9IGEucGF0aC5zbGljZSgwLCBhaSkuY29uY2F0KFthLnBhdGhbYWldIC0gMV0pXG4gICAgICAvLyBUT0RPOiByZW1vdmUgYG5vZGU6bnVsbGBcbiAgICAgIGNvbnN0IHJlbW92ZSA9IHsgdHlwZTogJ3JlbW92ZV9ub2RlJywgcGF0aCB9XG4gICAgICByZXR1cm4gW3JlbW92ZV1cbiAgICB9XG5cbiAgICBpZiAoXG4gICAgICAoYXBfYnAgPT0gJ2Fib3ZlJykgfHxcbiAgICAgIChhcF9icCA9PSAnZXhhY3QnICYmIGEudHlwZSAhPSAnaW5zZXJ0X25vZGUnKVxuICAgICkge1xuICAgICAgcmV0dXJuIG51bGxcbiAgICB9XG4gIH1cblxuICAvKipcbiAgICogU2V0IG5vZGUuXG4gICAqL1xuXG4gIGlmIChiLnR5cGUgPT0gJ3NldF9ub2RlJykge1xuICAgIGlmIChhLnR5cGUgPT0gJ3NldF9ub2RlJyAmJiBhcF9icCA9PSAnZXhhY3QnICYmIHAgPT0gJ3JpZ2h0Jykge1xuICAgICAgY29uc3QgYWsgPSBPYmplY3Qua2V5cyhhLnByb3BlcnRpZXMpXG4gICAgICBjb25zdCBiayA9IE9iamVjdC5rZXlzKGIucHJvcGVydGllcylcbiAgICAgIGNvbnN0IGRpZmYgPSBkaWZmZXJlbmNlKGFrLCBiaylcbiAgICAgIGlmIChkaWZmLmxlbmd0aCA9PSAwKSByZXR1cm4gbnVsbFxuICAgICAgYS5wcm9wZXJ0aWVzID0gcGljayhhLnByb3BlcnRpZXMsIGRpZmYpXG4gICAgfVxuICB9XG5cbiAgLyoqXG4gICAqIE1vdmUgbm9kZS5cbiAgICovXG5cbiAgaWYgKGIudHlwZSA9PSAnbW92ZV9ub2RlJykge1xuICAgIGlmIChhcF9icCA9PSAnZXhhY3QnICYmIGEudHlwZSA9PSAnbW92ZV9ub2RlJyAmJiBwID09ICdyaWdodCcpIHtcbiAgICAgIHJldHVybiBudWxsXG4gICAgfVxuXG4gICAgaWYgKGFwX2JwID09ICdleGFjdCcgJiYgYS50eXBlID09ICdtZXJnZV9ub2RlJykge1xuICAgICAgY29uc3QgaW52ZXJzZSA9IGludmVydChiKVxuICAgICAgY29uc3QgcGF0aCA9IGIucGF0aC5zbGljZSgwLCBiaSkuY29uY2F0KFtiLnBhdGhbYmldIC0gMV0pXG4gICAgICBsZXQgbmV3UGF0aCA9IGIubmV3UGF0aC5zbGljZSgpXG5cbiAgICAgIGlmIChpbnRlcnNlY3RQYXRoKGIubmV3UGF0aCwgYS5wYXRoKSA9PSAnYmVmb3JlJykge1xuICAgICAgICBuZXdQYXRoID0gYi5uZXdQYXRoLnNsaWNlKDAsIGJuaSkuY29uY2F0KGIubmV3UGF0aFtibmldIC0gMSlcbiAgICAgIH1cblxuICAgICAgY29uc3QgbW92ZSA9IHsgLi4uYiwgcGF0aCwgbmV3UGF0aCB9XG4gICAgICByZXR1cm4gW2ludmVyc2UsIGEsIG1vdmVdXG4gICAgfVxuXG4gICAgaWYgKGFwX2JwID09ICdiZWZvcmUnICYmIGEudHlwZSA9PSAnbWVyZ2Vfbm9kZScgJiYgYXYgPT0gYnYgKyAxKSB7XG4gICAgICBjb25zdCBpbnZlcnNlID0gaW52ZXJ0KGIpXG4gICAgICBjb25zdCBwYXRoID0gYi5wYXRoLnNsaWNlKClcbiAgICAgIGxldCBuZXdQYXRoID0gYi5uZXdQYXRoLnNsaWNlKClcblxuICAgICAgaWYgKGludGVyc2VjdFBhdGgoYi5uZXdQYXRoLCBhLnBhdGgpID09ICdiZWZvcmUnKSB7XG4gICAgICAgIG5ld1BhdGggPSBiLm5ld1BhdGguc2xpY2UoMCwgYm5pKS5jb25jYXQoYi5uZXdQYXRoW2JuaV0gLSAxKVxuICAgICAgfVxuXG4gICAgICBjb25zdCBtb3ZlID0geyAuLi5iLCBwYXRoLCBuZXdQYXRoIH1cbiAgICAgIHJldHVybiBbaW52ZXJzZSwgYSwgbW92ZV1cbiAgICB9XG5cbiAgICBpZiAoXG4gICAgICAoYXBfYm5wID09ICdleGFjdCcgJiYgYXBfYnAgIT0gJ2JlZm9yZScgJiYgYS50eXBlID09ICdtZXJnZV9ub2RlJykgfHxcbiAgICAgIChhcF9icCA9PSAnYmVmb3JlJyAmJiBhLnR5cGUgPT0gJ21lcmdlX25vZGUnICYmIGFwX2JucCA9PSAnYmVmb3JlJyAmJiBhdiA9PSBibnYgKyAxKVxuICAgICkge1xuICAgICAgY29uc3QgaW52ZXJzZSA9IGludmVydChiKVxuICAgICAgcmV0dXJuIFtpbnZlcnNlLCBhLCBiXVxuICAgIH1cblxuICAgIGlmIChhcF9ibnAgPT0gJ2JlZm9yZScgJiYgYXBfYnAgIT0gJ2JlZm9yZScgJiYgYS50eXBlID09ICdtZXJnZV9ub2RlJyAmJiBhdiA9PSBidiArIDEpIHtcbiAgICAgIGNvbnN0IGludmVyc2UgPSBpbnZlcnQoYilcbiAgICAgIGNvbnN0IHBhdGggPSBiLnBhdGguc2xpY2UoKVxuICAgICAgY29uc3QgbmV3UGF0aCA9IGIubmV3UGF0aC5zbGljZSgwLCBibmkpLmNvbmNhdChiLm5ld1BhdGhbYm5pXSAtIDEpXG4gICAgICBjb25zdCBtb3ZlID0geyAuLi5iLCBwYXRoLCBuZXdQYXRoIH1cbiAgICAgIHJldHVybiBbaW52ZXJzZSwgYSwgbW92ZV1cbiAgICB9XG5cbiAgICBpZiAoYXBfYnAgPT0gJ2V4YWN0Jykge1xuICAgICAgYS5wYXRoID0gYi5uZXdQYXRoLnNsaWNlKClcbiAgICB9XG5cbiAgICBpZiAoYW5wX2JwID09ICdleGFjdCcpIHtcbiAgICAgIGEubmV3UGF0aCA9IGIubmV3UGF0aC5zbGljZSgpXG4gICAgfVxuXG4gICAgaWYgKGFwX2JwID09ICdhYm92ZScpIHtcbiAgICAgIGEucGF0aCA9IGIubmV3UGF0aC5jb25jYXQoYS5wYXRoLnNsaWNlKGIubmV3UGF0aC5sZW5ndGgpKVxuICAgIH1cblxuICAgIGlmIChhbnBfYnAgPT0gJ2Fib3ZlJykge1xuICAgICAgYS5uZXdQYXRoID0gYi5uZXdQYXRoLmNvbmNhdChhLm5ld1BhdGguc2xpY2UoYi5uZXdQYXRoLmxlbmd0aCkpXG4gICAgfVxuXG4gICAgaWYgKGFwX2JwID09ICdiZWZvcmUnICYmIGFwX2JucCAhPSAnYmVmb3JlJykge1xuICAgICAgYS5wYXRoW2JpXS0tXG4gICAgfVxuXG4gICAgaWYgKGFwX2JwICE9ICdleGFjdCcgJiYgYW5wX2JwID09ICdiZWZvcmUnICYmIGFucF9ibnAgIT0gJ2JlZm9yZScpIHtcbiAgICAgIGEubmV3UGF0aFtiaV0tLVxuICAgIH1cblxuICAgIGlmIChcbiAgICAgIChhcF9icCA9PSAnYmVmb3JlJyAmJiBhcF9ibnAgPT0gJ2Fib3ZlJykgfHxcbiAgICAgIChhcF9icCA9PSAnbm9uZScgJiYgYXBfYm5wICE9ICdub25lJylcbiAgICApIHtcbiAgICAgIGEucGF0aFtibmldKytcbiAgICB9XG5cbiAgICBpZiAoXG4gICAgICAoYXBfYnAgIT0gJ2V4YWN0JyAmJiBhbnBfYnAgPT0gJ2JlZm9yZScgJiYgYW5wX2JucCA9PSAnYWJvdmUnKSB8fFxuICAgICAgKGFwX2JwICE9ICdleGFjdCcgJiYgYW5wX2JwID09ICdub25lJyAmJiBhbnBfYm5wICE9ICdub25lJylcbiAgICApIHtcbiAgICAgIGEubmV3UGF0aFtibmldKytcbiAgICB9XG4gIH1cblxuICAvKipcbiAgICogTWVyZ2Ugbm9kZS5cbiAgICovXG5cbiAgaWYgKGIudHlwZSA9PSAnbWVyZ2Vfbm9kZScpIHtcbiAgICBpZiAoYXBfYnAgPT0gJ2JlZm9yZScpIHtcbiAgICAgIGEucGF0aFtiaV0tLVxuICAgIH1cblxuICAgIGlmIChhcF9icCA9PSAnYmVmb3JlJyAmJiBhLnR5cGUgPT0gJ21lcmdlX25vZGUnICYmIGF2ID09IGJ2ICsgMSkge1xuICAgICAgYS5wb3NpdGlvbiArPSBiLnBvc2l0aW9uXG4gICAgfVxuXG4gICAgaWYgKGFucF9icCA9PSAnYmVmb3JlJykge1xuICAgICAgYS5uZXdQYXRoW2JpXS0tXG4gICAgfVxuXG4gICAgaWYgKGFwX2JwID09ICdhYm92ZScpIHtcbiAgICAgIGEucGF0aFtiaV0tLVxuICAgICAgYS5wYXRoW2JpICsgMV0gKz0gYi5wb3NpdGlvblxuICAgIH1cblxuICAgIGlmIChhbnBfYnAgPT0gJ2Fib3ZlJykge1xuICAgICAgYS5uZXdQYXRoW2JpXS0tXG4gICAgICBhLm5ld1BhdGhbYmkgKyAxXSArPSBiLnBvc2l0aW9uXG4gICAgfVxuXG4gICAgaWYgKGFwX2JwID09ICdleGFjdCcgJiYgYS50eXBlID09ICdtZXJnZV9ub2RlJykge1xuICAgICAgcmV0dXJuIG51bGxcbiAgICB9XG5cbiAgICBpZiAoYXBfYnAgPT0gJ2V4YWN0Jykge1xuICAgICAgYS5wYXRoID0gYS5wYXRoLnNsaWNlKDAsIGFpKS5jb25jYXQoW2EucGF0aFthaV0gLSAxXSlcblxuICAgICAgaWYgKGEudHlwZSA9PSAnc3BsaXRfbm9kZScpIHtcbiAgICAgICAgYS5wb3NpdGlvbiArPSBiLnBvc2l0aW9uXG4gICAgICB9XG5cbiAgICAgIGlmIChhLm9mZnNldCAhPSBudWxsKSB7XG4gICAgICAgIGEub2Zmc2V0ICs9IGIucG9zaXRpb25cbiAgICAgIH1cbiAgICB9XG4gIH1cblxuICAvKipcbiAgICogU3BsaXQgbm9kZS5cbiAgICovXG5cbiAgaWYgKGIudHlwZSA9PSAnc3BsaXRfbm9kZScpIHtcbiAgICBpZiAoYXBfYnAgPT0gJ2Fib3ZlJyAmJiBhLnR5cGUgPT0gJ21lcmdlX25vZGUnICYmIGF2ID09IGIucG9zaXRpb24pIHtcbiAgICAgIGNvbnN0IHBhdGggPSBhLnBhdGguc2xpY2UoMCwgYmkpLmNvbmNhdChbYS5wYXRoW2JpXSArIDFdKS5jb25jYXQoWzBdKVxuICAgICAgY29uc3QgbmV3UGF0aCA9IGEucGF0aC5zbGljZSgpXG4gICAgICBjb25zdCBtb3ZlID0geyB0eXBlOiAnbW92ZV9ub2RlJywgcGF0aCwgbmV3UGF0aCB9XG4gICAgICByZXR1cm4gW21vdmUsIGFdXG4gICAgfVxuXG4gICAgaWYgKGFwX2JwID09ICdleGFjdCcgJiYgYS50eXBlID09ICdzcGxpdF9ub2RlJyAmJiBhcG9fYnBvID09ICdiZWZvcmUnKSB7XG4gICAgICBhLnBhdGhbYmldKytcbiAgICAgIGEucG9zaXRpb24gLT0gYi5wb3NpdGlvblxuICAgIH1cblxuICAgIGlmIChhcF9icCA9PSAnZXhhY3QnICYmIGEudHlwZSA9PSAncmVtb3ZlX25vZGUnKSB7XG4gICAgICAvLyBUT0RPOiByZW1vdmUgYG5vZGU6bnVsbGBcbiAgICAgIGNvbnN0IHJlbW92ZSA9IHsgLi4uYSwgcGF0aDogYS5wYXRoLnNsaWNlKCkgfVxuICAgICAgcmV0dXJuIFthLCByZW1vdmVdXG4gICAgfVxuXG4gICAgaWYgKGFwX2JwID09ICdiZWZvcmUnKSB7XG4gICAgICBhLnBhdGhbYmldKytcblxuICAgICAgaWYgKGEudHlwZSA9PSAnbWVyZ2Vfbm9kZScgJiYgYXYgPT0gYnYgKyAxKSB7XG4gICAgICAgIGEucG9zaXRpb24gLT0gYi5wb3NpdGlvblxuICAgICAgfVxuICAgIH1cblxuICAgIGlmIChhcF9icCAhPSAnZXhhY3QnICYmIGFucF9icCA9PSAnYmVmb3JlJykge1xuICAgICAgYS5uZXdQYXRoW2JpXSsrXG4gICAgfVxuXG4gICAgaWYgKGFwX2JwID09ICdhYm92ZScgJiYgYWJpX2JwbyAhPSAnYWZ0ZXInKSB7XG4gICAgICBhLnBhdGhbYmldKytcbiAgICAgIGEucGF0aFtiaSArIDFdIC09IGIucG9zaXRpb25cbiAgICB9XG5cbiAgICBpZiAoYW5wX2JwID09ICdhYm92ZScgJiYgYWJpX2JwbyAhPSAnYWZ0ZXInKSB7XG4gICAgICBhLm5ld1BhdGhbYmldKytcbiAgICAgIGEubmV3UGF0aFtiaSArIDFdIC09IGIucG9zaXRpb25cbiAgICB9XG5cbiAgICBpZiAoYXBfYnAgPT0gJ2V4YWN0JyAmJiBhLnR5cGUgPT0gJ21vdmVfbm9kZScpIHtcbiAgICAgIGNvbnN0IHBhdGggPSBhLnBhdGguc2xpY2UoMCwgYWkpLmNvbmNhdChbYS5wYXRoW2FpXSArIDFdKVxuICAgICAgY29uc3QgbmV3UGF0aCA9IGEubmV3UGF0aC5zbGljZSgpXG4gICAgICBjb25zdCBtb3ZlID0geyAuLi5hLCBwYXRoLCBuZXdQYXRoIH1cbiAgICAgIHJldHVybiBbbW92ZSwgYV1cbiAgICB9XG5cbiAgICBpZiAoXG4gICAgICAoYXBfYnAgPT0gJ2V4YWN0JykgJiZcbiAgICAgIChhLm9mZnNldCAhPSBudWxsKSAmJlxuICAgICAgKGFvX2JwbyA9PSAnYmVmb3JlJykgJiZcbiAgICAgIChhb2xfYnBvID09ICdiZWZvcmUnIHx8IGFvdF9icG8gPT0gJ2JlZm9yZScpXG4gICAgKSB7XG4gICAgICBhLnBhdGhbYmldKytcbiAgICAgIGEub2Zmc2V0IC09IGIucG9zaXRpb25cbiAgICB9XG5cbiAgICBpZiAoXG4gICAgICAoYXBfYnAgPT0gJ2V4YWN0JykgJiZcbiAgICAgIChhLm9mZnNldCAhPSBudWxsKSAmJlxuICAgICAgKGFvX2JwbyA9PSAnYWZ0ZXInKSAmJlxuICAgICAgKGFvbF9icG8gPT0gJ2JlZm9yZScgfHwgYW90X2JwbyA9PSAnYmVmb3JlJylcbiAgICApIHtcbiAgICAgIGNvbnN0IHR3b1BhdGggPSBhLnBhdGguc2xpY2UoMCwgYWkpLmNvbmNhdChbYS5wYXRoW2FpXSArIDFdKVxuICAgICAgY29uc3Qgb25lID0geyAuLi5hLCBwYXRoOiBhLnBhdGguc2xpY2UoKSB9XG4gICAgICBjb25zdCB0d28gPSB7IC4uLmEsIHBhdGg6IHR3b1BhdGgsIG9mZnNldDogMCB9XG5cbiAgICAgIGlmIChhLnRleHQgPT0gbnVsbCkge1xuICAgICAgICBvbmUubGVuZ3RoID0gYi5wb3NpdGlvbiAtIGEub2Zmc2V0XG4gICAgICAgIHR3by5sZW5ndGggPSBhLmxlbmd0aCAtIG9uZS5sZW5ndGhcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIG9uZS50ZXh0ID0gYS50ZXh0LnNsaWNlKDAsIGIucG9zaXRpb24gLSBhLm9mZnNldClcbiAgICAgICAgdHdvLnRleHQgPSBhLnRleHQuc2xpY2Uob25lLnRleHQubGVuZ3RoKVxuICAgICAgfVxuXG4gICAgICByZXR1cm4gW29uZSwgdHdvXVxuICAgIH1cbiAgfVxuXG4gIC8qKlxuICAgKiBJbnNlcnQgdGV4dC5cbiAgICovXG5cbiAgaWYgKGIudHlwZSA9PSAnaW5zZXJ0X3RleHQnKSB7XG4gICAgaWYgKGFwX2JwID09ICdiZWZvcmUnICYmIGEudHlwZSA9PSAnbWVyZ2Vfbm9kZScgJiYgYXYgPT0gYnYgKyAxKSB7XG4gICAgICBhLnBvc2l0aW9uICs9IGIudGV4dC5sZW5ndGhcbiAgICB9XG5cbiAgICBpZiAoYXBfYnAgPT0gJ2V4YWN0JyAmJiBhLnR5cGUgPT0gJ3NwbGl0X25vZGUnICYmIChhcG9fYm8gPT0gJ2JlZm9yZScgfHwgYXBvX2JvID09ICdleGFjdCcpKSB7XG4gICAgICBhLnBvc2l0aW9uICs9IGIudGV4dC5sZW5ndGhcbiAgICB9XG5cbiAgICBpZiAoYXBfYnAgIT0gJ2V4YWN0JyB8fCBhLm9mZnNldCA9PSBudWxsKSB7XG4gICAgICByZXR1cm5cbiAgICB9XG5cbiAgICBpZiAoYW9fYm8gPT0gJ2FmdGVyJyAmJiBhLnR5cGUgPT0gJ3JlbW92ZV90ZXh0JyAmJiBiLm9mZnNldCA8IGEub2Zmc2V0ICsgYS50ZXh0Lmxlbmd0aCkge1xuICAgICAgY29uc3Qgb25lID0geyAuLi5hLCBwYXRoOiBhLnBhdGguc2xpY2UoKSwgdGV4dDogYS50ZXh0LnN1YnN0cmluZygwLCBiLm9mZnNldCAtIGEub2Zmc2V0KSB9XG4gICAgICBjb25zdCB0d28gPSB7IC4uLmEsIHBhdGg6IGEucGF0aC5zbGljZSgpLCBvZmZzZXQ6IGIub2Zmc2V0ICsgYi50ZXh0Lmxlbmd0aCAtIG9uZS50ZXh0Lmxlbmd0aCwgdGV4dDogYS50ZXh0LnN1YnN0cmluZyhvbmUudGV4dC5sZW5ndGgpIH1cbiAgICAgIHJldHVybiBbb25lLCB0d29dXG4gICAgfVxuXG4gICAgaWYgKGFvX2JvID09ICdhZnRlcicgJiYgYS5sZW5ndGggIT0gbnVsbCAmJiBiLm9mZnNldCA8IGEub2Zmc2V0ICsgYS5sZW5ndGgpIHtcbiAgICAgIGNvbnN0IG9uZSA9IHsgLi4uYSwgcGF0aDogYS5wYXRoLnNsaWNlKCksIGxlbmd0aDogYi5vZmZzZXQgLSBhLm9mZnNldCB9XG4gICAgICBjb25zdCB0d28gPSB7IC4uLmEsIHBhdGg6IGEucGF0aC5zbGljZSgpLCBvZmZzZXQ6IGIub2Zmc2V0ICsgYi50ZXh0Lmxlbmd0aCwgbGVuZ3RoOiBhLmxlbmd0aCAtIG9uZS5sZW5ndGggfVxuICAgICAgcmV0dXJuIFtvbmUsIHR3b11cbiAgICB9XG5cbiAgICBpZiAoXG4gICAgICAoYW9fYm8gPT0gJ2JlZm9yZScpIHx8XG4gICAgICAoYW9fYm8gPT0gJ2V4YWN0JyAmJiBwID09ICdyaWdodCcpIHx8XG4gICAgICAoYW9fYm8gPT0gJ2V4YWN0JyAmJiBhLnR5cGUgIT0gJ2luc2VydF90ZXh0JylcbiAgICApIHtcbiAgICAgIGEub2Zmc2V0ICs9IGIudGV4dC5sZW5ndGhcbiAgICB9XG4gIH1cblxuICAvKipcbiAgICogUmVtb3ZlIHRleHQuXG4gICAqL1xuXG4gIGlmIChiLnR5cGUgPT0gJ3JlbW92ZV90ZXh0Jykge1xuICAgIGlmIChhcF9icCA9PSAnYmVmb3JlJyAmJiBhLnR5cGUgPT0gJ21lcmdlX25vZGUnICYmIGF2ID09IGJ2ICsgMSkge1xuICAgICAgYS5wb3NpdGlvbiAtPSBiLnRleHQubGVuZ3RoXG4gICAgfVxuXG4gICAgaWYgKGFwX2JwID09ICdleGFjdCcgJiYgYS50eXBlID09ICdzcGxpdF9ub2RlJyAmJiAoYXBvX2JvID09ICdiZWZvcmUnKSkge1xuICAgICAgY29uc3QgZGVjcmVtZW50ID0gTWF0aC5taW4oYS5wb3NpdGlvbiwgYi50ZXh0Lmxlbmd0aClcbiAgICAgIGEucG9zaXRpb24gLT0gZGVjcmVtZW50XG4gICAgfVxuXG4gICAgaWYgKGFwX2JwICE9ICdleGFjdCcgfHwgYS5vZmZzZXQgPT0gbnVsbCkge1xuICAgICAgcmV0dXJuXG4gICAgfVxuXG4gICAgaWYgKGFvX2JvID09ICdiZWZvcmUnICYmIGEudHlwZSA9PSAnaW5zZXJ0X3RleHQnKSB7XG4gICAgICBjb25zdCByZW1vdmUgPSBNYXRoLm1pbihhLm9mZnNldCAtIGIub2Zmc2V0LCBiLnRleHQubGVuZ3RoKVxuICAgICAgYS5vZmZzZXQgLT0gcmVtb3ZlXG4gICAgfVxuXG4gICAgaWYgKGFvX2JvID09ICdiZWZvcmUnICYmIGEudHlwZSA9PSAncmVtb3ZlX3RleHQnKSB7XG4gICAgICBjb25zdCBkZWNyZW1lbnQgPSBNYXRoLm1pbihiLnRleHQubGVuZ3RoLCBhLm9mZnNldCAtIGIub2Zmc2V0KVxuICAgICAgY29uc3QgcmVtb3ZlID0gTWF0aC5taW4oYS50ZXh0Lmxlbmd0aCwgYi50ZXh0Lmxlbmd0aCAtIGRlY3JlbWVudClcbiAgICAgIGEub2Zmc2V0IC09IGRlY3JlbWVudFxuICAgICAgYS50ZXh0ID0gYS50ZXh0LnN1YnN0cmluZyhyZW1vdmUpXG4gICAgfVxuXG4gICAgaWYgKGFvX2JvID09ICdiZWZvcmUnICYmIGEubGVuZ3RoICE9IG51bGwpIHtcbiAgICAgIGNvbnN0IGRlY3JlbWVudCA9IE1hdGgubWluKGIudGV4dC5sZW5ndGgsIGEub2Zmc2V0IC0gYi5vZmZzZXQpXG4gICAgICBjb25zdCByZW1vdmUgPSBNYXRoLm1pbihhLmxlbmd0aCwgYi50ZXh0Lmxlbmd0aCAtIGRlY3JlbWVudClcbiAgICAgIGEub2Zmc2V0IC09IGRlY3JlbWVudFxuICAgICAgYS5sZW5ndGggLT0gcmVtb3ZlXG4gICAgfVxuXG4gICAgaWYgKGFvX2JvID09ICdleGFjdCcgJiYgYS50eXBlID09ICdyZW1vdmVfdGV4dCcpIHtcbiAgICAgIGNvbnN0IHJlbW92ZSA9IE1hdGgubWluKGEudGV4dC5sZW5ndGgsIGIudGV4dC5sZW5ndGgpXG4gICAgICBhLnRleHQgPSBhLnRleHQuc3Vic3RyaW5nKHJlbW92ZSlcbiAgICB9XG5cbiAgICBpZiAoYW9fYm8gPT0gJ2V4YWN0JyAmJiBhLmxlbmd0aCAhPSBudWxsKSB7XG4gICAgICBjb25zdCByZW1vdmUgPSBNYXRoLm1pbihhLmxlbmd0aCwgYi50ZXh0Lmxlbmd0aClcbiAgICAgIGEubGVuZ3RoIC09IHJlbW92ZVxuICAgIH1cblxuICAgIGlmIChhb19ibyA9PSAnYWZ0ZXInICYmIGEudHlwZSA9PSAncmVtb3ZlX3RleHQnKSB7XG4gICAgICBjb25zdCBkaWZmID0gYi5vZmZzZXQgLSBhLm9mZnNldFxuICAgICAgY29uc3Qgb3ZlcmxhcCA9IE1hdGgubWF4KDAsIE1hdGgubWluKGIudGV4dC5sZW5ndGgsIGEudGV4dC5sZW5ndGggLSBkaWZmKSlcbiAgICAgIGEudGV4dCA9IGEudGV4dC5zdWJzdHJpbmcoMCwgZGlmZikgKyBhLnRleHQuc3Vic3RyaW5nKGRpZmYgKyBvdmVybGFwKVxuICAgIH1cblxuICAgIGlmIChhb19ibyA9PSAnYWZ0ZXInICYmIGEubGVuZ3RoICE9IG51bGwpIHtcbiAgICAgIGNvbnN0IGRpZmYgPSBiLm9mZnNldCAtIGEub2Zmc2V0XG4gICAgICBjb25zdCBvdmVybGFwID0gTWF0aC5tYXgoMCwgTWF0aC5taW4oYi50ZXh0Lmxlbmd0aCwgYS5sZW5ndGggLSBkaWZmKSlcbiAgICAgIGNvbnN0IHJlbW92ZSA9IE1hdGgubWluKGEubGVuZ3RoLCBvdmVybGFwKVxuICAgICAgYS5sZW5ndGggLT0gcmVtb3ZlXG4gICAgfVxuXG4gICAgaWYgKGEudGV4dCA9PT0gJycgfHwgYS5sZW5ndGggPT09IDApIHtcbiAgICAgIHJldHVybiBudWxsXG4gICAgfVxuICB9XG5cbiAgLyoqXG4gICAqIEFkZCBtYXJrLlxuICAgKi9cblxuICBpZiAoYi50eXBlID09ICdhZGRfbWFyaycpIHtcbiAgICBpZiAoXG4gICAgICAoYS50eXBlICE9ICdyZW1vdmVfbWFyaycpIHx8XG4gICAgICAoYXBfYnAgIT0gJ2V4YWN0JykgfHxcbiAgICAgIChhb19ibyA9PSBudWxsKSB8fFxuICAgICAgKHAgPT0gJ2xlZnQnKSB8fFxuICAgICAgKCFpc0VxdWFsKGEubWFyaywgYi5tYXJrKSlcbiAgICApIHtcbiAgICAgIHJldHVyblxuICAgIH1cblxuICAgIGlmIChhb19ibyA9PSAnYmVmb3JlJyAmJiBhLm9mZnNldCA8IGIub2Zmc2V0ICsgYi5sZW5ndGgpIHtcbiAgICAgIGNvbnN0IHNraXAgPSBNYXRoLm1pbihiLmxlbmd0aCwgYS5vZmZzZXQgLSBiLm9mZnNldClcbiAgICAgIGNvbnN0IG9taXQgPSBNYXRoLm1pbihhLmxlbmd0aCwgYi5sZW5ndGggLSBza2lwKVxuICAgICAgYS5vZmZzZXQgKz0gc2tpcFxuICAgICAgYS5sZW5ndGggLT0gb21pdFxuICAgIH1cblxuICAgIGlmIChhb19ibyA9PSAnZXhhY3QnKSB7XG4gICAgICBjb25zdCBvbWl0ID0gTWF0aC5taW4oYS5sZW5ndGgsIGIubGVuZ3RoKVxuICAgICAgYS5vZmZzZXQgKz0gb21pdFxuICAgICAgYS5sZW5ndGggLT0gb21pdFxuICAgIH1cblxuICAgIGlmIChhb19ibyA9PSAnYWZ0ZXInICYmIGFvbF9ib2wgPT0gJ2JlZm9yZScpIHtcbiAgICAgIGNvbnN0IGJlZm9yZSA9IGIub2Zmc2V0IC0gYS5vZmZzZXRcbiAgICAgIGNvbnN0IGFmdGVyID0gYS5sZW5ndGggLSBiZWZvcmUgLSBiLmxlbmd0aFxuICAgICAgY29uc3QgbWlkZGxlID0gYS5sZW5ndGggLSBiZWZvcmUgLSBhZnRlclxuICAgICAgY29uc3Qgb25lID0geyAuLi5hLCBwYXRoOiBhLnBhdGguc2xpY2UoKSwgbGVuZ3RoOiBiZWZvcmUgfVxuICAgICAgY29uc3QgdHdvID0geyAuLi5hLCBwYXRoOiBhLnBhdGguc2xpY2UoKSwgb2Zmc2V0OiBhLm9mZnNldCArIGJlZm9yZSArIG1pZGRsZSwgbGVuZ3RoOiBhZnRlciB9XG4gICAgICByZXR1cm4gW29uZSwgdHdvXVxuICAgIH1cblxuICAgIGlmIChhb19ibyA9PSAnYWZ0ZXInKSB7XG4gICAgICBjb25zdCBkaWZmID0gYi5vZmZzZXQgLSBhLm9mZnNldFxuICAgICAgY29uc3Qgb3ZlcmxhcCA9IE1hdGgubWF4KDAsIE1hdGgubWluKGIubGVuZ3RoLCBhLmxlbmd0aCAtIGRpZmYpKVxuICAgICAgY29uc3QgcmVtb3ZlID0gTWF0aC5taW4oYS5sZW5ndGgsIG92ZXJsYXApXG4gICAgICBhLmxlbmd0aCAtPSByZW1vdmVcbiAgICB9XG5cbiAgICBpZiAoYS5sZW5ndGggPT09IDApIHtcbiAgICAgIHJldHVybiBudWxsXG4gICAgfVxuICB9XG5cbiAgLyoqXG4gICAqIFJlbW92ZSBtYXJrLlxuICAgKi9cblxuICBpZiAoYi50eXBlID09ICdyZW1vdmVfbWFyaycpIHtcbiAgICBpZiAoXG4gICAgICAoYXBfYnAgIT0gJ2V4YWN0JykgfHxcbiAgICAgIChhb19ibyA9PSBudWxsKSB8fFxuICAgICAgKGEudHlwZSAhPSAnYWRkX21hcmsnICYmIGEudHlwZSAhPSAnc2V0X21hcmsnKSB8fFxuICAgICAgKGEudHlwZSA9PSAnYWRkX21hcmsnICYmIHAgPT0gJ2xlZnQnKSB8fFxuICAgICAgKCFpc0VxdWFsKGEubWFyaywgYi5tYXJrKSlcbiAgICApIHtcbiAgICAgIHJldHVyblxuICAgIH1cblxuICAgIGlmIChhb19ibyA9PSAnYmVmb3JlJyAmJiBhLm9mZnNldCA8IGIub2Zmc2V0ICsgYi5sZW5ndGgpIHtcbiAgICAgIGNvbnN0IHNraXAgPSBNYXRoLm1pbihiLmxlbmd0aCwgYS5vZmZzZXQgLSBiLm9mZnNldClcbiAgICAgIGNvbnN0IG9taXQgPSBNYXRoLm1pbihhLmxlbmd0aCwgYi5sZW5ndGggLSBza2lwKVxuICAgICAgYS5vZmZzZXQgKz0gc2tpcFxuICAgICAgYS5sZW5ndGggLT0gb21pdFxuICAgIH1cblxuICAgIGlmIChhb19ibyA9PSAnZXhhY3QnKSB7XG4gICAgICBjb25zdCBvbWl0ID0gTWF0aC5taW4oYS5sZW5ndGgsIGIubGVuZ3RoKVxuICAgICAgYS5vZmZzZXQgKz0gb21pdFxuICAgICAgYS5sZW5ndGggLT0gb21pdFxuICAgIH1cblxuICAgIGlmIChhb19ibyA9PSAnYWZ0ZXInICYmIGFvbF9ib2wgPT0gJ2JlZm9yZScpIHtcbiAgICAgIGNvbnN0IGJlZm9yZSA9IGIub2Zmc2V0IC0gYS5vZmZzZXRcbiAgICAgIGNvbnN0IGFmdGVyID0gYS5sZW5ndGggLSBiZWZvcmUgLSBiLmxlbmd0aFxuICAgICAgY29uc3QgbWlkZGxlID0gYS5sZW5ndGggLSBiZWZvcmUgLSBhZnRlclxuICAgICAgY29uc3Qgb25lID0geyAuLi5hLCBwYXRoOiBhLnBhdGguc2xpY2UoKSwgbGVuZ3RoOiBiZWZvcmUgfVxuICAgICAgY29uc3QgdHdvID0geyAuLi5hLCBwYXRoOiBhLnBhdGguc2xpY2UoKSwgb2Zmc2V0OiBhLm9mZnNldCArIGJlZm9yZSArIG1pZGRsZSwgbGVuZ3RoOiBhZnRlciB9XG4gICAgICByZXR1cm4gW29uZSwgdHdvXVxuICAgIH1cblxuICAgIGlmIChhb19ibyA9PSAnYWZ0ZXInKSB7XG4gICAgICBjb25zdCBkaWZmID0gYi5vZmZzZXQgLSBhLm9mZnNldFxuICAgICAgY29uc3Qgb3ZlcmxhcCA9IE1hdGgubWF4KDAsIE1hdGgubWluKGIubGVuZ3RoLCBhLmxlbmd0aCAtIGRpZmYpKVxuICAgICAgY29uc3QgcmVtb3ZlID0gTWF0aC5taW4oYS5sZW5ndGgsIG92ZXJsYXApXG4gICAgICBhLmxlbmd0aCAtPSByZW1vdmVcbiAgICB9XG5cbiAgICBpZiAoYS5sZW5ndGggPT09IDApIHtcbiAgICAgIHJldHVybiBudWxsXG4gICAgfVxuICB9XG5cbiAgLyoqXG4gICAqIFNldCBtYXJrLlxuICAgKi9cblxuICBpZiAoYi50eXBlID09ICdzZXRfbWFyaycpIHtcbiAgICBpZiAoXG4gICAgICBhcF9icCAhPSAnZXhhY3QnIHx8XG4gICAgICBhb19ibyA9PSBudWxsIHx8XG4gICAgICAhaXNFcXVhbChhLm1hcmssIGIubWFyaylcbiAgICApIHtcbiAgICAgIHJldHVyblxuICAgIH1cblxuICAgIGlmIChhLnR5cGUgPT0gJ3NldF9tYXJrJyAmJiBwID09ICdsZWZ0Jykge1xuICAgICAgY29uc3Qgb3ZlcmxhcHMgPSAoYXkgPj0gYnggJiYgYnkgPj0gYXgpXG4gICAgICBpZiAoIW92ZXJsYXBzKSByZXR1cm5cblxuICAgICAgY29uc3QgeCA9IE1hdGgubWF4KGF4LCBieClcbiAgICAgIGNvbnN0IHkgPSBNYXRoLm1pbihheSwgYnkpXG4gICAgICBjb25zdCBvZmZzZXQgPSB4XG4gICAgICBjb25zdCBsZW5ndGggPSB5IC0geFxuICAgICAgaWYgKGxlbmd0aCA9PSAwKSByZXR1cm5cblxuICAgICAgLy8gVE9ETzogY2FuIGJlIHJlcGxhY2VkIHdpdGggYGludmVydChCKWA/XG4gICAgICBjb25zdCBtYXJrID0geyAuLi5jbG9uZShiLm1hcmspLCAuLi5jbG9uZShiLnByb3BlcnRpZXMpIH1cbiAgICAgIGNvbnN0IHByb3BlcnRpZXMgPSBwaWNrKGIubWFyaywgT2JqZWN0LmtleXMoYi5wcm9wZXJ0aWVzKSlcbiAgICAgIGNvbnN0IGludmVyc2UgPSB7IC4uLmIsIHBhdGg6IGIucGF0aC5zbGljZSgpLCBvZmZzZXQsIGxlbmd0aCwgbWFyaywgcHJvcGVydGllcyB9XG4gICAgICByZXR1cm4gW2ludmVyc2UsIGFdXG4gICAgfVxuICB9XG59XG5cbi8qKlxuICogRXhwb3J0LlxuICpcbiAqIEB0eXBlIHtGdW5jdGlvbn1cbiAqL1xuXG5leHBvcnQgZGVmYXVsdCB0cmFuc2Zvcm1cbiJdfQ==