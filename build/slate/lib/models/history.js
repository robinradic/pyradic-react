'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _modelTypes = require('../constants/model-types');

var _modelTypes2 = _interopRequireDefault(_modelTypes);

var _debug = require('debug');

var _debug2 = _interopRequireDefault(_debug);

var _isEqual = require('lodash/isEqual');

var _isEqual2 = _interopRequireDefault(_isEqual);

var _immutable = require('immutable');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

/**
 * Debug.
 *
 * @type {Function}
 */

var debug = (0, _debug2.default)('slate:history');

/**
 * Default properties.
 *
 * @type {Object}
 */

var DEFAULTS = {
  redos: new _immutable.Stack(),
  undos: new _immutable.Stack()
};

/**
 * History.
 *
 * @type {History}
 */

var History = function (_ref) {
  _inherits(History, _ref);

  function History() {
    _classCallCheck(this, History);

    return _possibleConstructorReturn(this, (History.__proto__ || Object.getPrototypeOf(History)).apply(this, arguments));
  }

  _createClass(History, [{
    key: 'save',


    /**
     * Save an `operation` into the history.
     *
     * @param {Object} operation
     * @param {Object} options
     * @return {History}
     */

    value: function save(operation) {
      var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

      var history = this;
      var _history = history,
          undos = _history.undos;
      var merge = options.merge,
          skip = options.skip;

      var prevBatch = undos.peek();
      var prevOperation = prevBatch && prevBatch[prevBatch.length - 1];

      if (skip == null) {
        skip = shouldSkip(operation, prevOperation);
      }

      if (skip) {
        return history;
      }

      if (merge == null) {
        merge = shouldMerge(operation, prevOperation);
      }

      debug('save', { operation: operation, merge: merge });

      // If the `merge` flag is true, add the operation to the previous batch.
      if (merge) {
        var batch = prevBatch.slice();
        batch.push(operation);
        undos = undos.pop();
        undos = undos.push(batch);
      }

      // Otherwise, create a new batch with the operation.
      else {
          var _batch = [operation];
          undos = undos.push(_batch);
        }

      // Constrain the history to 100 entries for memory's sake.
      if (undos.length > 100) {
        undos = undos.take(100);
      }

      history = history.set('undos', undos);
      return history;
    }
  }, {
    key: 'kind',


    /**
     * Get the kind.
     *
     * @return {String}
     */

    get: function get() {
      return 'history';
    }
  }], [{
    key: 'create',


    /**
     * Create a new `History` with `attrs`.
     *
     * @param {Object} attrs
     * @return {History}
     */

    value: function create() {
      var attrs = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

      if (History.isHistory(attrs)) return attrs;

      var history = new History({
        undos: attrs.undos || new _immutable.Stack(),
        redos: attrs.redos || new _immutable.Stack()
      });

      return history;
    }

    /**
     * Check if a `value` is a `History`.
     *
     * @param {Any} value
     * @return {Boolean}
     */

  }, {
    key: 'isHistory',
    value: function isHistory(value) {
      return !!(value && value[_modelTypes2.default.HISTORY]);
    }
  }]);

  return History;
}(new _immutable.Record(DEFAULTS));

/**
 * Attach a pseudo-symbol for type checking.
 */

History.prototype[_modelTypes2.default.HISTORY] = true;

/**
 * Check whether to merge a new operation `o` into the previous operation `p`.
 *
 * @param {Object} o
 * @param {Object} p
 * @return {Boolean}
 */

function shouldMerge(o, p) {
  if (!p) return false;

  var merge = o.type == 'set_selection' && p.type == 'set_selection' || o.type == 'insert_text' && p.type == 'insert_text' && o.offset == p.offset + p.text.length && (0, _isEqual2.default)(o.path, p.path) || o.type == 'remove_text' && p.type == 'remove_text' && o.offset + o.text.length == p.offset && (0, _isEqual2.default)(o.path, p.path);

  return merge;
}

/**
 * Check whether to skip a new operation `o`, given previous operation `p`.
 *
 * @param {Object} o
 * @param {Object} p
 * @return {Boolean}
 */

function shouldSkip(o, p) {
  if (!p) return false;

  var skip = o.type == 'set_selection' && p.type == 'set_selection';

  return skip;
}

/**
 * Export.
 *
 * @type {History}
 */

exports.default = History;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9tb2RlbHMvaGlzdG9yeS5qcyJdLCJuYW1lcyI6WyJkZWJ1ZyIsIkRFRkFVTFRTIiwicmVkb3MiLCJ1bmRvcyIsIkhpc3RvcnkiLCJvcGVyYXRpb24iLCJvcHRpb25zIiwiaGlzdG9yeSIsIm1lcmdlIiwic2tpcCIsInByZXZCYXRjaCIsInBlZWsiLCJwcmV2T3BlcmF0aW9uIiwibGVuZ3RoIiwic2hvdWxkU2tpcCIsInNob3VsZE1lcmdlIiwiYmF0Y2giLCJzbGljZSIsInB1c2giLCJwb3AiLCJ0YWtlIiwic2V0IiwiYXR0cnMiLCJpc0hpc3RvcnkiLCJ2YWx1ZSIsIkhJU1RPUlkiLCJwcm90b3R5cGUiLCJvIiwicCIsInR5cGUiLCJvZmZzZXQiLCJ0ZXh0IiwicGF0aCJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7QUFDQTs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFDQTs7Ozs7Ozs7OztBQUVBOzs7Ozs7QUFNQSxJQUFNQSxRQUFRLHFCQUFNLGVBQU4sQ0FBZDs7QUFFQTs7Ozs7O0FBTUEsSUFBTUMsV0FBVztBQUNmQyxTQUFPLHNCQURRO0FBRWZDLFNBQU87QUFGUSxDQUFqQjs7QUFLQTs7Ozs7O0lBTU1DLE87Ozs7Ozs7Ozs7Ozs7QUF5Q0o7Ozs7Ozs7O3lCQVFLQyxTLEVBQXlCO0FBQUEsVUFBZEMsT0FBYyx1RUFBSixFQUFJOztBQUM1QixVQUFJQyxVQUFVLElBQWQ7QUFENEIscUJBRVpBLE9BRlk7QUFBQSxVQUV0QkosS0FGc0IsWUFFdEJBLEtBRnNCO0FBQUEsVUFHdEJLLEtBSHNCLEdBR05GLE9BSE0sQ0FHdEJFLEtBSHNCO0FBQUEsVUFHZkMsSUFIZSxHQUdOSCxPQUhNLENBR2ZHLElBSGU7O0FBSTVCLFVBQU1DLFlBQVlQLE1BQU1RLElBQU4sRUFBbEI7QUFDQSxVQUFNQyxnQkFBZ0JGLGFBQWFBLFVBQVVBLFVBQVVHLE1BQVYsR0FBbUIsQ0FBN0IsQ0FBbkM7O0FBRUEsVUFBSUosUUFBUSxJQUFaLEVBQWtCO0FBQ2hCQSxlQUFPSyxXQUFXVCxTQUFYLEVBQXNCTyxhQUF0QixDQUFQO0FBQ0Q7O0FBRUQsVUFBSUgsSUFBSixFQUFVO0FBQ1IsZUFBT0YsT0FBUDtBQUNEOztBQUVELFVBQUlDLFNBQVMsSUFBYixFQUFtQjtBQUNqQkEsZ0JBQVFPLFlBQVlWLFNBQVosRUFBdUJPLGFBQXZCLENBQVI7QUFDRDs7QUFFRFosWUFBTSxNQUFOLEVBQWMsRUFBRUssb0JBQUYsRUFBYUcsWUFBYixFQUFkOztBQUVBO0FBQ0EsVUFBSUEsS0FBSixFQUFXO0FBQ1QsWUFBTVEsUUFBUU4sVUFBVU8sS0FBVixFQUFkO0FBQ0FELGNBQU1FLElBQU4sQ0FBV2IsU0FBWDtBQUNBRixnQkFBUUEsTUFBTWdCLEdBQU4sRUFBUjtBQUNBaEIsZ0JBQVFBLE1BQU1lLElBQU4sQ0FBV0YsS0FBWCxDQUFSO0FBQ0Q7O0FBRUQ7QUFQQSxXQVFLO0FBQ0gsY0FBTUEsU0FBUSxDQUFDWCxTQUFELENBQWQ7QUFDQUYsa0JBQVFBLE1BQU1lLElBQU4sQ0FBV0YsTUFBWCxDQUFSO0FBQ0Q7O0FBRUQ7QUFDQSxVQUFJYixNQUFNVSxNQUFOLEdBQWUsR0FBbkIsRUFBd0I7QUFDdEJWLGdCQUFRQSxNQUFNaUIsSUFBTixDQUFXLEdBQVgsQ0FBUjtBQUNEOztBQUVEYixnQkFBVUEsUUFBUWMsR0FBUixDQUFZLE9BQVosRUFBcUJsQixLQUFyQixDQUFWO0FBQ0EsYUFBT0ksT0FBUDtBQUNEOzs7OztBQTVERDs7Ozs7O3dCQU1XO0FBQ1QsYUFBTyxTQUFQO0FBQ0Q7Ozs7O0FBckNEOzs7Ozs7OzZCQU8wQjtBQUFBLFVBQVplLEtBQVksdUVBQUosRUFBSTs7QUFDeEIsVUFBSWxCLFFBQVFtQixTQUFSLENBQWtCRCxLQUFsQixDQUFKLEVBQThCLE9BQU9BLEtBQVA7O0FBRTlCLFVBQU1mLFVBQVUsSUFBSUgsT0FBSixDQUFZO0FBQzFCRCxlQUFPbUIsTUFBTW5CLEtBQU4sSUFBZSxzQkFESTtBQUUxQkQsZUFBT29CLE1BQU1wQixLQUFOLElBQWU7QUFGSSxPQUFaLENBQWhCOztBQUtBLGFBQU9LLE9BQVA7QUFDRDs7QUFFRDs7Ozs7Ozs7OzhCQU9pQmlCLEssRUFBTztBQUN0QixhQUFPLENBQUMsRUFBRUEsU0FBU0EsTUFBTSxxQkFBWUMsT0FBbEIsQ0FBWCxDQUFSO0FBQ0Q7Ozs7RUE3Qm1CLHNCQUFXeEIsUUFBWCxDOztBQStGdEI7Ozs7QUFJQUcsUUFBUXNCLFNBQVIsQ0FBa0IscUJBQVlELE9BQTlCLElBQXlDLElBQXpDOztBQUVBOzs7Ozs7OztBQVFBLFNBQVNWLFdBQVQsQ0FBcUJZLENBQXJCLEVBQXdCQyxDQUF4QixFQUEyQjtBQUN6QixNQUFJLENBQUNBLENBQUwsRUFBUSxPQUFPLEtBQVA7O0FBRVIsTUFBTXBCLFFBRUZtQixFQUFFRSxJQUFGLElBQVUsZUFBVixJQUNBRCxFQUFFQyxJQUFGLElBQVUsZUFGWixJQUlFRixFQUFFRSxJQUFGLElBQVUsYUFBVixJQUNBRCxFQUFFQyxJQUFGLElBQVUsYUFEVixJQUVBRixFQUFFRyxNQUFGLElBQVlGLEVBQUVFLE1BQUYsR0FBV0YsRUFBRUcsSUFBRixDQUFPbEIsTUFGOUIsSUFHQSx1QkFBUWMsRUFBRUssSUFBVixFQUFnQkosRUFBRUksSUFBbEIsQ0FQRixJQVNFTCxFQUFFRSxJQUFGLElBQVUsYUFBVixJQUNBRCxFQUFFQyxJQUFGLElBQVUsYUFEVixJQUVBRixFQUFFRyxNQUFGLEdBQVdILEVBQUVJLElBQUYsQ0FBT2xCLE1BQWxCLElBQTRCZSxFQUFFRSxNQUY5QixJQUdBLHVCQUFRSCxFQUFFSyxJQUFWLEVBQWdCSixFQUFFSSxJQUFsQixDQWJKOztBQWlCQSxTQUFPeEIsS0FBUDtBQUNEOztBQUVEOzs7Ozs7OztBQVFBLFNBQVNNLFVBQVQsQ0FBb0JhLENBQXBCLEVBQXVCQyxDQUF2QixFQUEwQjtBQUN4QixNQUFJLENBQUNBLENBQUwsRUFBUSxPQUFPLEtBQVA7O0FBRVIsTUFBTW5CLE9BQ0prQixFQUFFRSxJQUFGLElBQVUsZUFBVixJQUNBRCxFQUFFQyxJQUFGLElBQVUsZUFGWjs7QUFLQSxTQUFPcEIsSUFBUDtBQUNEOztBQUVEOzs7Ozs7a0JBTWVMLE8iLCJmaWxlIjoiaGlzdG9yeS5qcyIsInNvdXJjZXNDb250ZW50IjpbIlxuaW1wb3J0IE1PREVMX1RZUEVTIGZyb20gJy4uL2NvbnN0YW50cy9tb2RlbC10eXBlcydcbmltcG9ydCBEZWJ1ZyBmcm9tICdkZWJ1ZydcbmltcG9ydCBpc0VxdWFsIGZyb20gJ2xvZGFzaC9pc0VxdWFsJ1xuaW1wb3J0IHsgUmVjb3JkLCBTdGFjayB9IGZyb20gJ2ltbXV0YWJsZSdcblxuLyoqXG4gKiBEZWJ1Zy5cbiAqXG4gKiBAdHlwZSB7RnVuY3Rpb259XG4gKi9cblxuY29uc3QgZGVidWcgPSBEZWJ1Zygnc2xhdGU6aGlzdG9yeScpXG5cbi8qKlxuICogRGVmYXVsdCBwcm9wZXJ0aWVzLlxuICpcbiAqIEB0eXBlIHtPYmplY3R9XG4gKi9cblxuY29uc3QgREVGQVVMVFMgPSB7XG4gIHJlZG9zOiBuZXcgU3RhY2soKSxcbiAgdW5kb3M6IG5ldyBTdGFjaygpLFxufVxuXG4vKipcbiAqIEhpc3RvcnkuXG4gKlxuICogQHR5cGUge0hpc3Rvcnl9XG4gKi9cblxuY2xhc3MgSGlzdG9yeSBleHRlbmRzIG5ldyBSZWNvcmQoREVGQVVMVFMpIHtcblxuICAvKipcbiAgICogQ3JlYXRlIGEgbmV3IGBIaXN0b3J5YCB3aXRoIGBhdHRyc2AuXG4gICAqXG4gICAqIEBwYXJhbSB7T2JqZWN0fSBhdHRyc1xuICAgKiBAcmV0dXJuIHtIaXN0b3J5fVxuICAgKi9cblxuICBzdGF0aWMgY3JlYXRlKGF0dHJzID0ge30pIHtcbiAgICBpZiAoSGlzdG9yeS5pc0hpc3RvcnkoYXR0cnMpKSByZXR1cm4gYXR0cnNcblxuICAgIGNvbnN0IGhpc3RvcnkgPSBuZXcgSGlzdG9yeSh7XG4gICAgICB1bmRvczogYXR0cnMudW5kb3MgfHwgbmV3IFN0YWNrKCksXG4gICAgICByZWRvczogYXR0cnMucmVkb3MgfHwgbmV3IFN0YWNrKCksXG4gICAgfSlcblxuICAgIHJldHVybiBoaXN0b3J5XG4gIH1cblxuICAvKipcbiAgICogQ2hlY2sgaWYgYSBgdmFsdWVgIGlzIGEgYEhpc3RvcnlgLlxuICAgKlxuICAgKiBAcGFyYW0ge0FueX0gdmFsdWVcbiAgICogQHJldHVybiB7Qm9vbGVhbn1cbiAgICovXG5cbiAgc3RhdGljIGlzSGlzdG9yeSh2YWx1ZSkge1xuICAgIHJldHVybiAhISh2YWx1ZSAmJiB2YWx1ZVtNT0RFTF9UWVBFUy5ISVNUT1JZXSlcbiAgfVxuXG4gIC8qKlxuICAgKiBHZXQgdGhlIGtpbmQuXG4gICAqXG4gICAqIEByZXR1cm4ge1N0cmluZ31cbiAgICovXG5cbiAgZ2V0IGtpbmQoKSB7XG4gICAgcmV0dXJuICdoaXN0b3J5J1xuICB9XG5cbiAgLyoqXG4gICAqIFNhdmUgYW4gYG9wZXJhdGlvbmAgaW50byB0aGUgaGlzdG9yeS5cbiAgICpcbiAgICogQHBhcmFtIHtPYmplY3R9IG9wZXJhdGlvblxuICAgKiBAcGFyYW0ge09iamVjdH0gb3B0aW9uc1xuICAgKiBAcmV0dXJuIHtIaXN0b3J5fVxuICAgKi9cblxuICBzYXZlKG9wZXJhdGlvbiwgb3B0aW9ucyA9IHt9KSB7XG4gICAgbGV0IGhpc3RvcnkgPSB0aGlzXG4gICAgbGV0IHsgdW5kb3MgfSA9IGhpc3RvcnlcbiAgICBsZXQgeyBtZXJnZSwgc2tpcCB9ID0gb3B0aW9uc1xuICAgIGNvbnN0IHByZXZCYXRjaCA9IHVuZG9zLnBlZWsoKVxuICAgIGNvbnN0IHByZXZPcGVyYXRpb24gPSBwcmV2QmF0Y2ggJiYgcHJldkJhdGNoW3ByZXZCYXRjaC5sZW5ndGggLSAxXVxuXG4gICAgaWYgKHNraXAgPT0gbnVsbCkge1xuICAgICAgc2tpcCA9IHNob3VsZFNraXAob3BlcmF0aW9uLCBwcmV2T3BlcmF0aW9uKVxuICAgIH1cblxuICAgIGlmIChza2lwKSB7XG4gICAgICByZXR1cm4gaGlzdG9yeVxuICAgIH1cblxuICAgIGlmIChtZXJnZSA9PSBudWxsKSB7XG4gICAgICBtZXJnZSA9IHNob3VsZE1lcmdlKG9wZXJhdGlvbiwgcHJldk9wZXJhdGlvbilcbiAgICB9XG5cbiAgICBkZWJ1Zygnc2F2ZScsIHsgb3BlcmF0aW9uLCBtZXJnZSB9KVxuXG4gICAgLy8gSWYgdGhlIGBtZXJnZWAgZmxhZyBpcyB0cnVlLCBhZGQgdGhlIG9wZXJhdGlvbiB0byB0aGUgcHJldmlvdXMgYmF0Y2guXG4gICAgaWYgKG1lcmdlKSB7XG4gICAgICBjb25zdCBiYXRjaCA9IHByZXZCYXRjaC5zbGljZSgpXG4gICAgICBiYXRjaC5wdXNoKG9wZXJhdGlvbilcbiAgICAgIHVuZG9zID0gdW5kb3MucG9wKClcbiAgICAgIHVuZG9zID0gdW5kb3MucHVzaChiYXRjaClcbiAgICB9XG5cbiAgICAvLyBPdGhlcndpc2UsIGNyZWF0ZSBhIG5ldyBiYXRjaCB3aXRoIHRoZSBvcGVyYXRpb24uXG4gICAgZWxzZSB7XG4gICAgICBjb25zdCBiYXRjaCA9IFtvcGVyYXRpb25dXG4gICAgICB1bmRvcyA9IHVuZG9zLnB1c2goYmF0Y2gpXG4gICAgfVxuXG4gICAgLy8gQ29uc3RyYWluIHRoZSBoaXN0b3J5IHRvIDEwMCBlbnRyaWVzIGZvciBtZW1vcnkncyBzYWtlLlxuICAgIGlmICh1bmRvcy5sZW5ndGggPiAxMDApIHtcbiAgICAgIHVuZG9zID0gdW5kb3MudGFrZSgxMDApXG4gICAgfVxuXG4gICAgaGlzdG9yeSA9IGhpc3Rvcnkuc2V0KCd1bmRvcycsIHVuZG9zKVxuICAgIHJldHVybiBoaXN0b3J5XG4gIH1cblxufVxuXG4vKipcbiAqIEF0dGFjaCBhIHBzZXVkby1zeW1ib2wgZm9yIHR5cGUgY2hlY2tpbmcuXG4gKi9cblxuSGlzdG9yeS5wcm90b3R5cGVbTU9ERUxfVFlQRVMuSElTVE9SWV0gPSB0cnVlXG5cbi8qKlxuICogQ2hlY2sgd2hldGhlciB0byBtZXJnZSBhIG5ldyBvcGVyYXRpb24gYG9gIGludG8gdGhlIHByZXZpb3VzIG9wZXJhdGlvbiBgcGAuXG4gKlxuICogQHBhcmFtIHtPYmplY3R9IG9cbiAqIEBwYXJhbSB7T2JqZWN0fSBwXG4gKiBAcmV0dXJuIHtCb29sZWFufVxuICovXG5cbmZ1bmN0aW9uIHNob3VsZE1lcmdlKG8sIHApIHtcbiAgaWYgKCFwKSByZXR1cm4gZmFsc2VcblxuICBjb25zdCBtZXJnZSA9IChcbiAgICAoXG4gICAgICBvLnR5cGUgPT0gJ3NldF9zZWxlY3Rpb24nICYmXG4gICAgICBwLnR5cGUgPT0gJ3NldF9zZWxlY3Rpb24nXG4gICAgKSB8fCAoXG4gICAgICBvLnR5cGUgPT0gJ2luc2VydF90ZXh0JyAmJlxuICAgICAgcC50eXBlID09ICdpbnNlcnRfdGV4dCcgJiZcbiAgICAgIG8ub2Zmc2V0ID09IHAub2Zmc2V0ICsgcC50ZXh0Lmxlbmd0aCAmJlxuICAgICAgaXNFcXVhbChvLnBhdGgsIHAucGF0aClcbiAgICApIHx8IChcbiAgICAgIG8udHlwZSA9PSAncmVtb3ZlX3RleHQnICYmXG4gICAgICBwLnR5cGUgPT0gJ3JlbW92ZV90ZXh0JyAmJlxuICAgICAgby5vZmZzZXQgKyBvLnRleHQubGVuZ3RoID09IHAub2Zmc2V0ICYmXG4gICAgICBpc0VxdWFsKG8ucGF0aCwgcC5wYXRoKVxuICAgIClcbiAgKVxuXG4gIHJldHVybiBtZXJnZVxufVxuXG4vKipcbiAqIENoZWNrIHdoZXRoZXIgdG8gc2tpcCBhIG5ldyBvcGVyYXRpb24gYG9gLCBnaXZlbiBwcmV2aW91cyBvcGVyYXRpb24gYHBgLlxuICpcbiAqIEBwYXJhbSB7T2JqZWN0fSBvXG4gKiBAcGFyYW0ge09iamVjdH0gcFxuICogQHJldHVybiB7Qm9vbGVhbn1cbiAqL1xuXG5mdW5jdGlvbiBzaG91bGRTa2lwKG8sIHApIHtcbiAgaWYgKCFwKSByZXR1cm4gZmFsc2VcblxuICBjb25zdCBza2lwID0gKFxuICAgIG8udHlwZSA9PSAnc2V0X3NlbGVjdGlvbicgJiZcbiAgICBwLnR5cGUgPT0gJ3NldF9zZWxlY3Rpb24nXG4gIClcblxuICByZXR1cm4gc2tpcFxufVxuXG4vKipcbiAqIEV4cG9ydC5cbiAqXG4gKiBAdHlwZSB7SGlzdG9yeX1cbiAqL1xuXG5leHBvcnQgZGVmYXVsdCBIaXN0b3J5XG4iXX0=