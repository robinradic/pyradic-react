'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _debug = require('debug');

var _debug2 = _interopRequireDefault(_debug);

var _normalize = require('../utils/normalize');

var _normalize2 = _interopRequireDefault(_normalize);

var _logger = require('../utils/logger');

var _logger2 = _interopRequireDefault(_logger);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Debug.
 *
 * @type {Function}
 */

var debug = (0, _debug2.default)('slate:operation:apply');

/**
 * Applying functions.
 *
 * @type {Object}
 */

var APPLIERS = {

  /**
   * Add mark to text at `offset` and `length` in node by `path`.
   *
   * @param {State} state
   * @param {Object} operation
   * @return {State}
   */

  add_mark: function add_mark(state, operation) {
    var path = operation.path,
        offset = operation.offset,
        length = operation.length;

    var mark = _normalize2.default.mark(operation.mark);
    var _state = state,
        document = _state.document;

    var node = document.assertPath(path);
    node = node.addMark(offset, length, mark);
    document = document.updateNode(node);
    state = state.set('document', document);
    return state;
  },


  /**
   * Insert a `node` at `index` in a node by `path`.
   *
   * @param {State} state
   * @param {Object} operation
   * @return {State}
   */

  insert_node: function insert_node(state, operation) {
    var path = operation.path;

    var node = _normalize2.default.node(operation.node);
    var index = path[path.length - 1];
    var rest = path.slice(0, -1);
    var _state2 = state,
        document = _state2.document;

    var parent = document.assertPath(rest);
    parent = parent.insertNode(index, node);
    document = document.updateNode(parent);
    state = state.set('document', document);
    return state;
  },


  /**
   * Insert `text` at `offset` in node by `path`.
   *
   * @param {State} state
   * @param {Object} operation
   * @return {State}
   */

  insert_text: function insert_text(state, operation) {
    var path = operation.path,
        offset = operation.offset,
        text = operation.text;
    var marks = operation.marks;

    if (Array.isArray(marks)) marks = _normalize2.default.marks(marks);

    var _state3 = state,
        document = _state3.document,
        selection = _state3.selection;
    var _selection = selection,
        anchorKey = _selection.anchorKey,
        focusKey = _selection.focusKey,
        anchorOffset = _selection.anchorOffset,
        focusOffset = _selection.focusOffset;

    var node = document.assertPath(path);

    // Update the document
    node = node.insertText(offset, text, marks);
    document = document.updateNode(node);

    // Update the selection
    if (anchorKey == node.key && anchorOffset >= offset) {
      selection = selection.moveAnchor(text.length);
    }
    if (focusKey == node.key && focusOffset >= offset) {
      selection = selection.moveFocus(text.length);
    }

    state = state.set('document', document).set('selection', selection);
    return state;
  },


  /**
   * Merge a node at `path` with the previous node.
   *
   * @param {State} state
   * @param {Object} operation
   * @return {State}
   */

  merge_node: function merge_node(state, operation) {
    var path = operation.path;

    var withPath = path.slice(0, path.length - 1).concat([path[path.length - 1] - 1]);
    var _state4 = state,
        document = _state4.document,
        selection = _state4.selection;

    var one = document.assertPath(withPath);
    var two = document.assertPath(path);
    var parent = document.getParent(one.key);
    var oneIndex = parent.nodes.indexOf(one);
    var twoIndex = parent.nodes.indexOf(two);

    // Perform the merge in the document.
    parent = parent.mergeNode(oneIndex, twoIndex);
    document = document.updateNode(parent);

    // If the nodes are text nodes and the selection is inside the second node
    // update it to refer to the first node instead.
    if (one.kind == 'text') {
      var _selection2 = selection,
          anchorKey = _selection2.anchorKey,
          anchorOffset = _selection2.anchorOffset,
          focusKey = _selection2.focusKey,
          focusOffset = _selection2.focusOffset;

      var normalize = false;

      if (anchorKey == two.key) {
        selection = selection.moveAnchorTo(one.key, one.text.length + anchorOffset);
        normalize = true;
      }

      if (focusKey == two.key) {
        selection = selection.moveFocusTo(one.key, one.text.length + focusOffset);
        normalize = true;
      }

      if (normalize) {
        selection = selection.normalize(document);
      }
    }

    // Update the document and selection.
    state = state.set('document', document).set('selection', selection);
    return state;
  },


  /**
   * Move a node by `path` to `newPath`.
   *
   * @param {State} state
   * @param {Object} operation
   * @return {State}
   */

  move_node: function move_node(state, operation) {
    var path = operation.path,
        newPath = operation.newPath;

    var newIndex = newPath[newPath.length - 1];
    var newParentPath = newPath.slice(0, -1);
    var oldParentPath = path.slice(0, -1);
    var oldIndex = path[path.length - 1];
    var _state5 = state,
        document = _state5.document;

    var node = document.assertPath(path);

    // Remove the node from its current parent.
    var parent = document.getParent(node.key);
    parent = parent.removeNode(oldIndex);
    document = document.updateNode(parent);

    // Find the new target...
    var target = void 0;

    // If the old path and the rest of the new path are the same, then the new
    // target is the old parent.
    if (oldParentPath.every(function (x, i) {
      return x === newParentPath[i];
    }) && oldParentPath.length === newParentPath.length) {
      target = parent;
    }

    // Otherwise, if the old path removal resulted in the new path being no longer
    // correct, we need to decrement the new path at the old path's last index.
    else if (oldParentPath.every(function (x, i) {
        return x === newParentPath[i];
      }) && oldIndex < newParentPath[oldParentPath.length]) {
        newParentPath[oldParentPath.length]--;
        target = document.assertPath(newParentPath);
      }

      // Otherwise, we can just grab the target normally...
      else {
          target = document.assertPath(newParentPath);
        }

    // Insert the new node to its new parent.
    target = target.insertNode(newIndex, node);
    document = document.updateNode(target);
    state = state.set('document', document);
    return state;
  },


  /**
   * Remove mark from text at `offset` and `length` in node by `path`.
   *
   * @param {State} state
   * @param {Object} operation
   * @return {State}
   */

  remove_mark: function remove_mark(state, operation) {
    var path = operation.path,
        offset = operation.offset,
        length = operation.length;

    var mark = _normalize2.default.mark(operation.mark);
    var _state6 = state,
        document = _state6.document;

    var node = document.assertPath(path);
    node = node.removeMark(offset, length, mark);
    document = document.updateNode(node);
    state = state.set('document', document);
    return state;
  },


  /**
   * Remove a node by `path`.
   *
   * @param {State} state
   * @param {Object} operation
   * @return {State}
   */

  remove_node: function remove_node(state, operation) {
    var path = operation.path;
    var _state7 = state,
        document = _state7.document,
        selection = _state7.selection;
    var _selection3 = selection,
        startKey = _selection3.startKey,
        endKey = _selection3.endKey;

    var node = document.assertPath(path);

    // If the selection is set, check to see if it needs to be updated.
    if (selection.isSet) {
      var hasStartNode = node.hasNode(startKey);
      var hasEndNode = node.hasNode(endKey);
      var normalize = false;

      // If one of the selection's nodes is being removed, we need to update it.
      if (hasStartNode) {
        var prev = document.getPreviousText(startKey);
        var next = document.getNextText(startKey);

        if (prev) {
          selection = selection.moveStartTo(prev.key, prev.text.length);
          normalize = true;
        } else if (next) {
          selection = selection.moveStartTo(next.key, 0);
          normalize = true;
        } else {
          selection = selection.deselect();
        }
      }

      if (hasEndNode) {
        var _prev = document.getPreviousText(endKey);
        var _next = document.getNextText(endKey);

        if (_prev) {
          selection = selection.moveEndTo(_prev.key, _prev.text.length);
          normalize = true;
        } else if (_next) {
          selection = selection.moveEndTo(_next.key, 0);
          normalize = true;
        } else {
          selection = selection.deselect();
        }
      }

      if (normalize) {
        selection = selection.normalize(document);
      }
    }

    // Remove the node from the document.
    var parent = document.getParent(node.key);
    var index = parent.nodes.indexOf(node);
    parent = parent.removeNode(index);
    document = document.updateNode(parent);

    // Update the document and selection.
    state = state.set('document', document).set('selection', selection);
    return state;
  },


  /**
   * Remove `text` at `offset` in node by `path`.
   *
   * @param {State} state
   * @param {Object} operation
   * @return {State}
   */

  remove_text: function remove_text(state, operation) {
    var path = operation.path,
        offset = operation.offset,
        text = operation.text;
    var length = text.length;

    var rangeOffset = offset + length;
    var _state8 = state,
        document = _state8.document,
        selection = _state8.selection;
    var _selection4 = selection,
        anchorKey = _selection4.anchorKey,
        focusKey = _selection4.focusKey,
        anchorOffset = _selection4.anchorOffset,
        focusOffset = _selection4.focusOffset;

    var node = document.assertPath(path);

    // Update the selection.
    if (anchorKey == node.key && anchorOffset >= rangeOffset) {
      selection = selection.moveAnchor(-length);
    }

    if (focusKey == node.key && focusOffset >= rangeOffset) {
      selection = selection.moveFocus(-length);
    }

    node = node.removeText(offset, length);
    document = document.updateNode(node);
    state = state.set('document', document).set('selection', selection);
    return state;
  },


  /**
   * Set `data` on `state`.
   *
   * @param {State} state
   * @param {Object} operation
   * @return {State}
   */

  set_data: function set_data(state, operation) {
    var properties = operation.properties;
    var _state9 = state,
        data = _state9.data;


    data = data.merge(properties);
    state = state.set('data', data);
    return state;
  },


  /**
   * Set `properties` on mark on text at `offset` and `length` in node by `path`.
   *
   * @param {State} state
   * @param {Object} operation
   * @return {State}
   */

  set_mark: function set_mark(state, operation) {
    var path = operation.path,
        offset = operation.offset,
        length = operation.length,
        properties = operation.properties;

    var mark = _normalize2.default.mark(operation.mark);
    var _state10 = state,
        document = _state10.document;

    var node = document.assertPath(path);
    node = node.updateMark(offset, length, mark, properties);
    document = document.updateNode(node);
    state = state.set('document', document);
    return state;
  },


  /**
   * Set `properties` on a node by `path`.
   *
   * @param {State} state
   * @param {Object} operation
   * @return {State}
   */

  set_node: function set_node(state, operation) {
    var path = operation.path,
        properties = operation.properties;
    var _state11 = state,
        document = _state11.document;

    var node = document.assertPath(path);

    // Warn when trying to overwite a node's children.
    if (properties.nodes && properties.nodes != node.nodes) {
      _logger2.default.warn('Updating a Node\'s `nodes` property via `setNode()` is not allowed. Use the appropriate insertion and removal operations instead. The opeartion in question was:', operation);
      delete properties.nodes;
    }

    // Warn when trying to change a node's key.
    if (properties.key && properties.key != node.key) {
      _logger2.default.warn('Updating a Node\'s `key` property via `setNode()` is not allowed. There should be no reason to do this. The opeartion in question was:', operation);
      delete properties.key;
    }

    node = node.merge(properties);
    document = document.updateNode(node);
    state = state.set('document', document);
    return state;
  },


  /**
   * Set `properties` on the selection.
   *
   * @param {State} state
   * @param {Object} operation
   * @return {State}
   */

  set_selection: function set_selection(state, operation) {
    var properties = _extends({}, operation.properties);
    var _state12 = state,
        document = _state12.document,
        selection = _state12.selection;


    if (properties.marks !== undefined) {
      properties.marks = _normalize2.default.marks(properties.marks);
    }

    if (properties.anchorPath !== undefined) {
      properties.anchorKey = properties.anchorPath === null ? null : document.assertPath(properties.anchorPath).key;
      delete properties.anchorPath;
    }

    if (properties.focusPath !== undefined) {
      properties.focusKey = properties.focusPath === null ? null : document.assertPath(properties.focusPath).key;
      delete properties.focusPath;
    }

    selection = selection.merge(properties);
    selection = selection.normalize(document);
    state = state.set('selection', selection);
    return state;
  },


  /**
   * Split a node by `path` at `offset`.
   *
   * @param {State} state
   * @param {Object} operation
   * @return {State}
   */

  split_node: function split_node(state, operation) {
    var path = operation.path,
        position = operation.position;
    var _state13 = state,
        document = _state13.document,
        selection = _state13.selection;

    // Calculate a few things...

    var node = document.assertPath(path);
    var parent = document.getParent(node.key);
    var index = parent.nodes.indexOf(node);

    // Split the node by its parent.
    parent = parent.splitNode(index, position);
    document = document.updateNode(parent);

    // Determine whether we need to update the selection...
    var _selection5 = selection,
        startKey = _selection5.startKey,
        endKey = _selection5.endKey,
        startOffset = _selection5.startOffset,
        endOffset = _selection5.endOffset;

    var next = document.getNextText(node.key);
    var normalize = false;

    // If the start point is after or equal to the split, update it.
    if (node.key == startKey && position <= startOffset) {
      selection = selection.moveStartTo(next.key, startOffset - position);
      normalize = true;
    }

    // If the end point is after or equal to the split, update it.
    if (node.key == endKey && position <= endOffset) {
      selection = selection.moveEndTo(next.key, endOffset - position);
      normalize = true;
    }

    // Normalize the selection if we changed it, since the methods we use might
    // leave it in a non-normalized state.
    if (normalize) {
      selection = selection.normalize(document);
    }

    // Return the updated state.
    state = state.set('document', document).set('selection', selection);
    return state;
  }
};

/**
 * Apply an `operation` to a `state`.
 *
 * @param {State} state
 * @param {Object} operation
 * @return {State} state
 */

function applyOperation(state, operation) {
  var type = operation.type;

  var apply = APPLIERS[type];

  if (!apply) {
    throw new Error('Unknown operation type: "' + type + '".');
  }

  debug(type, operation);
  state = apply(state, operation);
  return state;
}

/**
 * Export.
 *
 * @type {Function}
 */

exports.default = applyOperation;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9vcGVyYXRpb25zL2FwcGx5LmpzIl0sIm5hbWVzIjpbImRlYnVnIiwiQVBQTElFUlMiLCJhZGRfbWFyayIsInN0YXRlIiwib3BlcmF0aW9uIiwicGF0aCIsIm9mZnNldCIsImxlbmd0aCIsIm1hcmsiLCJkb2N1bWVudCIsIm5vZGUiLCJhc3NlcnRQYXRoIiwiYWRkTWFyayIsInVwZGF0ZU5vZGUiLCJzZXQiLCJpbnNlcnRfbm9kZSIsImluZGV4IiwicmVzdCIsInNsaWNlIiwicGFyZW50IiwiaW5zZXJ0Tm9kZSIsImluc2VydF90ZXh0IiwidGV4dCIsIm1hcmtzIiwiQXJyYXkiLCJpc0FycmF5Iiwic2VsZWN0aW9uIiwiYW5jaG9yS2V5IiwiZm9jdXNLZXkiLCJhbmNob3JPZmZzZXQiLCJmb2N1c09mZnNldCIsImluc2VydFRleHQiLCJrZXkiLCJtb3ZlQW5jaG9yIiwibW92ZUZvY3VzIiwibWVyZ2Vfbm9kZSIsIndpdGhQYXRoIiwiY29uY2F0Iiwib25lIiwidHdvIiwiZ2V0UGFyZW50Iiwib25lSW5kZXgiLCJub2RlcyIsImluZGV4T2YiLCJ0d29JbmRleCIsIm1lcmdlTm9kZSIsImtpbmQiLCJub3JtYWxpemUiLCJtb3ZlQW5jaG9yVG8iLCJtb3ZlRm9jdXNUbyIsIm1vdmVfbm9kZSIsIm5ld1BhdGgiLCJuZXdJbmRleCIsIm5ld1BhcmVudFBhdGgiLCJvbGRQYXJlbnRQYXRoIiwib2xkSW5kZXgiLCJyZW1vdmVOb2RlIiwidGFyZ2V0IiwiZXZlcnkiLCJ4IiwiaSIsInJlbW92ZV9tYXJrIiwicmVtb3ZlTWFyayIsInJlbW92ZV9ub2RlIiwic3RhcnRLZXkiLCJlbmRLZXkiLCJpc1NldCIsImhhc1N0YXJ0Tm9kZSIsImhhc05vZGUiLCJoYXNFbmROb2RlIiwicHJldiIsImdldFByZXZpb3VzVGV4dCIsIm5leHQiLCJnZXROZXh0VGV4dCIsIm1vdmVTdGFydFRvIiwiZGVzZWxlY3QiLCJtb3ZlRW5kVG8iLCJyZW1vdmVfdGV4dCIsInJhbmdlT2Zmc2V0IiwicmVtb3ZlVGV4dCIsInNldF9kYXRhIiwicHJvcGVydGllcyIsImRhdGEiLCJtZXJnZSIsInNldF9tYXJrIiwidXBkYXRlTWFyayIsInNldF9ub2RlIiwid2FybiIsInNldF9zZWxlY3Rpb24iLCJ1bmRlZmluZWQiLCJhbmNob3JQYXRoIiwiZm9jdXNQYXRoIiwic3BsaXRfbm9kZSIsInBvc2l0aW9uIiwic3BsaXROb2RlIiwic3RhcnRPZmZzZXQiLCJlbmRPZmZzZXQiLCJhcHBseU9wZXJhdGlvbiIsInR5cGUiLCJhcHBseSIsIkVycm9yIl0sIm1hcHBpbmdzIjoiOzs7Ozs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFDQTs7Ozs7O0FBRUE7Ozs7OztBQU1BLElBQU1BLFFBQVEscUJBQU0sdUJBQU4sQ0FBZDs7QUFFQTs7Ozs7O0FBTUEsSUFBTUMsV0FBVzs7QUFFZjs7Ozs7Ozs7QUFRQUMsVUFWZSxvQkFVTkMsS0FWTSxFQVVDQyxTQVZELEVBVVk7QUFBQSxRQUNqQkMsSUFEaUIsR0FDUUQsU0FEUixDQUNqQkMsSUFEaUI7QUFBQSxRQUNYQyxNQURXLEdBQ1FGLFNBRFIsQ0FDWEUsTUFEVztBQUFBLFFBQ0hDLE1BREcsR0FDUUgsU0FEUixDQUNIRyxNQURHOztBQUV6QixRQUFNQyxPQUFPLG9CQUFVQSxJQUFWLENBQWVKLFVBQVVJLElBQXpCLENBQWI7QUFGeUIsaUJBR05MLEtBSE07QUFBQSxRQUduQk0sUUFIbUIsVUFHbkJBLFFBSG1COztBQUl6QixRQUFJQyxPQUFPRCxTQUFTRSxVQUFULENBQW9CTixJQUFwQixDQUFYO0FBQ0FLLFdBQU9BLEtBQUtFLE9BQUwsQ0FBYU4sTUFBYixFQUFxQkMsTUFBckIsRUFBNkJDLElBQTdCLENBQVA7QUFDQUMsZUFBV0EsU0FBU0ksVUFBVCxDQUFvQkgsSUFBcEIsQ0FBWDtBQUNBUCxZQUFRQSxNQUFNVyxHQUFOLENBQVUsVUFBVixFQUFzQkwsUUFBdEIsQ0FBUjtBQUNBLFdBQU9OLEtBQVA7QUFDRCxHQW5CYzs7O0FBcUJmOzs7Ozs7OztBQVFBWSxhQTdCZSx1QkE2QkhaLEtBN0JHLEVBNkJJQyxTQTdCSixFQTZCZTtBQUFBLFFBQ3BCQyxJQURvQixHQUNYRCxTQURXLENBQ3BCQyxJQURvQjs7QUFFNUIsUUFBTUssT0FBTyxvQkFBVUEsSUFBVixDQUFlTixVQUFVTSxJQUF6QixDQUFiO0FBQ0EsUUFBTU0sUUFBUVgsS0FBS0EsS0FBS0UsTUFBTCxHQUFjLENBQW5CLENBQWQ7QUFDQSxRQUFNVSxPQUFPWixLQUFLYSxLQUFMLENBQVcsQ0FBWCxFQUFjLENBQUMsQ0FBZixDQUFiO0FBSjRCLGtCQUtUZixLQUxTO0FBQUEsUUFLdEJNLFFBTHNCLFdBS3RCQSxRQUxzQjs7QUFNNUIsUUFBSVUsU0FBU1YsU0FBU0UsVUFBVCxDQUFvQk0sSUFBcEIsQ0FBYjtBQUNBRSxhQUFTQSxPQUFPQyxVQUFQLENBQWtCSixLQUFsQixFQUF5Qk4sSUFBekIsQ0FBVDtBQUNBRCxlQUFXQSxTQUFTSSxVQUFULENBQW9CTSxNQUFwQixDQUFYO0FBQ0FoQixZQUFRQSxNQUFNVyxHQUFOLENBQVUsVUFBVixFQUFzQkwsUUFBdEIsQ0FBUjtBQUNBLFdBQU9OLEtBQVA7QUFDRCxHQXhDYzs7O0FBMENmOzs7Ozs7OztBQVFBa0IsYUFsRGUsdUJBa0RIbEIsS0FsREcsRUFrRElDLFNBbERKLEVBa0RlO0FBQUEsUUFDcEJDLElBRG9CLEdBQ0dELFNBREgsQ0FDcEJDLElBRG9CO0FBQUEsUUFDZEMsTUFEYyxHQUNHRixTQURILENBQ2RFLE1BRGM7QUFBQSxRQUNOZ0IsSUFETSxHQUNHbEIsU0FESCxDQUNOa0IsSUFETTtBQUFBLFFBR3RCQyxLQUhzQixHQUdabkIsU0FIWSxDQUd0Qm1CLEtBSHNCOztBQUk1QixRQUFJQyxNQUFNQyxPQUFOLENBQWNGLEtBQWQsQ0FBSixFQUEwQkEsUUFBUSxvQkFBVUEsS0FBVixDQUFnQkEsS0FBaEIsQ0FBUjs7QUFKRSxrQkFNRXBCLEtBTkY7QUFBQSxRQU10Qk0sUUFOc0IsV0FNdEJBLFFBTnNCO0FBQUEsUUFNWmlCLFNBTlksV0FNWkEsU0FOWTtBQUFBLHFCQU8rQkEsU0FQL0I7QUFBQSxRQU9wQkMsU0FQb0IsY0FPcEJBLFNBUG9CO0FBQUEsUUFPVEMsUUFQUyxjQU9UQSxRQVBTO0FBQUEsUUFPQ0MsWUFQRCxjQU9DQSxZQVBEO0FBQUEsUUFPZUMsV0FQZixjQU9lQSxXQVBmOztBQVE1QixRQUFJcEIsT0FBT0QsU0FBU0UsVUFBVCxDQUFvQk4sSUFBcEIsQ0FBWDs7QUFFQTtBQUNBSyxXQUFPQSxLQUFLcUIsVUFBTCxDQUFnQnpCLE1BQWhCLEVBQXdCZ0IsSUFBeEIsRUFBOEJDLEtBQTlCLENBQVA7QUFDQWQsZUFBV0EsU0FBU0ksVUFBVCxDQUFvQkgsSUFBcEIsQ0FBWDs7QUFFQTtBQUNBLFFBQUlpQixhQUFhakIsS0FBS3NCLEdBQWxCLElBQXlCSCxnQkFBZ0J2QixNQUE3QyxFQUFxRDtBQUNuRG9CLGtCQUFZQSxVQUFVTyxVQUFWLENBQXFCWCxLQUFLZixNQUExQixDQUFaO0FBQ0Q7QUFDRCxRQUFJcUIsWUFBWWxCLEtBQUtzQixHQUFqQixJQUF3QkYsZUFBZXhCLE1BQTNDLEVBQW1EO0FBQ2pEb0Isa0JBQVlBLFVBQVVRLFNBQVYsQ0FBb0JaLEtBQUtmLE1BQXpCLENBQVo7QUFDRDs7QUFFREosWUFBUUEsTUFBTVcsR0FBTixDQUFVLFVBQVYsRUFBc0JMLFFBQXRCLEVBQWdDSyxHQUFoQyxDQUFvQyxXQUFwQyxFQUFpRFksU0FBakQsQ0FBUjtBQUNBLFdBQU92QixLQUFQO0FBQ0QsR0ExRWM7OztBQTRFZjs7Ozs7Ozs7QUFRQWdDLFlBcEZlLHNCQW9GSmhDLEtBcEZJLEVBb0ZHQyxTQXBGSCxFQW9GYztBQUFBLFFBQ25CQyxJQURtQixHQUNWRCxTQURVLENBQ25CQyxJQURtQjs7QUFFM0IsUUFBTStCLFdBQVcvQixLQUFLYSxLQUFMLENBQVcsQ0FBWCxFQUFjYixLQUFLRSxNQUFMLEdBQWMsQ0FBNUIsRUFBK0I4QixNQUEvQixDQUFzQyxDQUFDaEMsS0FBS0EsS0FBS0UsTUFBTCxHQUFjLENBQW5CLElBQXdCLENBQXpCLENBQXRDLENBQWpCO0FBRjJCLGtCQUdHSixLQUhIO0FBQUEsUUFHckJNLFFBSHFCLFdBR3JCQSxRQUhxQjtBQUFBLFFBR1hpQixTQUhXLFdBR1hBLFNBSFc7O0FBSTNCLFFBQU1ZLE1BQU03QixTQUFTRSxVQUFULENBQW9CeUIsUUFBcEIsQ0FBWjtBQUNBLFFBQU1HLE1BQU05QixTQUFTRSxVQUFULENBQW9CTixJQUFwQixDQUFaO0FBQ0EsUUFBSWMsU0FBU1YsU0FBUytCLFNBQVQsQ0FBbUJGLElBQUlOLEdBQXZCLENBQWI7QUFDQSxRQUFNUyxXQUFXdEIsT0FBT3VCLEtBQVAsQ0FBYUMsT0FBYixDQUFxQkwsR0FBckIsQ0FBakI7QUFDQSxRQUFNTSxXQUFXekIsT0FBT3VCLEtBQVAsQ0FBYUMsT0FBYixDQUFxQkosR0FBckIsQ0FBakI7O0FBRUE7QUFDQXBCLGFBQVNBLE9BQU8wQixTQUFQLENBQWlCSixRQUFqQixFQUEyQkcsUUFBM0IsQ0FBVDtBQUNBbkMsZUFBV0EsU0FBU0ksVUFBVCxDQUFvQk0sTUFBcEIsQ0FBWDs7QUFFQTtBQUNBO0FBQ0EsUUFBSW1CLElBQUlRLElBQUosSUFBWSxNQUFoQixFQUF3QjtBQUFBLHdCQUNxQ3BCLFNBRHJDO0FBQUEsVUFDZEMsU0FEYyxlQUNkQSxTQURjO0FBQUEsVUFDSEUsWUFERyxlQUNIQSxZQURHO0FBQUEsVUFDV0QsUUFEWCxlQUNXQSxRQURYO0FBQUEsVUFDcUJFLFdBRHJCLGVBQ3FCQSxXQURyQjs7QUFFdEIsVUFBSWlCLFlBQVksS0FBaEI7O0FBRUEsVUFBSXBCLGFBQWFZLElBQUlQLEdBQXJCLEVBQTBCO0FBQ3hCTixvQkFBWUEsVUFBVXNCLFlBQVYsQ0FBdUJWLElBQUlOLEdBQTNCLEVBQWdDTSxJQUFJaEIsSUFBSixDQUFTZixNQUFULEdBQWtCc0IsWUFBbEQsQ0FBWjtBQUNBa0Isb0JBQVksSUFBWjtBQUNEOztBQUVELFVBQUluQixZQUFZVyxJQUFJUCxHQUFwQixFQUF5QjtBQUN2Qk4sb0JBQVlBLFVBQVV1QixXQUFWLENBQXNCWCxJQUFJTixHQUExQixFQUErQk0sSUFBSWhCLElBQUosQ0FBU2YsTUFBVCxHQUFrQnVCLFdBQWpELENBQVo7QUFDQWlCLG9CQUFZLElBQVo7QUFDRDs7QUFFRCxVQUFJQSxTQUFKLEVBQWU7QUFDYnJCLG9CQUFZQSxVQUFVcUIsU0FBVixDQUFvQnRDLFFBQXBCLENBQVo7QUFDRDtBQUNGOztBQUVEO0FBQ0FOLFlBQVFBLE1BQU1XLEdBQU4sQ0FBVSxVQUFWLEVBQXNCTCxRQUF0QixFQUFnQ0ssR0FBaEMsQ0FBb0MsV0FBcEMsRUFBaURZLFNBQWpELENBQVI7QUFDQSxXQUFPdkIsS0FBUDtBQUNELEdBMUhjOzs7QUE0SGY7Ozs7Ozs7O0FBUUErQyxXQXBJZSxxQkFvSUwvQyxLQXBJSyxFQW9JRUMsU0FwSUYsRUFvSWE7QUFBQSxRQUNsQkMsSUFEa0IsR0FDQUQsU0FEQSxDQUNsQkMsSUFEa0I7QUFBQSxRQUNaOEMsT0FEWSxHQUNBL0MsU0FEQSxDQUNaK0MsT0FEWTs7QUFFMUIsUUFBTUMsV0FBV0QsUUFBUUEsUUFBUTVDLE1BQVIsR0FBaUIsQ0FBekIsQ0FBakI7QUFDQSxRQUFNOEMsZ0JBQWdCRixRQUFRakMsS0FBUixDQUFjLENBQWQsRUFBaUIsQ0FBQyxDQUFsQixDQUF0QjtBQUNBLFFBQU1vQyxnQkFBZ0JqRCxLQUFLYSxLQUFMLENBQVcsQ0FBWCxFQUFjLENBQUMsQ0FBZixDQUF0QjtBQUNBLFFBQU1xQyxXQUFXbEQsS0FBS0EsS0FBS0UsTUFBTCxHQUFjLENBQW5CLENBQWpCO0FBTDBCLGtCQU1QSixLQU5PO0FBQUEsUUFNcEJNLFFBTm9CLFdBTXBCQSxRQU5vQjs7QUFPMUIsUUFBTUMsT0FBT0QsU0FBU0UsVUFBVCxDQUFvQk4sSUFBcEIsQ0FBYjs7QUFFQTtBQUNBLFFBQUljLFNBQVNWLFNBQVMrQixTQUFULENBQW1COUIsS0FBS3NCLEdBQXhCLENBQWI7QUFDQWIsYUFBU0EsT0FBT3FDLFVBQVAsQ0FBa0JELFFBQWxCLENBQVQ7QUFDQTlDLGVBQVdBLFNBQVNJLFVBQVQsQ0FBb0JNLE1BQXBCLENBQVg7O0FBRUE7QUFDQSxRQUFJc0MsZUFBSjs7QUFFQTtBQUNBO0FBQ0EsUUFDR0gsY0FBY0ksS0FBZCxDQUFvQixVQUFDQyxDQUFELEVBQUlDLENBQUo7QUFBQSxhQUFVRCxNQUFNTixjQUFjTyxDQUFkLENBQWhCO0FBQUEsS0FBcEIsQ0FBRCxJQUNDTixjQUFjL0MsTUFBZCxLQUF5QjhDLGNBQWM5QyxNQUYxQyxFQUdFO0FBQ0FrRCxlQUFTdEMsTUFBVDtBQUNEOztBQUVEO0FBQ0E7QUFSQSxTQVNLLElBQ0ZtQyxjQUFjSSxLQUFkLENBQW9CLFVBQUNDLENBQUQsRUFBSUMsQ0FBSjtBQUFBLGVBQVVELE1BQU1OLGNBQWNPLENBQWQsQ0FBaEI7QUFBQSxPQUFwQixDQUFELElBQ0NMLFdBQVdGLGNBQWNDLGNBQWMvQyxNQUE1QixDQUZULEVBR0g7QUFDQThDLHNCQUFjQyxjQUFjL0MsTUFBNUI7QUFDQWtELGlCQUFTaEQsU0FBU0UsVUFBVCxDQUFvQjBDLGFBQXBCLENBQVQ7QUFDRDs7QUFFRDtBQVJLLFdBU0E7QUFDSEksbUJBQVNoRCxTQUFTRSxVQUFULENBQW9CMEMsYUFBcEIsQ0FBVDtBQUNEOztBQUVEO0FBQ0FJLGFBQVNBLE9BQU9yQyxVQUFQLENBQWtCZ0MsUUFBbEIsRUFBNEIxQyxJQUE1QixDQUFUO0FBQ0FELGVBQVdBLFNBQVNJLFVBQVQsQ0FBb0I0QyxNQUFwQixDQUFYO0FBQ0F0RCxZQUFRQSxNQUFNVyxHQUFOLENBQVUsVUFBVixFQUFzQkwsUUFBdEIsQ0FBUjtBQUNBLFdBQU9OLEtBQVA7QUFDRCxHQWxMYzs7O0FBb0xmOzs7Ozs7OztBQVFBMEQsYUE1TGUsdUJBNExIMUQsS0E1TEcsRUE0TElDLFNBNUxKLEVBNExlO0FBQUEsUUFDcEJDLElBRG9CLEdBQ0tELFNBREwsQ0FDcEJDLElBRG9CO0FBQUEsUUFDZEMsTUFEYyxHQUNLRixTQURMLENBQ2RFLE1BRGM7QUFBQSxRQUNOQyxNQURNLEdBQ0tILFNBREwsQ0FDTkcsTUFETTs7QUFFNUIsUUFBTUMsT0FBTyxvQkFBVUEsSUFBVixDQUFlSixVQUFVSSxJQUF6QixDQUFiO0FBRjRCLGtCQUdUTCxLQUhTO0FBQUEsUUFHdEJNLFFBSHNCLFdBR3RCQSxRQUhzQjs7QUFJNUIsUUFBSUMsT0FBT0QsU0FBU0UsVUFBVCxDQUFvQk4sSUFBcEIsQ0FBWDtBQUNBSyxXQUFPQSxLQUFLb0QsVUFBTCxDQUFnQnhELE1BQWhCLEVBQXdCQyxNQUF4QixFQUFnQ0MsSUFBaEMsQ0FBUDtBQUNBQyxlQUFXQSxTQUFTSSxVQUFULENBQW9CSCxJQUFwQixDQUFYO0FBQ0FQLFlBQVFBLE1BQU1XLEdBQU4sQ0FBVSxVQUFWLEVBQXNCTCxRQUF0QixDQUFSO0FBQ0EsV0FBT04sS0FBUDtBQUNELEdBck1jOzs7QUF1TWY7Ozs7Ozs7O0FBUUE0RCxhQS9NZSx1QkErTUg1RCxLQS9NRyxFQStNSUMsU0EvTUosRUErTWU7QUFBQSxRQUNwQkMsSUFEb0IsR0FDWEQsU0FEVyxDQUNwQkMsSUFEb0I7QUFBQSxrQkFFRUYsS0FGRjtBQUFBLFFBRXRCTSxRQUZzQixXQUV0QkEsUUFGc0I7QUFBQSxRQUVaaUIsU0FGWSxXQUVaQSxTQUZZO0FBQUEsc0JBR0NBLFNBSEQ7QUFBQSxRQUdwQnNDLFFBSG9CLGVBR3BCQSxRQUhvQjtBQUFBLFFBR1ZDLE1BSFUsZUFHVkEsTUFIVTs7QUFJNUIsUUFBTXZELE9BQU9ELFNBQVNFLFVBQVQsQ0FBb0JOLElBQXBCLENBQWI7O0FBRUE7QUFDQSxRQUFJcUIsVUFBVXdDLEtBQWQsRUFBcUI7QUFDbkIsVUFBTUMsZUFBZXpELEtBQUswRCxPQUFMLENBQWFKLFFBQWIsQ0FBckI7QUFDQSxVQUFNSyxhQUFhM0QsS0FBSzBELE9BQUwsQ0FBYUgsTUFBYixDQUFuQjtBQUNBLFVBQUlsQixZQUFZLEtBQWhCOztBQUVBO0FBQ0EsVUFBSW9CLFlBQUosRUFBa0I7QUFDaEIsWUFBTUcsT0FBTzdELFNBQVM4RCxlQUFULENBQXlCUCxRQUF6QixDQUFiO0FBQ0EsWUFBTVEsT0FBTy9ELFNBQVNnRSxXQUFULENBQXFCVCxRQUFyQixDQUFiOztBQUVBLFlBQUlNLElBQUosRUFBVTtBQUNSNUMsc0JBQVlBLFVBQVVnRCxXQUFWLENBQXNCSixLQUFLdEMsR0FBM0IsRUFBZ0NzQyxLQUFLaEQsSUFBTCxDQUFVZixNQUExQyxDQUFaO0FBQ0F3QyxzQkFBWSxJQUFaO0FBQ0QsU0FIRCxNQUdPLElBQUl5QixJQUFKLEVBQVU7QUFDZjlDLHNCQUFZQSxVQUFVZ0QsV0FBVixDQUFzQkYsS0FBS3hDLEdBQTNCLEVBQWdDLENBQWhDLENBQVo7QUFDQWUsc0JBQVksSUFBWjtBQUNELFNBSE0sTUFHQTtBQUNMckIsc0JBQVlBLFVBQVVpRCxRQUFWLEVBQVo7QUFDRDtBQUNGOztBQUVELFVBQUlOLFVBQUosRUFBZ0I7QUFDZCxZQUFNQyxRQUFPN0QsU0FBUzhELGVBQVQsQ0FBeUJOLE1BQXpCLENBQWI7QUFDQSxZQUFNTyxRQUFPL0QsU0FBU2dFLFdBQVQsQ0FBcUJSLE1BQXJCLENBQWI7O0FBRUEsWUFBSUssS0FBSixFQUFVO0FBQ1I1QyxzQkFBWUEsVUFBVWtELFNBQVYsQ0FBb0JOLE1BQUt0QyxHQUF6QixFQUE4QnNDLE1BQUtoRCxJQUFMLENBQVVmLE1BQXhDLENBQVo7QUFDQXdDLHNCQUFZLElBQVo7QUFDRCxTQUhELE1BR08sSUFBSXlCLEtBQUosRUFBVTtBQUNmOUMsc0JBQVlBLFVBQVVrRCxTQUFWLENBQW9CSixNQUFLeEMsR0FBekIsRUFBOEIsQ0FBOUIsQ0FBWjtBQUNBZSxzQkFBWSxJQUFaO0FBQ0QsU0FITSxNQUdBO0FBQ0xyQixzQkFBWUEsVUFBVWlELFFBQVYsRUFBWjtBQUNEO0FBQ0Y7O0FBRUQsVUFBSTVCLFNBQUosRUFBZTtBQUNickIsb0JBQVlBLFVBQVVxQixTQUFWLENBQW9CdEMsUUFBcEIsQ0FBWjtBQUNEO0FBQ0Y7O0FBRUQ7QUFDQSxRQUFJVSxTQUFTVixTQUFTK0IsU0FBVCxDQUFtQjlCLEtBQUtzQixHQUF4QixDQUFiO0FBQ0EsUUFBTWhCLFFBQVFHLE9BQU91QixLQUFQLENBQWFDLE9BQWIsQ0FBcUJqQyxJQUFyQixDQUFkO0FBQ0FTLGFBQVNBLE9BQU9xQyxVQUFQLENBQWtCeEMsS0FBbEIsQ0FBVDtBQUNBUCxlQUFXQSxTQUFTSSxVQUFULENBQW9CTSxNQUFwQixDQUFYOztBQUVBO0FBQ0FoQixZQUFRQSxNQUFNVyxHQUFOLENBQVUsVUFBVixFQUFzQkwsUUFBdEIsRUFBZ0NLLEdBQWhDLENBQW9DLFdBQXBDLEVBQWlEWSxTQUFqRCxDQUFSO0FBQ0EsV0FBT3ZCLEtBQVA7QUFDRCxHQXhRYzs7O0FBMFFmOzs7Ozs7OztBQVFBMEUsYUFsUmUsdUJBa1JIMUUsS0FsUkcsRUFrUklDLFNBbFJKLEVBa1JlO0FBQUEsUUFDcEJDLElBRG9CLEdBQ0dELFNBREgsQ0FDcEJDLElBRG9CO0FBQUEsUUFDZEMsTUFEYyxHQUNHRixTQURILENBQ2RFLE1BRGM7QUFBQSxRQUNOZ0IsSUFETSxHQUNHbEIsU0FESCxDQUNOa0IsSUFETTtBQUFBLFFBRXBCZixNQUZvQixHQUVUZSxJQUZTLENBRXBCZixNQUZvQjs7QUFHNUIsUUFBTXVFLGNBQWN4RSxTQUFTQyxNQUE3QjtBQUg0QixrQkFJRUosS0FKRjtBQUFBLFFBSXRCTSxRQUpzQixXQUl0QkEsUUFKc0I7QUFBQSxRQUlaaUIsU0FKWSxXQUlaQSxTQUpZO0FBQUEsc0JBSytCQSxTQUwvQjtBQUFBLFFBS3BCQyxTQUxvQixlQUtwQkEsU0FMb0I7QUFBQSxRQUtUQyxRQUxTLGVBS1RBLFFBTFM7QUFBQSxRQUtDQyxZQUxELGVBS0NBLFlBTEQ7QUFBQSxRQUtlQyxXQUxmLGVBS2VBLFdBTGY7O0FBTTVCLFFBQUlwQixPQUFPRCxTQUFTRSxVQUFULENBQW9CTixJQUFwQixDQUFYOztBQUVBO0FBQ0EsUUFBSXNCLGFBQWFqQixLQUFLc0IsR0FBbEIsSUFBeUJILGdCQUFnQmlELFdBQTdDLEVBQTBEO0FBQ3hEcEQsa0JBQVlBLFVBQVVPLFVBQVYsQ0FBcUIsQ0FBQzFCLE1BQXRCLENBQVo7QUFDRDs7QUFFRCxRQUFJcUIsWUFBWWxCLEtBQUtzQixHQUFqQixJQUF3QkYsZUFBZWdELFdBQTNDLEVBQXdEO0FBQ3REcEQsa0JBQVlBLFVBQVVRLFNBQVYsQ0FBb0IsQ0FBQzNCLE1BQXJCLENBQVo7QUFDRDs7QUFFREcsV0FBT0EsS0FBS3FFLFVBQUwsQ0FBZ0J6RSxNQUFoQixFQUF3QkMsTUFBeEIsQ0FBUDtBQUNBRSxlQUFXQSxTQUFTSSxVQUFULENBQW9CSCxJQUFwQixDQUFYO0FBQ0FQLFlBQVFBLE1BQU1XLEdBQU4sQ0FBVSxVQUFWLEVBQXNCTCxRQUF0QixFQUFnQ0ssR0FBaEMsQ0FBb0MsV0FBcEMsRUFBaURZLFNBQWpELENBQVI7QUFDQSxXQUFPdkIsS0FBUDtBQUNELEdBdlNjOzs7QUF5U2Y7Ozs7Ozs7O0FBUUE2RSxVQWpUZSxvQkFpVE43RSxLQWpUTSxFQWlUQ0MsU0FqVEQsRUFpVFk7QUFBQSxRQUNqQjZFLFVBRGlCLEdBQ0Y3RSxTQURFLENBQ2pCNkUsVUFEaUI7QUFBQSxrQkFFVjlFLEtBRlU7QUFBQSxRQUVuQitFLElBRm1CLFdBRW5CQSxJQUZtQjs7O0FBSXpCQSxXQUFPQSxLQUFLQyxLQUFMLENBQVdGLFVBQVgsQ0FBUDtBQUNBOUUsWUFBUUEsTUFBTVcsR0FBTixDQUFVLE1BQVYsRUFBa0JvRSxJQUFsQixDQUFSO0FBQ0EsV0FBTy9FLEtBQVA7QUFDRCxHQXhUYzs7O0FBMFRmOzs7Ozs7OztBQVFBaUYsVUFsVWUsb0JBa1VOakYsS0FsVU0sRUFrVUNDLFNBbFVELEVBa1VZO0FBQUEsUUFDakJDLElBRGlCLEdBQ29CRCxTQURwQixDQUNqQkMsSUFEaUI7QUFBQSxRQUNYQyxNQURXLEdBQ29CRixTQURwQixDQUNYRSxNQURXO0FBQUEsUUFDSEMsTUFERyxHQUNvQkgsU0FEcEIsQ0FDSEcsTUFERztBQUFBLFFBQ0swRSxVQURMLEdBQ29CN0UsU0FEcEIsQ0FDSzZFLFVBREw7O0FBRXpCLFFBQU16RSxPQUFPLG9CQUFVQSxJQUFWLENBQWVKLFVBQVVJLElBQXpCLENBQWI7QUFGeUIsbUJBR05MLEtBSE07QUFBQSxRQUduQk0sUUFIbUIsWUFHbkJBLFFBSG1COztBQUl6QixRQUFJQyxPQUFPRCxTQUFTRSxVQUFULENBQW9CTixJQUFwQixDQUFYO0FBQ0FLLFdBQU9BLEtBQUsyRSxVQUFMLENBQWdCL0UsTUFBaEIsRUFBd0JDLE1BQXhCLEVBQWdDQyxJQUFoQyxFQUFzQ3lFLFVBQXRDLENBQVA7QUFDQXhFLGVBQVdBLFNBQVNJLFVBQVQsQ0FBb0JILElBQXBCLENBQVg7QUFDQVAsWUFBUUEsTUFBTVcsR0FBTixDQUFVLFVBQVYsRUFBc0JMLFFBQXRCLENBQVI7QUFDQSxXQUFPTixLQUFQO0FBQ0QsR0EzVWM7OztBQTZVZjs7Ozs7Ozs7QUFRQW1GLFVBclZlLG9CQXFWTm5GLEtBclZNLEVBcVZDQyxTQXJWRCxFQXFWWTtBQUFBLFFBQ2pCQyxJQURpQixHQUNJRCxTQURKLENBQ2pCQyxJQURpQjtBQUFBLFFBQ1g0RSxVQURXLEdBQ0k3RSxTQURKLENBQ1g2RSxVQURXO0FBQUEsbUJBRU45RSxLQUZNO0FBQUEsUUFFbkJNLFFBRm1CLFlBRW5CQSxRQUZtQjs7QUFHekIsUUFBSUMsT0FBT0QsU0FBU0UsVUFBVCxDQUFvQk4sSUFBcEIsQ0FBWDs7QUFFQTtBQUNBLFFBQUk0RSxXQUFXdkMsS0FBWCxJQUFvQnVDLFdBQVd2QyxLQUFYLElBQW9CaEMsS0FBS2dDLEtBQWpELEVBQXdEO0FBQ3RELHVCQUFPNkMsSUFBUCxDQUFZLGtLQUFaLEVBQWdMbkYsU0FBaEw7QUFDQSxhQUFPNkUsV0FBV3ZDLEtBQWxCO0FBQ0Q7O0FBRUQ7QUFDQSxRQUFJdUMsV0FBV2pELEdBQVgsSUFBa0JpRCxXQUFXakQsR0FBWCxJQUFrQnRCLEtBQUtzQixHQUE3QyxFQUFrRDtBQUNoRCx1QkFBT3VELElBQVAsQ0FBWSx3SUFBWixFQUFzSm5GLFNBQXRKO0FBQ0EsYUFBTzZFLFdBQVdqRCxHQUFsQjtBQUNEOztBQUVEdEIsV0FBT0EsS0FBS3lFLEtBQUwsQ0FBV0YsVUFBWCxDQUFQO0FBQ0F4RSxlQUFXQSxTQUFTSSxVQUFULENBQW9CSCxJQUFwQixDQUFYO0FBQ0FQLFlBQVFBLE1BQU1XLEdBQU4sQ0FBVSxVQUFWLEVBQXNCTCxRQUF0QixDQUFSO0FBQ0EsV0FBT04sS0FBUDtBQUNELEdBMVdjOzs7QUE0V2Y7Ozs7Ozs7O0FBUUFxRixlQXBYZSx5QkFvWERyRixLQXBYQyxFQW9YTUMsU0FwWE4sRUFvWGlCO0FBQzlCLFFBQU02RSwwQkFBa0I3RSxVQUFVNkUsVUFBNUIsQ0FBTjtBQUQ4QixtQkFFQTlFLEtBRkE7QUFBQSxRQUV4Qk0sUUFGd0IsWUFFeEJBLFFBRndCO0FBQUEsUUFFZGlCLFNBRmMsWUFFZEEsU0FGYzs7O0FBSTlCLFFBQUl1RCxXQUFXMUQsS0FBWCxLQUFxQmtFLFNBQXpCLEVBQW9DO0FBQ2xDUixpQkFBVzFELEtBQVgsR0FBbUIsb0JBQVVBLEtBQVYsQ0FBZ0IwRCxXQUFXMUQsS0FBM0IsQ0FBbkI7QUFDRDs7QUFFRCxRQUFJMEQsV0FBV1MsVUFBWCxLQUEwQkQsU0FBOUIsRUFBeUM7QUFDdkNSLGlCQUFXdEQsU0FBWCxHQUF1QnNELFdBQVdTLFVBQVgsS0FBMEIsSUFBMUIsR0FDbkIsSUFEbUIsR0FFbkJqRixTQUFTRSxVQUFULENBQW9Cc0UsV0FBV1MsVUFBL0IsRUFBMkMxRCxHQUYvQztBQUdBLGFBQU9pRCxXQUFXUyxVQUFsQjtBQUNEOztBQUVELFFBQUlULFdBQVdVLFNBQVgsS0FBeUJGLFNBQTdCLEVBQXdDO0FBQ3RDUixpQkFBV3JELFFBQVgsR0FBc0JxRCxXQUFXVSxTQUFYLEtBQXlCLElBQXpCLEdBQ2xCLElBRGtCLEdBRWxCbEYsU0FBU0UsVUFBVCxDQUFvQnNFLFdBQVdVLFNBQS9CLEVBQTBDM0QsR0FGOUM7QUFHQSxhQUFPaUQsV0FBV1UsU0FBbEI7QUFDRDs7QUFFRGpFLGdCQUFZQSxVQUFVeUQsS0FBVixDQUFnQkYsVUFBaEIsQ0FBWjtBQUNBdkQsZ0JBQVlBLFVBQVVxQixTQUFWLENBQW9CdEMsUUFBcEIsQ0FBWjtBQUNBTixZQUFRQSxNQUFNVyxHQUFOLENBQVUsV0FBVixFQUF1QlksU0FBdkIsQ0FBUjtBQUNBLFdBQU92QixLQUFQO0FBQ0QsR0E5WWM7OztBQWdaZjs7Ozs7Ozs7QUFRQXlGLFlBeFplLHNCQXdaSnpGLEtBeFpJLEVBd1pHQyxTQXhaSCxFQXdaYztBQUFBLFFBQ25CQyxJQURtQixHQUNBRCxTQURBLENBQ25CQyxJQURtQjtBQUFBLFFBQ2J3RixRQURhLEdBQ0F6RixTQURBLENBQ2J5RixRQURhO0FBQUEsbUJBRUcxRixLQUZIO0FBQUEsUUFFckJNLFFBRnFCLFlBRXJCQSxRQUZxQjtBQUFBLFFBRVhpQixTQUZXLFlBRVhBLFNBRlc7O0FBSTNCOztBQUNBLFFBQU1oQixPQUFPRCxTQUFTRSxVQUFULENBQW9CTixJQUFwQixDQUFiO0FBQ0EsUUFBSWMsU0FBU1YsU0FBUytCLFNBQVQsQ0FBbUI5QixLQUFLc0IsR0FBeEIsQ0FBYjtBQUNBLFFBQU1oQixRQUFRRyxPQUFPdUIsS0FBUCxDQUFhQyxPQUFiLENBQXFCakMsSUFBckIsQ0FBZDs7QUFFQTtBQUNBUyxhQUFTQSxPQUFPMkUsU0FBUCxDQUFpQjlFLEtBQWpCLEVBQXdCNkUsUUFBeEIsQ0FBVDtBQUNBcEYsZUFBV0EsU0FBU0ksVUFBVCxDQUFvQk0sTUFBcEIsQ0FBWDs7QUFFQTtBQWIyQixzQkFjMEJPLFNBZDFCO0FBQUEsUUFjbkJzQyxRQWRtQixlQWNuQkEsUUFkbUI7QUFBQSxRQWNUQyxNQWRTLGVBY1RBLE1BZFM7QUFBQSxRQWNEOEIsV0FkQyxlQWNEQSxXQWRDO0FBQUEsUUFjWUMsU0FkWixlQWNZQSxTQWRaOztBQWUzQixRQUFNeEIsT0FBTy9ELFNBQVNnRSxXQUFULENBQXFCL0QsS0FBS3NCLEdBQTFCLENBQWI7QUFDQSxRQUFJZSxZQUFZLEtBQWhCOztBQUVBO0FBQ0EsUUFBSXJDLEtBQUtzQixHQUFMLElBQVlnQyxRQUFaLElBQXdCNkIsWUFBWUUsV0FBeEMsRUFBcUQ7QUFDbkRyRSxrQkFBWUEsVUFBVWdELFdBQVYsQ0FBc0JGLEtBQUt4QyxHQUEzQixFQUFnQytELGNBQWNGLFFBQTlDLENBQVo7QUFDQTlDLGtCQUFZLElBQVo7QUFDRDs7QUFFRDtBQUNBLFFBQUlyQyxLQUFLc0IsR0FBTCxJQUFZaUMsTUFBWixJQUFzQjRCLFlBQVlHLFNBQXRDLEVBQWlEO0FBQy9DdEUsa0JBQVlBLFVBQVVrRCxTQUFWLENBQW9CSixLQUFLeEMsR0FBekIsRUFBOEJnRSxZQUFZSCxRQUExQyxDQUFaO0FBQ0E5QyxrQkFBWSxJQUFaO0FBQ0Q7O0FBRUQ7QUFDQTtBQUNBLFFBQUlBLFNBQUosRUFBZTtBQUNickIsa0JBQVlBLFVBQVVxQixTQUFWLENBQW9CdEMsUUFBcEIsQ0FBWjtBQUNEOztBQUVEO0FBQ0FOLFlBQVFBLE1BQU1XLEdBQU4sQ0FBVSxVQUFWLEVBQXNCTCxRQUF0QixFQUFnQ0ssR0FBaEMsQ0FBb0MsV0FBcEMsRUFBaURZLFNBQWpELENBQVI7QUFDQSxXQUFPdkIsS0FBUDtBQUNEO0FBL2JjLENBQWpCOztBQW1jQTs7Ozs7Ozs7QUFRQSxTQUFTOEYsY0FBVCxDQUF3QjlGLEtBQXhCLEVBQStCQyxTQUEvQixFQUEwQztBQUFBLE1BQ2hDOEYsSUFEZ0MsR0FDdkI5RixTQUR1QixDQUNoQzhGLElBRGdDOztBQUV4QyxNQUFNQyxRQUFRbEcsU0FBU2lHLElBQVQsQ0FBZDs7QUFFQSxNQUFJLENBQUNDLEtBQUwsRUFBWTtBQUNWLFVBQU0sSUFBSUMsS0FBSiwrQkFBc0NGLElBQXRDLFFBQU47QUFDRDs7QUFFRGxHLFFBQU1rRyxJQUFOLEVBQVk5RixTQUFaO0FBQ0FELFVBQVFnRyxNQUFNaEcsS0FBTixFQUFhQyxTQUFiLENBQVI7QUFDQSxTQUFPRCxLQUFQO0FBQ0Q7O0FBRUQ7Ozs7OztrQkFNZThGLGMiLCJmaWxlIjoiYXBwbHkuanMiLCJzb3VyY2VzQ29udGVudCI6WyJcbmltcG9ydCBEZWJ1ZyBmcm9tICdkZWJ1ZydcbmltcG9ydCBOb3JtYWxpemUgZnJvbSAnLi4vdXRpbHMvbm9ybWFsaXplJ1xuaW1wb3J0IGxvZ2dlciBmcm9tICcuLi91dGlscy9sb2dnZXInXG5cbi8qKlxuICogRGVidWcuXG4gKlxuICogQHR5cGUge0Z1bmN0aW9ufVxuICovXG5cbmNvbnN0IGRlYnVnID0gRGVidWcoJ3NsYXRlOm9wZXJhdGlvbjphcHBseScpXG5cbi8qKlxuICogQXBwbHlpbmcgZnVuY3Rpb25zLlxuICpcbiAqIEB0eXBlIHtPYmplY3R9XG4gKi9cblxuY29uc3QgQVBQTElFUlMgPSB7XG5cbiAgLyoqXG4gICAqIEFkZCBtYXJrIHRvIHRleHQgYXQgYG9mZnNldGAgYW5kIGBsZW5ndGhgIGluIG5vZGUgYnkgYHBhdGhgLlxuICAgKlxuICAgKiBAcGFyYW0ge1N0YXRlfSBzdGF0ZVxuICAgKiBAcGFyYW0ge09iamVjdH0gb3BlcmF0aW9uXG4gICAqIEByZXR1cm4ge1N0YXRlfVxuICAgKi9cblxuICBhZGRfbWFyayhzdGF0ZSwgb3BlcmF0aW9uKSB7XG4gICAgY29uc3QgeyBwYXRoLCBvZmZzZXQsIGxlbmd0aCB9ID0gb3BlcmF0aW9uXG4gICAgY29uc3QgbWFyayA9IE5vcm1hbGl6ZS5tYXJrKG9wZXJhdGlvbi5tYXJrKVxuICAgIGxldCB7IGRvY3VtZW50IH0gPSBzdGF0ZVxuICAgIGxldCBub2RlID0gZG9jdW1lbnQuYXNzZXJ0UGF0aChwYXRoKVxuICAgIG5vZGUgPSBub2RlLmFkZE1hcmsob2Zmc2V0LCBsZW5ndGgsIG1hcmspXG4gICAgZG9jdW1lbnQgPSBkb2N1bWVudC51cGRhdGVOb2RlKG5vZGUpXG4gICAgc3RhdGUgPSBzdGF0ZS5zZXQoJ2RvY3VtZW50JywgZG9jdW1lbnQpXG4gICAgcmV0dXJuIHN0YXRlXG4gIH0sXG5cbiAgLyoqXG4gICAqIEluc2VydCBhIGBub2RlYCBhdCBgaW5kZXhgIGluIGEgbm9kZSBieSBgcGF0aGAuXG4gICAqXG4gICAqIEBwYXJhbSB7U3RhdGV9IHN0YXRlXG4gICAqIEBwYXJhbSB7T2JqZWN0fSBvcGVyYXRpb25cbiAgICogQHJldHVybiB7U3RhdGV9XG4gICAqL1xuXG4gIGluc2VydF9ub2RlKHN0YXRlLCBvcGVyYXRpb24pIHtcbiAgICBjb25zdCB7IHBhdGggfSA9IG9wZXJhdGlvblxuICAgIGNvbnN0IG5vZGUgPSBOb3JtYWxpemUubm9kZShvcGVyYXRpb24ubm9kZSlcbiAgICBjb25zdCBpbmRleCA9IHBhdGhbcGF0aC5sZW5ndGggLSAxXVxuICAgIGNvbnN0IHJlc3QgPSBwYXRoLnNsaWNlKDAsIC0xKVxuICAgIGxldCB7IGRvY3VtZW50IH0gPSBzdGF0ZVxuICAgIGxldCBwYXJlbnQgPSBkb2N1bWVudC5hc3NlcnRQYXRoKHJlc3QpXG4gICAgcGFyZW50ID0gcGFyZW50Lmluc2VydE5vZGUoaW5kZXgsIG5vZGUpXG4gICAgZG9jdW1lbnQgPSBkb2N1bWVudC51cGRhdGVOb2RlKHBhcmVudClcbiAgICBzdGF0ZSA9IHN0YXRlLnNldCgnZG9jdW1lbnQnLCBkb2N1bWVudClcbiAgICByZXR1cm4gc3RhdGVcbiAgfSxcblxuICAvKipcbiAgICogSW5zZXJ0IGB0ZXh0YCBhdCBgb2Zmc2V0YCBpbiBub2RlIGJ5IGBwYXRoYC5cbiAgICpcbiAgICogQHBhcmFtIHtTdGF0ZX0gc3RhdGVcbiAgICogQHBhcmFtIHtPYmplY3R9IG9wZXJhdGlvblxuICAgKiBAcmV0dXJuIHtTdGF0ZX1cbiAgICovXG5cbiAgaW5zZXJ0X3RleHQoc3RhdGUsIG9wZXJhdGlvbikge1xuICAgIGNvbnN0IHsgcGF0aCwgb2Zmc2V0LCB0ZXh0IH0gPSBvcGVyYXRpb25cblxuICAgIGxldCB7IG1hcmtzIH0gPSBvcGVyYXRpb25cbiAgICBpZiAoQXJyYXkuaXNBcnJheShtYXJrcykpIG1hcmtzID0gTm9ybWFsaXplLm1hcmtzKG1hcmtzKVxuXG4gICAgbGV0IHsgZG9jdW1lbnQsIHNlbGVjdGlvbiB9ID0gc3RhdGVcbiAgICBjb25zdCB7IGFuY2hvcktleSwgZm9jdXNLZXksIGFuY2hvck9mZnNldCwgZm9jdXNPZmZzZXQgfSA9IHNlbGVjdGlvblxuICAgIGxldCBub2RlID0gZG9jdW1lbnQuYXNzZXJ0UGF0aChwYXRoKVxuXG4gICAgLy8gVXBkYXRlIHRoZSBkb2N1bWVudFxuICAgIG5vZGUgPSBub2RlLmluc2VydFRleHQob2Zmc2V0LCB0ZXh0LCBtYXJrcylcbiAgICBkb2N1bWVudCA9IGRvY3VtZW50LnVwZGF0ZU5vZGUobm9kZSlcblxuICAgIC8vIFVwZGF0ZSB0aGUgc2VsZWN0aW9uXG4gICAgaWYgKGFuY2hvcktleSA9PSBub2RlLmtleSAmJiBhbmNob3JPZmZzZXQgPj0gb2Zmc2V0KSB7XG4gICAgICBzZWxlY3Rpb24gPSBzZWxlY3Rpb24ubW92ZUFuY2hvcih0ZXh0Lmxlbmd0aClcbiAgICB9XG4gICAgaWYgKGZvY3VzS2V5ID09IG5vZGUua2V5ICYmIGZvY3VzT2Zmc2V0ID49IG9mZnNldCkge1xuICAgICAgc2VsZWN0aW9uID0gc2VsZWN0aW9uLm1vdmVGb2N1cyh0ZXh0Lmxlbmd0aClcbiAgICB9XG5cbiAgICBzdGF0ZSA9IHN0YXRlLnNldCgnZG9jdW1lbnQnLCBkb2N1bWVudCkuc2V0KCdzZWxlY3Rpb24nLCBzZWxlY3Rpb24pXG4gICAgcmV0dXJuIHN0YXRlXG4gIH0sXG5cbiAgLyoqXG4gICAqIE1lcmdlIGEgbm9kZSBhdCBgcGF0aGAgd2l0aCB0aGUgcHJldmlvdXMgbm9kZS5cbiAgICpcbiAgICogQHBhcmFtIHtTdGF0ZX0gc3RhdGVcbiAgICogQHBhcmFtIHtPYmplY3R9IG9wZXJhdGlvblxuICAgKiBAcmV0dXJuIHtTdGF0ZX1cbiAgICovXG5cbiAgbWVyZ2Vfbm9kZShzdGF0ZSwgb3BlcmF0aW9uKSB7XG4gICAgY29uc3QgeyBwYXRoIH0gPSBvcGVyYXRpb25cbiAgICBjb25zdCB3aXRoUGF0aCA9IHBhdGguc2xpY2UoMCwgcGF0aC5sZW5ndGggLSAxKS5jb25jYXQoW3BhdGhbcGF0aC5sZW5ndGggLSAxXSAtIDFdKVxuICAgIGxldCB7IGRvY3VtZW50LCBzZWxlY3Rpb24gfSA9IHN0YXRlXG4gICAgY29uc3Qgb25lID0gZG9jdW1lbnQuYXNzZXJ0UGF0aCh3aXRoUGF0aClcbiAgICBjb25zdCB0d28gPSBkb2N1bWVudC5hc3NlcnRQYXRoKHBhdGgpXG4gICAgbGV0IHBhcmVudCA9IGRvY3VtZW50LmdldFBhcmVudChvbmUua2V5KVxuICAgIGNvbnN0IG9uZUluZGV4ID0gcGFyZW50Lm5vZGVzLmluZGV4T2Yob25lKVxuICAgIGNvbnN0IHR3b0luZGV4ID0gcGFyZW50Lm5vZGVzLmluZGV4T2YodHdvKVxuXG4gICAgLy8gUGVyZm9ybSB0aGUgbWVyZ2UgaW4gdGhlIGRvY3VtZW50LlxuICAgIHBhcmVudCA9IHBhcmVudC5tZXJnZU5vZGUob25lSW5kZXgsIHR3b0luZGV4KVxuICAgIGRvY3VtZW50ID0gZG9jdW1lbnQudXBkYXRlTm9kZShwYXJlbnQpXG5cbiAgICAvLyBJZiB0aGUgbm9kZXMgYXJlIHRleHQgbm9kZXMgYW5kIHRoZSBzZWxlY3Rpb24gaXMgaW5zaWRlIHRoZSBzZWNvbmQgbm9kZVxuICAgIC8vIHVwZGF0ZSBpdCB0byByZWZlciB0byB0aGUgZmlyc3Qgbm9kZSBpbnN0ZWFkLlxuICAgIGlmIChvbmUua2luZCA9PSAndGV4dCcpIHtcbiAgICAgIGNvbnN0IHsgYW5jaG9yS2V5LCBhbmNob3JPZmZzZXQsIGZvY3VzS2V5LCBmb2N1c09mZnNldCB9ID0gc2VsZWN0aW9uXG4gICAgICBsZXQgbm9ybWFsaXplID0gZmFsc2VcblxuICAgICAgaWYgKGFuY2hvcktleSA9PSB0d28ua2V5KSB7XG4gICAgICAgIHNlbGVjdGlvbiA9IHNlbGVjdGlvbi5tb3ZlQW5jaG9yVG8ob25lLmtleSwgb25lLnRleHQubGVuZ3RoICsgYW5jaG9yT2Zmc2V0KVxuICAgICAgICBub3JtYWxpemUgPSB0cnVlXG4gICAgICB9XG5cbiAgICAgIGlmIChmb2N1c0tleSA9PSB0d28ua2V5KSB7XG4gICAgICAgIHNlbGVjdGlvbiA9IHNlbGVjdGlvbi5tb3ZlRm9jdXNUbyhvbmUua2V5LCBvbmUudGV4dC5sZW5ndGggKyBmb2N1c09mZnNldClcbiAgICAgICAgbm9ybWFsaXplID0gdHJ1ZVxuICAgICAgfVxuXG4gICAgICBpZiAobm9ybWFsaXplKSB7XG4gICAgICAgIHNlbGVjdGlvbiA9IHNlbGVjdGlvbi5ub3JtYWxpemUoZG9jdW1lbnQpXG4gICAgICB9XG4gICAgfVxuXG4gICAgLy8gVXBkYXRlIHRoZSBkb2N1bWVudCBhbmQgc2VsZWN0aW9uLlxuICAgIHN0YXRlID0gc3RhdGUuc2V0KCdkb2N1bWVudCcsIGRvY3VtZW50KS5zZXQoJ3NlbGVjdGlvbicsIHNlbGVjdGlvbilcbiAgICByZXR1cm4gc3RhdGVcbiAgfSxcblxuICAvKipcbiAgICogTW92ZSBhIG5vZGUgYnkgYHBhdGhgIHRvIGBuZXdQYXRoYC5cbiAgICpcbiAgICogQHBhcmFtIHtTdGF0ZX0gc3RhdGVcbiAgICogQHBhcmFtIHtPYmplY3R9IG9wZXJhdGlvblxuICAgKiBAcmV0dXJuIHtTdGF0ZX1cbiAgICovXG5cbiAgbW92ZV9ub2RlKHN0YXRlLCBvcGVyYXRpb24pIHtcbiAgICBjb25zdCB7IHBhdGgsIG5ld1BhdGggfSA9IG9wZXJhdGlvblxuICAgIGNvbnN0IG5ld0luZGV4ID0gbmV3UGF0aFtuZXdQYXRoLmxlbmd0aCAtIDFdXG4gICAgY29uc3QgbmV3UGFyZW50UGF0aCA9IG5ld1BhdGguc2xpY2UoMCwgLTEpXG4gICAgY29uc3Qgb2xkUGFyZW50UGF0aCA9IHBhdGguc2xpY2UoMCwgLTEpXG4gICAgY29uc3Qgb2xkSW5kZXggPSBwYXRoW3BhdGgubGVuZ3RoIC0gMV1cbiAgICBsZXQgeyBkb2N1bWVudCB9ID0gc3RhdGVcbiAgICBjb25zdCBub2RlID0gZG9jdW1lbnQuYXNzZXJ0UGF0aChwYXRoKVxuXG4gICAgLy8gUmVtb3ZlIHRoZSBub2RlIGZyb20gaXRzIGN1cnJlbnQgcGFyZW50LlxuICAgIGxldCBwYXJlbnQgPSBkb2N1bWVudC5nZXRQYXJlbnQobm9kZS5rZXkpXG4gICAgcGFyZW50ID0gcGFyZW50LnJlbW92ZU5vZGUob2xkSW5kZXgpXG4gICAgZG9jdW1lbnQgPSBkb2N1bWVudC51cGRhdGVOb2RlKHBhcmVudClcblxuICAgIC8vIEZpbmQgdGhlIG5ldyB0YXJnZXQuLi5cbiAgICBsZXQgdGFyZ2V0XG5cbiAgICAvLyBJZiB0aGUgb2xkIHBhdGggYW5kIHRoZSByZXN0IG9mIHRoZSBuZXcgcGF0aCBhcmUgdGhlIHNhbWUsIHRoZW4gdGhlIG5ld1xuICAgIC8vIHRhcmdldCBpcyB0aGUgb2xkIHBhcmVudC5cbiAgICBpZiAoXG4gICAgICAob2xkUGFyZW50UGF0aC5ldmVyeSgoeCwgaSkgPT4geCA9PT0gbmV3UGFyZW50UGF0aFtpXSkpICYmXG4gICAgICAob2xkUGFyZW50UGF0aC5sZW5ndGggPT09IG5ld1BhcmVudFBhdGgubGVuZ3RoKVxuICAgICkge1xuICAgICAgdGFyZ2V0ID0gcGFyZW50XG4gICAgfVxuXG4gICAgLy8gT3RoZXJ3aXNlLCBpZiB0aGUgb2xkIHBhdGggcmVtb3ZhbCByZXN1bHRlZCBpbiB0aGUgbmV3IHBhdGggYmVpbmcgbm8gbG9uZ2VyXG4gICAgLy8gY29ycmVjdCwgd2UgbmVlZCB0byBkZWNyZW1lbnQgdGhlIG5ldyBwYXRoIGF0IHRoZSBvbGQgcGF0aCdzIGxhc3QgaW5kZXguXG4gICAgZWxzZSBpZiAoXG4gICAgICAob2xkUGFyZW50UGF0aC5ldmVyeSgoeCwgaSkgPT4geCA9PT0gbmV3UGFyZW50UGF0aFtpXSkpICYmXG4gICAgICAob2xkSW5kZXggPCBuZXdQYXJlbnRQYXRoW29sZFBhcmVudFBhdGgubGVuZ3RoXSlcbiAgICApIHtcbiAgICAgIG5ld1BhcmVudFBhdGhbb2xkUGFyZW50UGF0aC5sZW5ndGhdLS1cbiAgICAgIHRhcmdldCA9IGRvY3VtZW50LmFzc2VydFBhdGgobmV3UGFyZW50UGF0aClcbiAgICB9XG5cbiAgICAvLyBPdGhlcndpc2UsIHdlIGNhbiBqdXN0IGdyYWIgdGhlIHRhcmdldCBub3JtYWxseS4uLlxuICAgIGVsc2Uge1xuICAgICAgdGFyZ2V0ID0gZG9jdW1lbnQuYXNzZXJ0UGF0aChuZXdQYXJlbnRQYXRoKVxuICAgIH1cblxuICAgIC8vIEluc2VydCB0aGUgbmV3IG5vZGUgdG8gaXRzIG5ldyBwYXJlbnQuXG4gICAgdGFyZ2V0ID0gdGFyZ2V0Lmluc2VydE5vZGUobmV3SW5kZXgsIG5vZGUpXG4gICAgZG9jdW1lbnQgPSBkb2N1bWVudC51cGRhdGVOb2RlKHRhcmdldClcbiAgICBzdGF0ZSA9IHN0YXRlLnNldCgnZG9jdW1lbnQnLCBkb2N1bWVudClcbiAgICByZXR1cm4gc3RhdGVcbiAgfSxcblxuICAvKipcbiAgICogUmVtb3ZlIG1hcmsgZnJvbSB0ZXh0IGF0IGBvZmZzZXRgIGFuZCBgbGVuZ3RoYCBpbiBub2RlIGJ5IGBwYXRoYC5cbiAgICpcbiAgICogQHBhcmFtIHtTdGF0ZX0gc3RhdGVcbiAgICogQHBhcmFtIHtPYmplY3R9IG9wZXJhdGlvblxuICAgKiBAcmV0dXJuIHtTdGF0ZX1cbiAgICovXG5cbiAgcmVtb3ZlX21hcmsoc3RhdGUsIG9wZXJhdGlvbikge1xuICAgIGNvbnN0IHsgcGF0aCwgb2Zmc2V0LCBsZW5ndGggfSA9IG9wZXJhdGlvblxuICAgIGNvbnN0IG1hcmsgPSBOb3JtYWxpemUubWFyayhvcGVyYXRpb24ubWFyaylcbiAgICBsZXQgeyBkb2N1bWVudCB9ID0gc3RhdGVcbiAgICBsZXQgbm9kZSA9IGRvY3VtZW50LmFzc2VydFBhdGgocGF0aClcbiAgICBub2RlID0gbm9kZS5yZW1vdmVNYXJrKG9mZnNldCwgbGVuZ3RoLCBtYXJrKVxuICAgIGRvY3VtZW50ID0gZG9jdW1lbnQudXBkYXRlTm9kZShub2RlKVxuICAgIHN0YXRlID0gc3RhdGUuc2V0KCdkb2N1bWVudCcsIGRvY3VtZW50KVxuICAgIHJldHVybiBzdGF0ZVxuICB9LFxuXG4gIC8qKlxuICAgKiBSZW1vdmUgYSBub2RlIGJ5IGBwYXRoYC5cbiAgICpcbiAgICogQHBhcmFtIHtTdGF0ZX0gc3RhdGVcbiAgICogQHBhcmFtIHtPYmplY3R9IG9wZXJhdGlvblxuICAgKiBAcmV0dXJuIHtTdGF0ZX1cbiAgICovXG5cbiAgcmVtb3ZlX25vZGUoc3RhdGUsIG9wZXJhdGlvbikge1xuICAgIGNvbnN0IHsgcGF0aCB9ID0gb3BlcmF0aW9uXG4gICAgbGV0IHsgZG9jdW1lbnQsIHNlbGVjdGlvbiB9ID0gc3RhdGVcbiAgICBjb25zdCB7IHN0YXJ0S2V5LCBlbmRLZXkgfSA9IHNlbGVjdGlvblxuICAgIGNvbnN0IG5vZGUgPSBkb2N1bWVudC5hc3NlcnRQYXRoKHBhdGgpXG5cbiAgICAvLyBJZiB0aGUgc2VsZWN0aW9uIGlzIHNldCwgY2hlY2sgdG8gc2VlIGlmIGl0IG5lZWRzIHRvIGJlIHVwZGF0ZWQuXG4gICAgaWYgKHNlbGVjdGlvbi5pc1NldCkge1xuICAgICAgY29uc3QgaGFzU3RhcnROb2RlID0gbm9kZS5oYXNOb2RlKHN0YXJ0S2V5KVxuICAgICAgY29uc3QgaGFzRW5kTm9kZSA9IG5vZGUuaGFzTm9kZShlbmRLZXkpXG4gICAgICBsZXQgbm9ybWFsaXplID0gZmFsc2VcblxuICAgICAgLy8gSWYgb25lIG9mIHRoZSBzZWxlY3Rpb24ncyBub2RlcyBpcyBiZWluZyByZW1vdmVkLCB3ZSBuZWVkIHRvIHVwZGF0ZSBpdC5cbiAgICAgIGlmIChoYXNTdGFydE5vZGUpIHtcbiAgICAgICAgY29uc3QgcHJldiA9IGRvY3VtZW50LmdldFByZXZpb3VzVGV4dChzdGFydEtleSlcbiAgICAgICAgY29uc3QgbmV4dCA9IGRvY3VtZW50LmdldE5leHRUZXh0KHN0YXJ0S2V5KVxuXG4gICAgICAgIGlmIChwcmV2KSB7XG4gICAgICAgICAgc2VsZWN0aW9uID0gc2VsZWN0aW9uLm1vdmVTdGFydFRvKHByZXYua2V5LCBwcmV2LnRleHQubGVuZ3RoKVxuICAgICAgICAgIG5vcm1hbGl6ZSA9IHRydWVcbiAgICAgICAgfSBlbHNlIGlmIChuZXh0KSB7XG4gICAgICAgICAgc2VsZWN0aW9uID0gc2VsZWN0aW9uLm1vdmVTdGFydFRvKG5leHQua2V5LCAwKVxuICAgICAgICAgIG5vcm1hbGl6ZSA9IHRydWVcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICBzZWxlY3Rpb24gPSBzZWxlY3Rpb24uZGVzZWxlY3QoKVxuICAgICAgICB9XG4gICAgICB9XG5cbiAgICAgIGlmIChoYXNFbmROb2RlKSB7XG4gICAgICAgIGNvbnN0IHByZXYgPSBkb2N1bWVudC5nZXRQcmV2aW91c1RleHQoZW5kS2V5KVxuICAgICAgICBjb25zdCBuZXh0ID0gZG9jdW1lbnQuZ2V0TmV4dFRleHQoZW5kS2V5KVxuXG4gICAgICAgIGlmIChwcmV2KSB7XG4gICAgICAgICAgc2VsZWN0aW9uID0gc2VsZWN0aW9uLm1vdmVFbmRUbyhwcmV2LmtleSwgcHJldi50ZXh0Lmxlbmd0aClcbiAgICAgICAgICBub3JtYWxpemUgPSB0cnVlXG4gICAgICAgIH0gZWxzZSBpZiAobmV4dCkge1xuICAgICAgICAgIHNlbGVjdGlvbiA9IHNlbGVjdGlvbi5tb3ZlRW5kVG8obmV4dC5rZXksIDApXG4gICAgICAgICAgbm9ybWFsaXplID0gdHJ1ZVxuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIHNlbGVjdGlvbiA9IHNlbGVjdGlvbi5kZXNlbGVjdCgpXG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgaWYgKG5vcm1hbGl6ZSkge1xuICAgICAgICBzZWxlY3Rpb24gPSBzZWxlY3Rpb24ubm9ybWFsaXplKGRvY3VtZW50KVxuICAgICAgfVxuICAgIH1cblxuICAgIC8vIFJlbW92ZSB0aGUgbm9kZSBmcm9tIHRoZSBkb2N1bWVudC5cbiAgICBsZXQgcGFyZW50ID0gZG9jdW1lbnQuZ2V0UGFyZW50KG5vZGUua2V5KVxuICAgIGNvbnN0IGluZGV4ID0gcGFyZW50Lm5vZGVzLmluZGV4T2Yobm9kZSlcbiAgICBwYXJlbnQgPSBwYXJlbnQucmVtb3ZlTm9kZShpbmRleClcbiAgICBkb2N1bWVudCA9IGRvY3VtZW50LnVwZGF0ZU5vZGUocGFyZW50KVxuXG4gICAgLy8gVXBkYXRlIHRoZSBkb2N1bWVudCBhbmQgc2VsZWN0aW9uLlxuICAgIHN0YXRlID0gc3RhdGUuc2V0KCdkb2N1bWVudCcsIGRvY3VtZW50KS5zZXQoJ3NlbGVjdGlvbicsIHNlbGVjdGlvbilcbiAgICByZXR1cm4gc3RhdGVcbiAgfSxcblxuICAvKipcbiAgICogUmVtb3ZlIGB0ZXh0YCBhdCBgb2Zmc2V0YCBpbiBub2RlIGJ5IGBwYXRoYC5cbiAgICpcbiAgICogQHBhcmFtIHtTdGF0ZX0gc3RhdGVcbiAgICogQHBhcmFtIHtPYmplY3R9IG9wZXJhdGlvblxuICAgKiBAcmV0dXJuIHtTdGF0ZX1cbiAgICovXG5cbiAgcmVtb3ZlX3RleHQoc3RhdGUsIG9wZXJhdGlvbikge1xuICAgIGNvbnN0IHsgcGF0aCwgb2Zmc2V0LCB0ZXh0IH0gPSBvcGVyYXRpb25cbiAgICBjb25zdCB7IGxlbmd0aCB9ID0gdGV4dFxuICAgIGNvbnN0IHJhbmdlT2Zmc2V0ID0gb2Zmc2V0ICsgbGVuZ3RoXG4gICAgbGV0IHsgZG9jdW1lbnQsIHNlbGVjdGlvbiB9ID0gc3RhdGVcbiAgICBjb25zdCB7IGFuY2hvcktleSwgZm9jdXNLZXksIGFuY2hvck9mZnNldCwgZm9jdXNPZmZzZXQgfSA9IHNlbGVjdGlvblxuICAgIGxldCBub2RlID0gZG9jdW1lbnQuYXNzZXJ0UGF0aChwYXRoKVxuXG4gICAgLy8gVXBkYXRlIHRoZSBzZWxlY3Rpb24uXG4gICAgaWYgKGFuY2hvcktleSA9PSBub2RlLmtleSAmJiBhbmNob3JPZmZzZXQgPj0gcmFuZ2VPZmZzZXQpIHtcbiAgICAgIHNlbGVjdGlvbiA9IHNlbGVjdGlvbi5tb3ZlQW5jaG9yKC1sZW5ndGgpXG4gICAgfVxuXG4gICAgaWYgKGZvY3VzS2V5ID09IG5vZGUua2V5ICYmIGZvY3VzT2Zmc2V0ID49IHJhbmdlT2Zmc2V0KSB7XG4gICAgICBzZWxlY3Rpb24gPSBzZWxlY3Rpb24ubW92ZUZvY3VzKC1sZW5ndGgpXG4gICAgfVxuXG4gICAgbm9kZSA9IG5vZGUucmVtb3ZlVGV4dChvZmZzZXQsIGxlbmd0aClcbiAgICBkb2N1bWVudCA9IGRvY3VtZW50LnVwZGF0ZU5vZGUobm9kZSlcbiAgICBzdGF0ZSA9IHN0YXRlLnNldCgnZG9jdW1lbnQnLCBkb2N1bWVudCkuc2V0KCdzZWxlY3Rpb24nLCBzZWxlY3Rpb24pXG4gICAgcmV0dXJuIHN0YXRlXG4gIH0sXG5cbiAgLyoqXG4gICAqIFNldCBgZGF0YWAgb24gYHN0YXRlYC5cbiAgICpcbiAgICogQHBhcmFtIHtTdGF0ZX0gc3RhdGVcbiAgICogQHBhcmFtIHtPYmplY3R9IG9wZXJhdGlvblxuICAgKiBAcmV0dXJuIHtTdGF0ZX1cbiAgICovXG5cbiAgc2V0X2RhdGEoc3RhdGUsIG9wZXJhdGlvbikge1xuICAgIGNvbnN0IHsgcHJvcGVydGllcyB9ID0gb3BlcmF0aW9uXG4gICAgbGV0IHsgZGF0YSB9ID0gc3RhdGVcblxuICAgIGRhdGEgPSBkYXRhLm1lcmdlKHByb3BlcnRpZXMpXG4gICAgc3RhdGUgPSBzdGF0ZS5zZXQoJ2RhdGEnLCBkYXRhKVxuICAgIHJldHVybiBzdGF0ZVxuICB9LFxuXG4gIC8qKlxuICAgKiBTZXQgYHByb3BlcnRpZXNgIG9uIG1hcmsgb24gdGV4dCBhdCBgb2Zmc2V0YCBhbmQgYGxlbmd0aGAgaW4gbm9kZSBieSBgcGF0aGAuXG4gICAqXG4gICAqIEBwYXJhbSB7U3RhdGV9IHN0YXRlXG4gICAqIEBwYXJhbSB7T2JqZWN0fSBvcGVyYXRpb25cbiAgICogQHJldHVybiB7U3RhdGV9XG4gICAqL1xuXG4gIHNldF9tYXJrKHN0YXRlLCBvcGVyYXRpb24pIHtcbiAgICBjb25zdCB7IHBhdGgsIG9mZnNldCwgbGVuZ3RoLCBwcm9wZXJ0aWVzIH0gPSBvcGVyYXRpb25cbiAgICBjb25zdCBtYXJrID0gTm9ybWFsaXplLm1hcmsob3BlcmF0aW9uLm1hcmspXG4gICAgbGV0IHsgZG9jdW1lbnQgfSA9IHN0YXRlXG4gICAgbGV0IG5vZGUgPSBkb2N1bWVudC5hc3NlcnRQYXRoKHBhdGgpXG4gICAgbm9kZSA9IG5vZGUudXBkYXRlTWFyayhvZmZzZXQsIGxlbmd0aCwgbWFyaywgcHJvcGVydGllcylcbiAgICBkb2N1bWVudCA9IGRvY3VtZW50LnVwZGF0ZU5vZGUobm9kZSlcbiAgICBzdGF0ZSA9IHN0YXRlLnNldCgnZG9jdW1lbnQnLCBkb2N1bWVudClcbiAgICByZXR1cm4gc3RhdGVcbiAgfSxcblxuICAvKipcbiAgICogU2V0IGBwcm9wZXJ0aWVzYCBvbiBhIG5vZGUgYnkgYHBhdGhgLlxuICAgKlxuICAgKiBAcGFyYW0ge1N0YXRlfSBzdGF0ZVxuICAgKiBAcGFyYW0ge09iamVjdH0gb3BlcmF0aW9uXG4gICAqIEByZXR1cm4ge1N0YXRlfVxuICAgKi9cblxuICBzZXRfbm9kZShzdGF0ZSwgb3BlcmF0aW9uKSB7XG4gICAgY29uc3QgeyBwYXRoLCBwcm9wZXJ0aWVzIH0gPSBvcGVyYXRpb25cbiAgICBsZXQgeyBkb2N1bWVudCB9ID0gc3RhdGVcbiAgICBsZXQgbm9kZSA9IGRvY3VtZW50LmFzc2VydFBhdGgocGF0aClcblxuICAgIC8vIFdhcm4gd2hlbiB0cnlpbmcgdG8gb3ZlcndpdGUgYSBub2RlJ3MgY2hpbGRyZW4uXG4gICAgaWYgKHByb3BlcnRpZXMubm9kZXMgJiYgcHJvcGVydGllcy5ub2RlcyAhPSBub2RlLm5vZGVzKSB7XG4gICAgICBsb2dnZXIud2FybignVXBkYXRpbmcgYSBOb2RlXFwncyBgbm9kZXNgIHByb3BlcnR5IHZpYSBgc2V0Tm9kZSgpYCBpcyBub3QgYWxsb3dlZC4gVXNlIHRoZSBhcHByb3ByaWF0ZSBpbnNlcnRpb24gYW5kIHJlbW92YWwgb3BlcmF0aW9ucyBpbnN0ZWFkLiBUaGUgb3BlYXJ0aW9uIGluIHF1ZXN0aW9uIHdhczonLCBvcGVyYXRpb24pXG4gICAgICBkZWxldGUgcHJvcGVydGllcy5ub2Rlc1xuICAgIH1cblxuICAgIC8vIFdhcm4gd2hlbiB0cnlpbmcgdG8gY2hhbmdlIGEgbm9kZSdzIGtleS5cbiAgICBpZiAocHJvcGVydGllcy5rZXkgJiYgcHJvcGVydGllcy5rZXkgIT0gbm9kZS5rZXkpIHtcbiAgICAgIGxvZ2dlci53YXJuKCdVcGRhdGluZyBhIE5vZGVcXCdzIGBrZXlgIHByb3BlcnR5IHZpYSBgc2V0Tm9kZSgpYCBpcyBub3QgYWxsb3dlZC4gVGhlcmUgc2hvdWxkIGJlIG5vIHJlYXNvbiB0byBkbyB0aGlzLiBUaGUgb3BlYXJ0aW9uIGluIHF1ZXN0aW9uIHdhczonLCBvcGVyYXRpb24pXG4gICAgICBkZWxldGUgcHJvcGVydGllcy5rZXlcbiAgICB9XG5cbiAgICBub2RlID0gbm9kZS5tZXJnZShwcm9wZXJ0aWVzKVxuICAgIGRvY3VtZW50ID0gZG9jdW1lbnQudXBkYXRlTm9kZShub2RlKVxuICAgIHN0YXRlID0gc3RhdGUuc2V0KCdkb2N1bWVudCcsIGRvY3VtZW50KVxuICAgIHJldHVybiBzdGF0ZVxuICB9LFxuXG4gIC8qKlxuICAgKiBTZXQgYHByb3BlcnRpZXNgIG9uIHRoZSBzZWxlY3Rpb24uXG4gICAqXG4gICAqIEBwYXJhbSB7U3RhdGV9IHN0YXRlXG4gICAqIEBwYXJhbSB7T2JqZWN0fSBvcGVyYXRpb25cbiAgICogQHJldHVybiB7U3RhdGV9XG4gICAqL1xuXG4gIHNldF9zZWxlY3Rpb24oc3RhdGUsIG9wZXJhdGlvbikge1xuICAgIGNvbnN0IHByb3BlcnRpZXMgPSB7IC4uLm9wZXJhdGlvbi5wcm9wZXJ0aWVzIH1cbiAgICBsZXQgeyBkb2N1bWVudCwgc2VsZWN0aW9uIH0gPSBzdGF0ZVxuXG4gICAgaWYgKHByb3BlcnRpZXMubWFya3MgIT09IHVuZGVmaW5lZCkge1xuICAgICAgcHJvcGVydGllcy5tYXJrcyA9IE5vcm1hbGl6ZS5tYXJrcyhwcm9wZXJ0aWVzLm1hcmtzKVxuICAgIH1cblxuICAgIGlmIChwcm9wZXJ0aWVzLmFuY2hvclBhdGggIT09IHVuZGVmaW5lZCkge1xuICAgICAgcHJvcGVydGllcy5hbmNob3JLZXkgPSBwcm9wZXJ0aWVzLmFuY2hvclBhdGggPT09IG51bGxcbiAgICAgICAgPyBudWxsXG4gICAgICAgIDogZG9jdW1lbnQuYXNzZXJ0UGF0aChwcm9wZXJ0aWVzLmFuY2hvclBhdGgpLmtleVxuICAgICAgZGVsZXRlIHByb3BlcnRpZXMuYW5jaG9yUGF0aFxuICAgIH1cblxuICAgIGlmIChwcm9wZXJ0aWVzLmZvY3VzUGF0aCAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICBwcm9wZXJ0aWVzLmZvY3VzS2V5ID0gcHJvcGVydGllcy5mb2N1c1BhdGggPT09IG51bGxcbiAgICAgICAgPyBudWxsXG4gICAgICAgIDogZG9jdW1lbnQuYXNzZXJ0UGF0aChwcm9wZXJ0aWVzLmZvY3VzUGF0aCkua2V5XG4gICAgICBkZWxldGUgcHJvcGVydGllcy5mb2N1c1BhdGhcbiAgICB9XG5cbiAgICBzZWxlY3Rpb24gPSBzZWxlY3Rpb24ubWVyZ2UocHJvcGVydGllcylcbiAgICBzZWxlY3Rpb24gPSBzZWxlY3Rpb24ubm9ybWFsaXplKGRvY3VtZW50KVxuICAgIHN0YXRlID0gc3RhdGUuc2V0KCdzZWxlY3Rpb24nLCBzZWxlY3Rpb24pXG4gICAgcmV0dXJuIHN0YXRlXG4gIH0sXG5cbiAgLyoqXG4gICAqIFNwbGl0IGEgbm9kZSBieSBgcGF0aGAgYXQgYG9mZnNldGAuXG4gICAqXG4gICAqIEBwYXJhbSB7U3RhdGV9IHN0YXRlXG4gICAqIEBwYXJhbSB7T2JqZWN0fSBvcGVyYXRpb25cbiAgICogQHJldHVybiB7U3RhdGV9XG4gICAqL1xuXG4gIHNwbGl0X25vZGUoc3RhdGUsIG9wZXJhdGlvbikge1xuICAgIGNvbnN0IHsgcGF0aCwgcG9zaXRpb24gfSA9IG9wZXJhdGlvblxuICAgIGxldCB7IGRvY3VtZW50LCBzZWxlY3Rpb24gfSA9IHN0YXRlXG5cbiAgICAvLyBDYWxjdWxhdGUgYSBmZXcgdGhpbmdzLi4uXG4gICAgY29uc3Qgbm9kZSA9IGRvY3VtZW50LmFzc2VydFBhdGgocGF0aClcbiAgICBsZXQgcGFyZW50ID0gZG9jdW1lbnQuZ2V0UGFyZW50KG5vZGUua2V5KVxuICAgIGNvbnN0IGluZGV4ID0gcGFyZW50Lm5vZGVzLmluZGV4T2Yobm9kZSlcblxuICAgIC8vIFNwbGl0IHRoZSBub2RlIGJ5IGl0cyBwYXJlbnQuXG4gICAgcGFyZW50ID0gcGFyZW50LnNwbGl0Tm9kZShpbmRleCwgcG9zaXRpb24pXG4gICAgZG9jdW1lbnQgPSBkb2N1bWVudC51cGRhdGVOb2RlKHBhcmVudClcblxuICAgIC8vIERldGVybWluZSB3aGV0aGVyIHdlIG5lZWQgdG8gdXBkYXRlIHRoZSBzZWxlY3Rpb24uLi5cbiAgICBjb25zdCB7IHN0YXJ0S2V5LCBlbmRLZXksIHN0YXJ0T2Zmc2V0LCBlbmRPZmZzZXQgfSA9IHNlbGVjdGlvblxuICAgIGNvbnN0IG5leHQgPSBkb2N1bWVudC5nZXROZXh0VGV4dChub2RlLmtleSlcbiAgICBsZXQgbm9ybWFsaXplID0gZmFsc2VcblxuICAgIC8vIElmIHRoZSBzdGFydCBwb2ludCBpcyBhZnRlciBvciBlcXVhbCB0byB0aGUgc3BsaXQsIHVwZGF0ZSBpdC5cbiAgICBpZiAobm9kZS5rZXkgPT0gc3RhcnRLZXkgJiYgcG9zaXRpb24gPD0gc3RhcnRPZmZzZXQpIHtcbiAgICAgIHNlbGVjdGlvbiA9IHNlbGVjdGlvbi5tb3ZlU3RhcnRUbyhuZXh0LmtleSwgc3RhcnRPZmZzZXQgLSBwb3NpdGlvbilcbiAgICAgIG5vcm1hbGl6ZSA9IHRydWVcbiAgICB9XG5cbiAgICAvLyBJZiB0aGUgZW5kIHBvaW50IGlzIGFmdGVyIG9yIGVxdWFsIHRvIHRoZSBzcGxpdCwgdXBkYXRlIGl0LlxuICAgIGlmIChub2RlLmtleSA9PSBlbmRLZXkgJiYgcG9zaXRpb24gPD0gZW5kT2Zmc2V0KSB7XG4gICAgICBzZWxlY3Rpb24gPSBzZWxlY3Rpb24ubW92ZUVuZFRvKG5leHQua2V5LCBlbmRPZmZzZXQgLSBwb3NpdGlvbilcbiAgICAgIG5vcm1hbGl6ZSA9IHRydWVcbiAgICB9XG5cbiAgICAvLyBOb3JtYWxpemUgdGhlIHNlbGVjdGlvbiBpZiB3ZSBjaGFuZ2VkIGl0LCBzaW5jZSB0aGUgbWV0aG9kcyB3ZSB1c2UgbWlnaHRcbiAgICAvLyBsZWF2ZSBpdCBpbiBhIG5vbi1ub3JtYWxpemVkIHN0YXRlLlxuICAgIGlmIChub3JtYWxpemUpIHtcbiAgICAgIHNlbGVjdGlvbiA9IHNlbGVjdGlvbi5ub3JtYWxpemUoZG9jdW1lbnQpXG4gICAgfVxuXG4gICAgLy8gUmV0dXJuIHRoZSB1cGRhdGVkIHN0YXRlLlxuICAgIHN0YXRlID0gc3RhdGUuc2V0KCdkb2N1bWVudCcsIGRvY3VtZW50KS5zZXQoJ3NlbGVjdGlvbicsIHNlbGVjdGlvbilcbiAgICByZXR1cm4gc3RhdGVcbiAgfSxcblxufVxuXG4vKipcbiAqIEFwcGx5IGFuIGBvcGVyYXRpb25gIHRvIGEgYHN0YXRlYC5cbiAqXG4gKiBAcGFyYW0ge1N0YXRlfSBzdGF0ZVxuICogQHBhcmFtIHtPYmplY3R9IG9wZXJhdGlvblxuICogQHJldHVybiB7U3RhdGV9IHN0YXRlXG4gKi9cblxuZnVuY3Rpb24gYXBwbHlPcGVyYXRpb24oc3RhdGUsIG9wZXJhdGlvbikge1xuICBjb25zdCB7IHR5cGUgfSA9IG9wZXJhdGlvblxuICBjb25zdCBhcHBseSA9IEFQUExJRVJTW3R5cGVdXG5cbiAgaWYgKCFhcHBseSkge1xuICAgIHRocm93IG5ldyBFcnJvcihgVW5rbm93biBvcGVyYXRpb24gdHlwZTogXCIke3R5cGV9XCIuYClcbiAgfVxuXG4gIGRlYnVnKHR5cGUsIG9wZXJhdGlvbilcbiAgc3RhdGUgPSBhcHBseShzdGF0ZSwgb3BlcmF0aW9uKVxuICByZXR1cm4gc3RhdGVcbn1cblxuLyoqXG4gKiBFeHBvcnQuXG4gKlxuICogQHR5cGUge0Z1bmN0aW9ufVxuICovXG5cbmV4cG9ydCBkZWZhdWx0IGFwcGx5T3BlcmF0aW9uXG4iXX0=