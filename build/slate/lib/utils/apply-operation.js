'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _debug = require('debug');

var _debug2 = _interopRequireDefault(_debug);

var _normalize = require('../utils/normalize');

var _normalize2 = _interopRequireDefault(_normalize);

var _warn = require('../utils/warn');

var _warn2 = _interopRequireDefault(_warn);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Debug.
 *
 * @type {Function}
 */

var debug = (0, _debug2.default)('slate:operation');

/**
 * Operations.
 *
 * @type {Object}
 */

var OPERATIONS = {
  // Text operations.
  insert_text: insertText,
  remove_text: removeText,
  // Mark operations.
  add_mark: addMark,
  remove_mark: removeMark,
  set_mark: setMark,
  // Node operations.
  insert_node: insertNode,
  join_node: joinNode,
  move_node: moveNode,
  remove_node: removeNode,
  set_node: setNode,
  split_node: splitNode,
  // Selection operations.
  set_selection: setSelection,
  // State data operations.
  set_data: setData
};

/**
 * Apply an `operation` to a `state`.
 *
 * @param {State} state
 * @param {Object} operation
 * @return {State} state
 */

function apply(state, operation) {
  var type = operation.type;

  var fn = OPERATIONS[type];

  if (!fn) {
    throw new Error('Unknown operation type: "' + type + '".');
  }

  debug(type, operation);
  state = fn(state, operation);
  return state;
}

/**
 * Add mark to text at `offset` and `length` in node by `path`.
 *
 * @param {State} state
 * @param {Object} operation
 * @return {State}
 */

function addMark(state, operation) {
  var path = operation.path,
      offset = operation.offset,
      length = operation.length;

  var mark = _normalize2.default.mark(operation.mark);
  var _state = state,
      document = _state.document;

  var node = document.assertPath(path);
  node = node.addMark(offset, length, mark);
  document = document.updateDescendant(node);
  state = state.set('document', document);
  return state;
}

/**
 * Insert a `node` at `index` in a node by `path`.
 *
 * @param {State} state
 * @param {Object} operation
 * @return {State}
 */

function insertNode(state, operation) {
  var path = operation.path;

  var node = _normalize2.default.node(operation.node);
  var index = path[path.length - 1];
  var rest = path.slice(0, -1);
  var _state2 = state,
      document = _state2.document;

  var parent = document.assertPath(rest);
  var isParent = document == parent;
  parent = parent.insertNode(index, node);
  document = isParent ? parent : document.updateDescendant(parent);
  state = state.set('document', document);
  return state;
}

/**
 * Insert `text` at `offset` in node by `path`.
 *
 * @param {State} state
 * @param {Object} operation
 * @return {State}
 */

function insertText(state, operation) {
  var path = operation.path,
      offset = operation.offset,
      text = operation.text;

  var marks = operation.marks && operation.marks.map(_normalize2.default.mark);
  var _state3 = state,
      document = _state3.document,
      selection = _state3.selection;
  var _selection = selection,
      anchorKey = _selection.anchorKey,
      focusKey = _selection.focusKey,
      anchorOffset = _selection.anchorOffset,
      focusOffset = _selection.focusOffset;

  var node = document.assertPath(path);

  // Update the document
  node = node.insertText(offset, text, marks);
  document = document.updateDescendant(node);

  // Update the selection
  if (anchorKey == node.key && anchorOffset >= offset) {
    selection = selection.moveAnchor(text.length);
  }
  if (focusKey == node.key && focusOffset >= offset) {
    selection = selection.moveFocus(text.length);
  }

  state = state.set('document', document).set('selection', selection);
  return state;
}

/**
 * Join a node by `path` with a node `withPath`.
 *
 * @param {State} state
 * @param {Object} operation
 *   @param {Boolean} operation.deep (optional) Join recursively the
 *   respective last node and first node of the nodes' children. Like a zipper :)
 * @return {State}
 */

function joinNode(state, operation) {
  var path = operation.path,
      withPath = operation.withPath;
  var _state4 = state,
      document = _state4.document,
      selection = _state4.selection;

  var one = document.assertPath(withPath);
  var two = document.assertPath(path);
  var parent = document.getParent(one.key);
  var oneIndex = parent.nodes.indexOf(one);
  var twoIndex = parent.nodes.indexOf(two);

  // Perform the join in the document.
  parent = parent.joinNode(oneIndex, twoIndex);
  document = parent.key == document.key ? parent : document.updateDescendant(parent);

  // If the nodes are text nodes and the selection is inside the second node
  // update it to refer to the first node instead.
  if (one.kind == 'text') {
    var _selection2 = selection,
        anchorKey = _selection2.anchorKey,
        anchorOffset = _selection2.anchorOffset,
        focusKey = _selection2.focusKey,
        focusOffset = _selection2.focusOffset;


    if (anchorKey == two.key) {
      selection = selection.moveAnchorTo(one.key, one.length + anchorOffset);
    }

    if (focusKey == two.key) {
      selection = selection.moveFocusTo(one.key, one.length + focusOffset);
    }
  }

  // Update the document and selection.
  state = state.set('document', document).set('selection', selection);
  return state;
}

/**
 * Move a node by `path` to `newPath`.
 *
 * @param {State} state
 * @param {Object} operation
 * @return {State}
 */

function moveNode(state, operation) {
  var path = operation.path,
      newPath = operation.newPath;

  var newIndex = newPath[newPath.length - 1];
  var newParentPath = newPath.slice(0, -1);
  var oldParentPath = path.slice(0, -1);
  var oldIndex = path[path.length - 1];
  var _state5 = state,
      document = _state5.document;

  var node = document.assertPath(path);

  // Remove the node from its current parent.
  var parent = document.getParent(node.key);
  parent = parent.removeNode(oldIndex);
  document = parent.kind === 'document' ? parent : document.updateDescendant(parent);

  // Find the new target...
  var target = void 0;

  // If the old path and the rest of the new path are the same, then the new
  // target is the old parent.
  if (oldParentPath.every(function (x, i) {
    return x === newParentPath[i];
  }) && oldParentPath.length === newParentPath.length) {
    target = parent;
  }

  // Otherwise, if the old path removal resulted in the new path being no longer
  // correct, we need to decrement the new path at the old path's last index.
  else if (oldParentPath.every(function (x, i) {
      return x === newParentPath[i];
    }) && oldIndex < newParentPath[oldParentPath.length]) {
      newParentPath[oldParentPath.length]--;
      target = document.assertPath(newParentPath);
    }

    // Otherwise, we can just grab the target normally...
    else {
        target = document.assertPath(newParentPath);
      }

  // Insert the new node to its new parent.
  target = target.insertNode(newIndex, node);
  document = target.kind === 'document' ? target : document.updateDescendant(target);
  state = state.set('document', document);
  return state;
}

/**
 * Remove mark from text at `offset` and `length` in node by `path`.
 *
 * @param {State} state
 * @param {Object} operation
 * @return {State}
 */

function removeMark(state, operation) {
  var path = operation.path,
      offset = operation.offset,
      length = operation.length;

  var mark = _normalize2.default.mark(operation.mark);
  var _state6 = state,
      document = _state6.document;

  var node = document.assertPath(path);
  node = node.removeMark(offset, length, mark);
  document = document.updateDescendant(node);
  state = state.set('document', document);
  return state;
}

/**
 * Remove a node by `path`.
 *
 * @param {State} state
 * @param {Object} operation
 * @return {State}
 */

function removeNode(state, operation) {
  var path = operation.path;
  var _state7 = state,
      document = _state7.document,
      selection = _state7.selection;
  var _selection3 = selection,
      startKey = _selection3.startKey,
      endKey = _selection3.endKey;

  var node = document.assertPath(path);

  // If the selection is set, check to see if it needs to be updated.
  if (selection.isSet) {
    var hasStartNode = node.hasNode(startKey);
    var hasEndNode = node.hasNode(endKey);

    // If one of the selection's nodes is being removed, we need to update it.
    if (hasStartNode) {
      var prev = document.getPreviousText(startKey);
      var next = document.getNextText(startKey);

      if (prev) {
        selection = selection.moveStartTo(prev.key, prev.length);
      } else if (next) {
        selection = selection.moveStartTo(next.key, 0);
      } else {
        selection = selection.deselect();
      }
    }

    if (hasEndNode) {
      var _prev = document.getPreviousText(endKey);
      var _next = document.getNextText(endKey);

      if (_prev) {
        selection = selection.moveEndTo(_prev.key, _prev.length);
      } else if (_next) {
        selection = selection.moveEndTo(_next.key, 0);
      } else {
        selection = selection.deselect();
      }
    }
  }

  // Remove the node from the document.
  var parent = document.getParent(node.key);
  var index = parent.nodes.indexOf(node);
  var isParent = document == parent;
  parent = parent.removeNode(index);
  document = isParent ? parent : document.updateDescendant(parent);

  // Update the document and selection.
  state = state.set('document', document).set('selection', selection);
  return state;
}

/**
 * Remove text at `offset` and `length` in node by `path`.
 *
 * @param {State} state
 * @param {Object} operation
 * @return {State}
 */

function removeText(state, operation) {
  var path = operation.path,
      offset = operation.offset,
      length = operation.length;

  var rangeOffset = offset + length;
  var _state8 = state,
      document = _state8.document,
      selection = _state8.selection;
  var _selection4 = selection,
      anchorKey = _selection4.anchorKey,
      focusKey = _selection4.focusKey,
      anchorOffset = _selection4.anchorOffset,
      focusOffset = _selection4.focusOffset;

  var node = document.assertPath(path);

  // Update the selection
  if (anchorKey == node.key && anchorOffset >= rangeOffset) {
    selection = selection.moveAnchor(-length);
  }
  if (focusKey == node.key && focusOffset >= rangeOffset) {
    selection = selection.moveFocus(-length);
  }

  node = node.removeText(offset, length);
  document = document.updateDescendant(node);
  state = state.set('document', document).set('selection', selection);
  return state;
}

/**
 * Set `data` on `state`.
 *
 * @param {State} state
 * @param {Object} operation
 * @return {State}
 */

function setData(state, operation) {
  var properties = operation.properties;
  var _state9 = state,
      data = _state9.data;


  data = data.merge(properties);
  state = state.set('data', data);
  return state;
}

/**
 * Set `properties` on mark on text at `offset` and `length` in node by `path`.
 *
 * @param {State} state
 * @param {Object} operation
 * @return {State}
 */

function setMark(state, operation) {
  var path = operation.path,
      offset = operation.offset,
      length = operation.length,
      properties = operation.properties;

  var mark = _normalize2.default.mark(operation.mark);
  var _state10 = state,
      document = _state10.document;

  var node = document.assertPath(path);
  node = node.updateMark(offset, length, mark, properties);
  document = document.updateDescendant(node);
  state = state.set('document', document);
  return state;
}

/**
 * Set `properties` on a node by `path`.
 *
 * @param {State} state
 * @param {Object} operation
 * @return {State}
 */

function setNode(state, operation) {
  var path = operation.path,
      properties = operation.properties;
  var _state11 = state,
      document = _state11.document;

  var node = document.assertPath(path);

  // Deprecate the ability to overwite a node's children.
  if (properties.nodes && properties.nodes != node.nodes) {
    (0, _warn2.default)('Updating a Node\'s `nodes` property via `setNode()` is not allowed. Use the appropriate insertion and removal operations instead. The opeartion in question was:', operation);
    delete properties.nodes;
  }

  // Deprecate the ability to change a node's key.
  if (properties.key && properties.key != node.key) {
    (0, _warn2.default)('Updating a Node\'s `key` property via `setNode()` is not allowed. There should be no reason to do this. The opeartion in question was:', operation);
    delete properties.key;
  }

  node = node.merge(properties);
  document = node.kind === 'document' ? node : document.updateDescendant(node);
  state = state.set('document', document);
  return state;
}

/**
 * Set `properties` on the selection.
 *
 * @param {State} state
 * @param {Object} operation
 * @return {State}
 */

function setSelection(state, operation) {
  var properties = _extends({}, operation.properties);
  var _state12 = state,
      document = _state12.document,
      selection = _state12.selection;


  if (properties.anchorPath !== undefined) {
    properties.anchorKey = properties.anchorPath === null ? null : document.assertPath(properties.anchorPath).key;
    delete properties.anchorPath;
  }

  if (properties.focusPath !== undefined) {
    properties.focusKey = properties.focusPath === null ? null : document.assertPath(properties.focusPath).key;
    delete properties.focusPath;
  }

  selection = selection.merge(properties);
  selection = selection.normalize(document);
  state = state.set('selection', selection);
  return state;
}

/**
 * Split a node by `path` at `offset`.
 *
 * @param {State} state
 * @param {Object} operation
 * @return {State}
 */

function splitNode(state, operation) {
  var path = operation.path,
      position = operation.position;
  var _state13 = state,
      document = _state13.document,
      selection = _state13.selection;

  // Calculate a few things...

  var node = document.assertPath(path);
  var parent = document.getParent(node.key);
  var index = parent.nodes.indexOf(node);
  var isParent = parent == document;

  // Split the node by its parent.
  parent = parent.splitNode(index, position);
  document = isParent ? parent : document.updateDescendant(parent);

  // Determine whether we need to update the selection...
  var _selection5 = selection,
      startKey = _selection5.startKey,
      endKey = _selection5.endKey,
      startOffset = _selection5.startOffset,
      endOffset = _selection5.endOffset;

  var next = document.getNextText(node.key);

  // If the start point is after or equal to the split, update it.
  if (node.key == startKey && position <= startOffset) {
    selection = selection.moveStartTo(next.key, startOffset - position);
  }

  // If the end point is after or equal to the split, update it.
  if (node.key == endKey && position <= endOffset) {
    selection = selection.moveEndTo(next.key, endOffset - position);
  }

  // Return the updated state.
  state = state.set('document', document).set('selection', selection);
  return state;
}

/**
 * Export.
 *
 * @type {Function}
 */

exports.default = apply;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy91dGlscy9hcHBseS1vcGVyYXRpb24uanMiXSwibmFtZXMiOlsiZGVidWciLCJPUEVSQVRJT05TIiwiaW5zZXJ0X3RleHQiLCJpbnNlcnRUZXh0IiwicmVtb3ZlX3RleHQiLCJyZW1vdmVUZXh0IiwiYWRkX21hcmsiLCJhZGRNYXJrIiwicmVtb3ZlX21hcmsiLCJyZW1vdmVNYXJrIiwic2V0X21hcmsiLCJzZXRNYXJrIiwiaW5zZXJ0X25vZGUiLCJpbnNlcnROb2RlIiwiam9pbl9ub2RlIiwiam9pbk5vZGUiLCJtb3ZlX25vZGUiLCJtb3ZlTm9kZSIsInJlbW92ZV9ub2RlIiwicmVtb3ZlTm9kZSIsInNldF9ub2RlIiwic2V0Tm9kZSIsInNwbGl0X25vZGUiLCJzcGxpdE5vZGUiLCJzZXRfc2VsZWN0aW9uIiwic2V0U2VsZWN0aW9uIiwic2V0X2RhdGEiLCJzZXREYXRhIiwiYXBwbHkiLCJzdGF0ZSIsIm9wZXJhdGlvbiIsInR5cGUiLCJmbiIsIkVycm9yIiwicGF0aCIsIm9mZnNldCIsImxlbmd0aCIsIm1hcmsiLCJkb2N1bWVudCIsIm5vZGUiLCJhc3NlcnRQYXRoIiwidXBkYXRlRGVzY2VuZGFudCIsInNldCIsImluZGV4IiwicmVzdCIsInNsaWNlIiwicGFyZW50IiwiaXNQYXJlbnQiLCJ0ZXh0IiwibWFya3MiLCJtYXAiLCJzZWxlY3Rpb24iLCJhbmNob3JLZXkiLCJmb2N1c0tleSIsImFuY2hvck9mZnNldCIsImZvY3VzT2Zmc2V0Iiwia2V5IiwibW92ZUFuY2hvciIsIm1vdmVGb2N1cyIsIndpdGhQYXRoIiwib25lIiwidHdvIiwiZ2V0UGFyZW50Iiwib25lSW5kZXgiLCJub2RlcyIsImluZGV4T2YiLCJ0d29JbmRleCIsImtpbmQiLCJtb3ZlQW5jaG9yVG8iLCJtb3ZlRm9jdXNUbyIsIm5ld1BhdGgiLCJuZXdJbmRleCIsIm5ld1BhcmVudFBhdGgiLCJvbGRQYXJlbnRQYXRoIiwib2xkSW5kZXgiLCJ0YXJnZXQiLCJldmVyeSIsIngiLCJpIiwic3RhcnRLZXkiLCJlbmRLZXkiLCJpc1NldCIsImhhc1N0YXJ0Tm9kZSIsImhhc05vZGUiLCJoYXNFbmROb2RlIiwicHJldiIsImdldFByZXZpb3VzVGV4dCIsIm5leHQiLCJnZXROZXh0VGV4dCIsIm1vdmVTdGFydFRvIiwiZGVzZWxlY3QiLCJtb3ZlRW5kVG8iLCJyYW5nZU9mZnNldCIsInByb3BlcnRpZXMiLCJkYXRhIiwibWVyZ2UiLCJ1cGRhdGVNYXJrIiwiYW5jaG9yUGF0aCIsInVuZGVmaW5lZCIsImZvY3VzUGF0aCIsIm5vcm1hbGl6ZSIsInBvc2l0aW9uIiwic3RhcnRPZmZzZXQiLCJlbmRPZmZzZXQiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7O0FBQ0E7Ozs7QUFDQTs7OztBQUNBOzs7Ozs7QUFFQTs7Ozs7O0FBTUEsSUFBTUEsUUFBUSxxQkFBTSxpQkFBTixDQUFkOztBQUVBOzs7Ozs7QUFNQSxJQUFNQyxhQUFhO0FBQ2pCO0FBQ0FDLGVBQWFDLFVBRkk7QUFHakJDLGVBQWFDLFVBSEk7QUFJakI7QUFDQUMsWUFBVUMsT0FMTztBQU1qQkMsZUFBYUMsVUFOSTtBQU9qQkMsWUFBVUMsT0FQTztBQVFqQjtBQUNBQyxlQUFhQyxVQVRJO0FBVWpCQyxhQUFXQyxRQVZNO0FBV2pCQyxhQUFXQyxRQVhNO0FBWWpCQyxlQUFhQyxVQVpJO0FBYWpCQyxZQUFVQyxPQWJPO0FBY2pCQyxjQUFZQyxTQWRLO0FBZWpCO0FBQ0FDLGlCQUFlQyxZQWhCRTtBQWlCakI7QUFDQUMsWUFBVUM7QUFsQk8sQ0FBbkI7O0FBcUJBOzs7Ozs7OztBQVFBLFNBQVNDLEtBQVQsQ0FBZUMsS0FBZixFQUFzQkMsU0FBdEIsRUFBaUM7QUFBQSxNQUN2QkMsSUFEdUIsR0FDZEQsU0FEYyxDQUN2QkMsSUFEdUI7O0FBRS9CLE1BQU1DLEtBQUsvQixXQUFXOEIsSUFBWCxDQUFYOztBQUVBLE1BQUksQ0FBQ0MsRUFBTCxFQUFTO0FBQ1AsVUFBTSxJQUFJQyxLQUFKLCtCQUFzQ0YsSUFBdEMsUUFBTjtBQUNEOztBQUVEL0IsUUFBTStCLElBQU4sRUFBWUQsU0FBWjtBQUNBRCxVQUFRRyxHQUFHSCxLQUFILEVBQVVDLFNBQVYsQ0FBUjtBQUNBLFNBQU9ELEtBQVA7QUFDRDs7QUFFRDs7Ozs7Ozs7QUFRQSxTQUFTdEIsT0FBVCxDQUFpQnNCLEtBQWpCLEVBQXdCQyxTQUF4QixFQUFtQztBQUFBLE1BQ3pCSSxJQUR5QixHQUNBSixTQURBLENBQ3pCSSxJQUR5QjtBQUFBLE1BQ25CQyxNQURtQixHQUNBTCxTQURBLENBQ25CSyxNQURtQjtBQUFBLE1BQ1hDLE1BRFcsR0FDQU4sU0FEQSxDQUNYTSxNQURXOztBQUVqQyxNQUFNQyxPQUFPLG9CQUFVQSxJQUFWLENBQWVQLFVBQVVPLElBQXpCLENBQWI7QUFGaUMsZUFHZFIsS0FIYztBQUFBLE1BRzNCUyxRQUgyQixVQUczQkEsUUFIMkI7O0FBSWpDLE1BQUlDLE9BQU9ELFNBQVNFLFVBQVQsQ0FBb0JOLElBQXBCLENBQVg7QUFDQUssU0FBT0EsS0FBS2hDLE9BQUwsQ0FBYTRCLE1BQWIsRUFBcUJDLE1BQXJCLEVBQTZCQyxJQUE3QixDQUFQO0FBQ0FDLGFBQVdBLFNBQVNHLGdCQUFULENBQTBCRixJQUExQixDQUFYO0FBQ0FWLFVBQVFBLE1BQU1hLEdBQU4sQ0FBVSxVQUFWLEVBQXNCSixRQUF0QixDQUFSO0FBQ0EsU0FBT1QsS0FBUDtBQUNEOztBQUVEOzs7Ozs7OztBQVFBLFNBQVNoQixVQUFULENBQW9CZ0IsS0FBcEIsRUFBMkJDLFNBQTNCLEVBQXNDO0FBQUEsTUFDNUJJLElBRDRCLEdBQ25CSixTQURtQixDQUM1QkksSUFENEI7O0FBRXBDLE1BQU1LLE9BQU8sb0JBQVVBLElBQVYsQ0FBZVQsVUFBVVMsSUFBekIsQ0FBYjtBQUNBLE1BQU1JLFFBQVFULEtBQUtBLEtBQUtFLE1BQUwsR0FBYyxDQUFuQixDQUFkO0FBQ0EsTUFBTVEsT0FBT1YsS0FBS1csS0FBTCxDQUFXLENBQVgsRUFBYyxDQUFDLENBQWYsQ0FBYjtBQUpvQyxnQkFLakJoQixLQUxpQjtBQUFBLE1BSzlCUyxRQUw4QixXQUs5QkEsUUFMOEI7O0FBTXBDLE1BQUlRLFNBQVNSLFNBQVNFLFVBQVQsQ0FBb0JJLElBQXBCLENBQWI7QUFDQSxNQUFNRyxXQUFXVCxZQUFZUSxNQUE3QjtBQUNBQSxXQUFTQSxPQUFPakMsVUFBUCxDQUFrQjhCLEtBQWxCLEVBQXlCSixJQUF6QixDQUFUO0FBQ0FELGFBQVdTLFdBQVdELE1BQVgsR0FBb0JSLFNBQVNHLGdCQUFULENBQTBCSyxNQUExQixDQUEvQjtBQUNBakIsVUFBUUEsTUFBTWEsR0FBTixDQUFVLFVBQVYsRUFBc0JKLFFBQXRCLENBQVI7QUFDQSxTQUFPVCxLQUFQO0FBQ0Q7O0FBRUQ7Ozs7Ozs7O0FBUUEsU0FBUzFCLFVBQVQsQ0FBb0IwQixLQUFwQixFQUEyQkMsU0FBM0IsRUFBc0M7QUFBQSxNQUM1QkksSUFENEIsR0FDTEosU0FESyxDQUM1QkksSUFENEI7QUFBQSxNQUN0QkMsTUFEc0IsR0FDTEwsU0FESyxDQUN0QkssTUFEc0I7QUFBQSxNQUNkYSxJQURjLEdBQ0xsQixTQURLLENBQ2RrQixJQURjOztBQUVwQyxNQUFNQyxRQUFRbkIsVUFBVW1CLEtBQVYsSUFBbUJuQixVQUFVbUIsS0FBVixDQUFnQkMsR0FBaEIsQ0FBb0Isb0JBQVViLElBQTlCLENBQWpDO0FBRm9DLGdCQUdOUixLQUhNO0FBQUEsTUFHOUJTLFFBSDhCLFdBRzlCQSxRQUg4QjtBQUFBLE1BR3BCYSxTQUhvQixXQUdwQkEsU0FIb0I7QUFBQSxtQkFJdUJBLFNBSnZCO0FBQUEsTUFJNUJDLFNBSjRCLGNBSTVCQSxTQUo0QjtBQUFBLE1BSWpCQyxRQUppQixjQUlqQkEsUUFKaUI7QUFBQSxNQUlQQyxZQUpPLGNBSVBBLFlBSk87QUFBQSxNQUlPQyxXQUpQLGNBSU9BLFdBSlA7O0FBS3BDLE1BQUloQixPQUFPRCxTQUFTRSxVQUFULENBQW9CTixJQUFwQixDQUFYOztBQUVBO0FBQ0FLLFNBQU9BLEtBQUtwQyxVQUFMLENBQWdCZ0MsTUFBaEIsRUFBd0JhLElBQXhCLEVBQThCQyxLQUE5QixDQUFQO0FBQ0FYLGFBQVdBLFNBQVNHLGdCQUFULENBQTBCRixJQUExQixDQUFYOztBQUVBO0FBQ0EsTUFBSWEsYUFBYWIsS0FBS2lCLEdBQWxCLElBQXlCRixnQkFBZ0JuQixNQUE3QyxFQUFxRDtBQUNuRGdCLGdCQUFZQSxVQUFVTSxVQUFWLENBQXFCVCxLQUFLWixNQUExQixDQUFaO0FBQ0Q7QUFDRCxNQUFJaUIsWUFBWWQsS0FBS2lCLEdBQWpCLElBQXdCRCxlQUFlcEIsTUFBM0MsRUFBbUQ7QUFDakRnQixnQkFBWUEsVUFBVU8sU0FBVixDQUFvQlYsS0FBS1osTUFBekIsQ0FBWjtBQUNEOztBQUVEUCxVQUFRQSxNQUFNYSxHQUFOLENBQVUsVUFBVixFQUFzQkosUUFBdEIsRUFBZ0NJLEdBQWhDLENBQW9DLFdBQXBDLEVBQWlEUyxTQUFqRCxDQUFSO0FBQ0EsU0FBT3RCLEtBQVA7QUFDRDs7QUFFRDs7Ozs7Ozs7OztBQVVBLFNBQVNkLFFBQVQsQ0FBa0JjLEtBQWxCLEVBQXlCQyxTQUF6QixFQUFvQztBQUFBLE1BQzFCSSxJQUQwQixHQUNQSixTQURPLENBQzFCSSxJQUQwQjtBQUFBLE1BQ3BCeUIsUUFEb0IsR0FDUDdCLFNBRE8sQ0FDcEI2QixRQURvQjtBQUFBLGdCQUVKOUIsS0FGSTtBQUFBLE1BRTVCUyxRQUY0QixXQUU1QkEsUUFGNEI7QUFBQSxNQUVsQmEsU0FGa0IsV0FFbEJBLFNBRmtCOztBQUdsQyxNQUFNUyxNQUFNdEIsU0FBU0UsVUFBVCxDQUFvQm1CLFFBQXBCLENBQVo7QUFDQSxNQUFNRSxNQUFNdkIsU0FBU0UsVUFBVCxDQUFvQk4sSUFBcEIsQ0FBWjtBQUNBLE1BQUlZLFNBQVNSLFNBQVN3QixTQUFULENBQW1CRixJQUFJSixHQUF2QixDQUFiO0FBQ0EsTUFBTU8sV0FBV2pCLE9BQU9rQixLQUFQLENBQWFDLE9BQWIsQ0FBcUJMLEdBQXJCLENBQWpCO0FBQ0EsTUFBTU0sV0FBV3BCLE9BQU9rQixLQUFQLENBQWFDLE9BQWIsQ0FBcUJKLEdBQXJCLENBQWpCOztBQUVBO0FBQ0FmLFdBQVNBLE9BQU8vQixRQUFQLENBQWdCZ0QsUUFBaEIsRUFBMEJHLFFBQTFCLENBQVQ7QUFDQTVCLGFBQVdRLE9BQU9VLEdBQVAsSUFBY2xCLFNBQVNrQixHQUF2QixHQUE2QlYsTUFBN0IsR0FBc0NSLFNBQVNHLGdCQUFULENBQTBCSyxNQUExQixDQUFqRDs7QUFFQTtBQUNBO0FBQ0EsTUFBSWMsSUFBSU8sSUFBSixJQUFZLE1BQWhCLEVBQXdCO0FBQUEsc0JBQ3FDaEIsU0FEckM7QUFBQSxRQUNkQyxTQURjLGVBQ2RBLFNBRGM7QUFBQSxRQUNIRSxZQURHLGVBQ0hBLFlBREc7QUFBQSxRQUNXRCxRQURYLGVBQ1dBLFFBRFg7QUFBQSxRQUNxQkUsV0FEckIsZUFDcUJBLFdBRHJCOzs7QUFHdEIsUUFBSUgsYUFBYVMsSUFBSUwsR0FBckIsRUFBMEI7QUFDeEJMLGtCQUFZQSxVQUFVaUIsWUFBVixDQUF1QlIsSUFBSUosR0FBM0IsRUFBZ0NJLElBQUl4QixNQUFKLEdBQWFrQixZQUE3QyxDQUFaO0FBQ0Q7O0FBRUQsUUFBSUQsWUFBWVEsSUFBSUwsR0FBcEIsRUFBeUI7QUFDdkJMLGtCQUFZQSxVQUFVa0IsV0FBVixDQUFzQlQsSUFBSUosR0FBMUIsRUFBK0JJLElBQUl4QixNQUFKLEdBQWFtQixXQUE1QyxDQUFaO0FBQ0Q7QUFDRjs7QUFFRDtBQUNBMUIsVUFBUUEsTUFBTWEsR0FBTixDQUFVLFVBQVYsRUFBc0JKLFFBQXRCLEVBQWdDSSxHQUFoQyxDQUFvQyxXQUFwQyxFQUFpRFMsU0FBakQsQ0FBUjtBQUNBLFNBQU90QixLQUFQO0FBQ0Q7O0FBRUQ7Ozs7Ozs7O0FBUUEsU0FBU1osUUFBVCxDQUFrQlksS0FBbEIsRUFBeUJDLFNBQXpCLEVBQW9DO0FBQUEsTUFDMUJJLElBRDBCLEdBQ1JKLFNBRFEsQ0FDMUJJLElBRDBCO0FBQUEsTUFDcEJvQyxPQURvQixHQUNSeEMsU0FEUSxDQUNwQndDLE9BRG9COztBQUVsQyxNQUFNQyxXQUFXRCxRQUFRQSxRQUFRbEMsTUFBUixHQUFpQixDQUF6QixDQUFqQjtBQUNBLE1BQU1vQyxnQkFBZ0JGLFFBQVF6QixLQUFSLENBQWMsQ0FBZCxFQUFpQixDQUFDLENBQWxCLENBQXRCO0FBQ0EsTUFBTTRCLGdCQUFnQnZDLEtBQUtXLEtBQUwsQ0FBVyxDQUFYLEVBQWMsQ0FBQyxDQUFmLENBQXRCO0FBQ0EsTUFBTTZCLFdBQVd4QyxLQUFLQSxLQUFLRSxNQUFMLEdBQWMsQ0FBbkIsQ0FBakI7QUFMa0MsZ0JBTWZQLEtBTmU7QUFBQSxNQU01QlMsUUFONEIsV0FNNUJBLFFBTjRCOztBQU9sQyxNQUFNQyxPQUFPRCxTQUFTRSxVQUFULENBQW9CTixJQUFwQixDQUFiOztBQUVBO0FBQ0EsTUFBSVksU0FBU1IsU0FBU3dCLFNBQVQsQ0FBbUJ2QixLQUFLaUIsR0FBeEIsQ0FBYjtBQUNBVixXQUFTQSxPQUFPM0IsVUFBUCxDQUFrQnVELFFBQWxCLENBQVQ7QUFDQXBDLGFBQVdRLE9BQU9xQixJQUFQLEtBQWdCLFVBQWhCLEdBQTZCckIsTUFBN0IsR0FBc0NSLFNBQVNHLGdCQUFULENBQTBCSyxNQUExQixDQUFqRDs7QUFFQTtBQUNBLE1BQUk2QixlQUFKOztBQUVBO0FBQ0E7QUFDQSxNQUNHRixjQUFjRyxLQUFkLENBQW9CLFVBQUNDLENBQUQsRUFBSUMsQ0FBSjtBQUFBLFdBQVVELE1BQU1MLGNBQWNNLENBQWQsQ0FBaEI7QUFBQSxHQUFwQixDQUFELElBQ0NMLGNBQWNyQyxNQUFkLEtBQXlCb0MsY0FBY3BDLE1BRjFDLEVBR0U7QUFDQXVDLGFBQVM3QixNQUFUO0FBQ0Q7O0FBRUQ7QUFDQTtBQVJBLE9BU0ssSUFDRjJCLGNBQWNHLEtBQWQsQ0FBb0IsVUFBQ0MsQ0FBRCxFQUFJQyxDQUFKO0FBQUEsYUFBVUQsTUFBTUwsY0FBY00sQ0FBZCxDQUFoQjtBQUFBLEtBQXBCLENBQUQsSUFDQ0osV0FBV0YsY0FBY0MsY0FBY3JDLE1BQTVCLENBRlQsRUFHSDtBQUNBb0Msb0JBQWNDLGNBQWNyQyxNQUE1QjtBQUNBdUMsZUFBU3JDLFNBQVNFLFVBQVQsQ0FBb0JnQyxhQUFwQixDQUFUO0FBQ0Q7O0FBRUQ7QUFSSyxTQVNBO0FBQ0hHLGlCQUFTckMsU0FBU0UsVUFBVCxDQUFvQmdDLGFBQXBCLENBQVQ7QUFDRDs7QUFFRDtBQUNBRyxXQUFTQSxPQUFPOUQsVUFBUCxDQUFrQjBELFFBQWxCLEVBQTRCaEMsSUFBNUIsQ0FBVDtBQUNBRCxhQUFXcUMsT0FBT1IsSUFBUCxLQUFnQixVQUFoQixHQUE2QlEsTUFBN0IsR0FBc0NyQyxTQUFTRyxnQkFBVCxDQUEwQmtDLE1BQTFCLENBQWpEO0FBQ0E5QyxVQUFRQSxNQUFNYSxHQUFOLENBQVUsVUFBVixFQUFzQkosUUFBdEIsQ0FBUjtBQUNBLFNBQU9ULEtBQVA7QUFDRDs7QUFFRDs7Ozs7Ozs7QUFRQSxTQUFTcEIsVUFBVCxDQUFvQm9CLEtBQXBCLEVBQTJCQyxTQUEzQixFQUFzQztBQUFBLE1BQzVCSSxJQUQ0QixHQUNISixTQURHLENBQzVCSSxJQUQ0QjtBQUFBLE1BQ3RCQyxNQURzQixHQUNITCxTQURHLENBQ3RCSyxNQURzQjtBQUFBLE1BQ2RDLE1BRGMsR0FDSE4sU0FERyxDQUNkTSxNQURjOztBQUVwQyxNQUFNQyxPQUFPLG9CQUFVQSxJQUFWLENBQWVQLFVBQVVPLElBQXpCLENBQWI7QUFGb0MsZ0JBR2pCUixLQUhpQjtBQUFBLE1BRzlCUyxRQUg4QixXQUc5QkEsUUFIOEI7O0FBSXBDLE1BQUlDLE9BQU9ELFNBQVNFLFVBQVQsQ0FBb0JOLElBQXBCLENBQVg7QUFDQUssU0FBT0EsS0FBSzlCLFVBQUwsQ0FBZ0IwQixNQUFoQixFQUF3QkMsTUFBeEIsRUFBZ0NDLElBQWhDLENBQVA7QUFDQUMsYUFBV0EsU0FBU0csZ0JBQVQsQ0FBMEJGLElBQTFCLENBQVg7QUFDQVYsVUFBUUEsTUFBTWEsR0FBTixDQUFVLFVBQVYsRUFBc0JKLFFBQXRCLENBQVI7QUFDQSxTQUFPVCxLQUFQO0FBQ0Q7O0FBRUQ7Ozs7Ozs7O0FBUUEsU0FBU1YsVUFBVCxDQUFvQlUsS0FBcEIsRUFBMkJDLFNBQTNCLEVBQXNDO0FBQUEsTUFDNUJJLElBRDRCLEdBQ25CSixTQURtQixDQUM1QkksSUFENEI7QUFBQSxnQkFFTkwsS0FGTTtBQUFBLE1BRTlCUyxRQUY4QixXQUU5QkEsUUFGOEI7QUFBQSxNQUVwQmEsU0FGb0IsV0FFcEJBLFNBRm9CO0FBQUEsb0JBR1BBLFNBSE87QUFBQSxNQUc1QjRCLFFBSDRCLGVBRzVCQSxRQUg0QjtBQUFBLE1BR2xCQyxNQUhrQixlQUdsQkEsTUFIa0I7O0FBSXBDLE1BQU16QyxPQUFPRCxTQUFTRSxVQUFULENBQW9CTixJQUFwQixDQUFiOztBQUVBO0FBQ0EsTUFBSWlCLFVBQVU4QixLQUFkLEVBQXFCO0FBQ25CLFFBQU1DLGVBQWUzQyxLQUFLNEMsT0FBTCxDQUFhSixRQUFiLENBQXJCO0FBQ0EsUUFBTUssYUFBYTdDLEtBQUs0QyxPQUFMLENBQWFILE1BQWIsQ0FBbkI7O0FBRUE7QUFDQSxRQUFJRSxZQUFKLEVBQWtCO0FBQ2hCLFVBQU1HLE9BQU8vQyxTQUFTZ0QsZUFBVCxDQUF5QlAsUUFBekIsQ0FBYjtBQUNBLFVBQU1RLE9BQU9qRCxTQUFTa0QsV0FBVCxDQUFxQlQsUUFBckIsQ0FBYjs7QUFFQSxVQUFJTSxJQUFKLEVBQVU7QUFDUmxDLG9CQUFZQSxVQUFVc0MsV0FBVixDQUFzQkosS0FBSzdCLEdBQTNCLEVBQWdDNkIsS0FBS2pELE1BQXJDLENBQVo7QUFDRCxPQUZELE1BRU8sSUFBSW1ELElBQUosRUFBVTtBQUNmcEMsb0JBQVlBLFVBQVVzQyxXQUFWLENBQXNCRixLQUFLL0IsR0FBM0IsRUFBZ0MsQ0FBaEMsQ0FBWjtBQUNELE9BRk0sTUFFQTtBQUNMTCxvQkFBWUEsVUFBVXVDLFFBQVYsRUFBWjtBQUNEO0FBQ0Y7O0FBRUQsUUFBSU4sVUFBSixFQUFnQjtBQUNkLFVBQU1DLFFBQU8vQyxTQUFTZ0QsZUFBVCxDQUF5Qk4sTUFBekIsQ0FBYjtBQUNBLFVBQU1PLFFBQU9qRCxTQUFTa0QsV0FBVCxDQUFxQlIsTUFBckIsQ0FBYjs7QUFFQSxVQUFJSyxLQUFKLEVBQVU7QUFDUmxDLG9CQUFZQSxVQUFVd0MsU0FBVixDQUFvQk4sTUFBSzdCLEdBQXpCLEVBQThCNkIsTUFBS2pELE1BQW5DLENBQVo7QUFDRCxPQUZELE1BRU8sSUFBSW1ELEtBQUosRUFBVTtBQUNmcEMsb0JBQVlBLFVBQVV3QyxTQUFWLENBQW9CSixNQUFLL0IsR0FBekIsRUFBOEIsQ0FBOUIsQ0FBWjtBQUNELE9BRk0sTUFFQTtBQUNMTCxvQkFBWUEsVUFBVXVDLFFBQVYsRUFBWjtBQUNEO0FBQ0Y7QUFDRjs7QUFFRDtBQUNBLE1BQUk1QyxTQUFTUixTQUFTd0IsU0FBVCxDQUFtQnZCLEtBQUtpQixHQUF4QixDQUFiO0FBQ0EsTUFBTWIsUUFBUUcsT0FBT2tCLEtBQVAsQ0FBYUMsT0FBYixDQUFxQjFCLElBQXJCLENBQWQ7QUFDQSxNQUFNUSxXQUFXVCxZQUFZUSxNQUE3QjtBQUNBQSxXQUFTQSxPQUFPM0IsVUFBUCxDQUFrQndCLEtBQWxCLENBQVQ7QUFDQUwsYUFBV1MsV0FBV0QsTUFBWCxHQUFvQlIsU0FBU0csZ0JBQVQsQ0FBMEJLLE1BQTFCLENBQS9COztBQUVBO0FBQ0FqQixVQUFRQSxNQUFNYSxHQUFOLENBQVUsVUFBVixFQUFzQkosUUFBdEIsRUFBZ0NJLEdBQWhDLENBQW9DLFdBQXBDLEVBQWlEUyxTQUFqRCxDQUFSO0FBQ0EsU0FBT3RCLEtBQVA7QUFDRDs7QUFFRDs7Ozs7Ozs7QUFRQSxTQUFTeEIsVUFBVCxDQUFvQndCLEtBQXBCLEVBQTJCQyxTQUEzQixFQUFzQztBQUFBLE1BQzVCSSxJQUQ0QixHQUNISixTQURHLENBQzVCSSxJQUQ0QjtBQUFBLE1BQ3RCQyxNQURzQixHQUNITCxTQURHLENBQ3RCSyxNQURzQjtBQUFBLE1BQ2RDLE1BRGMsR0FDSE4sU0FERyxDQUNkTSxNQURjOztBQUVwQyxNQUFNd0QsY0FBY3pELFNBQVNDLE1BQTdCO0FBRm9DLGdCQUdOUCxLQUhNO0FBQUEsTUFHOUJTLFFBSDhCLFdBRzlCQSxRQUg4QjtBQUFBLE1BR3BCYSxTQUhvQixXQUdwQkEsU0FIb0I7QUFBQSxvQkFJdUJBLFNBSnZCO0FBQUEsTUFJNUJDLFNBSjRCLGVBSTVCQSxTQUo0QjtBQUFBLE1BSWpCQyxRQUppQixlQUlqQkEsUUFKaUI7QUFBQSxNQUlQQyxZQUpPLGVBSVBBLFlBSk87QUFBQSxNQUlPQyxXQUpQLGVBSU9BLFdBSlA7O0FBS3BDLE1BQUloQixPQUFPRCxTQUFTRSxVQUFULENBQW9CTixJQUFwQixDQUFYOztBQUVBO0FBQ0EsTUFBSWtCLGFBQWFiLEtBQUtpQixHQUFsQixJQUF5QkYsZ0JBQWdCc0MsV0FBN0MsRUFBMEQ7QUFDeER6QyxnQkFBWUEsVUFBVU0sVUFBVixDQUFxQixDQUFDckIsTUFBdEIsQ0FBWjtBQUNEO0FBQ0QsTUFBSWlCLFlBQVlkLEtBQUtpQixHQUFqQixJQUF3QkQsZUFBZXFDLFdBQTNDLEVBQXdEO0FBQ3REekMsZ0JBQVlBLFVBQVVPLFNBQVYsQ0FBb0IsQ0FBQ3RCLE1BQXJCLENBQVo7QUFDRDs7QUFFREcsU0FBT0EsS0FBS2xDLFVBQUwsQ0FBZ0I4QixNQUFoQixFQUF3QkMsTUFBeEIsQ0FBUDtBQUNBRSxhQUFXQSxTQUFTRyxnQkFBVCxDQUEwQkYsSUFBMUIsQ0FBWDtBQUNBVixVQUFRQSxNQUFNYSxHQUFOLENBQVUsVUFBVixFQUFzQkosUUFBdEIsRUFBZ0NJLEdBQWhDLENBQW9DLFdBQXBDLEVBQWlEUyxTQUFqRCxDQUFSO0FBQ0EsU0FBT3RCLEtBQVA7QUFDRDs7QUFFRDs7Ozs7Ozs7QUFRQSxTQUFTRixPQUFULENBQWlCRSxLQUFqQixFQUF3QkMsU0FBeEIsRUFBbUM7QUFBQSxNQUN6QitELFVBRHlCLEdBQ1YvRCxTQURVLENBQ3pCK0QsVUFEeUI7QUFBQSxnQkFFbEJoRSxLQUZrQjtBQUFBLE1BRTNCaUUsSUFGMkIsV0FFM0JBLElBRjJCOzs7QUFJakNBLFNBQU9BLEtBQUtDLEtBQUwsQ0FBV0YsVUFBWCxDQUFQO0FBQ0FoRSxVQUFRQSxNQUFNYSxHQUFOLENBQVUsTUFBVixFQUFrQm9ELElBQWxCLENBQVI7QUFDQSxTQUFPakUsS0FBUDtBQUNEOztBQUVEOzs7Ozs7OztBQVFBLFNBQVNsQixPQUFULENBQWlCa0IsS0FBakIsRUFBd0JDLFNBQXhCLEVBQW1DO0FBQUEsTUFDekJJLElBRHlCLEdBQ1lKLFNBRFosQ0FDekJJLElBRHlCO0FBQUEsTUFDbkJDLE1BRG1CLEdBQ1lMLFNBRFosQ0FDbkJLLE1BRG1CO0FBQUEsTUFDWEMsTUFEVyxHQUNZTixTQURaLENBQ1hNLE1BRFc7QUFBQSxNQUNIeUQsVUFERyxHQUNZL0QsU0FEWixDQUNIK0QsVUFERzs7QUFFakMsTUFBTXhELE9BQU8sb0JBQVVBLElBQVYsQ0FBZVAsVUFBVU8sSUFBekIsQ0FBYjtBQUZpQyxpQkFHZFIsS0FIYztBQUFBLE1BRzNCUyxRQUgyQixZQUczQkEsUUFIMkI7O0FBSWpDLE1BQUlDLE9BQU9ELFNBQVNFLFVBQVQsQ0FBb0JOLElBQXBCLENBQVg7QUFDQUssU0FBT0EsS0FBS3lELFVBQUwsQ0FBZ0I3RCxNQUFoQixFQUF3QkMsTUFBeEIsRUFBZ0NDLElBQWhDLEVBQXNDd0QsVUFBdEMsQ0FBUDtBQUNBdkQsYUFBV0EsU0FBU0csZ0JBQVQsQ0FBMEJGLElBQTFCLENBQVg7QUFDQVYsVUFBUUEsTUFBTWEsR0FBTixDQUFVLFVBQVYsRUFBc0JKLFFBQXRCLENBQVI7QUFDQSxTQUFPVCxLQUFQO0FBQ0Q7O0FBRUQ7Ozs7Ozs7O0FBUUEsU0FBU1IsT0FBVCxDQUFpQlEsS0FBakIsRUFBd0JDLFNBQXhCLEVBQW1DO0FBQUEsTUFDekJJLElBRHlCLEdBQ0pKLFNBREksQ0FDekJJLElBRHlCO0FBQUEsTUFDbkIyRCxVQURtQixHQUNKL0QsU0FESSxDQUNuQitELFVBRG1CO0FBQUEsaUJBRWRoRSxLQUZjO0FBQUEsTUFFM0JTLFFBRjJCLFlBRTNCQSxRQUYyQjs7QUFHakMsTUFBSUMsT0FBT0QsU0FBU0UsVUFBVCxDQUFvQk4sSUFBcEIsQ0FBWDs7QUFFQTtBQUNBLE1BQUkyRCxXQUFXN0IsS0FBWCxJQUFvQjZCLFdBQVc3QixLQUFYLElBQW9CekIsS0FBS3lCLEtBQWpELEVBQXdEO0FBQ3RELHdCQUFLLGtLQUFMLEVBQXlLbEMsU0FBeks7QUFDQSxXQUFPK0QsV0FBVzdCLEtBQWxCO0FBQ0Q7O0FBRUQ7QUFDQSxNQUFJNkIsV0FBV3JDLEdBQVgsSUFBa0JxQyxXQUFXckMsR0FBWCxJQUFrQmpCLEtBQUtpQixHQUE3QyxFQUFrRDtBQUNoRCx3QkFBSyx3SUFBTCxFQUErSTFCLFNBQS9JO0FBQ0EsV0FBTytELFdBQVdyQyxHQUFsQjtBQUNEOztBQUVEakIsU0FBT0EsS0FBS3dELEtBQUwsQ0FBV0YsVUFBWCxDQUFQO0FBQ0F2RCxhQUFXQyxLQUFLNEIsSUFBTCxLQUFjLFVBQWQsR0FBMkI1QixJQUEzQixHQUFrQ0QsU0FBU0csZ0JBQVQsQ0FBMEJGLElBQTFCLENBQTdDO0FBQ0FWLFVBQVFBLE1BQU1hLEdBQU4sQ0FBVSxVQUFWLEVBQXNCSixRQUF0QixDQUFSO0FBQ0EsU0FBT1QsS0FBUDtBQUNEOztBQUVEOzs7Ozs7OztBQVFBLFNBQVNKLFlBQVQsQ0FBc0JJLEtBQXRCLEVBQTZCQyxTQUE3QixFQUF3QztBQUN0QyxNQUFNK0QsMEJBQWtCL0QsVUFBVStELFVBQTVCLENBQU47QUFEc0MsaUJBRVJoRSxLQUZRO0FBQUEsTUFFaENTLFFBRmdDLFlBRWhDQSxRQUZnQztBQUFBLE1BRXRCYSxTQUZzQixZQUV0QkEsU0FGc0I7OztBQUl0QyxNQUFJMEMsV0FBV0ksVUFBWCxLQUEwQkMsU0FBOUIsRUFBeUM7QUFDdkNMLGVBQVd6QyxTQUFYLEdBQXVCeUMsV0FBV0ksVUFBWCxLQUEwQixJQUExQixHQUNuQixJQURtQixHQUVuQjNELFNBQVNFLFVBQVQsQ0FBb0JxRCxXQUFXSSxVQUEvQixFQUEyQ3pDLEdBRi9DO0FBR0EsV0FBT3FDLFdBQVdJLFVBQWxCO0FBQ0Q7O0FBRUQsTUFBSUosV0FBV00sU0FBWCxLQUF5QkQsU0FBN0IsRUFBd0M7QUFDdENMLGVBQVd4QyxRQUFYLEdBQXNCd0MsV0FBV00sU0FBWCxLQUF5QixJQUF6QixHQUNsQixJQURrQixHQUVsQjdELFNBQVNFLFVBQVQsQ0FBb0JxRCxXQUFXTSxTQUEvQixFQUEwQzNDLEdBRjlDO0FBR0EsV0FBT3FDLFdBQVdNLFNBQWxCO0FBQ0Q7O0FBRURoRCxjQUFZQSxVQUFVNEMsS0FBVixDQUFnQkYsVUFBaEIsQ0FBWjtBQUNBMUMsY0FBWUEsVUFBVWlELFNBQVYsQ0FBb0I5RCxRQUFwQixDQUFaO0FBQ0FULFVBQVFBLE1BQU1hLEdBQU4sQ0FBVSxXQUFWLEVBQXVCUyxTQUF2QixDQUFSO0FBQ0EsU0FBT3RCLEtBQVA7QUFDRDs7QUFFRDs7Ozs7Ozs7QUFRQSxTQUFTTixTQUFULENBQW1CTSxLQUFuQixFQUEwQkMsU0FBMUIsRUFBcUM7QUFBQSxNQUMzQkksSUFEMkIsR0FDUkosU0FEUSxDQUMzQkksSUFEMkI7QUFBQSxNQUNyQm1FLFFBRHFCLEdBQ1J2RSxTQURRLENBQ3JCdUUsUUFEcUI7QUFBQSxpQkFFTHhFLEtBRks7QUFBQSxNQUU3QlMsUUFGNkIsWUFFN0JBLFFBRjZCO0FBQUEsTUFFbkJhLFNBRm1CLFlBRW5CQSxTQUZtQjs7QUFJbkM7O0FBQ0EsTUFBTVosT0FBT0QsU0FBU0UsVUFBVCxDQUFvQk4sSUFBcEIsQ0FBYjtBQUNBLE1BQUlZLFNBQVNSLFNBQVN3QixTQUFULENBQW1CdkIsS0FBS2lCLEdBQXhCLENBQWI7QUFDQSxNQUFNYixRQUFRRyxPQUFPa0IsS0FBUCxDQUFhQyxPQUFiLENBQXFCMUIsSUFBckIsQ0FBZDtBQUNBLE1BQU1RLFdBQVdELFVBQVVSLFFBQTNCOztBQUVBO0FBQ0FRLFdBQVNBLE9BQU92QixTQUFQLENBQWlCb0IsS0FBakIsRUFBd0IwRCxRQUF4QixDQUFUO0FBQ0EvRCxhQUFXUyxXQUFXRCxNQUFYLEdBQW9CUixTQUFTRyxnQkFBVCxDQUEwQkssTUFBMUIsQ0FBL0I7O0FBRUE7QUFkbUMsb0JBZWtCSyxTQWZsQjtBQUFBLE1BZTNCNEIsUUFmMkIsZUFlM0JBLFFBZjJCO0FBQUEsTUFlakJDLE1BZmlCLGVBZWpCQSxNQWZpQjtBQUFBLE1BZVRzQixXQWZTLGVBZVRBLFdBZlM7QUFBQSxNQWVJQyxTQWZKLGVBZUlBLFNBZko7O0FBZ0JuQyxNQUFNaEIsT0FBT2pELFNBQVNrRCxXQUFULENBQXFCakQsS0FBS2lCLEdBQTFCLENBQWI7O0FBRUE7QUFDQSxNQUFJakIsS0FBS2lCLEdBQUwsSUFBWXVCLFFBQVosSUFBd0JzQixZQUFZQyxXQUF4QyxFQUFxRDtBQUNuRG5ELGdCQUFZQSxVQUFVc0MsV0FBVixDQUFzQkYsS0FBSy9CLEdBQTNCLEVBQWdDOEMsY0FBY0QsUUFBOUMsQ0FBWjtBQUNEOztBQUVEO0FBQ0EsTUFBSTlELEtBQUtpQixHQUFMLElBQVl3QixNQUFaLElBQXNCcUIsWUFBWUUsU0FBdEMsRUFBaUQ7QUFDL0NwRCxnQkFBWUEsVUFBVXdDLFNBQVYsQ0FBb0JKLEtBQUsvQixHQUF6QixFQUE4QitDLFlBQVlGLFFBQTFDLENBQVo7QUFDRDs7QUFFRDtBQUNBeEUsVUFBUUEsTUFBTWEsR0FBTixDQUFVLFVBQVYsRUFBc0JKLFFBQXRCLEVBQWdDSSxHQUFoQyxDQUFvQyxXQUFwQyxFQUFpRFMsU0FBakQsQ0FBUjtBQUNBLFNBQU90QixLQUFQO0FBQ0Q7O0FBRUQ7Ozs7OztrQkFNZUQsSyIsImZpbGUiOiJhcHBseS1vcGVyYXRpb24uanMiLCJzb3VyY2VzQ29udGVudCI6WyJcbmltcG9ydCBEZWJ1ZyBmcm9tICdkZWJ1ZydcbmltcG9ydCBOb3JtYWxpemUgZnJvbSAnLi4vdXRpbHMvbm9ybWFsaXplJ1xuaW1wb3J0IHdhcm4gZnJvbSAnLi4vdXRpbHMvd2FybidcblxuLyoqXG4gKiBEZWJ1Zy5cbiAqXG4gKiBAdHlwZSB7RnVuY3Rpb259XG4gKi9cblxuY29uc3QgZGVidWcgPSBEZWJ1Zygnc2xhdGU6b3BlcmF0aW9uJylcblxuLyoqXG4gKiBPcGVyYXRpb25zLlxuICpcbiAqIEB0eXBlIHtPYmplY3R9XG4gKi9cblxuY29uc3QgT1BFUkFUSU9OUyA9IHtcbiAgLy8gVGV4dCBvcGVyYXRpb25zLlxuICBpbnNlcnRfdGV4dDogaW5zZXJ0VGV4dCxcbiAgcmVtb3ZlX3RleHQ6IHJlbW92ZVRleHQsXG4gIC8vIE1hcmsgb3BlcmF0aW9ucy5cbiAgYWRkX21hcms6IGFkZE1hcmssXG4gIHJlbW92ZV9tYXJrOiByZW1vdmVNYXJrLFxuICBzZXRfbWFyazogc2V0TWFyayxcbiAgLy8gTm9kZSBvcGVyYXRpb25zLlxuICBpbnNlcnRfbm9kZTogaW5zZXJ0Tm9kZSxcbiAgam9pbl9ub2RlOiBqb2luTm9kZSxcbiAgbW92ZV9ub2RlOiBtb3ZlTm9kZSxcbiAgcmVtb3ZlX25vZGU6IHJlbW92ZU5vZGUsXG4gIHNldF9ub2RlOiBzZXROb2RlLFxuICBzcGxpdF9ub2RlOiBzcGxpdE5vZGUsXG4gIC8vIFNlbGVjdGlvbiBvcGVyYXRpb25zLlxuICBzZXRfc2VsZWN0aW9uOiBzZXRTZWxlY3Rpb24sXG4gIC8vIFN0YXRlIGRhdGEgb3BlcmF0aW9ucy5cbiAgc2V0X2RhdGE6IHNldERhdGFcbn1cblxuLyoqXG4gKiBBcHBseSBhbiBgb3BlcmF0aW9uYCB0byBhIGBzdGF0ZWAuXG4gKlxuICogQHBhcmFtIHtTdGF0ZX0gc3RhdGVcbiAqIEBwYXJhbSB7T2JqZWN0fSBvcGVyYXRpb25cbiAqIEByZXR1cm4ge1N0YXRlfSBzdGF0ZVxuICovXG5cbmZ1bmN0aW9uIGFwcGx5KHN0YXRlLCBvcGVyYXRpb24pIHtcbiAgY29uc3QgeyB0eXBlIH0gPSBvcGVyYXRpb25cbiAgY29uc3QgZm4gPSBPUEVSQVRJT05TW3R5cGVdXG5cbiAgaWYgKCFmbikge1xuICAgIHRocm93IG5ldyBFcnJvcihgVW5rbm93biBvcGVyYXRpb24gdHlwZTogXCIke3R5cGV9XCIuYClcbiAgfVxuXG4gIGRlYnVnKHR5cGUsIG9wZXJhdGlvbilcbiAgc3RhdGUgPSBmbihzdGF0ZSwgb3BlcmF0aW9uKVxuICByZXR1cm4gc3RhdGVcbn1cblxuLyoqXG4gKiBBZGQgbWFyayB0byB0ZXh0IGF0IGBvZmZzZXRgIGFuZCBgbGVuZ3RoYCBpbiBub2RlIGJ5IGBwYXRoYC5cbiAqXG4gKiBAcGFyYW0ge1N0YXRlfSBzdGF0ZVxuICogQHBhcmFtIHtPYmplY3R9IG9wZXJhdGlvblxuICogQHJldHVybiB7U3RhdGV9XG4gKi9cblxuZnVuY3Rpb24gYWRkTWFyayhzdGF0ZSwgb3BlcmF0aW9uKSB7XG4gIGNvbnN0IHsgcGF0aCwgb2Zmc2V0LCBsZW5ndGggfSA9IG9wZXJhdGlvblxuICBjb25zdCBtYXJrID0gTm9ybWFsaXplLm1hcmsob3BlcmF0aW9uLm1hcmspXG4gIGxldCB7IGRvY3VtZW50IH0gPSBzdGF0ZVxuICBsZXQgbm9kZSA9IGRvY3VtZW50LmFzc2VydFBhdGgocGF0aClcbiAgbm9kZSA9IG5vZGUuYWRkTWFyayhvZmZzZXQsIGxlbmd0aCwgbWFyaylcbiAgZG9jdW1lbnQgPSBkb2N1bWVudC51cGRhdGVEZXNjZW5kYW50KG5vZGUpXG4gIHN0YXRlID0gc3RhdGUuc2V0KCdkb2N1bWVudCcsIGRvY3VtZW50KVxuICByZXR1cm4gc3RhdGVcbn1cblxuLyoqXG4gKiBJbnNlcnQgYSBgbm9kZWAgYXQgYGluZGV4YCBpbiBhIG5vZGUgYnkgYHBhdGhgLlxuICpcbiAqIEBwYXJhbSB7U3RhdGV9IHN0YXRlXG4gKiBAcGFyYW0ge09iamVjdH0gb3BlcmF0aW9uXG4gKiBAcmV0dXJuIHtTdGF0ZX1cbiAqL1xuXG5mdW5jdGlvbiBpbnNlcnROb2RlKHN0YXRlLCBvcGVyYXRpb24pIHtcbiAgY29uc3QgeyBwYXRoIH0gPSBvcGVyYXRpb25cbiAgY29uc3Qgbm9kZSA9IE5vcm1hbGl6ZS5ub2RlKG9wZXJhdGlvbi5ub2RlKVxuICBjb25zdCBpbmRleCA9IHBhdGhbcGF0aC5sZW5ndGggLSAxXVxuICBjb25zdCByZXN0ID0gcGF0aC5zbGljZSgwLCAtMSlcbiAgbGV0IHsgZG9jdW1lbnQgfSA9IHN0YXRlXG4gIGxldCBwYXJlbnQgPSBkb2N1bWVudC5hc3NlcnRQYXRoKHJlc3QpXG4gIGNvbnN0IGlzUGFyZW50ID0gZG9jdW1lbnQgPT0gcGFyZW50XG4gIHBhcmVudCA9IHBhcmVudC5pbnNlcnROb2RlKGluZGV4LCBub2RlKVxuICBkb2N1bWVudCA9IGlzUGFyZW50ID8gcGFyZW50IDogZG9jdW1lbnQudXBkYXRlRGVzY2VuZGFudChwYXJlbnQpXG4gIHN0YXRlID0gc3RhdGUuc2V0KCdkb2N1bWVudCcsIGRvY3VtZW50KVxuICByZXR1cm4gc3RhdGVcbn1cblxuLyoqXG4gKiBJbnNlcnQgYHRleHRgIGF0IGBvZmZzZXRgIGluIG5vZGUgYnkgYHBhdGhgLlxuICpcbiAqIEBwYXJhbSB7U3RhdGV9IHN0YXRlXG4gKiBAcGFyYW0ge09iamVjdH0gb3BlcmF0aW9uXG4gKiBAcmV0dXJuIHtTdGF0ZX1cbiAqL1xuXG5mdW5jdGlvbiBpbnNlcnRUZXh0KHN0YXRlLCBvcGVyYXRpb24pIHtcbiAgY29uc3QgeyBwYXRoLCBvZmZzZXQsIHRleHQgfSA9IG9wZXJhdGlvblxuICBjb25zdCBtYXJrcyA9IG9wZXJhdGlvbi5tYXJrcyAmJiBvcGVyYXRpb24ubWFya3MubWFwKE5vcm1hbGl6ZS5tYXJrKVxuICBsZXQgeyBkb2N1bWVudCwgc2VsZWN0aW9uIH0gPSBzdGF0ZVxuICBjb25zdCB7IGFuY2hvcktleSwgZm9jdXNLZXksIGFuY2hvck9mZnNldCwgZm9jdXNPZmZzZXQgfSA9IHNlbGVjdGlvblxuICBsZXQgbm9kZSA9IGRvY3VtZW50LmFzc2VydFBhdGgocGF0aClcblxuICAvLyBVcGRhdGUgdGhlIGRvY3VtZW50XG4gIG5vZGUgPSBub2RlLmluc2VydFRleHQob2Zmc2V0LCB0ZXh0LCBtYXJrcylcbiAgZG9jdW1lbnQgPSBkb2N1bWVudC51cGRhdGVEZXNjZW5kYW50KG5vZGUpXG5cbiAgLy8gVXBkYXRlIHRoZSBzZWxlY3Rpb25cbiAgaWYgKGFuY2hvcktleSA9PSBub2RlLmtleSAmJiBhbmNob3JPZmZzZXQgPj0gb2Zmc2V0KSB7XG4gICAgc2VsZWN0aW9uID0gc2VsZWN0aW9uLm1vdmVBbmNob3IodGV4dC5sZW5ndGgpXG4gIH1cbiAgaWYgKGZvY3VzS2V5ID09IG5vZGUua2V5ICYmIGZvY3VzT2Zmc2V0ID49IG9mZnNldCkge1xuICAgIHNlbGVjdGlvbiA9IHNlbGVjdGlvbi5tb3ZlRm9jdXModGV4dC5sZW5ndGgpXG4gIH1cblxuICBzdGF0ZSA9IHN0YXRlLnNldCgnZG9jdW1lbnQnLCBkb2N1bWVudCkuc2V0KCdzZWxlY3Rpb24nLCBzZWxlY3Rpb24pXG4gIHJldHVybiBzdGF0ZVxufVxuXG4vKipcbiAqIEpvaW4gYSBub2RlIGJ5IGBwYXRoYCB3aXRoIGEgbm9kZSBgd2l0aFBhdGhgLlxuICpcbiAqIEBwYXJhbSB7U3RhdGV9IHN0YXRlXG4gKiBAcGFyYW0ge09iamVjdH0gb3BlcmF0aW9uXG4gKiAgIEBwYXJhbSB7Qm9vbGVhbn0gb3BlcmF0aW9uLmRlZXAgKG9wdGlvbmFsKSBKb2luIHJlY3Vyc2l2ZWx5IHRoZVxuICogICByZXNwZWN0aXZlIGxhc3Qgbm9kZSBhbmQgZmlyc3Qgbm9kZSBvZiB0aGUgbm9kZXMnIGNoaWxkcmVuLiBMaWtlIGEgemlwcGVyIDopXG4gKiBAcmV0dXJuIHtTdGF0ZX1cbiAqL1xuXG5mdW5jdGlvbiBqb2luTm9kZShzdGF0ZSwgb3BlcmF0aW9uKSB7XG4gIGNvbnN0IHsgcGF0aCwgd2l0aFBhdGggfSA9IG9wZXJhdGlvblxuICBsZXQgeyBkb2N1bWVudCwgc2VsZWN0aW9uIH0gPSBzdGF0ZVxuICBjb25zdCBvbmUgPSBkb2N1bWVudC5hc3NlcnRQYXRoKHdpdGhQYXRoKVxuICBjb25zdCB0d28gPSBkb2N1bWVudC5hc3NlcnRQYXRoKHBhdGgpXG4gIGxldCBwYXJlbnQgPSBkb2N1bWVudC5nZXRQYXJlbnQob25lLmtleSlcbiAgY29uc3Qgb25lSW5kZXggPSBwYXJlbnQubm9kZXMuaW5kZXhPZihvbmUpXG4gIGNvbnN0IHR3b0luZGV4ID0gcGFyZW50Lm5vZGVzLmluZGV4T2YodHdvKVxuXG4gIC8vIFBlcmZvcm0gdGhlIGpvaW4gaW4gdGhlIGRvY3VtZW50LlxuICBwYXJlbnQgPSBwYXJlbnQuam9pbk5vZGUob25lSW5kZXgsIHR3b0luZGV4KVxuICBkb2N1bWVudCA9IHBhcmVudC5rZXkgPT0gZG9jdW1lbnQua2V5ID8gcGFyZW50IDogZG9jdW1lbnQudXBkYXRlRGVzY2VuZGFudChwYXJlbnQpXG5cbiAgLy8gSWYgdGhlIG5vZGVzIGFyZSB0ZXh0IG5vZGVzIGFuZCB0aGUgc2VsZWN0aW9uIGlzIGluc2lkZSB0aGUgc2Vjb25kIG5vZGVcbiAgLy8gdXBkYXRlIGl0IHRvIHJlZmVyIHRvIHRoZSBmaXJzdCBub2RlIGluc3RlYWQuXG4gIGlmIChvbmUua2luZCA9PSAndGV4dCcpIHtcbiAgICBjb25zdCB7IGFuY2hvcktleSwgYW5jaG9yT2Zmc2V0LCBmb2N1c0tleSwgZm9jdXNPZmZzZXQgfSA9IHNlbGVjdGlvblxuXG4gICAgaWYgKGFuY2hvcktleSA9PSB0d28ua2V5KSB7XG4gICAgICBzZWxlY3Rpb24gPSBzZWxlY3Rpb24ubW92ZUFuY2hvclRvKG9uZS5rZXksIG9uZS5sZW5ndGggKyBhbmNob3JPZmZzZXQpXG4gICAgfVxuXG4gICAgaWYgKGZvY3VzS2V5ID09IHR3by5rZXkpIHtcbiAgICAgIHNlbGVjdGlvbiA9IHNlbGVjdGlvbi5tb3ZlRm9jdXNUbyhvbmUua2V5LCBvbmUubGVuZ3RoICsgZm9jdXNPZmZzZXQpXG4gICAgfVxuICB9XG5cbiAgLy8gVXBkYXRlIHRoZSBkb2N1bWVudCBhbmQgc2VsZWN0aW9uLlxuICBzdGF0ZSA9IHN0YXRlLnNldCgnZG9jdW1lbnQnLCBkb2N1bWVudCkuc2V0KCdzZWxlY3Rpb24nLCBzZWxlY3Rpb24pXG4gIHJldHVybiBzdGF0ZVxufVxuXG4vKipcbiAqIE1vdmUgYSBub2RlIGJ5IGBwYXRoYCB0byBgbmV3UGF0aGAuXG4gKlxuICogQHBhcmFtIHtTdGF0ZX0gc3RhdGVcbiAqIEBwYXJhbSB7T2JqZWN0fSBvcGVyYXRpb25cbiAqIEByZXR1cm4ge1N0YXRlfVxuICovXG5cbmZ1bmN0aW9uIG1vdmVOb2RlKHN0YXRlLCBvcGVyYXRpb24pIHtcbiAgY29uc3QgeyBwYXRoLCBuZXdQYXRoIH0gPSBvcGVyYXRpb25cbiAgY29uc3QgbmV3SW5kZXggPSBuZXdQYXRoW25ld1BhdGgubGVuZ3RoIC0gMV1cbiAgY29uc3QgbmV3UGFyZW50UGF0aCA9IG5ld1BhdGguc2xpY2UoMCwgLTEpXG4gIGNvbnN0IG9sZFBhcmVudFBhdGggPSBwYXRoLnNsaWNlKDAsIC0xKVxuICBjb25zdCBvbGRJbmRleCA9IHBhdGhbcGF0aC5sZW5ndGggLSAxXVxuICBsZXQgeyBkb2N1bWVudCB9ID0gc3RhdGVcbiAgY29uc3Qgbm9kZSA9IGRvY3VtZW50LmFzc2VydFBhdGgocGF0aClcblxuICAvLyBSZW1vdmUgdGhlIG5vZGUgZnJvbSBpdHMgY3VycmVudCBwYXJlbnQuXG4gIGxldCBwYXJlbnQgPSBkb2N1bWVudC5nZXRQYXJlbnQobm9kZS5rZXkpXG4gIHBhcmVudCA9IHBhcmVudC5yZW1vdmVOb2RlKG9sZEluZGV4KVxuICBkb2N1bWVudCA9IHBhcmVudC5raW5kID09PSAnZG9jdW1lbnQnID8gcGFyZW50IDogZG9jdW1lbnQudXBkYXRlRGVzY2VuZGFudChwYXJlbnQpXG5cbiAgLy8gRmluZCB0aGUgbmV3IHRhcmdldC4uLlxuICBsZXQgdGFyZ2V0XG5cbiAgLy8gSWYgdGhlIG9sZCBwYXRoIGFuZCB0aGUgcmVzdCBvZiB0aGUgbmV3IHBhdGggYXJlIHRoZSBzYW1lLCB0aGVuIHRoZSBuZXdcbiAgLy8gdGFyZ2V0IGlzIHRoZSBvbGQgcGFyZW50LlxuICBpZiAoXG4gICAgKG9sZFBhcmVudFBhdGguZXZlcnkoKHgsIGkpID0+IHggPT09IG5ld1BhcmVudFBhdGhbaV0pKSAmJlxuICAgIChvbGRQYXJlbnRQYXRoLmxlbmd0aCA9PT0gbmV3UGFyZW50UGF0aC5sZW5ndGgpXG4gICkge1xuICAgIHRhcmdldCA9IHBhcmVudFxuICB9XG5cbiAgLy8gT3RoZXJ3aXNlLCBpZiB0aGUgb2xkIHBhdGggcmVtb3ZhbCByZXN1bHRlZCBpbiB0aGUgbmV3IHBhdGggYmVpbmcgbm8gbG9uZ2VyXG4gIC8vIGNvcnJlY3QsIHdlIG5lZWQgdG8gZGVjcmVtZW50IHRoZSBuZXcgcGF0aCBhdCB0aGUgb2xkIHBhdGgncyBsYXN0IGluZGV4LlxuICBlbHNlIGlmIChcbiAgICAob2xkUGFyZW50UGF0aC5ldmVyeSgoeCwgaSkgPT4geCA9PT0gbmV3UGFyZW50UGF0aFtpXSkpICYmXG4gICAgKG9sZEluZGV4IDwgbmV3UGFyZW50UGF0aFtvbGRQYXJlbnRQYXRoLmxlbmd0aF0pXG4gICkge1xuICAgIG5ld1BhcmVudFBhdGhbb2xkUGFyZW50UGF0aC5sZW5ndGhdLS1cbiAgICB0YXJnZXQgPSBkb2N1bWVudC5hc3NlcnRQYXRoKG5ld1BhcmVudFBhdGgpXG4gIH1cblxuICAvLyBPdGhlcndpc2UsIHdlIGNhbiBqdXN0IGdyYWIgdGhlIHRhcmdldCBub3JtYWxseS4uLlxuICBlbHNlIHtcbiAgICB0YXJnZXQgPSBkb2N1bWVudC5hc3NlcnRQYXRoKG5ld1BhcmVudFBhdGgpXG4gIH1cblxuICAvLyBJbnNlcnQgdGhlIG5ldyBub2RlIHRvIGl0cyBuZXcgcGFyZW50LlxuICB0YXJnZXQgPSB0YXJnZXQuaW5zZXJ0Tm9kZShuZXdJbmRleCwgbm9kZSlcbiAgZG9jdW1lbnQgPSB0YXJnZXQua2luZCA9PT0gJ2RvY3VtZW50JyA/IHRhcmdldCA6IGRvY3VtZW50LnVwZGF0ZURlc2NlbmRhbnQodGFyZ2V0KVxuICBzdGF0ZSA9IHN0YXRlLnNldCgnZG9jdW1lbnQnLCBkb2N1bWVudClcbiAgcmV0dXJuIHN0YXRlXG59XG5cbi8qKlxuICogUmVtb3ZlIG1hcmsgZnJvbSB0ZXh0IGF0IGBvZmZzZXRgIGFuZCBgbGVuZ3RoYCBpbiBub2RlIGJ5IGBwYXRoYC5cbiAqXG4gKiBAcGFyYW0ge1N0YXRlfSBzdGF0ZVxuICogQHBhcmFtIHtPYmplY3R9IG9wZXJhdGlvblxuICogQHJldHVybiB7U3RhdGV9XG4gKi9cblxuZnVuY3Rpb24gcmVtb3ZlTWFyayhzdGF0ZSwgb3BlcmF0aW9uKSB7XG4gIGNvbnN0IHsgcGF0aCwgb2Zmc2V0LCBsZW5ndGggfSA9IG9wZXJhdGlvblxuICBjb25zdCBtYXJrID0gTm9ybWFsaXplLm1hcmsob3BlcmF0aW9uLm1hcmspXG4gIGxldCB7IGRvY3VtZW50IH0gPSBzdGF0ZVxuICBsZXQgbm9kZSA9IGRvY3VtZW50LmFzc2VydFBhdGgocGF0aClcbiAgbm9kZSA9IG5vZGUucmVtb3ZlTWFyayhvZmZzZXQsIGxlbmd0aCwgbWFyaylcbiAgZG9jdW1lbnQgPSBkb2N1bWVudC51cGRhdGVEZXNjZW5kYW50KG5vZGUpXG4gIHN0YXRlID0gc3RhdGUuc2V0KCdkb2N1bWVudCcsIGRvY3VtZW50KVxuICByZXR1cm4gc3RhdGVcbn1cblxuLyoqXG4gKiBSZW1vdmUgYSBub2RlIGJ5IGBwYXRoYC5cbiAqXG4gKiBAcGFyYW0ge1N0YXRlfSBzdGF0ZVxuICogQHBhcmFtIHtPYmplY3R9IG9wZXJhdGlvblxuICogQHJldHVybiB7U3RhdGV9XG4gKi9cblxuZnVuY3Rpb24gcmVtb3ZlTm9kZShzdGF0ZSwgb3BlcmF0aW9uKSB7XG4gIGNvbnN0IHsgcGF0aCB9ID0gb3BlcmF0aW9uXG4gIGxldCB7IGRvY3VtZW50LCBzZWxlY3Rpb24gfSA9IHN0YXRlXG4gIGNvbnN0IHsgc3RhcnRLZXksIGVuZEtleSB9ID0gc2VsZWN0aW9uXG4gIGNvbnN0IG5vZGUgPSBkb2N1bWVudC5hc3NlcnRQYXRoKHBhdGgpXG5cbiAgLy8gSWYgdGhlIHNlbGVjdGlvbiBpcyBzZXQsIGNoZWNrIHRvIHNlZSBpZiBpdCBuZWVkcyB0byBiZSB1cGRhdGVkLlxuICBpZiAoc2VsZWN0aW9uLmlzU2V0KSB7XG4gICAgY29uc3QgaGFzU3RhcnROb2RlID0gbm9kZS5oYXNOb2RlKHN0YXJ0S2V5KVxuICAgIGNvbnN0IGhhc0VuZE5vZGUgPSBub2RlLmhhc05vZGUoZW5kS2V5KVxuXG4gICAgLy8gSWYgb25lIG9mIHRoZSBzZWxlY3Rpb24ncyBub2RlcyBpcyBiZWluZyByZW1vdmVkLCB3ZSBuZWVkIHRvIHVwZGF0ZSBpdC5cbiAgICBpZiAoaGFzU3RhcnROb2RlKSB7XG4gICAgICBjb25zdCBwcmV2ID0gZG9jdW1lbnQuZ2V0UHJldmlvdXNUZXh0KHN0YXJ0S2V5KVxuICAgICAgY29uc3QgbmV4dCA9IGRvY3VtZW50LmdldE5leHRUZXh0KHN0YXJ0S2V5KVxuXG4gICAgICBpZiAocHJldikge1xuICAgICAgICBzZWxlY3Rpb24gPSBzZWxlY3Rpb24ubW92ZVN0YXJ0VG8ocHJldi5rZXksIHByZXYubGVuZ3RoKVxuICAgICAgfSBlbHNlIGlmIChuZXh0KSB7XG4gICAgICAgIHNlbGVjdGlvbiA9IHNlbGVjdGlvbi5tb3ZlU3RhcnRUbyhuZXh0LmtleSwgMClcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHNlbGVjdGlvbiA9IHNlbGVjdGlvbi5kZXNlbGVjdCgpXG4gICAgICB9XG4gICAgfVxuXG4gICAgaWYgKGhhc0VuZE5vZGUpIHtcbiAgICAgIGNvbnN0IHByZXYgPSBkb2N1bWVudC5nZXRQcmV2aW91c1RleHQoZW5kS2V5KVxuICAgICAgY29uc3QgbmV4dCA9IGRvY3VtZW50LmdldE5leHRUZXh0KGVuZEtleSlcblxuICAgICAgaWYgKHByZXYpIHtcbiAgICAgICAgc2VsZWN0aW9uID0gc2VsZWN0aW9uLm1vdmVFbmRUbyhwcmV2LmtleSwgcHJldi5sZW5ndGgpXG4gICAgICB9IGVsc2UgaWYgKG5leHQpIHtcbiAgICAgICAgc2VsZWN0aW9uID0gc2VsZWN0aW9uLm1vdmVFbmRUbyhuZXh0LmtleSwgMClcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHNlbGVjdGlvbiA9IHNlbGVjdGlvbi5kZXNlbGVjdCgpXG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgLy8gUmVtb3ZlIHRoZSBub2RlIGZyb20gdGhlIGRvY3VtZW50LlxuICBsZXQgcGFyZW50ID0gZG9jdW1lbnQuZ2V0UGFyZW50KG5vZGUua2V5KVxuICBjb25zdCBpbmRleCA9IHBhcmVudC5ub2Rlcy5pbmRleE9mKG5vZGUpXG4gIGNvbnN0IGlzUGFyZW50ID0gZG9jdW1lbnQgPT0gcGFyZW50XG4gIHBhcmVudCA9IHBhcmVudC5yZW1vdmVOb2RlKGluZGV4KVxuICBkb2N1bWVudCA9IGlzUGFyZW50ID8gcGFyZW50IDogZG9jdW1lbnQudXBkYXRlRGVzY2VuZGFudChwYXJlbnQpXG5cbiAgLy8gVXBkYXRlIHRoZSBkb2N1bWVudCBhbmQgc2VsZWN0aW9uLlxuICBzdGF0ZSA9IHN0YXRlLnNldCgnZG9jdW1lbnQnLCBkb2N1bWVudCkuc2V0KCdzZWxlY3Rpb24nLCBzZWxlY3Rpb24pXG4gIHJldHVybiBzdGF0ZVxufVxuXG4vKipcbiAqIFJlbW92ZSB0ZXh0IGF0IGBvZmZzZXRgIGFuZCBgbGVuZ3RoYCBpbiBub2RlIGJ5IGBwYXRoYC5cbiAqXG4gKiBAcGFyYW0ge1N0YXRlfSBzdGF0ZVxuICogQHBhcmFtIHtPYmplY3R9IG9wZXJhdGlvblxuICogQHJldHVybiB7U3RhdGV9XG4gKi9cblxuZnVuY3Rpb24gcmVtb3ZlVGV4dChzdGF0ZSwgb3BlcmF0aW9uKSB7XG4gIGNvbnN0IHsgcGF0aCwgb2Zmc2V0LCBsZW5ndGggfSA9IG9wZXJhdGlvblxuICBjb25zdCByYW5nZU9mZnNldCA9IG9mZnNldCArIGxlbmd0aFxuICBsZXQgeyBkb2N1bWVudCwgc2VsZWN0aW9uIH0gPSBzdGF0ZVxuICBjb25zdCB7IGFuY2hvcktleSwgZm9jdXNLZXksIGFuY2hvck9mZnNldCwgZm9jdXNPZmZzZXQgfSA9IHNlbGVjdGlvblxuICBsZXQgbm9kZSA9IGRvY3VtZW50LmFzc2VydFBhdGgocGF0aClcblxuICAvLyBVcGRhdGUgdGhlIHNlbGVjdGlvblxuICBpZiAoYW5jaG9yS2V5ID09IG5vZGUua2V5ICYmIGFuY2hvck9mZnNldCA+PSByYW5nZU9mZnNldCkge1xuICAgIHNlbGVjdGlvbiA9IHNlbGVjdGlvbi5tb3ZlQW5jaG9yKC1sZW5ndGgpXG4gIH1cbiAgaWYgKGZvY3VzS2V5ID09IG5vZGUua2V5ICYmIGZvY3VzT2Zmc2V0ID49IHJhbmdlT2Zmc2V0KSB7XG4gICAgc2VsZWN0aW9uID0gc2VsZWN0aW9uLm1vdmVGb2N1cygtbGVuZ3RoKVxuICB9XG5cbiAgbm9kZSA9IG5vZGUucmVtb3ZlVGV4dChvZmZzZXQsIGxlbmd0aClcbiAgZG9jdW1lbnQgPSBkb2N1bWVudC51cGRhdGVEZXNjZW5kYW50KG5vZGUpXG4gIHN0YXRlID0gc3RhdGUuc2V0KCdkb2N1bWVudCcsIGRvY3VtZW50KS5zZXQoJ3NlbGVjdGlvbicsIHNlbGVjdGlvbilcbiAgcmV0dXJuIHN0YXRlXG59XG5cbi8qKlxuICogU2V0IGBkYXRhYCBvbiBgc3RhdGVgLlxuICpcbiAqIEBwYXJhbSB7U3RhdGV9IHN0YXRlXG4gKiBAcGFyYW0ge09iamVjdH0gb3BlcmF0aW9uXG4gKiBAcmV0dXJuIHtTdGF0ZX1cbiAqL1xuXG5mdW5jdGlvbiBzZXREYXRhKHN0YXRlLCBvcGVyYXRpb24pIHtcbiAgY29uc3QgeyBwcm9wZXJ0aWVzIH0gPSBvcGVyYXRpb25cbiAgbGV0IHsgZGF0YSB9ID0gc3RhdGVcblxuICBkYXRhID0gZGF0YS5tZXJnZShwcm9wZXJ0aWVzKVxuICBzdGF0ZSA9IHN0YXRlLnNldCgnZGF0YScsIGRhdGEpXG4gIHJldHVybiBzdGF0ZVxufVxuXG4vKipcbiAqIFNldCBgcHJvcGVydGllc2Agb24gbWFyayBvbiB0ZXh0IGF0IGBvZmZzZXRgIGFuZCBgbGVuZ3RoYCBpbiBub2RlIGJ5IGBwYXRoYC5cbiAqXG4gKiBAcGFyYW0ge1N0YXRlfSBzdGF0ZVxuICogQHBhcmFtIHtPYmplY3R9IG9wZXJhdGlvblxuICogQHJldHVybiB7U3RhdGV9XG4gKi9cblxuZnVuY3Rpb24gc2V0TWFyayhzdGF0ZSwgb3BlcmF0aW9uKSB7XG4gIGNvbnN0IHsgcGF0aCwgb2Zmc2V0LCBsZW5ndGgsIHByb3BlcnRpZXMgfSA9IG9wZXJhdGlvblxuICBjb25zdCBtYXJrID0gTm9ybWFsaXplLm1hcmsob3BlcmF0aW9uLm1hcmspXG4gIGxldCB7IGRvY3VtZW50IH0gPSBzdGF0ZVxuICBsZXQgbm9kZSA9IGRvY3VtZW50LmFzc2VydFBhdGgocGF0aClcbiAgbm9kZSA9IG5vZGUudXBkYXRlTWFyayhvZmZzZXQsIGxlbmd0aCwgbWFyaywgcHJvcGVydGllcylcbiAgZG9jdW1lbnQgPSBkb2N1bWVudC51cGRhdGVEZXNjZW5kYW50KG5vZGUpXG4gIHN0YXRlID0gc3RhdGUuc2V0KCdkb2N1bWVudCcsIGRvY3VtZW50KVxuICByZXR1cm4gc3RhdGVcbn1cblxuLyoqXG4gKiBTZXQgYHByb3BlcnRpZXNgIG9uIGEgbm9kZSBieSBgcGF0aGAuXG4gKlxuICogQHBhcmFtIHtTdGF0ZX0gc3RhdGVcbiAqIEBwYXJhbSB7T2JqZWN0fSBvcGVyYXRpb25cbiAqIEByZXR1cm4ge1N0YXRlfVxuICovXG5cbmZ1bmN0aW9uIHNldE5vZGUoc3RhdGUsIG9wZXJhdGlvbikge1xuICBjb25zdCB7IHBhdGgsIHByb3BlcnRpZXMgfSA9IG9wZXJhdGlvblxuICBsZXQgeyBkb2N1bWVudCB9ID0gc3RhdGVcbiAgbGV0IG5vZGUgPSBkb2N1bWVudC5hc3NlcnRQYXRoKHBhdGgpXG5cbiAgLy8gRGVwcmVjYXRlIHRoZSBhYmlsaXR5IHRvIG92ZXJ3aXRlIGEgbm9kZSdzIGNoaWxkcmVuLlxuICBpZiAocHJvcGVydGllcy5ub2RlcyAmJiBwcm9wZXJ0aWVzLm5vZGVzICE9IG5vZGUubm9kZXMpIHtcbiAgICB3YXJuKCdVcGRhdGluZyBhIE5vZGVcXCdzIGBub2Rlc2AgcHJvcGVydHkgdmlhIGBzZXROb2RlKClgIGlzIG5vdCBhbGxvd2VkLiBVc2UgdGhlIGFwcHJvcHJpYXRlIGluc2VydGlvbiBhbmQgcmVtb3ZhbCBvcGVyYXRpb25zIGluc3RlYWQuIFRoZSBvcGVhcnRpb24gaW4gcXVlc3Rpb24gd2FzOicsIG9wZXJhdGlvbilcbiAgICBkZWxldGUgcHJvcGVydGllcy5ub2Rlc1xuICB9XG5cbiAgLy8gRGVwcmVjYXRlIHRoZSBhYmlsaXR5IHRvIGNoYW5nZSBhIG5vZGUncyBrZXkuXG4gIGlmIChwcm9wZXJ0aWVzLmtleSAmJiBwcm9wZXJ0aWVzLmtleSAhPSBub2RlLmtleSkge1xuICAgIHdhcm4oJ1VwZGF0aW5nIGEgTm9kZVxcJ3MgYGtleWAgcHJvcGVydHkgdmlhIGBzZXROb2RlKClgIGlzIG5vdCBhbGxvd2VkLiBUaGVyZSBzaG91bGQgYmUgbm8gcmVhc29uIHRvIGRvIHRoaXMuIFRoZSBvcGVhcnRpb24gaW4gcXVlc3Rpb24gd2FzOicsIG9wZXJhdGlvbilcbiAgICBkZWxldGUgcHJvcGVydGllcy5rZXlcbiAgfVxuXG4gIG5vZGUgPSBub2RlLm1lcmdlKHByb3BlcnRpZXMpXG4gIGRvY3VtZW50ID0gbm9kZS5raW5kID09PSAnZG9jdW1lbnQnID8gbm9kZSA6IGRvY3VtZW50LnVwZGF0ZURlc2NlbmRhbnQobm9kZSlcbiAgc3RhdGUgPSBzdGF0ZS5zZXQoJ2RvY3VtZW50JywgZG9jdW1lbnQpXG4gIHJldHVybiBzdGF0ZVxufVxuXG4vKipcbiAqIFNldCBgcHJvcGVydGllc2Agb24gdGhlIHNlbGVjdGlvbi5cbiAqXG4gKiBAcGFyYW0ge1N0YXRlfSBzdGF0ZVxuICogQHBhcmFtIHtPYmplY3R9IG9wZXJhdGlvblxuICogQHJldHVybiB7U3RhdGV9XG4gKi9cblxuZnVuY3Rpb24gc2V0U2VsZWN0aW9uKHN0YXRlLCBvcGVyYXRpb24pIHtcbiAgY29uc3QgcHJvcGVydGllcyA9IHsgLi4ub3BlcmF0aW9uLnByb3BlcnRpZXMgfVxuICBsZXQgeyBkb2N1bWVudCwgc2VsZWN0aW9uIH0gPSBzdGF0ZVxuXG4gIGlmIChwcm9wZXJ0aWVzLmFuY2hvclBhdGggIT09IHVuZGVmaW5lZCkge1xuICAgIHByb3BlcnRpZXMuYW5jaG9yS2V5ID0gcHJvcGVydGllcy5hbmNob3JQYXRoID09PSBudWxsXG4gICAgICA/IG51bGxcbiAgICAgIDogZG9jdW1lbnQuYXNzZXJ0UGF0aChwcm9wZXJ0aWVzLmFuY2hvclBhdGgpLmtleVxuICAgIGRlbGV0ZSBwcm9wZXJ0aWVzLmFuY2hvclBhdGhcbiAgfVxuXG4gIGlmIChwcm9wZXJ0aWVzLmZvY3VzUGF0aCAhPT0gdW5kZWZpbmVkKSB7XG4gICAgcHJvcGVydGllcy5mb2N1c0tleSA9IHByb3BlcnRpZXMuZm9jdXNQYXRoID09PSBudWxsXG4gICAgICA/IG51bGxcbiAgICAgIDogZG9jdW1lbnQuYXNzZXJ0UGF0aChwcm9wZXJ0aWVzLmZvY3VzUGF0aCkua2V5XG4gICAgZGVsZXRlIHByb3BlcnRpZXMuZm9jdXNQYXRoXG4gIH1cblxuICBzZWxlY3Rpb24gPSBzZWxlY3Rpb24ubWVyZ2UocHJvcGVydGllcylcbiAgc2VsZWN0aW9uID0gc2VsZWN0aW9uLm5vcm1hbGl6ZShkb2N1bWVudClcbiAgc3RhdGUgPSBzdGF0ZS5zZXQoJ3NlbGVjdGlvbicsIHNlbGVjdGlvbilcbiAgcmV0dXJuIHN0YXRlXG59XG5cbi8qKlxuICogU3BsaXQgYSBub2RlIGJ5IGBwYXRoYCBhdCBgb2Zmc2V0YC5cbiAqXG4gKiBAcGFyYW0ge1N0YXRlfSBzdGF0ZVxuICogQHBhcmFtIHtPYmplY3R9IG9wZXJhdGlvblxuICogQHJldHVybiB7U3RhdGV9XG4gKi9cblxuZnVuY3Rpb24gc3BsaXROb2RlKHN0YXRlLCBvcGVyYXRpb24pIHtcbiAgY29uc3QgeyBwYXRoLCBwb3NpdGlvbiB9ID0gb3BlcmF0aW9uXG4gIGxldCB7IGRvY3VtZW50LCBzZWxlY3Rpb24gfSA9IHN0YXRlXG5cbiAgLy8gQ2FsY3VsYXRlIGEgZmV3IHRoaW5ncy4uLlxuICBjb25zdCBub2RlID0gZG9jdW1lbnQuYXNzZXJ0UGF0aChwYXRoKVxuICBsZXQgcGFyZW50ID0gZG9jdW1lbnQuZ2V0UGFyZW50KG5vZGUua2V5KVxuICBjb25zdCBpbmRleCA9IHBhcmVudC5ub2Rlcy5pbmRleE9mKG5vZGUpXG4gIGNvbnN0IGlzUGFyZW50ID0gcGFyZW50ID09IGRvY3VtZW50XG5cbiAgLy8gU3BsaXQgdGhlIG5vZGUgYnkgaXRzIHBhcmVudC5cbiAgcGFyZW50ID0gcGFyZW50LnNwbGl0Tm9kZShpbmRleCwgcG9zaXRpb24pXG4gIGRvY3VtZW50ID0gaXNQYXJlbnQgPyBwYXJlbnQgOiBkb2N1bWVudC51cGRhdGVEZXNjZW5kYW50KHBhcmVudClcblxuICAvLyBEZXRlcm1pbmUgd2hldGhlciB3ZSBuZWVkIHRvIHVwZGF0ZSB0aGUgc2VsZWN0aW9uLi4uXG4gIGNvbnN0IHsgc3RhcnRLZXksIGVuZEtleSwgc3RhcnRPZmZzZXQsIGVuZE9mZnNldCB9ID0gc2VsZWN0aW9uXG4gIGNvbnN0IG5leHQgPSBkb2N1bWVudC5nZXROZXh0VGV4dChub2RlLmtleSlcblxuICAvLyBJZiB0aGUgc3RhcnQgcG9pbnQgaXMgYWZ0ZXIgb3IgZXF1YWwgdG8gdGhlIHNwbGl0LCB1cGRhdGUgaXQuXG4gIGlmIChub2RlLmtleSA9PSBzdGFydEtleSAmJiBwb3NpdGlvbiA8PSBzdGFydE9mZnNldCkge1xuICAgIHNlbGVjdGlvbiA9IHNlbGVjdGlvbi5tb3ZlU3RhcnRUbyhuZXh0LmtleSwgc3RhcnRPZmZzZXQgLSBwb3NpdGlvbilcbiAgfVxuXG4gIC8vIElmIHRoZSBlbmQgcG9pbnQgaXMgYWZ0ZXIgb3IgZXF1YWwgdG8gdGhlIHNwbGl0LCB1cGRhdGUgaXQuXG4gIGlmIChub2RlLmtleSA9PSBlbmRLZXkgJiYgcG9zaXRpb24gPD0gZW5kT2Zmc2V0KSB7XG4gICAgc2VsZWN0aW9uID0gc2VsZWN0aW9uLm1vdmVFbmRUbyhuZXh0LmtleSwgZW5kT2Zmc2V0IC0gcG9zaXRpb24pXG4gIH1cblxuICAvLyBSZXR1cm4gdGhlIHVwZGF0ZWQgc3RhdGUuXG4gIHN0YXRlID0gc3RhdGUuc2V0KCdkb2N1bWVudCcsIGRvY3VtZW50KS5zZXQoJ3NlbGVjdGlvbicsIHNlbGVjdGlvbilcbiAgcmV0dXJuIHN0YXRlXG59XG5cbi8qKlxuICogRXhwb3J0LlxuICpcbiAqIEB0eXBlIHtGdW5jdGlvbn1cbiAqL1xuXG5leHBvcnQgZGVmYXVsdCBhcHBseVxuIl19