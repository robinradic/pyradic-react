'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _pick = require('lodash/pick');

var _pick2 = _interopRequireDefault(_pick);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Operations.
 *
 * @type {Object}
 */

var INVERTERS = {

  /**
   * Node operations.
   */

  insert_node: function insert_node(operation) {
    return _extends({}, operation, {
      type: 'remove_node'
    });
  },
  remove_node: function remove_node(operation) {
    return _extends({}, operation, {
      type: 'insert_node'
    });
  },
  move_node: function move_node(operation) {
    return _extends({}, operation, {
      path: operation.newPath,
      newPath: operation.path
    });
  },
  join_node: function join_node(operation) {
    return _extends({}, operation, {
      type: 'split_node',
      path: operation.path.slice(-1).concat([operation.path[operation.path.length - 1] - 1])
    });
  },
  split_node: function split_node(operation) {
    return _extends({}, operation, {
      type: 'join_node',
      path: operation.path.slice(-1).concat([operation.path[operation.path.length - 1] + 1])
    });
  },
  set_node: function set_node(operation) {
    var properties = operation.properties,
        node = operation.node;

    return _extends({}, operation, {
      node: node.merge(properties),
      properties: (0, _pick2.default)(node, Object.keys(properties))
    });
  },


  /**
   * Text operations.
   */

  insert_text: function insert_text(operation) {
    return _extends({}, operation, {
      type: 'remove_text'
    });
  },
  remove_text: function remove_text(operation) {
    return _extends({}, operation, {
      type: 'insert_text'
    });
  },


  /**
   * Mark operations.
   */

  add_mark: function add_mark(operation) {
    return _extends({}, operation, {
      type: 'remove_mark'
    });
  },
  remove_mark: function remove_mark(operation) {
    return _extends({}, operation, {
      type: 'add_mark'
    });
  },
  set_mark: function set_mark(operation) {
    var properties = operation.properties,
        mark = operation.mark;

    return _extends({}, operation, {
      mark: mark.merge(properties),
      properties: (0, _pick2.default)(mark, Object.keys(properties))
    });
  },


  /**
   * State operations.
   */

  set_data: function set_data(operation) {
    var properties = operation.properties,
        data = operation.data;

    return _extends({}, operation, {
      data: data.merge(properties),
      properties: (0, _pick2.default)(data, Object.keys(properties))
    });
  },
  set_selection: function set_selection(operation) {
    var properties = operation.properties,
        selection = operation.selection;

    return _extends({}, operation, {
      selection: selection.merge(properties),
      properties: (0, _pick2.default)(selection, Object.keys(properties))
    });
  }
};

/**
 * Invert an `operation`.
 *
 * @param {Object} operation
 * @return {Object}
 */

function invertOperation(operation) {
  var type = operation.type;

  var fn = INVERTERS[type];

  if (!fn) {
    throw new Error('Unknown operation type: "' + type + '".');
  }

  var inverse = fn(operation);
  return inverse;
}

/**
 * Export.
 *
 * @type {Function}
 */

exports.default = invertOperation;