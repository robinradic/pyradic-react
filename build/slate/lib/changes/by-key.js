'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _normalize = require('../utils/normalize');

var _normalize2 = _interopRequireDefault(_normalize);

var _core = require('../schemas/core');

var _core2 = _interopRequireDefault(_core);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

/**
 * Changes.
 *
 * @type {Object}
 */

var Changes = {};

/**
 * Add mark to text at `offset` and `length` in node by `key`.
 *
 * @param {Change} change
 * @param {String} key
 * @param {Number} offset
 * @param {Number} length
 * @param {Mixed} mark
 * @param {Object} options
 *   @property {Boolean} normalize
 */

Changes.addMarkByKey = function (change, key, offset, length, mark) {
  var options = arguments.length > 5 && arguments[5] !== undefined ? arguments[5] : {};

  mark = _normalize2.default.mark(mark);
  var _options$normalize = options.normalize,
      normalize = _options$normalize === undefined ? true : _options$normalize;
  var state = change.state;
  var document = state.document;

  var path = document.getPath(key);

  change.applyOperation({
    type: 'add_mark',
    path: path,
    offset: offset,
    length: length,
    mark: mark
  });

  if (normalize) {
    var parent = document.getParent(key);
    change.normalizeNodeByKey(parent.key, _core2.default);
  }
};

/**
 * Insert a `fragment` at `index` in a node by `key`.
 *
 * @param {Change} change
 * @param {String} key
 * @param {Number} index
 * @param {Fragment} fragment
 * @param {Object} options
 *   @property {Boolean} normalize
 */

Changes.insertFragmentByKey = function (change, key, index, fragment) {
  var options = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : {};
  var _options$normalize2 = options.normalize,
      normalize = _options$normalize2 === undefined ? true : _options$normalize2;


  fragment.nodes.forEach(function (node, i) {
    change.insertNodeByKey(key, index + i, node);
  });

  if (normalize) {
    change.normalizeNodeByKey(key, _core2.default);
  }
};

/**
 * Insert a `node` at `index` in a node by `key`.
 *
 * @param {Change} change
 * @param {String} key
 * @param {Number} index
 * @param {Node} node
 * @param {Object} options
 *   @property {Boolean} normalize
 */

Changes.insertNodeByKey = function (change, key, index, node) {
  var options = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : {};
  var _options$normalize3 = options.normalize,
      normalize = _options$normalize3 === undefined ? true : _options$normalize3;
  var state = change.state;
  var document = state.document;

  var path = document.getPath(key);

  change.applyOperation({
    type: 'insert_node',
    path: [].concat(_toConsumableArray(path), [index]),
    node: node
  });

  if (normalize) {
    change.normalizeNodeByKey(key, _core2.default);
  }
};

/**
 * Insert `text` at `offset` in node by `key`.
 *
 * @param {Change} change
 * @param {String} key
 * @param {Number} offset
 * @param {String} text
 * @param {Set<Mark>} marks (optional)
 * @param {Object} options
 *   @property {Boolean} normalize
 */

Changes.insertTextByKey = function (change, key, offset, text, marks) {
  var options = arguments.length > 5 && arguments[5] !== undefined ? arguments[5] : {};
  var _options$normalize4 = options.normalize,
      normalize = _options$normalize4 === undefined ? true : _options$normalize4;
  var state = change.state;
  var document = state.document;

  var path = document.getPath(key);
  var node = document.getNode(key);
  marks = marks || node.getMarksAtIndex(offset);

  change.applyOperation({
    type: 'insert_text',
    path: path,
    offset: offset,
    text: text,
    marks: marks
  });

  if (normalize) {
    var parent = document.getParent(key);
    change.normalizeNodeByKey(parent.key, _core2.default);
  }
};

/**
 * Merge a node by `key` with the previous node.
 *
 * @param {Change} change
 * @param {String} key
 * @param {Object} options
 *   @property {Boolean} normalize
 */

Changes.mergeNodeByKey = function (change, key) {
  var options = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};
  var _options$normalize5 = options.normalize,
      normalize = _options$normalize5 === undefined ? true : _options$normalize5;
  var state = change.state;
  var document = state.document;

  var path = document.getPath(key);
  var previous = document.getPreviousSibling(key);

  if (!previous) {
    throw new Error('Unable to merge node with key "' + key + '", no previous key.');
  }

  var position = previous.kind == 'text' ? previous.text.length : previous.nodes.size;

  change.applyOperation({
    type: 'merge_node',
    path: path,
    position: position
  });

  if (normalize) {
    var parent = document.getParent(key);
    change.normalizeNodeByKey(parent.key, _core2.default);
  }
};

/**
 * Move a node by `key` to a new parent by `newKey` and `index`.
 * `newKey` is the key of the container (it can be the document itself)
 *
 * @param {Change} change
 * @param {String} key
 * @param {String} newKey
 * @param {Number} index
 * @param {Object} options
 *   @property {Boolean} normalize
 */

Changes.moveNodeByKey = function (change, key, newKey, newIndex) {
  var options = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : {};
  var _options$normalize6 = options.normalize,
      normalize = _options$normalize6 === undefined ? true : _options$normalize6;
  var state = change.state;
  var document = state.document;

  var path = document.getPath(key);
  var newPath = document.getPath(newKey);

  change.applyOperation({
    type: 'move_node',
    path: path,
    newPath: [].concat(_toConsumableArray(newPath), [newIndex])
  });

  if (normalize) {
    var parent = document.getCommonAncestor(key, newKey);
    change.normalizeNodeByKey(parent.key, _core2.default);
  }
};

/**
 * Remove mark from text at `offset` and `length` in node by `key`.
 *
 * @param {Change} change
 * @param {String} key
 * @param {Number} offset
 * @param {Number} length
 * @param {Mark} mark
 * @param {Object} options
 *   @property {Boolean} normalize
 */

Changes.removeMarkByKey = function (change, key, offset, length, mark) {
  var options = arguments.length > 5 && arguments[5] !== undefined ? arguments[5] : {};

  mark = _normalize2.default.mark(mark);
  var _options$normalize7 = options.normalize,
      normalize = _options$normalize7 === undefined ? true : _options$normalize7;
  var state = change.state;
  var document = state.document;

  var path = document.getPath(key);

  change.applyOperation({
    type: 'remove_mark',
    path: path,
    offset: offset,
    length: length,
    mark: mark
  });

  if (normalize) {
    var parent = document.getParent(key);
    change.normalizeNodeByKey(parent.key, _core2.default);
  }
};

/**
 * Remove a node by `key`.
 *
 * @param {Change} change
 * @param {String} key
 * @param {Object} options
 *   @property {Boolean} normalize
 */

Changes.removeNodeByKey = function (change, key) {
  var options = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};
  var _options$normalize8 = options.normalize,
      normalize = _options$normalize8 === undefined ? true : _options$normalize8;
  var state = change.state;
  var document = state.document;

  var path = document.getPath(key);
  var node = document.getNode(key);

  change.applyOperation({
    type: 'remove_node',
    path: path,
    node: node
  });

  if (normalize) {
    var parent = document.getParent(key);
    change.normalizeNodeByKey(parent.key, _core2.default);
  }
};

/**
 * Remove text at `offset` and `length` in node by `key`.
 *
 * @param {Change} change
 * @param {String} key
 * @param {Number} offset
 * @param {Number} length
 * @param {Object} options
 *   @property {Boolean} normalize
 */

Changes.removeTextByKey = function (change, key, offset, length) {
  var options = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : {};
  var _options$normalize9 = options.normalize,
      normalize = _options$normalize9 === undefined ? true : _options$normalize9;
  var state = change.state;
  var document = state.document;

  var path = document.getPath(key);
  var node = document.getNode(key);
  var ranges = node.getRanges();
  var text = node.text;


  var removals = [];
  var bx = offset;
  var by = offset + length;
  var o = 0;

  ranges.forEach(function (range) {
    var marks = range.marks;

    var ax = o;
    var ay = ax + range.text.length;

    o += range.text.length;

    // If the range doesn't overlap with the removal, continue on.
    if (ay < bx || by < ax) return;

    // Otherwise, determine which offset and characters overlap.
    var start = Math.max(ax, bx);
    var end = Math.min(ay, by);
    var string = text.slice(start, end);

    removals.push({
      type: 'remove_text',
      path: path,
      offset: start,
      text: string,
      marks: marks
    });
  });

  // Apply the removals in reverse order, so that subsequent removals aren't
  // impacted by previous ones.
  removals.reverse().forEach(function (op) {
    change.applyOperation(op);
  });

  if (normalize) {
    var block = document.getClosestBlock(key);
    change.normalizeNodeByKey(block.key, _core2.default);
  }
};

/**
 * Set `properties` on mark on text at `offset` and `length` in node by `key`.
 *
 * @param {Change} change
 * @param {String} key
 * @param {Number} offset
 * @param {Number} length
 * @param {Mark} mark
 * @param {Object} options
 *   @property {Boolean} normalize
 */

Changes.setMarkByKey = function (change, key, offset, length, mark, properties) {
  var options = arguments.length > 6 && arguments[6] !== undefined ? arguments[6] : {};

  mark = _normalize2.default.mark(mark);
  properties = _normalize2.default.markProperties(properties);
  var _options$normalize10 = options.normalize,
      normalize = _options$normalize10 === undefined ? true : _options$normalize10;
  var state = change.state;
  var document = state.document;

  var path = document.getPath(key);

  change.applyOperation({
    type: 'set_mark',
    path: path,
    offset: offset,
    length: length,
    mark: mark,
    properties: properties
  });

  if (normalize) {
    var parent = document.getParent(key);
    change.normalizeNodeByKey(parent.key, _core2.default);
  }
};

/**
 * Set `properties` on a node by `key`.
 *
 * @param {Change} change
 * @param {String} key
 * @param {Object|String} properties
 * @param {Object} options
 *   @property {Boolean} normalize
 */

Changes.setNodeByKey = function (change, key, properties) {
  var options = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : {};

  properties = _normalize2.default.nodeProperties(properties);
  var _options$normalize11 = options.normalize,
      normalize = _options$normalize11 === undefined ? true : _options$normalize11;
  var state = change.state;
  var document = state.document;

  var path = document.getPath(key);
  var node = document.getNode(key);

  change.applyOperation({
    type: 'set_node',
    path: path,
    node: node,
    properties: properties
  });

  if (normalize) {
    change.normalizeNodeByKey(node.key, _core2.default);
  }
};

/**
 * Split a node by `key` at `position`.
 *
 * @param {Change} change
 * @param {String} key
 * @param {Number} position
 * @param {Object} options
 *   @property {Boolean} normalize
 */

Changes.splitNodeByKey = function (change, key, position) {
  var options = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : {};
  var _options$normalize12 = options.normalize,
      normalize = _options$normalize12 === undefined ? true : _options$normalize12;
  var state = change.state;
  var document = state.document;

  var path = document.getPath(key);

  change.applyOperation({
    type: 'split_node',
    path: path,
    position: position
  });

  if (normalize) {
    var parent = document.getParent(key);
    change.normalizeNodeByKey(parent.key, _core2.default);
  }
};

/**
 * Split a node deeply down the tree by `key`, `textKey` and `textOffset`.
 *
 * @param {Change} change
 * @param {String} key
 * @param {Number} position
 * @param {Object} options
 *   @property {Boolean} normalize
 */

Changes.splitDescendantsByKey = function (change, key, textKey, textOffset) {
  var options = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : {};

  if (key == textKey) {
    change.splitNodeByKey(textKey, textOffset, options);
    return;
  }

  var _options$normalize13 = options.normalize,
      normalize = _options$normalize13 === undefined ? true : _options$normalize13;
  var state = change.state;
  var document = state.document;


  var text = document.getNode(textKey);
  var ancestors = document.getAncestors(textKey);
  var nodes = ancestors.skipUntil(function (a) {
    return a.key == key;
  }).reverse().unshift(text);
  var previous = void 0;

  nodes.forEach(function (node) {
    var index = previous ? node.nodes.indexOf(previous) + 1 : textOffset;
    previous = node;
    change.splitNodeByKey(node.key, index, { normalize: false });
  });

  if (normalize) {
    var parent = document.getParent(key);
    change.normalizeNodeByKey(parent.key, _core2.default);
  }
};

/**
 * Unwrap content from an inline parent with `properties`.
 *
 * @param {Change} change
 * @param {String} key
 * @param {Object|String} properties
 * @param {Object} options
 *   @property {Boolean} normalize
 */

Changes.unwrapInlineByKey = function (change, key, properties, options) {
  var state = change.state;
  var document = state.document,
      selection = state.selection;

  var node = document.assertDescendant(key);
  var first = node.getFirstText();
  var last = node.getLastText();
  var range = selection.moveToRangeOf(first, last);
  change.unwrapInlineAtRange(range, properties, options);
};

/**
 * Unwrap content from a block parent with `properties`.
 *
 * @param {Change} change
 * @param {String} key
 * @param {Object|String} properties
 * @param {Object} options
 *   @property {Boolean} normalize
 */

Changes.unwrapBlockByKey = function (change, key, properties, options) {
  var state = change.state;
  var document = state.document,
      selection = state.selection;

  var node = document.assertDescendant(key);
  var first = node.getFirstText();
  var last = node.getLastText();
  var range = selection.moveToRangeOf(first, last);
  change.unwrapBlockAtRange(range, properties, options);
};

/**
 * Unwrap a single node from its parent.
 *
 * If the node is surrounded with siblings, its parent will be
 * split. If the node is the only child, the parent is removed, and
 * simply replaced by the node itself.  Cannot unwrap a root node.
 *
 * @param {Change} change
 * @param {String} key
 * @param {Object} options
 *   @property {Boolean} normalize
 */

Changes.unwrapNodeByKey = function (change, key) {
  var options = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};
  var _options$normalize14 = options.normalize,
      normalize = _options$normalize14 === undefined ? true : _options$normalize14;
  var state = change.state;
  var document = state.document;

  var parent = document.getParent(key);
  var node = parent.getChild(key);

  var index = parent.nodes.indexOf(node);
  var isFirst = index === 0;
  var isLast = index === parent.nodes.size - 1;

  var parentParent = document.getParent(parent.key);
  var parentIndex = parentParent.nodes.indexOf(parent);

  if (parent.nodes.size === 1) {
    change.moveNodeByKey(key, parentParent.key, parentIndex, { normalize: false });
    change.removeNodeByKey(parent.key, options);
  } else if (isFirst) {
    // Just move the node before its parent.
    change.moveNodeByKey(key, parentParent.key, parentIndex, options);
  } else if (isLast) {
    // Just move the node after its parent.
    change.moveNodeByKey(key, parentParent.key, parentIndex + 1, options);
  } else {
    // Split the parent.
    change.splitNodeByKey(parent.key, index, { normalize: false });

    // Extract the node in between the splitted parent.
    change.moveNodeByKey(key, parentParent.key, parentIndex + 1, { normalize: false });

    if (normalize) {
      change.normalizeNodeByKey(parentParent.key, _core2.default);
    }
  }
};

/**
 * Wrap a node in an inline with `properties`.
 *
 * @param {Change} change
 * @param {String} key The node to wrap
 * @param {Block|Object|String} inline The wrapping inline (its children are discarded)
 * @param {Object} options
 *   @property {Boolean} normalize
 */

Changes.wrapInlineByKey = function (change, key, inline, options) {
  inline = _normalize2.default.inline(inline);
  inline = inline.set('nodes', inline.nodes.clear());

  var document = change.state.document;

  var node = document.assertDescendant(key);
  var parent = document.getParent(node.key);
  var index = parent.nodes.indexOf(node);

  change.insertNodeByKey(parent.key, index, inline, { normalize: false });
  change.moveNodeByKey(node.key, inline.key, 0, options);
};

/**
 * Wrap a node in a block with `properties`.
 *
 * @param {Change} change
 * @param {String} key The node to wrap
 * @param {Block|Object|String} block The wrapping block (its children are discarded)
 * @param {Object} options
 *   @property {Boolean} normalize
 */

Changes.wrapBlockByKey = function (change, key, block, options) {
  block = _normalize2.default.block(block);
  block = block.set('nodes', block.nodes.clear());

  var document = change.state.document;

  var node = document.assertDescendant(key);
  var parent = document.getParent(node.key);
  var index = parent.nodes.indexOf(node);

  change.insertNodeByKey(parent.key, index, block, { normalize: false });
  change.moveNodeByKey(node.key, block.key, 0, options);
};

/**
 * Export.
 *
 * @type {Object}
 */

exports.default = Changes;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9jaGFuZ2VzL2J5LWtleS5qcyJdLCJuYW1lcyI6WyJDaGFuZ2VzIiwiYWRkTWFya0J5S2V5IiwiY2hhbmdlIiwia2V5Iiwib2Zmc2V0IiwibGVuZ3RoIiwibWFyayIsIm9wdGlvbnMiLCJub3JtYWxpemUiLCJzdGF0ZSIsImRvY3VtZW50IiwicGF0aCIsImdldFBhdGgiLCJhcHBseU9wZXJhdGlvbiIsInR5cGUiLCJwYXJlbnQiLCJnZXRQYXJlbnQiLCJub3JtYWxpemVOb2RlQnlLZXkiLCJpbnNlcnRGcmFnbWVudEJ5S2V5IiwiaW5kZXgiLCJmcmFnbWVudCIsIm5vZGVzIiwiZm9yRWFjaCIsIm5vZGUiLCJpIiwiaW5zZXJ0Tm9kZUJ5S2V5IiwiaW5zZXJ0VGV4dEJ5S2V5IiwidGV4dCIsIm1hcmtzIiwiZ2V0Tm9kZSIsImdldE1hcmtzQXRJbmRleCIsIm1lcmdlTm9kZUJ5S2V5IiwicHJldmlvdXMiLCJnZXRQcmV2aW91c1NpYmxpbmciLCJFcnJvciIsInBvc2l0aW9uIiwia2luZCIsInNpemUiLCJtb3ZlTm9kZUJ5S2V5IiwibmV3S2V5IiwibmV3SW5kZXgiLCJuZXdQYXRoIiwiZ2V0Q29tbW9uQW5jZXN0b3IiLCJyZW1vdmVNYXJrQnlLZXkiLCJyZW1vdmVOb2RlQnlLZXkiLCJyZW1vdmVUZXh0QnlLZXkiLCJyYW5nZXMiLCJnZXRSYW5nZXMiLCJyZW1vdmFscyIsImJ4IiwiYnkiLCJvIiwicmFuZ2UiLCJheCIsImF5Iiwic3RhcnQiLCJNYXRoIiwibWF4IiwiZW5kIiwibWluIiwic3RyaW5nIiwic2xpY2UiLCJwdXNoIiwicmV2ZXJzZSIsIm9wIiwiYmxvY2siLCJnZXRDbG9zZXN0QmxvY2siLCJzZXRNYXJrQnlLZXkiLCJwcm9wZXJ0aWVzIiwibWFya1Byb3BlcnRpZXMiLCJzZXROb2RlQnlLZXkiLCJub2RlUHJvcGVydGllcyIsInNwbGl0Tm9kZUJ5S2V5Iiwic3BsaXREZXNjZW5kYW50c0J5S2V5IiwidGV4dEtleSIsInRleHRPZmZzZXQiLCJhbmNlc3RvcnMiLCJnZXRBbmNlc3RvcnMiLCJza2lwVW50aWwiLCJhIiwidW5zaGlmdCIsImluZGV4T2YiLCJ1bndyYXBJbmxpbmVCeUtleSIsInNlbGVjdGlvbiIsImFzc2VydERlc2NlbmRhbnQiLCJmaXJzdCIsImdldEZpcnN0VGV4dCIsImxhc3QiLCJnZXRMYXN0VGV4dCIsIm1vdmVUb1JhbmdlT2YiLCJ1bndyYXBJbmxpbmVBdFJhbmdlIiwidW53cmFwQmxvY2tCeUtleSIsInVud3JhcEJsb2NrQXRSYW5nZSIsInVud3JhcE5vZGVCeUtleSIsImdldENoaWxkIiwiaXNGaXJzdCIsImlzTGFzdCIsInBhcmVudFBhcmVudCIsInBhcmVudEluZGV4Iiwid3JhcElubGluZUJ5S2V5IiwiaW5saW5lIiwic2V0IiwiY2xlYXIiLCJ3cmFwQmxvY2tCeUtleSJdLCJtYXBwaW5ncyI6Ijs7Ozs7O0FBQ0E7Ozs7QUFDQTs7Ozs7Ozs7QUFFQTs7Ozs7O0FBTUEsSUFBTUEsVUFBVSxFQUFoQjs7QUFFQTs7Ozs7Ozs7Ozs7O0FBWUFBLFFBQVFDLFlBQVIsR0FBdUIsVUFBQ0MsTUFBRCxFQUFTQyxHQUFULEVBQWNDLE1BQWQsRUFBc0JDLE1BQXRCLEVBQThCQyxJQUE5QixFQUFxRDtBQUFBLE1BQWpCQyxPQUFpQix1RUFBUCxFQUFPOztBQUMxRUQsU0FBTyxvQkFBVUEsSUFBVixDQUFlQSxJQUFmLENBQVA7QUFEMEUsMkJBRTdDQyxPQUY2QyxDQUVsRUMsU0FGa0U7QUFBQSxNQUVsRUEsU0FGa0Usc0NBRXRELElBRnNEO0FBQUEsTUFHbEVDLEtBSGtFLEdBR3hEUCxNQUh3RCxDQUdsRU8sS0FIa0U7QUFBQSxNQUlsRUMsUUFKa0UsR0FJckRELEtBSnFELENBSWxFQyxRQUprRTs7QUFLMUUsTUFBTUMsT0FBT0QsU0FBU0UsT0FBVCxDQUFpQlQsR0FBakIsQ0FBYjs7QUFFQUQsU0FBT1csY0FBUCxDQUFzQjtBQUNwQkMsVUFBTSxVQURjO0FBRXBCSCxjQUZvQjtBQUdwQlAsa0JBSG9CO0FBSXBCQyxrQkFKb0I7QUFLcEJDO0FBTG9CLEdBQXRCOztBQVFBLE1BQUlFLFNBQUosRUFBZTtBQUNiLFFBQU1PLFNBQVNMLFNBQVNNLFNBQVQsQ0FBbUJiLEdBQW5CLENBQWY7QUFDQUQsV0FBT2Usa0JBQVAsQ0FBMEJGLE9BQU9aLEdBQWpDO0FBQ0Q7QUFDRixDQW5CRDs7QUFxQkE7Ozs7Ozs7Ozs7O0FBV0FILFFBQVFrQixtQkFBUixHQUE4QixVQUFDaEIsTUFBRCxFQUFTQyxHQUFULEVBQWNnQixLQUFkLEVBQXFCQyxRQUFyQixFQUFnRDtBQUFBLE1BQWpCYixPQUFpQix1RUFBUCxFQUFPO0FBQUEsNEJBQy9DQSxPQUQrQyxDQUNwRUMsU0FEb0U7QUFBQSxNQUNwRUEsU0FEb0UsdUNBQ3hELElBRHdEOzs7QUFHNUVZLFdBQVNDLEtBQVQsQ0FBZUMsT0FBZixDQUF1QixVQUFDQyxJQUFELEVBQU9DLENBQVAsRUFBYTtBQUNsQ3RCLFdBQU91QixlQUFQLENBQXVCdEIsR0FBdkIsRUFBNEJnQixRQUFRSyxDQUFwQyxFQUF1Q0QsSUFBdkM7QUFDRCxHQUZEOztBQUlBLE1BQUlmLFNBQUosRUFBZTtBQUNiTixXQUFPZSxrQkFBUCxDQUEwQmQsR0FBMUI7QUFDRDtBQUNGLENBVkQ7O0FBWUE7Ozs7Ozs7Ozs7O0FBV0FILFFBQVF5QixlQUFSLEdBQTBCLFVBQUN2QixNQUFELEVBQVNDLEdBQVQsRUFBY2dCLEtBQWQsRUFBcUJJLElBQXJCLEVBQTRDO0FBQUEsTUFBakJoQixPQUFpQix1RUFBUCxFQUFPO0FBQUEsNEJBQ3ZDQSxPQUR1QyxDQUM1REMsU0FENEQ7QUFBQSxNQUM1REEsU0FENEQsdUNBQ2hELElBRGdEO0FBQUEsTUFFNURDLEtBRjRELEdBRWxEUCxNQUZrRCxDQUU1RE8sS0FGNEQ7QUFBQSxNQUc1REMsUUFINEQsR0FHL0NELEtBSCtDLENBRzVEQyxRQUg0RDs7QUFJcEUsTUFBTUMsT0FBT0QsU0FBU0UsT0FBVCxDQUFpQlQsR0FBakIsQ0FBYjs7QUFFQUQsU0FBT1csY0FBUCxDQUFzQjtBQUNwQkMsVUFBTSxhQURjO0FBRXBCSCx1Q0FBVUEsSUFBVixJQUFnQlEsS0FBaEIsRUFGb0I7QUFHcEJJO0FBSG9CLEdBQXRCOztBQU1BLE1BQUlmLFNBQUosRUFBZTtBQUNiTixXQUFPZSxrQkFBUCxDQUEwQmQsR0FBMUI7QUFDRDtBQUNGLENBZkQ7O0FBaUJBOzs7Ozs7Ozs7Ozs7QUFZQUgsUUFBUTBCLGVBQVIsR0FBMEIsVUFBQ3hCLE1BQUQsRUFBU0MsR0FBVCxFQUFjQyxNQUFkLEVBQXNCdUIsSUFBdEIsRUFBNEJDLEtBQTVCLEVBQW9EO0FBQUEsTUFBakJyQixPQUFpQix1RUFBUCxFQUFPO0FBQUEsNEJBQy9DQSxPQUQrQyxDQUNwRUMsU0FEb0U7QUFBQSxNQUNwRUEsU0FEb0UsdUNBQ3hELElBRHdEO0FBQUEsTUFFcEVDLEtBRm9FLEdBRTFEUCxNQUYwRCxDQUVwRU8sS0FGb0U7QUFBQSxNQUdwRUMsUUFIb0UsR0FHdkRELEtBSHVELENBR3BFQyxRQUhvRTs7QUFJNUUsTUFBTUMsT0FBT0QsU0FBU0UsT0FBVCxDQUFpQlQsR0FBakIsQ0FBYjtBQUNBLE1BQU1vQixPQUFPYixTQUFTbUIsT0FBVCxDQUFpQjFCLEdBQWpCLENBQWI7QUFDQXlCLFVBQVFBLFNBQVNMLEtBQUtPLGVBQUwsQ0FBcUIxQixNQUFyQixDQUFqQjs7QUFFQUYsU0FBT1csY0FBUCxDQUFzQjtBQUNwQkMsVUFBTSxhQURjO0FBRXBCSCxjQUZvQjtBQUdwQlAsa0JBSG9CO0FBSXBCdUIsY0FKb0I7QUFLcEJDO0FBTG9CLEdBQXRCOztBQVFBLE1BQUlwQixTQUFKLEVBQWU7QUFDYixRQUFNTyxTQUFTTCxTQUFTTSxTQUFULENBQW1CYixHQUFuQixDQUFmO0FBQ0FELFdBQU9lLGtCQUFQLENBQTBCRixPQUFPWixHQUFqQztBQUNEO0FBQ0YsQ0FwQkQ7O0FBc0JBOzs7Ozs7Ozs7QUFTQUgsUUFBUStCLGNBQVIsR0FBeUIsVUFBQzdCLE1BQUQsRUFBU0MsR0FBVCxFQUErQjtBQUFBLE1BQWpCSSxPQUFpQix1RUFBUCxFQUFPO0FBQUEsNEJBQ3pCQSxPQUR5QixDQUM5Q0MsU0FEOEM7QUFBQSxNQUM5Q0EsU0FEOEMsdUNBQ2xDLElBRGtDO0FBQUEsTUFFOUNDLEtBRjhDLEdBRXBDUCxNQUZvQyxDQUU5Q08sS0FGOEM7QUFBQSxNQUc5Q0MsUUFIOEMsR0FHakNELEtBSGlDLENBRzlDQyxRQUg4Qzs7QUFJdEQsTUFBTUMsT0FBT0QsU0FBU0UsT0FBVCxDQUFpQlQsR0FBakIsQ0FBYjtBQUNBLE1BQU02QixXQUFXdEIsU0FBU3VCLGtCQUFULENBQTRCOUIsR0FBNUIsQ0FBakI7O0FBRUEsTUFBSSxDQUFDNkIsUUFBTCxFQUFlO0FBQ2IsVUFBTSxJQUFJRSxLQUFKLHFDQUE0Qy9CLEdBQTVDLHlCQUFOO0FBQ0Q7O0FBRUQsTUFBTWdDLFdBQVdILFNBQVNJLElBQVQsSUFBaUIsTUFBakIsR0FBMEJKLFNBQVNMLElBQVQsQ0FBY3RCLE1BQXhDLEdBQWlEMkIsU0FBU1gsS0FBVCxDQUFlZ0IsSUFBakY7O0FBRUFuQyxTQUFPVyxjQUFQLENBQXNCO0FBQ3BCQyxVQUFNLFlBRGM7QUFFcEJILGNBRm9CO0FBR3BCd0I7QUFIb0IsR0FBdEI7O0FBTUEsTUFBSTNCLFNBQUosRUFBZTtBQUNiLFFBQU1PLFNBQVNMLFNBQVNNLFNBQVQsQ0FBbUJiLEdBQW5CLENBQWY7QUFDQUQsV0FBT2Usa0JBQVAsQ0FBMEJGLE9BQU9aLEdBQWpDO0FBQ0Q7QUFDRixDQXZCRDs7QUF5QkE7Ozs7Ozs7Ozs7OztBQVlBSCxRQUFRc0MsYUFBUixHQUF3QixVQUFDcEMsTUFBRCxFQUFTQyxHQUFULEVBQWNvQyxNQUFkLEVBQXNCQyxRQUF0QixFQUFpRDtBQUFBLE1BQWpCakMsT0FBaUIsdUVBQVAsRUFBTztBQUFBLDRCQUMxQ0EsT0FEMEMsQ0FDL0RDLFNBRCtEO0FBQUEsTUFDL0RBLFNBRCtELHVDQUNuRCxJQURtRDtBQUFBLE1BRS9EQyxLQUYrRCxHQUVyRFAsTUFGcUQsQ0FFL0RPLEtBRitEO0FBQUEsTUFHL0RDLFFBSCtELEdBR2xERCxLQUhrRCxDQUcvREMsUUFIK0Q7O0FBSXZFLE1BQU1DLE9BQU9ELFNBQVNFLE9BQVQsQ0FBaUJULEdBQWpCLENBQWI7QUFDQSxNQUFNc0MsVUFBVS9CLFNBQVNFLE9BQVQsQ0FBaUIyQixNQUFqQixDQUFoQjs7QUFFQXJDLFNBQU9XLGNBQVAsQ0FBc0I7QUFDcEJDLFVBQU0sV0FEYztBQUVwQkgsY0FGb0I7QUFHcEI4QiwwQ0FBYUEsT0FBYixJQUFzQkQsUUFBdEI7QUFIb0IsR0FBdEI7O0FBTUEsTUFBSWhDLFNBQUosRUFBZTtBQUNiLFFBQU1PLFNBQVNMLFNBQVNnQyxpQkFBVCxDQUEyQnZDLEdBQTNCLEVBQWdDb0MsTUFBaEMsQ0FBZjtBQUNBckMsV0FBT2Usa0JBQVAsQ0FBMEJGLE9BQU9aLEdBQWpDO0FBQ0Q7QUFDRixDQWpCRDs7QUFtQkE7Ozs7Ozs7Ozs7OztBQVlBSCxRQUFRMkMsZUFBUixHQUEwQixVQUFDekMsTUFBRCxFQUFTQyxHQUFULEVBQWNDLE1BQWQsRUFBc0JDLE1BQXRCLEVBQThCQyxJQUE5QixFQUFxRDtBQUFBLE1BQWpCQyxPQUFpQix1RUFBUCxFQUFPOztBQUM3RUQsU0FBTyxvQkFBVUEsSUFBVixDQUFlQSxJQUFmLENBQVA7QUFENkUsNEJBRWhEQyxPQUZnRCxDQUVyRUMsU0FGcUU7QUFBQSxNQUVyRUEsU0FGcUUsdUNBRXpELElBRnlEO0FBQUEsTUFHckVDLEtBSHFFLEdBRzNEUCxNQUgyRCxDQUdyRU8sS0FIcUU7QUFBQSxNQUlyRUMsUUFKcUUsR0FJeERELEtBSndELENBSXJFQyxRQUpxRTs7QUFLN0UsTUFBTUMsT0FBT0QsU0FBU0UsT0FBVCxDQUFpQlQsR0FBakIsQ0FBYjs7QUFFQUQsU0FBT1csY0FBUCxDQUFzQjtBQUNwQkMsVUFBTSxhQURjO0FBRXBCSCxjQUZvQjtBQUdwQlAsa0JBSG9CO0FBSXBCQyxrQkFKb0I7QUFLcEJDO0FBTG9CLEdBQXRCOztBQVFBLE1BQUlFLFNBQUosRUFBZTtBQUNiLFFBQU1PLFNBQVNMLFNBQVNNLFNBQVQsQ0FBbUJiLEdBQW5CLENBQWY7QUFDQUQsV0FBT2Usa0JBQVAsQ0FBMEJGLE9BQU9aLEdBQWpDO0FBQ0Q7QUFDRixDQW5CRDs7QUFxQkE7Ozs7Ozs7OztBQVNBSCxRQUFRNEMsZUFBUixHQUEwQixVQUFDMUMsTUFBRCxFQUFTQyxHQUFULEVBQStCO0FBQUEsTUFBakJJLE9BQWlCLHVFQUFQLEVBQU87QUFBQSw0QkFDMUJBLE9BRDBCLENBQy9DQyxTQUQrQztBQUFBLE1BQy9DQSxTQUQrQyx1Q0FDbkMsSUFEbUM7QUFBQSxNQUUvQ0MsS0FGK0MsR0FFckNQLE1BRnFDLENBRS9DTyxLQUYrQztBQUFBLE1BRy9DQyxRQUgrQyxHQUdsQ0QsS0FIa0MsQ0FHL0NDLFFBSCtDOztBQUl2RCxNQUFNQyxPQUFPRCxTQUFTRSxPQUFULENBQWlCVCxHQUFqQixDQUFiO0FBQ0EsTUFBTW9CLE9BQU9iLFNBQVNtQixPQUFULENBQWlCMUIsR0FBakIsQ0FBYjs7QUFFQUQsU0FBT1csY0FBUCxDQUFzQjtBQUNwQkMsVUFBTSxhQURjO0FBRXBCSCxjQUZvQjtBQUdwQlk7QUFIb0IsR0FBdEI7O0FBTUEsTUFBSWYsU0FBSixFQUFlO0FBQ2IsUUFBTU8sU0FBU0wsU0FBU00sU0FBVCxDQUFtQmIsR0FBbkIsQ0FBZjtBQUNBRCxXQUFPZSxrQkFBUCxDQUEwQkYsT0FBT1osR0FBakM7QUFDRDtBQUNGLENBakJEOztBQW1CQTs7Ozs7Ozs7Ozs7QUFXQUgsUUFBUTZDLGVBQVIsR0FBMEIsVUFBQzNDLE1BQUQsRUFBU0MsR0FBVCxFQUFjQyxNQUFkLEVBQXNCQyxNQUF0QixFQUErQztBQUFBLE1BQWpCRSxPQUFpQix1RUFBUCxFQUFPO0FBQUEsNEJBQzFDQSxPQUQwQyxDQUMvREMsU0FEK0Q7QUFBQSxNQUMvREEsU0FEK0QsdUNBQ25ELElBRG1EO0FBQUEsTUFFL0RDLEtBRitELEdBRXJEUCxNQUZxRCxDQUUvRE8sS0FGK0Q7QUFBQSxNQUcvREMsUUFIK0QsR0FHbERELEtBSGtELENBRy9EQyxRQUgrRDs7QUFJdkUsTUFBTUMsT0FBT0QsU0FBU0UsT0FBVCxDQUFpQlQsR0FBakIsQ0FBYjtBQUNBLE1BQU1vQixPQUFPYixTQUFTbUIsT0FBVCxDQUFpQjFCLEdBQWpCLENBQWI7QUFDQSxNQUFNMkMsU0FBU3ZCLEtBQUt3QixTQUFMLEVBQWY7QUFOdUUsTUFPL0RwQixJQVArRCxHQU90REosSUFQc0QsQ0FPL0RJLElBUCtEOzs7QUFTdkUsTUFBTXFCLFdBQVcsRUFBakI7QUFDQSxNQUFNQyxLQUFLN0MsTUFBWDtBQUNBLE1BQU04QyxLQUFLOUMsU0FBU0MsTUFBcEI7QUFDQSxNQUFJOEMsSUFBSSxDQUFSOztBQUVBTCxTQUFPeEIsT0FBUCxDQUFlLFVBQUM4QixLQUFELEVBQVc7QUFBQSxRQUNoQnhCLEtBRGdCLEdBQ053QixLQURNLENBQ2hCeEIsS0FEZ0I7O0FBRXhCLFFBQU15QixLQUFLRixDQUFYO0FBQ0EsUUFBTUcsS0FBS0QsS0FBS0QsTUFBTXpCLElBQU4sQ0FBV3RCLE1BQTNCOztBQUVBOEMsU0FBS0MsTUFBTXpCLElBQU4sQ0FBV3RCLE1BQWhCOztBQUVBO0FBQ0EsUUFBSWlELEtBQUtMLEVBQUwsSUFBV0MsS0FBS0csRUFBcEIsRUFBd0I7O0FBRXhCO0FBQ0EsUUFBTUUsUUFBUUMsS0FBS0MsR0FBTCxDQUFTSixFQUFULEVBQWFKLEVBQWIsQ0FBZDtBQUNBLFFBQU1TLE1BQU1GLEtBQUtHLEdBQUwsQ0FBU0wsRUFBVCxFQUFhSixFQUFiLENBQVo7QUFDQSxRQUFNVSxTQUFTakMsS0FBS2tDLEtBQUwsQ0FBV04sS0FBWCxFQUFrQkcsR0FBbEIsQ0FBZjs7QUFFQVYsYUFBU2MsSUFBVCxDQUFjO0FBQ1poRCxZQUFNLGFBRE07QUFFWkgsZ0JBRlk7QUFHWlAsY0FBUW1ELEtBSEk7QUFJWjVCLFlBQU1pQyxNQUpNO0FBS1poQztBQUxZLEtBQWQ7QUFPRCxHQXRCRDs7QUF3QkE7QUFDQTtBQUNBb0IsV0FBU2UsT0FBVCxHQUFtQnpDLE9BQW5CLENBQTJCLFVBQUMwQyxFQUFELEVBQVE7QUFDakM5RCxXQUFPVyxjQUFQLENBQXNCbUQsRUFBdEI7QUFDRCxHQUZEOztBQUlBLE1BQUl4RCxTQUFKLEVBQWU7QUFDYixRQUFNeUQsUUFBUXZELFNBQVN3RCxlQUFULENBQXlCL0QsR0FBekIsQ0FBZDtBQUNBRCxXQUFPZSxrQkFBUCxDQUEwQmdELE1BQU05RCxHQUFoQztBQUNEO0FBQ0YsQ0FoREQ7O0FBa0RBOzs7Ozs7Ozs7Ozs7QUFZQUgsUUFBUW1FLFlBQVIsR0FBdUIsVUFBQ2pFLE1BQUQsRUFBU0MsR0FBVCxFQUFjQyxNQUFkLEVBQXNCQyxNQUF0QixFQUE4QkMsSUFBOUIsRUFBb0M4RCxVQUFwQyxFQUFpRTtBQUFBLE1BQWpCN0QsT0FBaUIsdUVBQVAsRUFBTzs7QUFDdEZELFNBQU8sb0JBQVVBLElBQVYsQ0FBZUEsSUFBZixDQUFQO0FBQ0E4RCxlQUFhLG9CQUFVQyxjQUFWLENBQXlCRCxVQUF6QixDQUFiO0FBRnNGLDZCQUd6RDdELE9BSHlELENBRzlFQyxTQUg4RTtBQUFBLE1BRzlFQSxTQUg4RSx3Q0FHbEUsSUFIa0U7QUFBQSxNQUk5RUMsS0FKOEUsR0FJcEVQLE1BSm9FLENBSTlFTyxLQUo4RTtBQUFBLE1BSzlFQyxRQUw4RSxHQUtqRUQsS0FMaUUsQ0FLOUVDLFFBTDhFOztBQU10RixNQUFNQyxPQUFPRCxTQUFTRSxPQUFULENBQWlCVCxHQUFqQixDQUFiOztBQUVBRCxTQUFPVyxjQUFQLENBQXNCO0FBQ3BCQyxVQUFNLFVBRGM7QUFFcEJILGNBRm9CO0FBR3BCUCxrQkFIb0I7QUFJcEJDLGtCQUpvQjtBQUtwQkMsY0FMb0I7QUFNcEI4RDtBQU5vQixHQUF0Qjs7QUFTQSxNQUFJNUQsU0FBSixFQUFlO0FBQ2IsUUFBTU8sU0FBU0wsU0FBU00sU0FBVCxDQUFtQmIsR0FBbkIsQ0FBZjtBQUNBRCxXQUFPZSxrQkFBUCxDQUEwQkYsT0FBT1osR0FBakM7QUFDRDtBQUNGLENBckJEOztBQXVCQTs7Ozs7Ozs7OztBQVVBSCxRQUFRc0UsWUFBUixHQUF1QixVQUFDcEUsTUFBRCxFQUFTQyxHQUFULEVBQWNpRSxVQUFkLEVBQTJDO0FBQUEsTUFBakI3RCxPQUFpQix1RUFBUCxFQUFPOztBQUNoRTZELGVBQWEsb0JBQVVHLGNBQVYsQ0FBeUJILFVBQXpCLENBQWI7QUFEZ0UsNkJBRW5DN0QsT0FGbUMsQ0FFeERDLFNBRndEO0FBQUEsTUFFeERBLFNBRndELHdDQUU1QyxJQUY0QztBQUFBLE1BR3hEQyxLQUh3RCxHQUc5Q1AsTUFIOEMsQ0FHeERPLEtBSHdEO0FBQUEsTUFJeERDLFFBSndELEdBSTNDRCxLQUoyQyxDQUl4REMsUUFKd0Q7O0FBS2hFLE1BQU1DLE9BQU9ELFNBQVNFLE9BQVQsQ0FBaUJULEdBQWpCLENBQWI7QUFDQSxNQUFNb0IsT0FBT2IsU0FBU21CLE9BQVQsQ0FBaUIxQixHQUFqQixDQUFiOztBQUVBRCxTQUFPVyxjQUFQLENBQXNCO0FBQ3BCQyxVQUFNLFVBRGM7QUFFcEJILGNBRm9CO0FBR3BCWSxjQUhvQjtBQUlwQjZDO0FBSm9CLEdBQXRCOztBQU9BLE1BQUk1RCxTQUFKLEVBQWU7QUFDYk4sV0FBT2Usa0JBQVAsQ0FBMEJNLEtBQUtwQixHQUEvQjtBQUNEO0FBQ0YsQ0FsQkQ7O0FBb0JBOzs7Ozs7Ozs7O0FBVUFILFFBQVF3RSxjQUFSLEdBQXlCLFVBQUN0RSxNQUFELEVBQVNDLEdBQVQsRUFBY2dDLFFBQWQsRUFBeUM7QUFBQSxNQUFqQjVCLE9BQWlCLHVFQUFQLEVBQU87QUFBQSw2QkFDbkNBLE9BRG1DLENBQ3hEQyxTQUR3RDtBQUFBLE1BQ3hEQSxTQUR3RCx3Q0FDNUMsSUFENEM7QUFBQSxNQUV4REMsS0FGd0QsR0FFOUNQLE1BRjhDLENBRXhETyxLQUZ3RDtBQUFBLE1BR3hEQyxRQUh3RCxHQUczQ0QsS0FIMkMsQ0FHeERDLFFBSHdEOztBQUloRSxNQUFNQyxPQUFPRCxTQUFTRSxPQUFULENBQWlCVCxHQUFqQixDQUFiOztBQUVBRCxTQUFPVyxjQUFQLENBQXNCO0FBQ3BCQyxVQUFNLFlBRGM7QUFFcEJILGNBRm9CO0FBR3BCd0I7QUFIb0IsR0FBdEI7O0FBTUEsTUFBSTNCLFNBQUosRUFBZTtBQUNiLFFBQU1PLFNBQVNMLFNBQVNNLFNBQVQsQ0FBbUJiLEdBQW5CLENBQWY7QUFDQUQsV0FBT2Usa0JBQVAsQ0FBMEJGLE9BQU9aLEdBQWpDO0FBQ0Q7QUFDRixDQWhCRDs7QUFrQkE7Ozs7Ozs7Ozs7QUFVQUgsUUFBUXlFLHFCQUFSLEdBQWdDLFVBQUN2RSxNQUFELEVBQVNDLEdBQVQsRUFBY3VFLE9BQWQsRUFBdUJDLFVBQXZCLEVBQW9EO0FBQUEsTUFBakJwRSxPQUFpQix1RUFBUCxFQUFPOztBQUNsRixNQUFJSixPQUFPdUUsT0FBWCxFQUFvQjtBQUNsQnhFLFdBQU9zRSxjQUFQLENBQXNCRSxPQUF0QixFQUErQkMsVUFBL0IsRUFBMkNwRSxPQUEzQztBQUNBO0FBQ0Q7O0FBSmlGLDZCQU1yREEsT0FOcUQsQ0FNMUVDLFNBTjBFO0FBQUEsTUFNMUVBLFNBTjBFLHdDQU05RCxJQU44RDtBQUFBLE1BTzFFQyxLQVAwRSxHQU9oRVAsTUFQZ0UsQ0FPMUVPLEtBUDBFO0FBQUEsTUFRMUVDLFFBUjBFLEdBUTdERCxLQVI2RCxDQVExRUMsUUFSMEU7OztBQVVsRixNQUFNaUIsT0FBT2pCLFNBQVNtQixPQUFULENBQWlCNkMsT0FBakIsQ0FBYjtBQUNBLE1BQU1FLFlBQVlsRSxTQUFTbUUsWUFBVCxDQUFzQkgsT0FBdEIsQ0FBbEI7QUFDQSxNQUFNckQsUUFBUXVELFVBQVVFLFNBQVYsQ0FBb0I7QUFBQSxXQUFLQyxFQUFFNUUsR0FBRixJQUFTQSxHQUFkO0FBQUEsR0FBcEIsRUFBdUM0RCxPQUF2QyxHQUFpRGlCLE9BQWpELENBQXlEckQsSUFBekQsQ0FBZDtBQUNBLE1BQUlLLGlCQUFKOztBQUVBWCxRQUFNQyxPQUFOLENBQWMsVUFBQ0MsSUFBRCxFQUFVO0FBQ3RCLFFBQU1KLFFBQVFhLFdBQVdULEtBQUtGLEtBQUwsQ0FBVzRELE9BQVgsQ0FBbUJqRCxRQUFuQixJQUErQixDQUExQyxHQUE4QzJDLFVBQTVEO0FBQ0EzQyxlQUFXVCxJQUFYO0FBQ0FyQixXQUFPc0UsY0FBUCxDQUFzQmpELEtBQUtwQixHQUEzQixFQUFnQ2dCLEtBQWhDLEVBQXVDLEVBQUVYLFdBQVcsS0FBYixFQUF2QztBQUNELEdBSkQ7O0FBTUEsTUFBSUEsU0FBSixFQUFlO0FBQ2IsUUFBTU8sU0FBU0wsU0FBU00sU0FBVCxDQUFtQmIsR0FBbkIsQ0FBZjtBQUNBRCxXQUFPZSxrQkFBUCxDQUEwQkYsT0FBT1osR0FBakM7QUFDRDtBQUNGLENBekJEOztBQTJCQTs7Ozs7Ozs7OztBQVVBSCxRQUFRa0YsaUJBQVIsR0FBNEIsVUFBQ2hGLE1BQUQsRUFBU0MsR0FBVCxFQUFjaUUsVUFBZCxFQUEwQjdELE9BQTFCLEVBQXNDO0FBQUEsTUFDeERFLEtBRHdELEdBQzlDUCxNQUQ4QyxDQUN4RE8sS0FEd0Q7QUFBQSxNQUV4REMsUUFGd0QsR0FFaENELEtBRmdDLENBRXhEQyxRQUZ3RDtBQUFBLE1BRTlDeUUsU0FGOEMsR0FFaEMxRSxLQUZnQyxDQUU5QzBFLFNBRjhDOztBQUdoRSxNQUFNNUQsT0FBT2IsU0FBUzBFLGdCQUFULENBQTBCakYsR0FBMUIsQ0FBYjtBQUNBLE1BQU1rRixRQUFROUQsS0FBSytELFlBQUwsRUFBZDtBQUNBLE1BQU1DLE9BQU9oRSxLQUFLaUUsV0FBTCxFQUFiO0FBQ0EsTUFBTXBDLFFBQVErQixVQUFVTSxhQUFWLENBQXdCSixLQUF4QixFQUErQkUsSUFBL0IsQ0FBZDtBQUNBckYsU0FBT3dGLG1CQUFQLENBQTJCdEMsS0FBM0IsRUFBa0NnQixVQUFsQyxFQUE4QzdELE9BQTlDO0FBQ0QsQ0FSRDs7QUFVQTs7Ozs7Ozs7OztBQVVBUCxRQUFRMkYsZ0JBQVIsR0FBMkIsVUFBQ3pGLE1BQUQsRUFBU0MsR0FBVCxFQUFjaUUsVUFBZCxFQUEwQjdELE9BQTFCLEVBQXNDO0FBQUEsTUFDdkRFLEtBRHVELEdBQzdDUCxNQUQ2QyxDQUN2RE8sS0FEdUQ7QUFBQSxNQUV2REMsUUFGdUQsR0FFL0JELEtBRitCLENBRXZEQyxRQUZ1RDtBQUFBLE1BRTdDeUUsU0FGNkMsR0FFL0IxRSxLQUYrQixDQUU3QzBFLFNBRjZDOztBQUcvRCxNQUFNNUQsT0FBT2IsU0FBUzBFLGdCQUFULENBQTBCakYsR0FBMUIsQ0FBYjtBQUNBLE1BQU1rRixRQUFROUQsS0FBSytELFlBQUwsRUFBZDtBQUNBLE1BQU1DLE9BQU9oRSxLQUFLaUUsV0FBTCxFQUFiO0FBQ0EsTUFBTXBDLFFBQVErQixVQUFVTSxhQUFWLENBQXdCSixLQUF4QixFQUErQkUsSUFBL0IsQ0FBZDtBQUNBckYsU0FBTzBGLGtCQUFQLENBQTBCeEMsS0FBMUIsRUFBaUNnQixVQUFqQyxFQUE2QzdELE9BQTdDO0FBQ0QsQ0FSRDs7QUFVQTs7Ozs7Ozs7Ozs7OztBQWFBUCxRQUFRNkYsZUFBUixHQUEwQixVQUFDM0YsTUFBRCxFQUFTQyxHQUFULEVBQStCO0FBQUEsTUFBakJJLE9BQWlCLHVFQUFQLEVBQU87QUFBQSw2QkFDMUJBLE9BRDBCLENBQy9DQyxTQUQrQztBQUFBLE1BQy9DQSxTQUQrQyx3Q0FDbkMsSUFEbUM7QUFBQSxNQUUvQ0MsS0FGK0MsR0FFckNQLE1BRnFDLENBRS9DTyxLQUYrQztBQUFBLE1BRy9DQyxRQUgrQyxHQUdsQ0QsS0FIa0MsQ0FHL0NDLFFBSCtDOztBQUl2RCxNQUFNSyxTQUFTTCxTQUFTTSxTQUFULENBQW1CYixHQUFuQixDQUFmO0FBQ0EsTUFBTW9CLE9BQU9SLE9BQU8rRSxRQUFQLENBQWdCM0YsR0FBaEIsQ0FBYjs7QUFFQSxNQUFNZ0IsUUFBUUosT0FBT00sS0FBUCxDQUFhNEQsT0FBYixDQUFxQjFELElBQXJCLENBQWQ7QUFDQSxNQUFNd0UsVUFBVTVFLFVBQVUsQ0FBMUI7QUFDQSxNQUFNNkUsU0FBUzdFLFVBQVVKLE9BQU9NLEtBQVAsQ0FBYWdCLElBQWIsR0FBb0IsQ0FBN0M7O0FBRUEsTUFBTTRELGVBQWV2RixTQUFTTSxTQUFULENBQW1CRCxPQUFPWixHQUExQixDQUFyQjtBQUNBLE1BQU0rRixjQUFjRCxhQUFhNUUsS0FBYixDQUFtQjRELE9BQW5CLENBQTJCbEUsTUFBM0IsQ0FBcEI7O0FBRUEsTUFBSUEsT0FBT00sS0FBUCxDQUFhZ0IsSUFBYixLQUFzQixDQUExQixFQUE2QjtBQUMzQm5DLFdBQU9vQyxhQUFQLENBQXFCbkMsR0FBckIsRUFBMEI4RixhQUFhOUYsR0FBdkMsRUFBNEMrRixXQUE1QyxFQUF5RCxFQUFFMUYsV0FBVyxLQUFiLEVBQXpEO0FBQ0FOLFdBQU8wQyxlQUFQLENBQXVCN0IsT0FBT1osR0FBOUIsRUFBbUNJLE9BQW5DO0FBQ0QsR0FIRCxNQUtLLElBQUl3RixPQUFKLEVBQWE7QUFDaEI7QUFDQTdGLFdBQU9vQyxhQUFQLENBQXFCbkMsR0FBckIsRUFBMEI4RixhQUFhOUYsR0FBdkMsRUFBNEMrRixXQUE1QyxFQUF5RDNGLE9BQXpEO0FBQ0QsR0FISSxNQUtBLElBQUl5RixNQUFKLEVBQVk7QUFDZjtBQUNBOUYsV0FBT29DLGFBQVAsQ0FBcUJuQyxHQUFyQixFQUEwQjhGLGFBQWE5RixHQUF2QyxFQUE0QytGLGNBQWMsQ0FBMUQsRUFBNkQzRixPQUE3RDtBQUNELEdBSEksTUFLQTtBQUNIO0FBQ0FMLFdBQU9zRSxjQUFQLENBQXNCekQsT0FBT1osR0FBN0IsRUFBa0NnQixLQUFsQyxFQUF5QyxFQUFFWCxXQUFXLEtBQWIsRUFBekM7O0FBRUE7QUFDQU4sV0FBT29DLGFBQVAsQ0FBcUJuQyxHQUFyQixFQUEwQjhGLGFBQWE5RixHQUF2QyxFQUE0QytGLGNBQWMsQ0FBMUQsRUFBNkQsRUFBRTFGLFdBQVcsS0FBYixFQUE3RDs7QUFFQSxRQUFJQSxTQUFKLEVBQWU7QUFDYk4sYUFBT2Usa0JBQVAsQ0FBMEJnRixhQUFhOUYsR0FBdkM7QUFDRDtBQUNGO0FBQ0YsQ0F4Q0Q7O0FBMENBOzs7Ozs7Ozs7O0FBVUFILFFBQVFtRyxlQUFSLEdBQTBCLFVBQUNqRyxNQUFELEVBQVNDLEdBQVQsRUFBY2lHLE1BQWQsRUFBc0I3RixPQUF0QixFQUFrQztBQUMxRDZGLFdBQVMsb0JBQVVBLE1BQVYsQ0FBaUJBLE1BQWpCLENBQVQ7QUFDQUEsV0FBU0EsT0FBT0MsR0FBUCxDQUFXLE9BQVgsRUFBb0JELE9BQU8vRSxLQUFQLENBQWFpRixLQUFiLEVBQXBCLENBQVQ7O0FBRjBELE1BSWxENUYsUUFKa0QsR0FJckNSLE9BQU9PLEtBSjhCLENBSWxEQyxRQUprRDs7QUFLMUQsTUFBTWEsT0FBT2IsU0FBUzBFLGdCQUFULENBQTBCakYsR0FBMUIsQ0FBYjtBQUNBLE1BQU1ZLFNBQVNMLFNBQVNNLFNBQVQsQ0FBbUJPLEtBQUtwQixHQUF4QixDQUFmO0FBQ0EsTUFBTWdCLFFBQVFKLE9BQU9NLEtBQVAsQ0FBYTRELE9BQWIsQ0FBcUIxRCxJQUFyQixDQUFkOztBQUVBckIsU0FBT3VCLGVBQVAsQ0FBdUJWLE9BQU9aLEdBQTlCLEVBQW1DZ0IsS0FBbkMsRUFBMENpRixNQUExQyxFQUFrRCxFQUFFNUYsV0FBVyxLQUFiLEVBQWxEO0FBQ0FOLFNBQU9vQyxhQUFQLENBQXFCZixLQUFLcEIsR0FBMUIsRUFBK0JpRyxPQUFPakcsR0FBdEMsRUFBMkMsQ0FBM0MsRUFBOENJLE9BQTlDO0FBQ0QsQ0FYRDs7QUFhQTs7Ozs7Ozs7OztBQVVBUCxRQUFRdUcsY0FBUixHQUF5QixVQUFDckcsTUFBRCxFQUFTQyxHQUFULEVBQWM4RCxLQUFkLEVBQXFCMUQsT0FBckIsRUFBaUM7QUFDeEQwRCxVQUFRLG9CQUFVQSxLQUFWLENBQWdCQSxLQUFoQixDQUFSO0FBQ0FBLFVBQVFBLE1BQU1vQyxHQUFOLENBQVUsT0FBVixFQUFtQnBDLE1BQU01QyxLQUFOLENBQVlpRixLQUFaLEVBQW5CLENBQVI7O0FBRndELE1BSWhENUYsUUFKZ0QsR0FJbkNSLE9BQU9PLEtBSjRCLENBSWhEQyxRQUpnRDs7QUFLeEQsTUFBTWEsT0FBT2IsU0FBUzBFLGdCQUFULENBQTBCakYsR0FBMUIsQ0FBYjtBQUNBLE1BQU1ZLFNBQVNMLFNBQVNNLFNBQVQsQ0FBbUJPLEtBQUtwQixHQUF4QixDQUFmO0FBQ0EsTUFBTWdCLFFBQVFKLE9BQU9NLEtBQVAsQ0FBYTRELE9BQWIsQ0FBcUIxRCxJQUFyQixDQUFkOztBQUVBckIsU0FBT3VCLGVBQVAsQ0FBdUJWLE9BQU9aLEdBQTlCLEVBQW1DZ0IsS0FBbkMsRUFBMEM4QyxLQUExQyxFQUFpRCxFQUFFekQsV0FBVyxLQUFiLEVBQWpEO0FBQ0FOLFNBQU9vQyxhQUFQLENBQXFCZixLQUFLcEIsR0FBMUIsRUFBK0I4RCxNQUFNOUQsR0FBckMsRUFBMEMsQ0FBMUMsRUFBNkNJLE9BQTdDO0FBQ0QsQ0FYRDs7QUFhQTs7Ozs7O2tCQU1lUCxPIiwiZmlsZSI6ImJ5LWtleS5qcyIsInNvdXJjZXNDb250ZW50IjpbIlxuaW1wb3J0IE5vcm1hbGl6ZSBmcm9tICcuLi91dGlscy9ub3JtYWxpemUnXG5pbXBvcnQgU0NIRU1BIGZyb20gJy4uL3NjaGVtYXMvY29yZSdcblxuLyoqXG4gKiBDaGFuZ2VzLlxuICpcbiAqIEB0eXBlIHtPYmplY3R9XG4gKi9cblxuY29uc3QgQ2hhbmdlcyA9IHt9XG5cbi8qKlxuICogQWRkIG1hcmsgdG8gdGV4dCBhdCBgb2Zmc2V0YCBhbmQgYGxlbmd0aGAgaW4gbm9kZSBieSBga2V5YC5cbiAqXG4gKiBAcGFyYW0ge0NoYW5nZX0gY2hhbmdlXG4gKiBAcGFyYW0ge1N0cmluZ30ga2V5XG4gKiBAcGFyYW0ge051bWJlcn0gb2Zmc2V0XG4gKiBAcGFyYW0ge051bWJlcn0gbGVuZ3RoXG4gKiBAcGFyYW0ge01peGVkfSBtYXJrXG4gKiBAcGFyYW0ge09iamVjdH0gb3B0aW9uc1xuICogICBAcHJvcGVydHkge0Jvb2xlYW59IG5vcm1hbGl6ZVxuICovXG5cbkNoYW5nZXMuYWRkTWFya0J5S2V5ID0gKGNoYW5nZSwga2V5LCBvZmZzZXQsIGxlbmd0aCwgbWFyaywgb3B0aW9ucyA9IHt9KSA9PiB7XG4gIG1hcmsgPSBOb3JtYWxpemUubWFyayhtYXJrKVxuICBjb25zdCB7IG5vcm1hbGl6ZSA9IHRydWUgfSA9IG9wdGlvbnNcbiAgY29uc3QgeyBzdGF0ZSB9ID0gY2hhbmdlXG4gIGNvbnN0IHsgZG9jdW1lbnQgfSA9IHN0YXRlXG4gIGNvbnN0IHBhdGggPSBkb2N1bWVudC5nZXRQYXRoKGtleSlcblxuICBjaGFuZ2UuYXBwbHlPcGVyYXRpb24oe1xuICAgIHR5cGU6ICdhZGRfbWFyaycsXG4gICAgcGF0aCxcbiAgICBvZmZzZXQsXG4gICAgbGVuZ3RoLFxuICAgIG1hcmssXG4gIH0pXG5cbiAgaWYgKG5vcm1hbGl6ZSkge1xuICAgIGNvbnN0IHBhcmVudCA9IGRvY3VtZW50LmdldFBhcmVudChrZXkpXG4gICAgY2hhbmdlLm5vcm1hbGl6ZU5vZGVCeUtleShwYXJlbnQua2V5LCBTQ0hFTUEpXG4gIH1cbn1cblxuLyoqXG4gKiBJbnNlcnQgYSBgZnJhZ21lbnRgIGF0IGBpbmRleGAgaW4gYSBub2RlIGJ5IGBrZXlgLlxuICpcbiAqIEBwYXJhbSB7Q2hhbmdlfSBjaGFuZ2VcbiAqIEBwYXJhbSB7U3RyaW5nfSBrZXlcbiAqIEBwYXJhbSB7TnVtYmVyfSBpbmRleFxuICogQHBhcmFtIHtGcmFnbWVudH0gZnJhZ21lbnRcbiAqIEBwYXJhbSB7T2JqZWN0fSBvcHRpb25zXG4gKiAgIEBwcm9wZXJ0eSB7Qm9vbGVhbn0gbm9ybWFsaXplXG4gKi9cblxuQ2hhbmdlcy5pbnNlcnRGcmFnbWVudEJ5S2V5ID0gKGNoYW5nZSwga2V5LCBpbmRleCwgZnJhZ21lbnQsIG9wdGlvbnMgPSB7fSkgPT4ge1xuICBjb25zdCB7IG5vcm1hbGl6ZSA9IHRydWUgfSA9IG9wdGlvbnNcblxuICBmcmFnbWVudC5ub2Rlcy5mb3JFYWNoKChub2RlLCBpKSA9PiB7XG4gICAgY2hhbmdlLmluc2VydE5vZGVCeUtleShrZXksIGluZGV4ICsgaSwgbm9kZSlcbiAgfSlcblxuICBpZiAobm9ybWFsaXplKSB7XG4gICAgY2hhbmdlLm5vcm1hbGl6ZU5vZGVCeUtleShrZXksIFNDSEVNQSlcbiAgfVxufVxuXG4vKipcbiAqIEluc2VydCBhIGBub2RlYCBhdCBgaW5kZXhgIGluIGEgbm9kZSBieSBga2V5YC5cbiAqXG4gKiBAcGFyYW0ge0NoYW5nZX0gY2hhbmdlXG4gKiBAcGFyYW0ge1N0cmluZ30ga2V5XG4gKiBAcGFyYW0ge051bWJlcn0gaW5kZXhcbiAqIEBwYXJhbSB7Tm9kZX0gbm9kZVxuICogQHBhcmFtIHtPYmplY3R9IG9wdGlvbnNcbiAqICAgQHByb3BlcnR5IHtCb29sZWFufSBub3JtYWxpemVcbiAqL1xuXG5DaGFuZ2VzLmluc2VydE5vZGVCeUtleSA9IChjaGFuZ2UsIGtleSwgaW5kZXgsIG5vZGUsIG9wdGlvbnMgPSB7fSkgPT4ge1xuICBjb25zdCB7IG5vcm1hbGl6ZSA9IHRydWUgfSA9IG9wdGlvbnNcbiAgY29uc3QgeyBzdGF0ZSB9ID0gY2hhbmdlXG4gIGNvbnN0IHsgZG9jdW1lbnQgfSA9IHN0YXRlXG4gIGNvbnN0IHBhdGggPSBkb2N1bWVudC5nZXRQYXRoKGtleSlcblxuICBjaGFuZ2UuYXBwbHlPcGVyYXRpb24oe1xuICAgIHR5cGU6ICdpbnNlcnRfbm9kZScsXG4gICAgcGF0aDogWy4uLnBhdGgsIGluZGV4XSxcbiAgICBub2RlLFxuICB9KVxuXG4gIGlmIChub3JtYWxpemUpIHtcbiAgICBjaGFuZ2Uubm9ybWFsaXplTm9kZUJ5S2V5KGtleSwgU0NIRU1BKVxuICB9XG59XG5cbi8qKlxuICogSW5zZXJ0IGB0ZXh0YCBhdCBgb2Zmc2V0YCBpbiBub2RlIGJ5IGBrZXlgLlxuICpcbiAqIEBwYXJhbSB7Q2hhbmdlfSBjaGFuZ2VcbiAqIEBwYXJhbSB7U3RyaW5nfSBrZXlcbiAqIEBwYXJhbSB7TnVtYmVyfSBvZmZzZXRcbiAqIEBwYXJhbSB7U3RyaW5nfSB0ZXh0XG4gKiBAcGFyYW0ge1NldDxNYXJrPn0gbWFya3MgKG9wdGlvbmFsKVxuICogQHBhcmFtIHtPYmplY3R9IG9wdGlvbnNcbiAqICAgQHByb3BlcnR5IHtCb29sZWFufSBub3JtYWxpemVcbiAqL1xuXG5DaGFuZ2VzLmluc2VydFRleHRCeUtleSA9IChjaGFuZ2UsIGtleSwgb2Zmc2V0LCB0ZXh0LCBtYXJrcywgb3B0aW9ucyA9IHt9KSA9PiB7XG4gIGNvbnN0IHsgbm9ybWFsaXplID0gdHJ1ZSB9ID0gb3B0aW9uc1xuICBjb25zdCB7IHN0YXRlIH0gPSBjaGFuZ2VcbiAgY29uc3QgeyBkb2N1bWVudCB9ID0gc3RhdGVcbiAgY29uc3QgcGF0aCA9IGRvY3VtZW50LmdldFBhdGgoa2V5KVxuICBjb25zdCBub2RlID0gZG9jdW1lbnQuZ2V0Tm9kZShrZXkpXG4gIG1hcmtzID0gbWFya3MgfHwgbm9kZS5nZXRNYXJrc0F0SW5kZXgob2Zmc2V0KVxuXG4gIGNoYW5nZS5hcHBseU9wZXJhdGlvbih7XG4gICAgdHlwZTogJ2luc2VydF90ZXh0JyxcbiAgICBwYXRoLFxuICAgIG9mZnNldCxcbiAgICB0ZXh0LFxuICAgIG1hcmtzLFxuICB9KVxuXG4gIGlmIChub3JtYWxpemUpIHtcbiAgICBjb25zdCBwYXJlbnQgPSBkb2N1bWVudC5nZXRQYXJlbnQoa2V5KVxuICAgIGNoYW5nZS5ub3JtYWxpemVOb2RlQnlLZXkocGFyZW50LmtleSwgU0NIRU1BKVxuICB9XG59XG5cbi8qKlxuICogTWVyZ2UgYSBub2RlIGJ5IGBrZXlgIHdpdGggdGhlIHByZXZpb3VzIG5vZGUuXG4gKlxuICogQHBhcmFtIHtDaGFuZ2V9IGNoYW5nZVxuICogQHBhcmFtIHtTdHJpbmd9IGtleVxuICogQHBhcmFtIHtPYmplY3R9IG9wdGlvbnNcbiAqICAgQHByb3BlcnR5IHtCb29sZWFufSBub3JtYWxpemVcbiAqL1xuXG5DaGFuZ2VzLm1lcmdlTm9kZUJ5S2V5ID0gKGNoYW5nZSwga2V5LCBvcHRpb25zID0ge30pID0+IHtcbiAgY29uc3QgeyBub3JtYWxpemUgPSB0cnVlIH0gPSBvcHRpb25zXG4gIGNvbnN0IHsgc3RhdGUgfSA9IGNoYW5nZVxuICBjb25zdCB7IGRvY3VtZW50IH0gPSBzdGF0ZVxuICBjb25zdCBwYXRoID0gZG9jdW1lbnQuZ2V0UGF0aChrZXkpXG4gIGNvbnN0IHByZXZpb3VzID0gZG9jdW1lbnQuZ2V0UHJldmlvdXNTaWJsaW5nKGtleSlcblxuICBpZiAoIXByZXZpb3VzKSB7XG4gICAgdGhyb3cgbmV3IEVycm9yKGBVbmFibGUgdG8gbWVyZ2Ugbm9kZSB3aXRoIGtleSBcIiR7a2V5fVwiLCBubyBwcmV2aW91cyBrZXkuYClcbiAgfVxuXG4gIGNvbnN0IHBvc2l0aW9uID0gcHJldmlvdXMua2luZCA9PSAndGV4dCcgPyBwcmV2aW91cy50ZXh0Lmxlbmd0aCA6IHByZXZpb3VzLm5vZGVzLnNpemVcblxuICBjaGFuZ2UuYXBwbHlPcGVyYXRpb24oe1xuICAgIHR5cGU6ICdtZXJnZV9ub2RlJyxcbiAgICBwYXRoLFxuICAgIHBvc2l0aW9uLFxuICB9KVxuXG4gIGlmIChub3JtYWxpemUpIHtcbiAgICBjb25zdCBwYXJlbnQgPSBkb2N1bWVudC5nZXRQYXJlbnQoa2V5KVxuICAgIGNoYW5nZS5ub3JtYWxpemVOb2RlQnlLZXkocGFyZW50LmtleSwgU0NIRU1BKVxuICB9XG59XG5cbi8qKlxuICogTW92ZSBhIG5vZGUgYnkgYGtleWAgdG8gYSBuZXcgcGFyZW50IGJ5IGBuZXdLZXlgIGFuZCBgaW5kZXhgLlxuICogYG5ld0tleWAgaXMgdGhlIGtleSBvZiB0aGUgY29udGFpbmVyIChpdCBjYW4gYmUgdGhlIGRvY3VtZW50IGl0c2VsZilcbiAqXG4gKiBAcGFyYW0ge0NoYW5nZX0gY2hhbmdlXG4gKiBAcGFyYW0ge1N0cmluZ30ga2V5XG4gKiBAcGFyYW0ge1N0cmluZ30gbmV3S2V5XG4gKiBAcGFyYW0ge051bWJlcn0gaW5kZXhcbiAqIEBwYXJhbSB7T2JqZWN0fSBvcHRpb25zXG4gKiAgIEBwcm9wZXJ0eSB7Qm9vbGVhbn0gbm9ybWFsaXplXG4gKi9cblxuQ2hhbmdlcy5tb3ZlTm9kZUJ5S2V5ID0gKGNoYW5nZSwga2V5LCBuZXdLZXksIG5ld0luZGV4LCBvcHRpb25zID0ge30pID0+IHtcbiAgY29uc3QgeyBub3JtYWxpemUgPSB0cnVlIH0gPSBvcHRpb25zXG4gIGNvbnN0IHsgc3RhdGUgfSA9IGNoYW5nZVxuICBjb25zdCB7IGRvY3VtZW50IH0gPSBzdGF0ZVxuICBjb25zdCBwYXRoID0gZG9jdW1lbnQuZ2V0UGF0aChrZXkpXG4gIGNvbnN0IG5ld1BhdGggPSBkb2N1bWVudC5nZXRQYXRoKG5ld0tleSlcblxuICBjaGFuZ2UuYXBwbHlPcGVyYXRpb24oe1xuICAgIHR5cGU6ICdtb3ZlX25vZGUnLFxuICAgIHBhdGgsXG4gICAgbmV3UGF0aDogWy4uLm5ld1BhdGgsIG5ld0luZGV4XSxcbiAgfSlcblxuICBpZiAobm9ybWFsaXplKSB7XG4gICAgY29uc3QgcGFyZW50ID0gZG9jdW1lbnQuZ2V0Q29tbW9uQW5jZXN0b3Ioa2V5LCBuZXdLZXkpXG4gICAgY2hhbmdlLm5vcm1hbGl6ZU5vZGVCeUtleShwYXJlbnQua2V5LCBTQ0hFTUEpXG4gIH1cbn1cblxuLyoqXG4gKiBSZW1vdmUgbWFyayBmcm9tIHRleHQgYXQgYG9mZnNldGAgYW5kIGBsZW5ndGhgIGluIG5vZGUgYnkgYGtleWAuXG4gKlxuICogQHBhcmFtIHtDaGFuZ2V9IGNoYW5nZVxuICogQHBhcmFtIHtTdHJpbmd9IGtleVxuICogQHBhcmFtIHtOdW1iZXJ9IG9mZnNldFxuICogQHBhcmFtIHtOdW1iZXJ9IGxlbmd0aFxuICogQHBhcmFtIHtNYXJrfSBtYXJrXG4gKiBAcGFyYW0ge09iamVjdH0gb3B0aW9uc1xuICogICBAcHJvcGVydHkge0Jvb2xlYW59IG5vcm1hbGl6ZVxuICovXG5cbkNoYW5nZXMucmVtb3ZlTWFya0J5S2V5ID0gKGNoYW5nZSwga2V5LCBvZmZzZXQsIGxlbmd0aCwgbWFyaywgb3B0aW9ucyA9IHt9KSA9PiB7XG4gIG1hcmsgPSBOb3JtYWxpemUubWFyayhtYXJrKVxuICBjb25zdCB7IG5vcm1hbGl6ZSA9IHRydWUgfSA9IG9wdGlvbnNcbiAgY29uc3QgeyBzdGF0ZSB9ID0gY2hhbmdlXG4gIGNvbnN0IHsgZG9jdW1lbnQgfSA9IHN0YXRlXG4gIGNvbnN0IHBhdGggPSBkb2N1bWVudC5nZXRQYXRoKGtleSlcblxuICBjaGFuZ2UuYXBwbHlPcGVyYXRpb24oe1xuICAgIHR5cGU6ICdyZW1vdmVfbWFyaycsXG4gICAgcGF0aCxcbiAgICBvZmZzZXQsXG4gICAgbGVuZ3RoLFxuICAgIG1hcmssXG4gIH0pXG5cbiAgaWYgKG5vcm1hbGl6ZSkge1xuICAgIGNvbnN0IHBhcmVudCA9IGRvY3VtZW50LmdldFBhcmVudChrZXkpXG4gICAgY2hhbmdlLm5vcm1hbGl6ZU5vZGVCeUtleShwYXJlbnQua2V5LCBTQ0hFTUEpXG4gIH1cbn1cblxuLyoqXG4gKiBSZW1vdmUgYSBub2RlIGJ5IGBrZXlgLlxuICpcbiAqIEBwYXJhbSB7Q2hhbmdlfSBjaGFuZ2VcbiAqIEBwYXJhbSB7U3RyaW5nfSBrZXlcbiAqIEBwYXJhbSB7T2JqZWN0fSBvcHRpb25zXG4gKiAgIEBwcm9wZXJ0eSB7Qm9vbGVhbn0gbm9ybWFsaXplXG4gKi9cblxuQ2hhbmdlcy5yZW1vdmVOb2RlQnlLZXkgPSAoY2hhbmdlLCBrZXksIG9wdGlvbnMgPSB7fSkgPT4ge1xuICBjb25zdCB7IG5vcm1hbGl6ZSA9IHRydWUgfSA9IG9wdGlvbnNcbiAgY29uc3QgeyBzdGF0ZSB9ID0gY2hhbmdlXG4gIGNvbnN0IHsgZG9jdW1lbnQgfSA9IHN0YXRlXG4gIGNvbnN0IHBhdGggPSBkb2N1bWVudC5nZXRQYXRoKGtleSlcbiAgY29uc3Qgbm9kZSA9IGRvY3VtZW50LmdldE5vZGUoa2V5KVxuXG4gIGNoYW5nZS5hcHBseU9wZXJhdGlvbih7XG4gICAgdHlwZTogJ3JlbW92ZV9ub2RlJyxcbiAgICBwYXRoLFxuICAgIG5vZGUsXG4gIH0pXG5cbiAgaWYgKG5vcm1hbGl6ZSkge1xuICAgIGNvbnN0IHBhcmVudCA9IGRvY3VtZW50LmdldFBhcmVudChrZXkpXG4gICAgY2hhbmdlLm5vcm1hbGl6ZU5vZGVCeUtleShwYXJlbnQua2V5LCBTQ0hFTUEpXG4gIH1cbn1cblxuLyoqXG4gKiBSZW1vdmUgdGV4dCBhdCBgb2Zmc2V0YCBhbmQgYGxlbmd0aGAgaW4gbm9kZSBieSBga2V5YC5cbiAqXG4gKiBAcGFyYW0ge0NoYW5nZX0gY2hhbmdlXG4gKiBAcGFyYW0ge1N0cmluZ30ga2V5XG4gKiBAcGFyYW0ge051bWJlcn0gb2Zmc2V0XG4gKiBAcGFyYW0ge051bWJlcn0gbGVuZ3RoXG4gKiBAcGFyYW0ge09iamVjdH0gb3B0aW9uc1xuICogICBAcHJvcGVydHkge0Jvb2xlYW59IG5vcm1hbGl6ZVxuICovXG5cbkNoYW5nZXMucmVtb3ZlVGV4dEJ5S2V5ID0gKGNoYW5nZSwga2V5LCBvZmZzZXQsIGxlbmd0aCwgb3B0aW9ucyA9IHt9KSA9PiB7XG4gIGNvbnN0IHsgbm9ybWFsaXplID0gdHJ1ZSB9ID0gb3B0aW9uc1xuICBjb25zdCB7IHN0YXRlIH0gPSBjaGFuZ2VcbiAgY29uc3QgeyBkb2N1bWVudCB9ID0gc3RhdGVcbiAgY29uc3QgcGF0aCA9IGRvY3VtZW50LmdldFBhdGgoa2V5KVxuICBjb25zdCBub2RlID0gZG9jdW1lbnQuZ2V0Tm9kZShrZXkpXG4gIGNvbnN0IHJhbmdlcyA9IG5vZGUuZ2V0UmFuZ2VzKClcbiAgY29uc3QgeyB0ZXh0IH0gPSBub2RlXG5cbiAgY29uc3QgcmVtb3ZhbHMgPSBbXVxuICBjb25zdCBieCA9IG9mZnNldFxuICBjb25zdCBieSA9IG9mZnNldCArIGxlbmd0aFxuICBsZXQgbyA9IDBcblxuICByYW5nZXMuZm9yRWFjaCgocmFuZ2UpID0+IHtcbiAgICBjb25zdCB7IG1hcmtzIH0gPSByYW5nZVxuICAgIGNvbnN0IGF4ID0gb1xuICAgIGNvbnN0IGF5ID0gYXggKyByYW5nZS50ZXh0Lmxlbmd0aFxuXG4gICAgbyArPSByYW5nZS50ZXh0Lmxlbmd0aFxuXG4gICAgLy8gSWYgdGhlIHJhbmdlIGRvZXNuJ3Qgb3ZlcmxhcCB3aXRoIHRoZSByZW1vdmFsLCBjb250aW51ZSBvbi5cbiAgICBpZiAoYXkgPCBieCB8fCBieSA8IGF4KSByZXR1cm5cblxuICAgIC8vIE90aGVyd2lzZSwgZGV0ZXJtaW5lIHdoaWNoIG9mZnNldCBhbmQgY2hhcmFjdGVycyBvdmVybGFwLlxuICAgIGNvbnN0IHN0YXJ0ID0gTWF0aC5tYXgoYXgsIGJ4KVxuICAgIGNvbnN0IGVuZCA9IE1hdGgubWluKGF5LCBieSlcbiAgICBjb25zdCBzdHJpbmcgPSB0ZXh0LnNsaWNlKHN0YXJ0LCBlbmQpXG5cbiAgICByZW1vdmFscy5wdXNoKHtcbiAgICAgIHR5cGU6ICdyZW1vdmVfdGV4dCcsXG4gICAgICBwYXRoLFxuICAgICAgb2Zmc2V0OiBzdGFydCxcbiAgICAgIHRleHQ6IHN0cmluZyxcbiAgICAgIG1hcmtzLFxuICAgIH0pXG4gIH0pXG5cbiAgLy8gQXBwbHkgdGhlIHJlbW92YWxzIGluIHJldmVyc2Ugb3JkZXIsIHNvIHRoYXQgc3Vic2VxdWVudCByZW1vdmFscyBhcmVuJ3RcbiAgLy8gaW1wYWN0ZWQgYnkgcHJldmlvdXMgb25lcy5cbiAgcmVtb3ZhbHMucmV2ZXJzZSgpLmZvckVhY2goKG9wKSA9PiB7XG4gICAgY2hhbmdlLmFwcGx5T3BlcmF0aW9uKG9wKVxuICB9KVxuXG4gIGlmIChub3JtYWxpemUpIHtcbiAgICBjb25zdCBibG9jayA9IGRvY3VtZW50LmdldENsb3Nlc3RCbG9jayhrZXkpXG4gICAgY2hhbmdlLm5vcm1hbGl6ZU5vZGVCeUtleShibG9jay5rZXksIFNDSEVNQSlcbiAgfVxufVxuXG4vKipcbiAqIFNldCBgcHJvcGVydGllc2Agb24gbWFyayBvbiB0ZXh0IGF0IGBvZmZzZXRgIGFuZCBgbGVuZ3RoYCBpbiBub2RlIGJ5IGBrZXlgLlxuICpcbiAqIEBwYXJhbSB7Q2hhbmdlfSBjaGFuZ2VcbiAqIEBwYXJhbSB7U3RyaW5nfSBrZXlcbiAqIEBwYXJhbSB7TnVtYmVyfSBvZmZzZXRcbiAqIEBwYXJhbSB7TnVtYmVyfSBsZW5ndGhcbiAqIEBwYXJhbSB7TWFya30gbWFya1xuICogQHBhcmFtIHtPYmplY3R9IG9wdGlvbnNcbiAqICAgQHByb3BlcnR5IHtCb29sZWFufSBub3JtYWxpemVcbiAqL1xuXG5DaGFuZ2VzLnNldE1hcmtCeUtleSA9IChjaGFuZ2UsIGtleSwgb2Zmc2V0LCBsZW5ndGgsIG1hcmssIHByb3BlcnRpZXMsIG9wdGlvbnMgPSB7fSkgPT4ge1xuICBtYXJrID0gTm9ybWFsaXplLm1hcmsobWFyaylcbiAgcHJvcGVydGllcyA9IE5vcm1hbGl6ZS5tYXJrUHJvcGVydGllcyhwcm9wZXJ0aWVzKVxuICBjb25zdCB7IG5vcm1hbGl6ZSA9IHRydWUgfSA9IG9wdGlvbnNcbiAgY29uc3QgeyBzdGF0ZSB9ID0gY2hhbmdlXG4gIGNvbnN0IHsgZG9jdW1lbnQgfSA9IHN0YXRlXG4gIGNvbnN0IHBhdGggPSBkb2N1bWVudC5nZXRQYXRoKGtleSlcblxuICBjaGFuZ2UuYXBwbHlPcGVyYXRpb24oe1xuICAgIHR5cGU6ICdzZXRfbWFyaycsXG4gICAgcGF0aCxcbiAgICBvZmZzZXQsXG4gICAgbGVuZ3RoLFxuICAgIG1hcmssXG4gICAgcHJvcGVydGllcyxcbiAgfSlcblxuICBpZiAobm9ybWFsaXplKSB7XG4gICAgY29uc3QgcGFyZW50ID0gZG9jdW1lbnQuZ2V0UGFyZW50KGtleSlcbiAgICBjaGFuZ2Uubm9ybWFsaXplTm9kZUJ5S2V5KHBhcmVudC5rZXksIFNDSEVNQSlcbiAgfVxufVxuXG4vKipcbiAqIFNldCBgcHJvcGVydGllc2Agb24gYSBub2RlIGJ5IGBrZXlgLlxuICpcbiAqIEBwYXJhbSB7Q2hhbmdlfSBjaGFuZ2VcbiAqIEBwYXJhbSB7U3RyaW5nfSBrZXlcbiAqIEBwYXJhbSB7T2JqZWN0fFN0cmluZ30gcHJvcGVydGllc1xuICogQHBhcmFtIHtPYmplY3R9IG9wdGlvbnNcbiAqICAgQHByb3BlcnR5IHtCb29sZWFufSBub3JtYWxpemVcbiAqL1xuXG5DaGFuZ2VzLnNldE5vZGVCeUtleSA9IChjaGFuZ2UsIGtleSwgcHJvcGVydGllcywgb3B0aW9ucyA9IHt9KSA9PiB7XG4gIHByb3BlcnRpZXMgPSBOb3JtYWxpemUubm9kZVByb3BlcnRpZXMocHJvcGVydGllcylcbiAgY29uc3QgeyBub3JtYWxpemUgPSB0cnVlIH0gPSBvcHRpb25zXG4gIGNvbnN0IHsgc3RhdGUgfSA9IGNoYW5nZVxuICBjb25zdCB7IGRvY3VtZW50IH0gPSBzdGF0ZVxuICBjb25zdCBwYXRoID0gZG9jdW1lbnQuZ2V0UGF0aChrZXkpXG4gIGNvbnN0IG5vZGUgPSBkb2N1bWVudC5nZXROb2RlKGtleSlcblxuICBjaGFuZ2UuYXBwbHlPcGVyYXRpb24oe1xuICAgIHR5cGU6ICdzZXRfbm9kZScsXG4gICAgcGF0aCxcbiAgICBub2RlLFxuICAgIHByb3BlcnRpZXMsXG4gIH0pXG5cbiAgaWYgKG5vcm1hbGl6ZSkge1xuICAgIGNoYW5nZS5ub3JtYWxpemVOb2RlQnlLZXkobm9kZS5rZXksIFNDSEVNQSlcbiAgfVxufVxuXG4vKipcbiAqIFNwbGl0IGEgbm9kZSBieSBga2V5YCBhdCBgcG9zaXRpb25gLlxuICpcbiAqIEBwYXJhbSB7Q2hhbmdlfSBjaGFuZ2VcbiAqIEBwYXJhbSB7U3RyaW5nfSBrZXlcbiAqIEBwYXJhbSB7TnVtYmVyfSBwb3NpdGlvblxuICogQHBhcmFtIHtPYmplY3R9IG9wdGlvbnNcbiAqICAgQHByb3BlcnR5IHtCb29sZWFufSBub3JtYWxpemVcbiAqL1xuXG5DaGFuZ2VzLnNwbGl0Tm9kZUJ5S2V5ID0gKGNoYW5nZSwga2V5LCBwb3NpdGlvbiwgb3B0aW9ucyA9IHt9KSA9PiB7XG4gIGNvbnN0IHsgbm9ybWFsaXplID0gdHJ1ZSB9ID0gb3B0aW9uc1xuICBjb25zdCB7IHN0YXRlIH0gPSBjaGFuZ2VcbiAgY29uc3QgeyBkb2N1bWVudCB9ID0gc3RhdGVcbiAgY29uc3QgcGF0aCA9IGRvY3VtZW50LmdldFBhdGgoa2V5KVxuXG4gIGNoYW5nZS5hcHBseU9wZXJhdGlvbih7XG4gICAgdHlwZTogJ3NwbGl0X25vZGUnLFxuICAgIHBhdGgsXG4gICAgcG9zaXRpb24sXG4gIH0pXG5cbiAgaWYgKG5vcm1hbGl6ZSkge1xuICAgIGNvbnN0IHBhcmVudCA9IGRvY3VtZW50LmdldFBhcmVudChrZXkpXG4gICAgY2hhbmdlLm5vcm1hbGl6ZU5vZGVCeUtleShwYXJlbnQua2V5LCBTQ0hFTUEpXG4gIH1cbn1cblxuLyoqXG4gKiBTcGxpdCBhIG5vZGUgZGVlcGx5IGRvd24gdGhlIHRyZWUgYnkgYGtleWAsIGB0ZXh0S2V5YCBhbmQgYHRleHRPZmZzZXRgLlxuICpcbiAqIEBwYXJhbSB7Q2hhbmdlfSBjaGFuZ2VcbiAqIEBwYXJhbSB7U3RyaW5nfSBrZXlcbiAqIEBwYXJhbSB7TnVtYmVyfSBwb3NpdGlvblxuICogQHBhcmFtIHtPYmplY3R9IG9wdGlvbnNcbiAqICAgQHByb3BlcnR5IHtCb29sZWFufSBub3JtYWxpemVcbiAqL1xuXG5DaGFuZ2VzLnNwbGl0RGVzY2VuZGFudHNCeUtleSA9IChjaGFuZ2UsIGtleSwgdGV4dEtleSwgdGV4dE9mZnNldCwgb3B0aW9ucyA9IHt9KSA9PiB7XG4gIGlmIChrZXkgPT0gdGV4dEtleSkge1xuICAgIGNoYW5nZS5zcGxpdE5vZGVCeUtleSh0ZXh0S2V5LCB0ZXh0T2Zmc2V0LCBvcHRpb25zKVxuICAgIHJldHVyblxuICB9XG5cbiAgY29uc3QgeyBub3JtYWxpemUgPSB0cnVlIH0gPSBvcHRpb25zXG4gIGNvbnN0IHsgc3RhdGUgfSA9IGNoYW5nZVxuICBjb25zdCB7IGRvY3VtZW50IH0gPSBzdGF0ZVxuXG4gIGNvbnN0IHRleHQgPSBkb2N1bWVudC5nZXROb2RlKHRleHRLZXkpXG4gIGNvbnN0IGFuY2VzdG9ycyA9IGRvY3VtZW50LmdldEFuY2VzdG9ycyh0ZXh0S2V5KVxuICBjb25zdCBub2RlcyA9IGFuY2VzdG9ycy5za2lwVW50aWwoYSA9PiBhLmtleSA9PSBrZXkpLnJldmVyc2UoKS51bnNoaWZ0KHRleHQpXG4gIGxldCBwcmV2aW91c1xuXG4gIG5vZGVzLmZvckVhY2goKG5vZGUpID0+IHtcbiAgICBjb25zdCBpbmRleCA9IHByZXZpb3VzID8gbm9kZS5ub2Rlcy5pbmRleE9mKHByZXZpb3VzKSArIDEgOiB0ZXh0T2Zmc2V0XG4gICAgcHJldmlvdXMgPSBub2RlXG4gICAgY2hhbmdlLnNwbGl0Tm9kZUJ5S2V5KG5vZGUua2V5LCBpbmRleCwgeyBub3JtYWxpemU6IGZhbHNlIH0pXG4gIH0pXG5cbiAgaWYgKG5vcm1hbGl6ZSkge1xuICAgIGNvbnN0IHBhcmVudCA9IGRvY3VtZW50LmdldFBhcmVudChrZXkpXG4gICAgY2hhbmdlLm5vcm1hbGl6ZU5vZGVCeUtleShwYXJlbnQua2V5LCBTQ0hFTUEpXG4gIH1cbn1cblxuLyoqXG4gKiBVbndyYXAgY29udGVudCBmcm9tIGFuIGlubGluZSBwYXJlbnQgd2l0aCBgcHJvcGVydGllc2AuXG4gKlxuICogQHBhcmFtIHtDaGFuZ2V9IGNoYW5nZVxuICogQHBhcmFtIHtTdHJpbmd9IGtleVxuICogQHBhcmFtIHtPYmplY3R8U3RyaW5nfSBwcm9wZXJ0aWVzXG4gKiBAcGFyYW0ge09iamVjdH0gb3B0aW9uc1xuICogICBAcHJvcGVydHkge0Jvb2xlYW59IG5vcm1hbGl6ZVxuICovXG5cbkNoYW5nZXMudW53cmFwSW5saW5lQnlLZXkgPSAoY2hhbmdlLCBrZXksIHByb3BlcnRpZXMsIG9wdGlvbnMpID0+IHtcbiAgY29uc3QgeyBzdGF0ZSB9ID0gY2hhbmdlXG4gIGNvbnN0IHsgZG9jdW1lbnQsIHNlbGVjdGlvbiB9ID0gc3RhdGVcbiAgY29uc3Qgbm9kZSA9IGRvY3VtZW50LmFzc2VydERlc2NlbmRhbnQoa2V5KVxuICBjb25zdCBmaXJzdCA9IG5vZGUuZ2V0Rmlyc3RUZXh0KClcbiAgY29uc3QgbGFzdCA9IG5vZGUuZ2V0TGFzdFRleHQoKVxuICBjb25zdCByYW5nZSA9IHNlbGVjdGlvbi5tb3ZlVG9SYW5nZU9mKGZpcnN0LCBsYXN0KVxuICBjaGFuZ2UudW53cmFwSW5saW5lQXRSYW5nZShyYW5nZSwgcHJvcGVydGllcywgb3B0aW9ucylcbn1cblxuLyoqXG4gKiBVbndyYXAgY29udGVudCBmcm9tIGEgYmxvY2sgcGFyZW50IHdpdGggYHByb3BlcnRpZXNgLlxuICpcbiAqIEBwYXJhbSB7Q2hhbmdlfSBjaGFuZ2VcbiAqIEBwYXJhbSB7U3RyaW5nfSBrZXlcbiAqIEBwYXJhbSB7T2JqZWN0fFN0cmluZ30gcHJvcGVydGllc1xuICogQHBhcmFtIHtPYmplY3R9IG9wdGlvbnNcbiAqICAgQHByb3BlcnR5IHtCb29sZWFufSBub3JtYWxpemVcbiAqL1xuXG5DaGFuZ2VzLnVud3JhcEJsb2NrQnlLZXkgPSAoY2hhbmdlLCBrZXksIHByb3BlcnRpZXMsIG9wdGlvbnMpID0+IHtcbiAgY29uc3QgeyBzdGF0ZSB9ID0gY2hhbmdlXG4gIGNvbnN0IHsgZG9jdW1lbnQsIHNlbGVjdGlvbiB9ID0gc3RhdGVcbiAgY29uc3Qgbm9kZSA9IGRvY3VtZW50LmFzc2VydERlc2NlbmRhbnQoa2V5KVxuICBjb25zdCBmaXJzdCA9IG5vZGUuZ2V0Rmlyc3RUZXh0KClcbiAgY29uc3QgbGFzdCA9IG5vZGUuZ2V0TGFzdFRleHQoKVxuICBjb25zdCByYW5nZSA9IHNlbGVjdGlvbi5tb3ZlVG9SYW5nZU9mKGZpcnN0LCBsYXN0KVxuICBjaGFuZ2UudW53cmFwQmxvY2tBdFJhbmdlKHJhbmdlLCBwcm9wZXJ0aWVzLCBvcHRpb25zKVxufVxuXG4vKipcbiAqIFVud3JhcCBhIHNpbmdsZSBub2RlIGZyb20gaXRzIHBhcmVudC5cbiAqXG4gKiBJZiB0aGUgbm9kZSBpcyBzdXJyb3VuZGVkIHdpdGggc2libGluZ3MsIGl0cyBwYXJlbnQgd2lsbCBiZVxuICogc3BsaXQuIElmIHRoZSBub2RlIGlzIHRoZSBvbmx5IGNoaWxkLCB0aGUgcGFyZW50IGlzIHJlbW92ZWQsIGFuZFxuICogc2ltcGx5IHJlcGxhY2VkIGJ5IHRoZSBub2RlIGl0c2VsZi4gIENhbm5vdCB1bndyYXAgYSByb290IG5vZGUuXG4gKlxuICogQHBhcmFtIHtDaGFuZ2V9IGNoYW5nZVxuICogQHBhcmFtIHtTdHJpbmd9IGtleVxuICogQHBhcmFtIHtPYmplY3R9IG9wdGlvbnNcbiAqICAgQHByb3BlcnR5IHtCb29sZWFufSBub3JtYWxpemVcbiAqL1xuXG5DaGFuZ2VzLnVud3JhcE5vZGVCeUtleSA9IChjaGFuZ2UsIGtleSwgb3B0aW9ucyA9IHt9KSA9PiB7XG4gIGNvbnN0IHsgbm9ybWFsaXplID0gdHJ1ZSB9ID0gb3B0aW9uc1xuICBjb25zdCB7IHN0YXRlIH0gPSBjaGFuZ2VcbiAgY29uc3QgeyBkb2N1bWVudCB9ID0gc3RhdGVcbiAgY29uc3QgcGFyZW50ID0gZG9jdW1lbnQuZ2V0UGFyZW50KGtleSlcbiAgY29uc3Qgbm9kZSA9IHBhcmVudC5nZXRDaGlsZChrZXkpXG5cbiAgY29uc3QgaW5kZXggPSBwYXJlbnQubm9kZXMuaW5kZXhPZihub2RlKVxuICBjb25zdCBpc0ZpcnN0ID0gaW5kZXggPT09IDBcbiAgY29uc3QgaXNMYXN0ID0gaW5kZXggPT09IHBhcmVudC5ub2Rlcy5zaXplIC0gMVxuXG4gIGNvbnN0IHBhcmVudFBhcmVudCA9IGRvY3VtZW50LmdldFBhcmVudChwYXJlbnQua2V5KVxuICBjb25zdCBwYXJlbnRJbmRleCA9IHBhcmVudFBhcmVudC5ub2Rlcy5pbmRleE9mKHBhcmVudClcblxuICBpZiAocGFyZW50Lm5vZGVzLnNpemUgPT09IDEpIHtcbiAgICBjaGFuZ2UubW92ZU5vZGVCeUtleShrZXksIHBhcmVudFBhcmVudC5rZXksIHBhcmVudEluZGV4LCB7IG5vcm1hbGl6ZTogZmFsc2UgfSlcbiAgICBjaGFuZ2UucmVtb3ZlTm9kZUJ5S2V5KHBhcmVudC5rZXksIG9wdGlvbnMpXG4gIH1cblxuICBlbHNlIGlmIChpc0ZpcnN0KSB7XG4gICAgLy8gSnVzdCBtb3ZlIHRoZSBub2RlIGJlZm9yZSBpdHMgcGFyZW50LlxuICAgIGNoYW5nZS5tb3ZlTm9kZUJ5S2V5KGtleSwgcGFyZW50UGFyZW50LmtleSwgcGFyZW50SW5kZXgsIG9wdGlvbnMpXG4gIH1cblxuICBlbHNlIGlmIChpc0xhc3QpIHtcbiAgICAvLyBKdXN0IG1vdmUgdGhlIG5vZGUgYWZ0ZXIgaXRzIHBhcmVudC5cbiAgICBjaGFuZ2UubW92ZU5vZGVCeUtleShrZXksIHBhcmVudFBhcmVudC5rZXksIHBhcmVudEluZGV4ICsgMSwgb3B0aW9ucylcbiAgfVxuXG4gIGVsc2Uge1xuICAgIC8vIFNwbGl0IHRoZSBwYXJlbnQuXG4gICAgY2hhbmdlLnNwbGl0Tm9kZUJ5S2V5KHBhcmVudC5rZXksIGluZGV4LCB7IG5vcm1hbGl6ZTogZmFsc2UgfSlcblxuICAgIC8vIEV4dHJhY3QgdGhlIG5vZGUgaW4gYmV0d2VlbiB0aGUgc3BsaXR0ZWQgcGFyZW50LlxuICAgIGNoYW5nZS5tb3ZlTm9kZUJ5S2V5KGtleSwgcGFyZW50UGFyZW50LmtleSwgcGFyZW50SW5kZXggKyAxLCB7IG5vcm1hbGl6ZTogZmFsc2UgfSlcblxuICAgIGlmIChub3JtYWxpemUpIHtcbiAgICAgIGNoYW5nZS5ub3JtYWxpemVOb2RlQnlLZXkocGFyZW50UGFyZW50LmtleSwgU0NIRU1BKVxuICAgIH1cbiAgfVxufVxuXG4vKipcbiAqIFdyYXAgYSBub2RlIGluIGFuIGlubGluZSB3aXRoIGBwcm9wZXJ0aWVzYC5cbiAqXG4gKiBAcGFyYW0ge0NoYW5nZX0gY2hhbmdlXG4gKiBAcGFyYW0ge1N0cmluZ30ga2V5IFRoZSBub2RlIHRvIHdyYXBcbiAqIEBwYXJhbSB7QmxvY2t8T2JqZWN0fFN0cmluZ30gaW5saW5lIFRoZSB3cmFwcGluZyBpbmxpbmUgKGl0cyBjaGlsZHJlbiBhcmUgZGlzY2FyZGVkKVxuICogQHBhcmFtIHtPYmplY3R9IG9wdGlvbnNcbiAqICAgQHByb3BlcnR5IHtCb29sZWFufSBub3JtYWxpemVcbiAqL1xuXG5DaGFuZ2VzLndyYXBJbmxpbmVCeUtleSA9IChjaGFuZ2UsIGtleSwgaW5saW5lLCBvcHRpb25zKSA9PiB7XG4gIGlubGluZSA9IE5vcm1hbGl6ZS5pbmxpbmUoaW5saW5lKVxuICBpbmxpbmUgPSBpbmxpbmUuc2V0KCdub2RlcycsIGlubGluZS5ub2Rlcy5jbGVhcigpKVxuXG4gIGNvbnN0IHsgZG9jdW1lbnQgfSA9IGNoYW5nZS5zdGF0ZVxuICBjb25zdCBub2RlID0gZG9jdW1lbnQuYXNzZXJ0RGVzY2VuZGFudChrZXkpXG4gIGNvbnN0IHBhcmVudCA9IGRvY3VtZW50LmdldFBhcmVudChub2RlLmtleSlcbiAgY29uc3QgaW5kZXggPSBwYXJlbnQubm9kZXMuaW5kZXhPZihub2RlKVxuXG4gIGNoYW5nZS5pbnNlcnROb2RlQnlLZXkocGFyZW50LmtleSwgaW5kZXgsIGlubGluZSwgeyBub3JtYWxpemU6IGZhbHNlIH0pXG4gIGNoYW5nZS5tb3ZlTm9kZUJ5S2V5KG5vZGUua2V5LCBpbmxpbmUua2V5LCAwLCBvcHRpb25zKVxufVxuXG4vKipcbiAqIFdyYXAgYSBub2RlIGluIGEgYmxvY2sgd2l0aCBgcHJvcGVydGllc2AuXG4gKlxuICogQHBhcmFtIHtDaGFuZ2V9IGNoYW5nZVxuICogQHBhcmFtIHtTdHJpbmd9IGtleSBUaGUgbm9kZSB0byB3cmFwXG4gKiBAcGFyYW0ge0Jsb2NrfE9iamVjdHxTdHJpbmd9IGJsb2NrIFRoZSB3cmFwcGluZyBibG9jayAoaXRzIGNoaWxkcmVuIGFyZSBkaXNjYXJkZWQpXG4gKiBAcGFyYW0ge09iamVjdH0gb3B0aW9uc1xuICogICBAcHJvcGVydHkge0Jvb2xlYW59IG5vcm1hbGl6ZVxuICovXG5cbkNoYW5nZXMud3JhcEJsb2NrQnlLZXkgPSAoY2hhbmdlLCBrZXksIGJsb2NrLCBvcHRpb25zKSA9PiB7XG4gIGJsb2NrID0gTm9ybWFsaXplLmJsb2NrKGJsb2NrKVxuICBibG9jayA9IGJsb2NrLnNldCgnbm9kZXMnLCBibG9jay5ub2Rlcy5jbGVhcigpKVxuXG4gIGNvbnN0IHsgZG9jdW1lbnQgfSA9IGNoYW5nZS5zdGF0ZVxuICBjb25zdCBub2RlID0gZG9jdW1lbnQuYXNzZXJ0RGVzY2VuZGFudChrZXkpXG4gIGNvbnN0IHBhcmVudCA9IGRvY3VtZW50LmdldFBhcmVudChub2RlLmtleSlcbiAgY29uc3QgaW5kZXggPSBwYXJlbnQubm9kZXMuaW5kZXhPZihub2RlKVxuXG4gIGNoYW5nZS5pbnNlcnROb2RlQnlLZXkocGFyZW50LmtleSwgaW5kZXgsIGJsb2NrLCB7IG5vcm1hbGl6ZTogZmFsc2UgfSlcbiAgY2hhbmdlLm1vdmVOb2RlQnlLZXkobm9kZS5rZXksIGJsb2NrLmtleSwgMCwgb3B0aW9ucylcbn1cblxuLyoqXG4gKiBFeHBvcnQuXG4gKlxuICogQHR5cGUge09iamVjdH1cbiAqL1xuXG5leHBvcnQgZGVmYXVsdCBDaGFuZ2VzXG4iXX0=