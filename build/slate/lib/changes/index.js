'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _atCurrentRange = require('./at-current-range');

var _atCurrentRange2 = _interopRequireDefault(_atCurrentRange);

var _atRange = require('./at-range');

var _atRange2 = _interopRequireDefault(_atRange);

var _byKey = require('./by-key');

var _byKey2 = _interopRequireDefault(_byKey);

var _normalize = require('./normalize');

var _normalize2 = _interopRequireDefault(_normalize);

var _onHistory = require('./on-history');

var _onHistory2 = _interopRequireDefault(_onHistory);

var _onSelection = require('./on-selection');

var _onSelection2 = _interopRequireDefault(_onSelection);

var _onState = require('./on-state');

var _onState2 = _interopRequireDefault(_onState);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Export.
 *
 * @type {Object}
 */

exports.default = _extends({}, _atCurrentRange2.default, _atRange2.default, _byKey2.default, _normalize2.default, _onHistory2.default, _onSelection2.default, _onState2.default);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9jaGFuZ2VzL2luZGV4LmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7O0FBQ0E7Ozs7QUFDQTs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUNBOzs7O0FBQ0E7Ozs7OztBQUVBIiwiZmlsZSI6ImluZGV4LmpzIiwic291cmNlc0NvbnRlbnQiOlsiXG5pbXBvcnQgQXRDdXJyZW50UmFuZ2UgZnJvbSAnLi9hdC1jdXJyZW50LXJhbmdlJ1xuaW1wb3J0IEF0UmFuZ2UgZnJvbSAnLi9hdC1yYW5nZSdcbmltcG9ydCBCeUtleSBmcm9tICcuL2J5LWtleSdcbmltcG9ydCBOb3JtYWxpemUgZnJvbSAnLi9ub3JtYWxpemUnXG5pbXBvcnQgT25IaXN0b3J5IGZyb20gJy4vb24taGlzdG9yeSdcbmltcG9ydCBPblNlbGVjdGlvbiBmcm9tICcuL29uLXNlbGVjdGlvbidcbmltcG9ydCBPblN0YXRlIGZyb20gJy4vb24tc3RhdGUnXG5cbi8qKlxuICogRXhwb3J0LlxuICpcbiAqIEB0eXBlIHtPYmplY3R9XG4gKi9cblxuZXhwb3J0IGRlZmF1bHQge1xuICAuLi5BdEN1cnJlbnRSYW5nZSxcbiAgLi4uQXRSYW5nZSxcbiAgLi4uQnlLZXksXG4gIC4uLk5vcm1hbGl6ZSxcbiAgLi4uT25IaXN0b3J5LFxuICAuLi5PblNlbGVjdGlvbixcbiAgLi4uT25TdGF0ZSxcbn1cbiJdfQ==