'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _normalize = require('../utils/normalize');

var _normalize2 = _interopRequireDefault(_normalize);

var _string = require('../utils/string');

var _string2 = _interopRequireDefault(_string);

var _core = require('../schemas/core');

var _core2 = _interopRequireDefault(_core);

var _immutable = require('immutable');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Changes.
 *
 * @type {Object}
 */

var Changes = {};

/**
 * An options object with normalize set to `false`.
 *
 * @type {Object}
 */

var OPTS = {
  normalize: false
};

/**
 * Add a new `mark` to the characters at `range`.
 *
 * @param {Change} change
 * @param {Selection} range
 * @param {Mixed} mark
 * @param {Object} options
 *   @property {Boolean} normalize
 */

Changes.addMarkAtRange = function (change, range, mark) {
  var options = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : {};

  if (range.isCollapsed) return;

  var _options$normalize = options.normalize,
      normalize = _options$normalize === undefined ? true : _options$normalize;
  var state = change.state;
  var document = state.document;
  var startKey = range.startKey,
      startOffset = range.startOffset,
      endKey = range.endKey,
      endOffset = range.endOffset;

  var texts = document.getTextsAtRange(range);

  texts.forEach(function (node) {
    var key = node.key;

    var index = 0;
    var length = node.text.length;

    if (key == startKey) index = startOffset;
    if (key == endKey) length = endOffset;
    if (key == startKey && key == endKey) length = endOffset - startOffset;

    change.addMarkByKey(key, index, length, mark, { normalize: normalize });
  });
};

/**
 * Delete everything in a `range`.
 *
 * @param {Change} change
 * @param {Selection} range
 * @param {Object} options
 *   @property {Boolean} normalize
 */

Changes.deleteAtRange = function (change, range) {
  var options = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};

  if (range.isCollapsed) return;

  change.snapshotSelection();

  var _options$normalize2 = options.normalize,
      normalize = _options$normalize2 === undefined ? true : _options$normalize2;
  var startKey = range.startKey,
      startOffset = range.startOffset,
      endKey = range.endKey,
      endOffset = range.endOffset;

  // Split at the range edges within a common ancestor, without normalizing.

  var state = change.state;
  var _state = state,
      document = _state.document;

  var ancestor = document.getCommonAncestor(startKey, endKey);
  var startChild = ancestor.getFurthestAncestor(startKey);
  var endChild = ancestor.getFurthestAncestor(endKey);

  // If the start child is a void node, and the range begins or
  // ends (when range is backward) at the start of it, remove it
  // and set nextSibling as startChild until there is no startChild
  // that is a void node and included in the selection range
  var startChildIncludesVoid = startChild.isVoid && (range.anchorOffset === 0 && !range.isBackward || range.focusOffset === 0 && range.isBackward);

  while (startChildIncludesVoid) {
    var nextSibling = document.getNextSibling(startChild.key);
    change.removeNodeByKey(startChild.key, OPTS);
    // Abort if no nextSibling or we are about to process the endChild which is aslo a void node
    if (!nextSibling || endChild.key === nextSibling.key && nextSibling.isVoid) {
      startChildIncludesVoid = false;
      return;
    }
    // Process the next void
    if (nextSibling.isVoid) {
      startChild = nextSibling;
    }
    // Set the startChild, startKey and startOffset in the beginning of the next non void sibling
    if (!nextSibling.isVoid) {
      startChild = nextSibling;
      if (startChild.getTexts) {
        startKey = startChild.getTexts().first().key;
      } else {
        startKey = startChild.key;
      }
      startOffset = 0;
      startChildIncludesVoid = false;
    }
  }

  // If the start child is a void node, and the range ends or
  // begins (when range is backward) at the end of it move to nextSibling
  var startChildEndOfVoid = startChild.isVoid && (range.anchorOffset === 1 && !range.isBackward || range.focusOffset === 1 && range.isBackward);

  if (startChildEndOfVoid) {
    var _nextSibling = document.getNextSibling(startChild.key);
    if (_nextSibling) {
      startChild = _nextSibling;
      if (startChild.getTexts) {
        startKey = startChild.getTexts().first().key;
      } else {
        startKey = startChild.key;
      }
      startOffset = 0;
    }
  }

  // If the start and end key are the same, we can just remove it.
  if (startKey == endKey) {
    // If it is a void node, remove the whole node
    if (ancestor.isVoid) {
      // Deselect if this is the only node left in document
      if (document.nodes.size === 1) {
        change.deselect();
      }
      change.removeNodeByKey(ancestor.key, OPTS);
      return;
    }
    // Remove the text
    var index = startOffset;
    var length = endOffset - startOffset;
    change.removeTextByKey(startKey, index, length, { normalize: normalize });
    return;
  }

  // Split at the range edges within a common ancestor, without normalizing.
  state = change.state;
  document = state.document;
  ancestor = document.getCommonAncestor(startKey, endKey);
  startChild = ancestor.getFurthestAncestor(startKey);
  endChild = ancestor.getFurthestAncestor(endKey);

  if (startChild.kind == 'text') {
    change.splitNodeByKey(startChild.key, startOffset, OPTS);
  } else {
    change.splitDescendantsByKey(startChild.key, startKey, startOffset, OPTS);
  }

  if (endChild.kind == 'text') {
    change.splitNodeByKey(endChild.key, endOffset, OPTS);
  } else {
    change.splitDescendantsByKey(endChild.key, endKey, endOffset, OPTS);
  }

  // Refresh variables.
  state = change.state;
  document = state.document;
  ancestor = document.getCommonAncestor(startKey, endKey);
  startChild = ancestor.getFurthestAncestor(startKey);
  endChild = ancestor.getFurthestAncestor(endKey);
  var startIndex = ancestor.nodes.indexOf(startChild);
  var endIndex = ancestor.nodes.indexOf(endChild);
  var middles = ancestor.nodes.slice(startIndex + 1, endIndex + 1);
  var next = document.getNextText(endKey);

  // Remove all of the middle nodes, between the splits.
  if (middles.size) {
    middles.forEach(function (child) {
      change.removeNodeByKey(child.key, OPTS);
    });
  }

  // If the start and end block are different, move all of the nodes from the
  // end block into the start block.
  state = change.state;
  document = state.document;
  var startBlock = document.getClosestBlock(startKey);
  var endBlock = document.getClosestBlock(next.key);

  // If the endBlock is void, just remove the startBlock
  if (endBlock.isVoid) {
    change.removeNodeByKey(startBlock.key);
    return;
  }

  // If the start and end block are different, move all of the nodes from the
  // end block into the start block
  if (startBlock.key !== endBlock.key) {
    endBlock.nodes.forEach(function (child, i) {
      var newKey = startBlock.key;
      var newIndex = startBlock.nodes.size + i;
      change.moveNodeByKey(child.key, newKey, newIndex, OPTS);
    });

    // Remove parents of endBlock as long as they have a single child
    var lonely = document.getFurthestOnlyChildAncestor(endBlock.key) || endBlock;
    change.removeNodeByKey(lonely.key, OPTS);
  }

  if (normalize) {
    change.normalizeNodeByKey(ancestor.key, _core2.default);
  }
};

/**
 * Delete backward until the character boundary at a `range`.
 *
 * @param {Change} change
 * @param {Selection} range
 * @param {Object} options
 *   @property {Boolean} normalize
 */

Changes.deleteCharBackwardAtRange = function (change, range, options) {
  var state = change.state;
  var document = state.document;
  var startKey = range.startKey,
      startOffset = range.startOffset;

  var startBlock = document.getClosestBlock(startKey);
  var offset = startBlock.getOffset(startKey);
  var o = offset + startOffset;
  var text = startBlock.text;

  var n = _string2.default.getCharOffsetBackward(text, o);
  change.deleteBackwardAtRange(range, n, options);
};

/**
 * Delete backward until the line boundary at a `range`.
 *
 * @param {Change} change
 * @param {Selection} range
 * @param {Object} options
 *   @property {Boolean} normalize
 */

Changes.deleteLineBackwardAtRange = function (change, range, options) {
  var state = change.state;
  var document = state.document;
  var startKey = range.startKey,
      startOffset = range.startOffset;

  var startBlock = document.getClosestBlock(startKey);
  var offset = startBlock.getOffset(startKey);
  var o = offset + startOffset;
  change.deleteBackwardAtRange(range, o, options);
};

/**
 * Delete backward until the word boundary at a `range`.
 *
 * @param {Change} change
 * @param {Selection} range
 * @param {Object} options
 *   @property {Boolean} normalize
 */

Changes.deleteWordBackwardAtRange = function (change, range, options) {
  var state = change.state;
  var document = state.document;
  var startKey = range.startKey,
      startOffset = range.startOffset;

  var startBlock = document.getClosestBlock(startKey);
  var offset = startBlock.getOffset(startKey);
  var o = offset + startOffset;
  var text = startBlock.text;

  var n = _string2.default.getWordOffsetBackward(text, o);
  change.deleteBackwardAtRange(range, n, options);
};

/**
 * Delete backward `n` characters at a `range`.
 *
 * @param {Change} change
 * @param {Selection} range
 * @param {Number} n (optional)
 * @param {Object} options
 *   @property {Boolean} normalize
 */

Changes.deleteBackwardAtRange = function (change, range) {
  var n = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 1;
  var options = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : {};
  var _options$normalize3 = options.normalize,
      normalize = _options$normalize3 === undefined ? true : _options$normalize3;
  var state = change.state;
  var document = state.document;
  var _range = range,
      startKey = _range.startKey,
      focusOffset = _range.focusOffset;

  // If the range is expanded, perform a regular delete instead.

  if (range.isExpanded) {
    change.deleteAtRange(range, { normalize: normalize });
    return;
  }

  var block = document.getClosestBlock(startKey);
  // If the closest block is void, delete it.
  if (block && block.isVoid) {
    change.removeNodeByKey(block.key, { normalize: normalize });
    return;
  }
  // If the closest is not void, but empty, remove it
  if (block && !block.isVoid && block.isEmpty && document.nodes.size !== 1) {
    change.removeNodeByKey(block.key, { normalize: normalize });
    return;
  }

  // If the closest inline is void, delete it.
  var inline = document.getClosestInline(startKey);
  if (inline && inline.isVoid) {
    change.removeNodeByKey(inline.key, { normalize: normalize });
    return;
  }

  // If the range is at the start of the document, abort.
  if (range.isAtStartOf(document)) {
    return;
  }

  // If the range is at the start of the text node, we need to figure out what
  // is behind it to know how to delete...
  var text = document.getDescendant(startKey);
  if (range.isAtStartOf(text)) {
    var prev = document.getPreviousText(text.key);
    var prevBlock = document.getClosestBlock(prev.key);
    var prevInline = document.getClosestInline(prev.key);

    // If the previous block is void, remove it.
    if (prevBlock && prevBlock.isVoid) {
      change.removeNodeByKey(prevBlock.key, { normalize: normalize });
      return;
    }

    // If the previous inline is void, remove it.
    if (prevInline && prevInline.isVoid) {
      change.removeNodeByKey(prevInline.key, { normalize: normalize });
      return;
    }

    // If we're deleting by one character and the previous text node is not
    // inside the current block, we need to merge the two blocks together.
    if (n == 1 && prevBlock != block) {
      range = range.merge({
        anchorKey: prev.key,
        anchorOffset: prev.text.length
      });

      change.deleteAtRange(range, { normalize: normalize });
      return;
    }
  }

  // If the focus offset is farther than the number of characters to delete,
  // just remove the characters backwards inside the current node.
  if (n < focusOffset) {
    range = range.merge({
      focusOffset: focusOffset - n,
      isBackward: true
    });

    change.deleteAtRange(range, { normalize: normalize });
    return;
  }

  // Otherwise, we need to see how many nodes backwards to go.
  var node = text;
  var offset = 0;
  var traversed = focusOffset;

  while (n > traversed) {
    node = document.getPreviousText(node.key);
    var next = traversed + node.text.length;
    if (n <= next) {
      offset = next - n;
      break;
    } else {
      traversed = next;
    }
  }

  // If the focus node is inside a void, go up until right after it.
  if (document.hasVoidParent(node.key)) {
    var parent = document.getClosestVoid(node.key);
    node = document.getNextText(parent.key);
    offset = 0;
  }

  range = range.merge({
    focusKey: node.key,
    focusOffset: offset,
    isBackward: true
  });

  change.deleteAtRange(range, { normalize: normalize });
};

/**
 * Delete forward until the character boundary at a `range`.
 *
 * @param {Change} change
 * @param {Selection} range
 * @param {Object} options
 *   @property {Boolean} normalize
 */

Changes.deleteCharForwardAtRange = function (change, range, options) {
  var state = change.state;
  var document = state.document;
  var startKey = range.startKey,
      startOffset = range.startOffset;

  var startBlock = document.getClosestBlock(startKey);
  var offset = startBlock.getOffset(startKey);
  var o = offset + startOffset;
  var text = startBlock.text;

  var n = _string2.default.getCharOffsetForward(text, o);
  change.deleteForwardAtRange(range, n, options);
};

/**
 * Delete forward until the line boundary at a `range`.
 *
 * @param {Change} change
 * @param {Selection} range
 * @param {Object} options
 *   @property {Boolean} normalize
 */

Changes.deleteLineForwardAtRange = function (change, range, options) {
  var state = change.state;
  var document = state.document;
  var startKey = range.startKey,
      startOffset = range.startOffset;

  var startBlock = document.getClosestBlock(startKey);
  var offset = startBlock.getOffset(startKey);
  var o = offset + startOffset;
  change.deleteForwardAtRange(range, o, options);
};

/**
 * Delete forward until the word boundary at a `range`.
 *
 * @param {Change} change
 * @param {Selection} range
 * @param {Object} options
 *   @property {Boolean} normalize
 */

Changes.deleteWordForwardAtRange = function (change, range, options) {
  var state = change.state;
  var document = state.document;
  var startKey = range.startKey,
      startOffset = range.startOffset;

  var startBlock = document.getClosestBlock(startKey);
  var offset = startBlock.getOffset(startKey);
  var o = offset + startOffset;
  var text = startBlock.text;

  var n = _string2.default.getWordOffsetForward(text, o);
  change.deleteForwardAtRange(range, n, options);
};

/**
 * Delete forward `n` characters at a `range`.
 *
 * @param {Change} change
 * @param {Selection} range
 * @param {Number} n (optional)
 * @param {Object} options
 *   @property {Boolean} normalize
 */

Changes.deleteForwardAtRange = function (change, range) {
  var n = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 1;
  var options = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : {};
  var _options$normalize4 = options.normalize,
      normalize = _options$normalize4 === undefined ? true : _options$normalize4;
  var state = change.state;
  var document = state.document;
  var _range2 = range,
      startKey = _range2.startKey,
      focusOffset = _range2.focusOffset;

  // If the range is expanded, perform a regular delete instead.

  if (range.isExpanded) {
    change.deleteAtRange(range, { normalize: normalize });
    return;
  }

  var block = document.getClosestBlock(startKey);
  // If the closest block is void, delete it.
  if (block && block.isVoid) {
    change.removeNodeByKey(block.key, { normalize: normalize });
    return;
  }
  // If the closest is not void, but empty, remove it
  if (block && !block.isVoid && block.isEmpty && document.nodes.size !== 1) {
    change.removeNodeByKey(block.key, { normalize: normalize });
    return;
  }

  // If the closest inline is void, delete it.
  var inline = document.getClosestInline(startKey);
  if (inline && inline.isVoid) {
    change.removeNodeByKey(inline.key, { normalize: normalize });
    return;
  }

  // If the range is at the start of the document, abort.
  if (range.isAtEndOf(document)) {
    return;
  }

  // If the range is at the start of the text node, we need to figure out what
  // is behind it to know how to delete...
  var text = document.getDescendant(startKey);
  if (range.isAtEndOf(text)) {
    var next = document.getNextText(text.key);
    var nextBlock = document.getClosestBlock(next.key);
    var nextInline = document.getClosestInline(next.key);

    // If the previous block is void, remove it.
    if (nextBlock && nextBlock.isVoid) {
      change.removeNodeByKey(nextBlock.key, { normalize: normalize });
      return;
    }

    // If the previous inline is void, remove it.
    if (nextInline && nextInline.isVoid) {
      change.removeNodeByKey(nextInline.key, { normalize: normalize });
      return;
    }

    // If we're deleting by one character and the previous text node is not
    // inside the current block, we need to merge the two blocks together.
    if (n == 1 && nextBlock != block) {
      range = range.merge({
        focusKey: next.key,
        focusOffset: 0
      });

      change.deleteAtRange(range, { normalize: normalize });
      return;
    }
  }

  // If the remaining characters to the end of the node is greater than or equal
  // to the number of characters to delete, just remove the characters forwards
  // inside the current node.
  if (n <= text.text.length - focusOffset) {
    range = range.merge({
      focusOffset: focusOffset + n
    });

    change.deleteAtRange(range, { normalize: normalize });
    return;
  }

  // Otherwise, we need to see how many nodes forwards to go.
  var node = text;
  var offset = focusOffset;
  var traversed = text.text.length - focusOffset;

  while (n > traversed) {
    node = document.getNextText(node.key);
    var _next = traversed + node.text.length;
    if (n <= _next) {
      offset = n - traversed;
      break;
    } else {
      traversed = _next;
    }
  }

  // If the focus node is inside a void, go up until right before it.
  if (document.hasVoidParent(node.key)) {
    var parent = document.getClosestVoid(node.key);
    node = document.getPreviousText(parent.key);
    offset = node.text.length;
  }

  range = range.merge({
    focusKey: node.key,
    focusOffset: offset
  });

  change.deleteAtRange(range, { normalize: normalize });
};

/**
 * Insert a `block` node at `range`.
 *
 * @param {Change} change
 * @param {Selection} range
 * @param {Block|String|Object} block
 * @param {Object} options
 *   @property {Boolean} normalize
 */

Changes.insertBlockAtRange = function (change, range, block) {
  var options = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : {};

  block = _normalize2.default.block(block);
  var _options$normalize5 = options.normalize,
      normalize = _options$normalize5 === undefined ? true : _options$normalize5;


  if (range.isExpanded) {
    change.deleteAtRange(range);
    range = range.collapseToStart();
  }

  var state = change.state;
  var document = state.document;
  var _range3 = range,
      startKey = _range3.startKey,
      startOffset = _range3.startOffset;

  var startBlock = document.getClosestBlock(startKey);
  var parent = document.getParent(startBlock.key);
  var index = parent.nodes.indexOf(startBlock);

  if (startBlock.isVoid) {
    var extra = range.isAtEndOf(startBlock) ? 1 : 0;
    change.insertNodeByKey(parent.key, index + extra, block, { normalize: normalize });
  } else if (startBlock.isEmpty) {
    change.removeNodeByKey(startBlock.key);
    change.insertNodeByKey(parent.key, index, block, { normalize: normalize });
  } else if (range.isAtStartOf(startBlock)) {
    change.insertNodeByKey(parent.key, index, block, { normalize: normalize });
  } else if (range.isAtEndOf(startBlock)) {
    change.insertNodeByKey(parent.key, index + 1, block, { normalize: normalize });
  } else {
    change.splitDescendantsByKey(startBlock.key, startKey, startOffset, OPTS);
    change.insertNodeByKey(parent.key, index + 1, block, { normalize: normalize });
  }

  if (normalize) {
    change.normalizeNodeByKey(parent.key, _core2.default);
  }
};

/**
 * Insert a `fragment` at a `range`.
 *
 * @param {Change} change
 * @param {Selection} range
 * @param {Document} fragment
 * @param {Object} options
 *   @property {Boolean} normalize
 */

Changes.insertFragmentAtRange = function (change, range, fragment) {
  var options = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : {};
  var _options$normalize6 = options.normalize,
      normalize = _options$normalize6 === undefined ? true : _options$normalize6;

  // If the range is expanded, delete it first.

  if (range.isExpanded) {
    change.deleteAtRange(range, OPTS);
    range = range.collapseToStart();
  }

  // If the fragment is empty, there's nothing to do after deleting.
  if (!fragment.nodes.size) return;

  // Regenerate the keys for all of the fragments nodes, so that they're
  // guaranteed not to collide with the existing keys in the document. Otherwise
  // they will be rengerated automatically and we won't have an easy way to
  // reference them.
  fragment = fragment.mapDescendants(function (child) {
    return child.regenerateKey();
  });

  // Calculate a few things...
  var _range4 = range,
      startKey = _range4.startKey,
      startOffset = _range4.startOffset;
  var state = change.state;
  var _state2 = state,
      document = _state2.document;

  var startText = document.getDescendant(startKey);
  var startBlock = document.getClosestBlock(startText.key);
  var startChild = startBlock.getFurthestAncestor(startText.key);
  var isAtStart = range.isAtStartOf(startBlock);
  var parent = document.getParent(startBlock.key);
  var index = parent.nodes.indexOf(startBlock);
  var blocks = fragment.getBlocks();
  var firstBlock = blocks.first();
  var lastBlock = blocks.last();

  // If the fragment only contains a void block, use `insertBlock` instead.
  if (firstBlock == lastBlock && firstBlock.isVoid) {
    change.insertBlockAtRange(range, firstBlock, options);
    return;
  }

  // If the first and last block aren't the same, we need to insert all of the
  // nodes after the fragment's first block at the index.
  if (firstBlock != lastBlock) {
    var lonelyParent = fragment.getFurthest(firstBlock.key, function (p) {
      return p.nodes.size == 1;
    });
    var lonelyChild = lonelyParent || firstBlock;
    var startIndex = parent.nodes.indexOf(startBlock);
    fragment = fragment.removeDescendant(lonelyChild.key);

    fragment.nodes.forEach(function (node, i) {
      var newIndex = startIndex + i + 1;
      change.insertNodeByKey(parent.key, newIndex, node, OPTS);
    });
  }

  // Check if we need to split the node.
  if (startOffset != 0) {
    change.splitDescendantsByKey(startChild.key, startKey, startOffset, OPTS);
  }

  // Update our variables with the new state.
  state = change.state;
  document = state.document;
  startText = document.getDescendant(startKey);
  startBlock = document.getClosestBlock(startKey);
  startChild = startBlock.getFurthestAncestor(startText.key);

  // If the first and last block aren't the same, we need to move any of the
  // starting block's children after the split into the last block of the
  // fragment, which has already been inserted.
  if (firstBlock != lastBlock) {
    var nextChild = isAtStart ? startChild : startBlock.getNextSibling(startChild.key);
    var nextNodes = nextChild ? startBlock.nodes.skipUntil(function (n) {
      return n.key == nextChild.key;
    }) : (0, _immutable.List)();
    var lastIndex = lastBlock.nodes.size;

    nextNodes.forEach(function (node, i) {
      var newIndex = lastIndex + i;
      change.moveNodeByKey(node.key, lastBlock.key, newIndex, OPTS);
    });
  }

  // If the starting block is empty, we replace it entirely with the first block
  // of the fragment, since this leads to a more expected behavior for the user.
  if (startBlock.isEmpty) {
    change.removeNodeByKey(startBlock.key, OPTS);
    change.insertNodeByKey(parent.key, index, firstBlock, OPTS);
  }

  // Otherwise, we maintain the starting block, and insert all of the first
  // block's inline nodes into it at the split point.
  else {
      var inlineChild = startBlock.getFurthestAncestor(startText.key);
      var inlineIndex = startBlock.nodes.indexOf(inlineChild);

      firstBlock.nodes.forEach(function (inline, i) {
        var o = startOffset == 0 ? 0 : 1;
        var newIndex = inlineIndex + i + o;
        change.insertNodeByKey(startBlock.key, newIndex, inline, OPTS);
      });
    }

  // Normalize if requested.
  if (normalize) {
    change.normalizeNodeByKey(parent.key, _core2.default);
  }
};

/**
 * Insert an `inline` node at `range`.
 *
 * @param {Change} change
 * @param {Selection} range
 * @param {Inline|String|Object} inline
 * @param {Object} options
 *   @property {Boolean} normalize
 */

Changes.insertInlineAtRange = function (change, range, inline) {
  var options = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : {};
  var _options$normalize7 = options.normalize,
      normalize = _options$normalize7 === undefined ? true : _options$normalize7;

  inline = _normalize2.default.inline(inline);

  if (range.isExpanded) {
    change.deleteAtRange(range, OPTS);
    range = range.collapseToStart();
  }

  var state = change.state;
  var document = state.document;
  var _range5 = range,
      startKey = _range5.startKey,
      startOffset = _range5.startOffset;

  var parent = document.getParent(startKey);
  var startText = document.assertDescendant(startKey);
  var index = parent.nodes.indexOf(startText);

  if (parent.isVoid) return;

  change.splitNodeByKey(startKey, startOffset, OPTS);
  change.insertNodeByKey(parent.key, index + 1, inline, OPTS);

  if (normalize) {
    change.normalizeNodeByKey(parent.key, _core2.default);
  }
};

/**
 * Insert `text` at a `range`, with optional `marks`.
 *
 * @param {Change} change
 * @param {Selection} range
 * @param {String} text
 * @param {Set<Mark>} marks (optional)
 * @param {Object} options
 *   @property {Boolean} normalize
 */

Changes.insertTextAtRange = function (change, range, text, marks) {
  var options = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : {};
  var normalize = options.normalize;
  var state = change.state;
  var document = state.document;
  var startKey = range.startKey,
      startOffset = range.startOffset;

  var parent = document.getParent(startKey);

  if (parent.isVoid) return;

  if (range.isExpanded) {
    change.deleteAtRange(range, OPTS);
  }

  // PERF: Unless specified, don't normalize if only inserting text.
  if (normalize !== undefined) {
    normalize = range.isExpanded;
  }

  change.insertTextByKey(startKey, startOffset, text, marks, { normalize: normalize });
};

/**
 * Remove an existing `mark` to the characters at `range`.
 *
 * @param {Change} change
 * @param {Selection} range
 * @param {Mark|String} mark (optional)
 * @param {Object} options
 *   @property {Boolean} normalize
 */

Changes.removeMarkAtRange = function (change, range, mark) {
  var options = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : {};

  if (range.isCollapsed) return;

  var _options$normalize8 = options.normalize,
      normalize = _options$normalize8 === undefined ? true : _options$normalize8;
  var state = change.state;
  var document = state.document;

  var texts = document.getTextsAtRange(range);
  var startKey = range.startKey,
      startOffset = range.startOffset,
      endKey = range.endKey,
      endOffset = range.endOffset;


  texts.forEach(function (node) {
    var key = node.key;

    var index = 0;
    var length = node.text.length;

    if (key == startKey) index = startOffset;
    if (key == endKey) length = endOffset;
    if (key == startKey && key == endKey) length = endOffset - startOffset;

    change.removeMarkByKey(key, index, length, mark, { normalize: normalize });
  });
};

/**
 * Set the `properties` of block nodes in a `range`.
 *
 * @param {Change} change
 * @param {Selection} range
 * @param {Object|String} properties
 * @param {Object} options
 *   @property {Boolean} normalize
 */

Changes.setBlockAtRange = function (change, range, properties) {
  var options = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : {};
  var _options$normalize9 = options.normalize,
      normalize = _options$normalize9 === undefined ? true : _options$normalize9;
  var state = change.state;
  var document = state.document;

  var blocks = document.getBlocksAtRange(range);

  blocks.forEach(function (block) {
    change.setNodeByKey(block.key, properties, { normalize: normalize });
  });
};

/**
 * Set the `properties` of inline nodes in a `range`.
 *
 * @param {Change} change
 * @param {Selection} range
 * @param {Object|String} properties
 * @param {Object} options
 *   @property {Boolean} normalize
 */

Changes.setInlineAtRange = function (change, range, properties) {
  var options = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : {};
  var _options$normalize10 = options.normalize,
      normalize = _options$normalize10 === undefined ? true : _options$normalize10;
  var state = change.state;
  var document = state.document;

  var inlines = document.getInlinesAtRange(range);

  inlines.forEach(function (inline) {
    change.setNodeByKey(inline.key, properties, { normalize: normalize });
  });
};

/**
 * Split the block nodes at a `range`, to optional `height`.
 *
 * @param {Change} change
 * @param {Selection} range
 * @param {Number} height (optional)
 * @param {Object} options
 *   @property {Boolean} normalize
 */

Changes.splitBlockAtRange = function (change, range) {
  var height = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 1;
  var options = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : {};
  var _options$normalize11 = options.normalize,
      normalize = _options$normalize11 === undefined ? true : _options$normalize11;


  if (range.isExpanded) {
    change.deleteAtRange(range, { normalize: normalize });
    range = range.collapseToStart();
  }

  var _range6 = range,
      startKey = _range6.startKey,
      startOffset = _range6.startOffset;
  var state = change.state;
  var document = state.document;

  var node = document.assertDescendant(startKey);
  var parent = document.getClosestBlock(node.key);
  var h = 0;

  while (parent && parent.kind == 'block' && h < height) {
    node = parent;
    parent = document.getClosestBlock(parent.key);
    h++;
  }

  change.splitDescendantsByKey(node.key, startKey, startOffset, { normalize: normalize });
};

/**
 * Split the inline nodes at a `range`, to optional `height`.
 *
 * @param {Change} change
 * @param {Selection} range
 * @param {Number} height (optional)
 * @param {Object} options
 *   @property {Boolean} normalize
 */

Changes.splitInlineAtRange = function (change, range) {
  var height = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : Infinity;
  var options = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : {};
  var _options$normalize12 = options.normalize,
      normalize = _options$normalize12 === undefined ? true : _options$normalize12;


  if (range.isExpanded) {
    change.deleteAtRange(range, { normalize: normalize });
    range = range.collapseToStart();
  }

  var _range7 = range,
      startKey = _range7.startKey,
      startOffset = _range7.startOffset;
  var state = change.state;
  var document = state.document;

  var node = document.assertDescendant(startKey);
  var parent = document.getClosestInline(node.key);
  var h = 0;

  while (parent && parent.kind == 'inline' && h < height) {
    node = parent;
    parent = document.getClosestInline(parent.key);
    h++;
  }

  change.splitDescendantsByKey(node.key, startKey, startOffset, { normalize: normalize });
};

/**
 * Add or remove a `mark` from the characters at `range`, depending on whether
 * it's already there.
 *
 * @param {Change} change
 * @param {Selection} range
 * @param {Mixed} mark
 * @param {Object} options
 *   @property {Boolean} normalize
 */

Changes.toggleMarkAtRange = function (change, range, mark) {
  var options = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : {};

  if (range.isCollapsed) return;

  mark = _normalize2.default.mark(mark);

  var _options$normalize13 = options.normalize,
      normalize = _options$normalize13 === undefined ? true : _options$normalize13;
  var state = change.state;
  var document = state.document;

  var marks = document.getMarksAtRange(range);
  var exists = marks.some(function (m) {
    return m.equals(mark);
  });

  if (exists) {
    change.removeMarkAtRange(range, mark, { normalize: normalize });
  } else {
    change.addMarkAtRange(range, mark, { normalize: normalize });
  }
};

/**
 * Unwrap all of the block nodes in a `range` from a block with `properties`.
 *
 * @param {Change} change
 * @param {Selection} range
 * @param {String|Object} properties
 * @param {Object} options
 *   @property {Boolean} normalize
 */

Changes.unwrapBlockAtRange = function (change, range, properties) {
  var options = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : {};

  properties = _normalize2.default.nodeProperties(properties);

  var _options$normalize14 = options.normalize,
      normalize = _options$normalize14 === undefined ? true : _options$normalize14;
  var state = change.state;
  var _state3 = state,
      document = _state3.document;

  var blocks = document.getBlocksAtRange(range);
  var wrappers = blocks.map(function (block) {
    return document.getClosest(block.key, function (parent) {
      if (parent.kind != 'block') return false;
      if (properties.type != null && parent.type != properties.type) return false;
      if (properties.isVoid != null && parent.isVoid != properties.isVoid) return false;
      if (properties.data != null && !parent.data.isSuperset(properties.data)) return false;
      return true;
    });
  }).filter(function (exists) {
    return exists;
  }).toOrderedSet().toList();

  wrappers.forEach(function (block) {
    var first = block.nodes.first();
    var last = block.nodes.last();
    var parent = document.getParent(block.key);
    var index = parent.nodes.indexOf(block);

    var children = block.nodes.filter(function (child) {
      return blocks.some(function (b) {
        return child == b || child.hasDescendant(b.key);
      });
    });

    var firstMatch = children.first();
    var lastMatch = children.last();

    if (first == firstMatch && last == lastMatch) {
      block.nodes.forEach(function (child, i) {
        change.moveNodeByKey(child.key, parent.key, index + i, OPTS);
      });

      change.removeNodeByKey(block.key, OPTS);
    } else if (last == lastMatch) {
      block.nodes.skipUntil(function (n) {
        return n == firstMatch;
      }).forEach(function (child, i) {
        change.moveNodeByKey(child.key, parent.key, index + 1 + i, OPTS);
      });
    } else if (first == firstMatch) {
      block.nodes.takeUntil(function (n) {
        return n == lastMatch;
      }).push(lastMatch).forEach(function (child, i) {
        change.moveNodeByKey(child.key, parent.key, index + i, OPTS);
      });
    } else {
      var firstText = firstMatch.getFirstText();
      change.splitDescendantsByKey(block.key, firstText.key, 0, OPTS);
      state = change.state;
      document = state.document;

      children.forEach(function (child, i) {
        if (i == 0) {
          var extra = child;
          child = document.getNextBlock(child.key);
          change.removeNodeByKey(extra.key, OPTS);
        }

        change.moveNodeByKey(child.key, parent.key, index + 1 + i, OPTS);
      });
    }
  });

  // TODO: optmize to only normalize the right block
  if (normalize) {
    change.normalizeDocument(_core2.default);
  }
};

/**
 * Unwrap the inline nodes in a `range` from an inline with `properties`.
 *
 * @param {Change} change
 * @param {Selection} range
 * @param {String|Object} properties
 * @param {Object} options
 *   @property {Boolean} normalize
 */

Changes.unwrapInlineAtRange = function (change, range, properties) {
  var options = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : {};

  properties = _normalize2.default.nodeProperties(properties);

  var _options$normalize15 = options.normalize,
      normalize = _options$normalize15 === undefined ? true : _options$normalize15;
  var state = change.state;
  var document = state.document;

  var texts = document.getTextsAtRange(range);
  var inlines = texts.map(function (text) {
    return document.getClosest(text.key, function (parent) {
      if (parent.kind != 'inline') return false;
      if (properties.type != null && parent.type != properties.type) return false;
      if (properties.isVoid != null && parent.isVoid != properties.isVoid) return false;
      if (properties.data != null && !parent.data.isSuperset(properties.data)) return false;
      return true;
    });
  }).filter(function (exists) {
    return exists;
  }).toOrderedSet().toList();

  inlines.forEach(function (inline) {
    var parent = change.state.document.getParent(inline.key);
    var index = parent.nodes.indexOf(inline);

    inline.nodes.forEach(function (child, i) {
      change.moveNodeByKey(child.key, parent.key, index + i, OPTS);
    });
  });

  // TODO: optmize to only normalize the right block
  if (normalize) {
    change.normalizeDocument(_core2.default);
  }
};

/**
 * Wrap all of the blocks in a `range` in a new `block`.
 *
 * @param {Change} change
 * @param {Selection} range
 * @param {Block|Object|String} block
 * @param {Object} options
 *   @property {Boolean} normalize
 */

Changes.wrapBlockAtRange = function (change, range, block) {
  var options = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : {};

  block = _normalize2.default.block(block);
  block = block.set('nodes', block.nodes.clear());

  var _options$normalize16 = options.normalize,
      normalize = _options$normalize16 === undefined ? true : _options$normalize16;
  var state = change.state;
  var document = state.document;


  var blocks = document.getBlocksAtRange(range);
  var firstblock = blocks.first();
  var lastblock = blocks.last();
  var parent = void 0,
      siblings = void 0,
      index = void 0;

  // If there is only one block in the selection then we know the parent and
  // siblings.
  if (blocks.length === 1) {
    parent = document.getParent(firstblock.key);
    siblings = blocks;
  }

  // Determine closest shared parent to all blocks in selection.
  else {
      parent = document.getClosest(firstblock.key, function (p1) {
        return !!document.getClosest(lastblock.key, function (p2) {
          return p1 == p2;
        });
      });
    }

  // If no shared parent could be found then the parent is the document.
  if (parent == null) parent = document;

  // Create a list of direct children siblings of parent that fall in the
  // selection.
  if (siblings == null) {
    var indexes = parent.nodes.reduce(function (ind, node, i) {
      if (node == firstblock || node.hasDescendant(firstblock.key)) ind[0] = i;
      if (node == lastblock || node.hasDescendant(lastblock.key)) ind[1] = i;
      return ind;
    }, []);

    index = indexes[0];
    siblings = parent.nodes.slice(indexes[0], indexes[1] + 1);
  }

  // Get the index to place the new wrapped node at.
  if (index == null) {
    index = parent.nodes.indexOf(siblings.first());
  }

  // Inject the new block node into the parent.
  change.insertNodeByKey(parent.key, index, block, OPTS);

  // Move the sibling nodes into the new block node.
  siblings.forEach(function (node, i) {
    change.moveNodeByKey(node.key, block.key, i, OPTS);
  });

  if (normalize) {
    change.normalizeNodeByKey(parent.key, _core2.default);
  }
};

/**
 * Wrap the text and inlines in a `range` in a new `inline`.
 *
 * @param {Change} change
 * @param {Selection} range
 * @param {Inline|Object|String} inline
 * @param {Object} options
 *   @property {Boolean} normalize
 */

Changes.wrapInlineAtRange = function (change, range, inline) {
  var options = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : {};
  var state = change.state;
  var _state4 = state,
      document = _state4.document;
  var _options$normalize17 = options.normalize,
      normalize = _options$normalize17 === undefined ? true : _options$normalize17;
  var startKey = range.startKey,
      startOffset = range.startOffset,
      endKey = range.endKey,
      endOffset = range.endOffset;


  if (range.isCollapsed) {
    // Wrapping an inline void
    var inlineParent = document.getClosestInline(startKey);
    if (!inlineParent.isVoid) {
      return;
    }

    return change.wrapInlineByKey(inlineParent.key, inline, options);
  }

  inline = _normalize2.default.inline(inline);
  inline = inline.set('nodes', inline.nodes.clear());

  var blocks = document.getBlocksAtRange(range);
  var startBlock = document.getClosestBlock(startKey);
  var endBlock = document.getClosestBlock(endKey);
  var startChild = startBlock.getFurthestAncestor(startKey);
  var endChild = endBlock.getFurthestAncestor(endKey);

  change.splitDescendantsByKey(endChild.key, endKey, endOffset, OPTS);
  change.splitDescendantsByKey(startChild.key, startKey, startOffset, OPTS);

  state = change.state;
  document = state.document;
  startBlock = document.getDescendant(startBlock.key);
  endBlock = document.getDescendant(endBlock.key);
  startChild = startBlock.getFurthestAncestor(startKey);
  endChild = endBlock.getFurthestAncestor(endKey);
  var startIndex = startBlock.nodes.indexOf(startChild);
  var endIndex = endBlock.nodes.indexOf(endChild);

  if (startBlock == endBlock) {
    state = change.state;
    document = state.document;
    startBlock = document.getClosestBlock(startKey);
    startChild = startBlock.getFurthestAncestor(startKey);

    var startInner = document.getNextSibling(startChild.key);
    var startInnerIndex = startBlock.nodes.indexOf(startInner);
    var endInner = startKey == endKey ? startInner : startBlock.getFurthestAncestor(endKey);
    var inlines = startBlock.nodes.skipUntil(function (n) {
      return n == startInner;
    }).takeUntil(function (n) {
      return n == endInner;
    }).push(endInner);

    var node = inline.regenerateKey();

    change.insertNodeByKey(startBlock.key, startInnerIndex, node, OPTS);

    inlines.forEach(function (child, i) {
      change.moveNodeByKey(child.key, node.key, i, OPTS);
    });

    if (normalize) {
      change.normalizeNodeByKey(startBlock.key, _core2.default);
    }
  } else {
    var startInlines = startBlock.nodes.slice(startIndex + 1);
    var endInlines = endBlock.nodes.slice(0, endIndex + 1);
    var startNode = inline.regenerateKey();
    var endNode = inline.regenerateKey();

    change.insertNodeByKey(startBlock.key, startIndex - 1, startNode, OPTS);
    change.insertNodeByKey(endBlock.key, endIndex, endNode, OPTS);

    startInlines.forEach(function (child, i) {
      change.moveNodeByKey(child.key, startNode.key, i, OPTS);
    });

    endInlines.forEach(function (child, i) {
      change.moveNodeByKey(child.key, endNode.key, i, OPTS);
    });

    if (normalize) {
      change.normalizeNodeByKey(startBlock.key, _core2.default).normalizeNodeByKey(endBlock.key, _core2.default);
    }

    blocks.slice(1, -1).forEach(function (block) {
      var node = inline.regenerateKey();
      change.insertNodeByKey(block.key, 0, node, OPTS);

      block.nodes.forEach(function (child, i) {
        change.moveNodeByKey(child.key, node.key, i, OPTS);
      });

      if (normalize) {
        change.normalizeNodeByKey(block.key, _core2.default);
      }
    });
  }
};

/**
 * Wrap the text in a `range` in a prefix/suffix.
 *
 * @param {Change} change
 * @param {Selection} range
 * @param {String} prefix
 * @param {String} suffix (optional)
 * @param {Object} options
 *   @property {Boolean} normalize
 */

Changes.wrapTextAtRange = function (change, range, prefix) {
  var suffix = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : prefix;
  var options = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : {};
  var _options$normalize18 = options.normalize,
      normalize = _options$normalize18 === undefined ? true : _options$normalize18;
  var startKey = range.startKey,
      endKey = range.endKey;

  var start = range.collapseToStart();
  var end = range.collapseToEnd();

  if (startKey == endKey) {
    end = end.move(prefix.length);
  }

  change.insertTextAtRange(start, prefix, [], { normalize: normalize });
  change.insertTextAtRange(end, suffix, [], { normalize: normalize });
};

/**
 * Export.
 *
 * @type {Object}
 */

exports.default = Changes;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9jaGFuZ2VzL2F0LXJhbmdlLmpzIl0sIm5hbWVzIjpbIkNoYW5nZXMiLCJPUFRTIiwibm9ybWFsaXplIiwiYWRkTWFya0F0UmFuZ2UiLCJjaGFuZ2UiLCJyYW5nZSIsIm1hcmsiLCJvcHRpb25zIiwiaXNDb2xsYXBzZWQiLCJzdGF0ZSIsImRvY3VtZW50Iiwic3RhcnRLZXkiLCJzdGFydE9mZnNldCIsImVuZEtleSIsImVuZE9mZnNldCIsInRleHRzIiwiZ2V0VGV4dHNBdFJhbmdlIiwiZm9yRWFjaCIsIm5vZGUiLCJrZXkiLCJpbmRleCIsImxlbmd0aCIsInRleHQiLCJhZGRNYXJrQnlLZXkiLCJkZWxldGVBdFJhbmdlIiwic25hcHNob3RTZWxlY3Rpb24iLCJhbmNlc3RvciIsImdldENvbW1vbkFuY2VzdG9yIiwic3RhcnRDaGlsZCIsImdldEZ1cnRoZXN0QW5jZXN0b3IiLCJlbmRDaGlsZCIsInN0YXJ0Q2hpbGRJbmNsdWRlc1ZvaWQiLCJpc1ZvaWQiLCJhbmNob3JPZmZzZXQiLCJpc0JhY2t3YXJkIiwiZm9jdXNPZmZzZXQiLCJuZXh0U2libGluZyIsImdldE5leHRTaWJsaW5nIiwicmVtb3ZlTm9kZUJ5S2V5IiwiZ2V0VGV4dHMiLCJmaXJzdCIsInN0YXJ0Q2hpbGRFbmRPZlZvaWQiLCJub2RlcyIsInNpemUiLCJkZXNlbGVjdCIsInJlbW92ZVRleHRCeUtleSIsImtpbmQiLCJzcGxpdE5vZGVCeUtleSIsInNwbGl0RGVzY2VuZGFudHNCeUtleSIsInN0YXJ0SW5kZXgiLCJpbmRleE9mIiwiZW5kSW5kZXgiLCJtaWRkbGVzIiwic2xpY2UiLCJuZXh0IiwiZ2V0TmV4dFRleHQiLCJjaGlsZCIsInN0YXJ0QmxvY2siLCJnZXRDbG9zZXN0QmxvY2siLCJlbmRCbG9jayIsImkiLCJuZXdLZXkiLCJuZXdJbmRleCIsIm1vdmVOb2RlQnlLZXkiLCJsb25lbHkiLCJnZXRGdXJ0aGVzdE9ubHlDaGlsZEFuY2VzdG9yIiwibm9ybWFsaXplTm9kZUJ5S2V5IiwiZGVsZXRlQ2hhckJhY2t3YXJkQXRSYW5nZSIsIm9mZnNldCIsImdldE9mZnNldCIsIm8iLCJuIiwiZ2V0Q2hhck9mZnNldEJhY2t3YXJkIiwiZGVsZXRlQmFja3dhcmRBdFJhbmdlIiwiZGVsZXRlTGluZUJhY2t3YXJkQXRSYW5nZSIsImRlbGV0ZVdvcmRCYWNrd2FyZEF0UmFuZ2UiLCJnZXRXb3JkT2Zmc2V0QmFja3dhcmQiLCJpc0V4cGFuZGVkIiwiYmxvY2siLCJpc0VtcHR5IiwiaW5saW5lIiwiZ2V0Q2xvc2VzdElubGluZSIsImlzQXRTdGFydE9mIiwiZ2V0RGVzY2VuZGFudCIsInByZXYiLCJnZXRQcmV2aW91c1RleHQiLCJwcmV2QmxvY2siLCJwcmV2SW5saW5lIiwibWVyZ2UiLCJhbmNob3JLZXkiLCJ0cmF2ZXJzZWQiLCJoYXNWb2lkUGFyZW50IiwicGFyZW50IiwiZ2V0Q2xvc2VzdFZvaWQiLCJmb2N1c0tleSIsImRlbGV0ZUNoYXJGb3J3YXJkQXRSYW5nZSIsImdldENoYXJPZmZzZXRGb3J3YXJkIiwiZGVsZXRlRm9yd2FyZEF0UmFuZ2UiLCJkZWxldGVMaW5lRm9yd2FyZEF0UmFuZ2UiLCJkZWxldGVXb3JkRm9yd2FyZEF0UmFuZ2UiLCJnZXRXb3JkT2Zmc2V0Rm9yd2FyZCIsImlzQXRFbmRPZiIsIm5leHRCbG9jayIsIm5leHRJbmxpbmUiLCJpbnNlcnRCbG9ja0F0UmFuZ2UiLCJjb2xsYXBzZVRvU3RhcnQiLCJnZXRQYXJlbnQiLCJleHRyYSIsImluc2VydE5vZGVCeUtleSIsImluc2VydEZyYWdtZW50QXRSYW5nZSIsImZyYWdtZW50IiwibWFwRGVzY2VuZGFudHMiLCJyZWdlbmVyYXRlS2V5Iiwic3RhcnRUZXh0IiwiaXNBdFN0YXJ0IiwiYmxvY2tzIiwiZ2V0QmxvY2tzIiwiZmlyc3RCbG9jayIsImxhc3RCbG9jayIsImxhc3QiLCJsb25lbHlQYXJlbnQiLCJnZXRGdXJ0aGVzdCIsInAiLCJsb25lbHlDaGlsZCIsInJlbW92ZURlc2NlbmRhbnQiLCJuZXh0Q2hpbGQiLCJuZXh0Tm9kZXMiLCJza2lwVW50aWwiLCJsYXN0SW5kZXgiLCJpbmxpbmVDaGlsZCIsImlubGluZUluZGV4IiwiaW5zZXJ0SW5saW5lQXRSYW5nZSIsImFzc2VydERlc2NlbmRhbnQiLCJpbnNlcnRUZXh0QXRSYW5nZSIsIm1hcmtzIiwidW5kZWZpbmVkIiwiaW5zZXJ0VGV4dEJ5S2V5IiwicmVtb3ZlTWFya0F0UmFuZ2UiLCJyZW1vdmVNYXJrQnlLZXkiLCJzZXRCbG9ja0F0UmFuZ2UiLCJwcm9wZXJ0aWVzIiwiZ2V0QmxvY2tzQXRSYW5nZSIsInNldE5vZGVCeUtleSIsInNldElubGluZUF0UmFuZ2UiLCJpbmxpbmVzIiwiZ2V0SW5saW5lc0F0UmFuZ2UiLCJzcGxpdEJsb2NrQXRSYW5nZSIsImhlaWdodCIsImgiLCJzcGxpdElubGluZUF0UmFuZ2UiLCJJbmZpbml0eSIsInRvZ2dsZU1hcmtBdFJhbmdlIiwiZ2V0TWFya3NBdFJhbmdlIiwiZXhpc3RzIiwic29tZSIsIm0iLCJlcXVhbHMiLCJ1bndyYXBCbG9ja0F0UmFuZ2UiLCJub2RlUHJvcGVydGllcyIsIndyYXBwZXJzIiwibWFwIiwiZ2V0Q2xvc2VzdCIsInR5cGUiLCJkYXRhIiwiaXNTdXBlcnNldCIsImZpbHRlciIsInRvT3JkZXJlZFNldCIsInRvTGlzdCIsImNoaWxkcmVuIiwiYiIsImhhc0Rlc2NlbmRhbnQiLCJmaXJzdE1hdGNoIiwibGFzdE1hdGNoIiwidGFrZVVudGlsIiwicHVzaCIsImZpcnN0VGV4dCIsImdldEZpcnN0VGV4dCIsImdldE5leHRCbG9jayIsIm5vcm1hbGl6ZURvY3VtZW50IiwidW53cmFwSW5saW5lQXRSYW5nZSIsIndyYXBCbG9ja0F0UmFuZ2UiLCJzZXQiLCJjbGVhciIsImZpcnN0YmxvY2siLCJsYXN0YmxvY2siLCJzaWJsaW5ncyIsInAxIiwicDIiLCJpbmRleGVzIiwicmVkdWNlIiwiaW5kIiwid3JhcElubGluZUF0UmFuZ2UiLCJpbmxpbmVQYXJlbnQiLCJ3cmFwSW5saW5lQnlLZXkiLCJzdGFydElubmVyIiwic3RhcnRJbm5lckluZGV4IiwiZW5kSW5uZXIiLCJzdGFydElubGluZXMiLCJlbmRJbmxpbmVzIiwic3RhcnROb2RlIiwiZW5kTm9kZSIsIndyYXBUZXh0QXRSYW5nZSIsInByZWZpeCIsInN1ZmZpeCIsInN0YXJ0IiwiZW5kIiwiY29sbGFwc2VUb0VuZCIsIm1vdmUiXSwibWFwcGluZ3MiOiI7Ozs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUNBOzs7O0FBRUE7Ozs7OztBQU1BLElBQU1BLFVBQVUsRUFBaEI7O0FBRUE7Ozs7OztBQU1BLElBQU1DLE9BQU87QUFDWEMsYUFBVztBQURBLENBQWI7O0FBSUE7Ozs7Ozs7Ozs7QUFVQUYsUUFBUUcsY0FBUixHQUF5QixVQUFDQyxNQUFELEVBQVNDLEtBQVQsRUFBZ0JDLElBQWhCLEVBQXVDO0FBQUEsTUFBakJDLE9BQWlCLHVFQUFQLEVBQU87O0FBQzlELE1BQUlGLE1BQU1HLFdBQVYsRUFBdUI7O0FBRHVDLDJCQUdqQ0QsT0FIaUMsQ0FHdERMLFNBSHNEO0FBQUEsTUFHdERBLFNBSHNELHNDQUcxQyxJQUgwQztBQUFBLE1BSXRETyxLQUpzRCxHQUk1Q0wsTUFKNEMsQ0FJdERLLEtBSnNEO0FBQUEsTUFLdERDLFFBTHNELEdBS3pDRCxLQUx5QyxDQUt0REMsUUFMc0Q7QUFBQSxNQU10REMsUUFOc0QsR0FNVE4sS0FOUyxDQU10RE0sUUFOc0Q7QUFBQSxNQU01Q0MsV0FONEMsR0FNVFAsS0FOUyxDQU01Q08sV0FONEM7QUFBQSxNQU0vQkMsTUFOK0IsR0FNVFIsS0FOUyxDQU0vQlEsTUFOK0I7QUFBQSxNQU12QkMsU0FOdUIsR0FNVFQsS0FOUyxDQU12QlMsU0FOdUI7O0FBTzlELE1BQU1DLFFBQVFMLFNBQVNNLGVBQVQsQ0FBeUJYLEtBQXpCLENBQWQ7O0FBRUFVLFFBQU1FLE9BQU4sQ0FBYyxVQUFDQyxJQUFELEVBQVU7QUFBQSxRQUNkQyxHQURjLEdBQ05ELElBRE0sQ0FDZEMsR0FEYzs7QUFFdEIsUUFBSUMsUUFBUSxDQUFaO0FBQ0EsUUFBSUMsU0FBU0gsS0FBS0ksSUFBTCxDQUFVRCxNQUF2Qjs7QUFFQSxRQUFJRixPQUFPUixRQUFYLEVBQXFCUyxRQUFRUixXQUFSO0FBQ3JCLFFBQUlPLE9BQU9OLE1BQVgsRUFBbUJRLFNBQVNQLFNBQVQ7QUFDbkIsUUFBSUssT0FBT1IsUUFBUCxJQUFtQlEsT0FBT04sTUFBOUIsRUFBc0NRLFNBQVNQLFlBQVlGLFdBQXJCOztBQUV0Q1IsV0FBT21CLFlBQVAsQ0FBb0JKLEdBQXBCLEVBQXlCQyxLQUF6QixFQUFnQ0MsTUFBaEMsRUFBd0NmLElBQXhDLEVBQThDLEVBQUVKLG9CQUFGLEVBQTlDO0FBQ0QsR0FWRDtBQVdELENBcEJEOztBQXNCQTs7Ozs7Ozs7O0FBU0FGLFFBQVF3QixhQUFSLEdBQXdCLFVBQUNwQixNQUFELEVBQVNDLEtBQVQsRUFBaUM7QUFBQSxNQUFqQkUsT0FBaUIsdUVBQVAsRUFBTzs7QUFDdkQsTUFBSUYsTUFBTUcsV0FBVixFQUF1Qjs7QUFFdkJKLFNBQU9xQixpQkFBUDs7QUFIdUQsNEJBSzFCbEIsT0FMMEIsQ0FLL0NMLFNBTCtDO0FBQUEsTUFLL0NBLFNBTCtDLHVDQUtuQyxJQUxtQztBQUFBLE1BTWpEUyxRQU5pRCxHQU1KTixLQU5JLENBTWpETSxRQU5pRDtBQUFBLE1BTXZDQyxXQU51QyxHQU1KUCxLQU5JLENBTXZDTyxXQU51QztBQUFBLE1BTTFCQyxNQU4wQixHQU1KUixLQU5JLENBTTFCUSxNQU4wQjtBQUFBLE1BTWxCQyxTQU5rQixHQU1KVCxLQU5JLENBTWxCUyxTQU5rQjs7QUFRdkQ7O0FBUnVELE1BU2pETCxLQVRpRCxHQVN2Q0wsTUFUdUMsQ0FTakRLLEtBVGlEO0FBQUEsZUFVcENBLEtBVm9DO0FBQUEsTUFVakRDLFFBVmlELFVBVWpEQSxRQVZpRDs7QUFXdkQsTUFBSWdCLFdBQVdoQixTQUFTaUIsaUJBQVQsQ0FBMkJoQixRQUEzQixFQUFxQ0UsTUFBckMsQ0FBZjtBQUNBLE1BQUllLGFBQWFGLFNBQVNHLG1CQUFULENBQTZCbEIsUUFBN0IsQ0FBakI7QUFDQSxNQUFJbUIsV0FBV0osU0FBU0csbUJBQVQsQ0FBNkJoQixNQUE3QixDQUFmOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsTUFBSWtCLHlCQUF5QkgsV0FBV0ksTUFBWCxLQUMzQjNCLE1BQU00QixZQUFOLEtBQXVCLENBQXZCLElBQTRCLENBQUM1QixNQUFNNkIsVUFBbkMsSUFDQTdCLE1BQU04QixXQUFOLEtBQXNCLENBQXRCLElBQTJCOUIsTUFBTTZCLFVBRk4sQ0FBN0I7O0FBS0EsU0FBT0gsc0JBQVAsRUFBK0I7QUFDN0IsUUFBTUssY0FBYzFCLFNBQVMyQixjQUFULENBQXdCVCxXQUFXVCxHQUFuQyxDQUFwQjtBQUNBZixXQUFPa0MsZUFBUCxDQUF1QlYsV0FBV1QsR0FBbEMsRUFBdUNsQixJQUF2QztBQUNBO0FBQ0EsUUFBSSxDQUFDbUMsV0FBRCxJQUFnQk4sU0FBU1gsR0FBVCxLQUFpQmlCLFlBQVlqQixHQUE3QixJQUFvQ2lCLFlBQVlKLE1BQXBFLEVBQTRFO0FBQzFFRCwrQkFBeUIsS0FBekI7QUFDQTtBQUNEO0FBQ0Q7QUFDQSxRQUFJSyxZQUFZSixNQUFoQixFQUF3QjtBQUN0QkosbUJBQWFRLFdBQWI7QUFDRDtBQUNEO0FBQ0EsUUFBSSxDQUFDQSxZQUFZSixNQUFqQixFQUF5QjtBQUN2QkosbUJBQWFRLFdBQWI7QUFDQSxVQUFJUixXQUFXVyxRQUFmLEVBQXlCO0FBQ3ZCNUIsbUJBQVdpQixXQUFXVyxRQUFYLEdBQXNCQyxLQUF0QixHQUE4QnJCLEdBQXpDO0FBQ0QsT0FGRCxNQUVPO0FBQ0xSLG1CQUFXaUIsV0FBV1QsR0FBdEI7QUFDRDtBQUNEUCxvQkFBYyxDQUFkO0FBQ0FtQiwrQkFBeUIsS0FBekI7QUFDRDtBQUNGOztBQUVEO0FBQ0E7QUFDQSxNQUFNVSxzQkFBc0JiLFdBQVdJLE1BQVgsS0FDMUIzQixNQUFNNEIsWUFBTixLQUF1QixDQUF2QixJQUE0QixDQUFDNUIsTUFBTTZCLFVBQW5DLElBQ0E3QixNQUFNOEIsV0FBTixLQUFzQixDQUF0QixJQUEyQjlCLE1BQU02QixVQUZQLENBQTVCOztBQUtBLE1BQUlPLG1CQUFKLEVBQXlCO0FBQ3ZCLFFBQU1MLGVBQWMxQixTQUFTMkIsY0FBVCxDQUF3QlQsV0FBV1QsR0FBbkMsQ0FBcEI7QUFDQSxRQUFJaUIsWUFBSixFQUFpQjtBQUNmUixtQkFBYVEsWUFBYjtBQUNBLFVBQUlSLFdBQVdXLFFBQWYsRUFBeUI7QUFDdkI1QixtQkFBV2lCLFdBQVdXLFFBQVgsR0FBc0JDLEtBQXRCLEdBQThCckIsR0FBekM7QUFDRCxPQUZELE1BRU87QUFDTFIsbUJBQVdpQixXQUFXVCxHQUF0QjtBQUNEO0FBQ0RQLG9CQUFjLENBQWQ7QUFDRDtBQUNGOztBQUVEO0FBQ0EsTUFBSUQsWUFBWUUsTUFBaEIsRUFBd0I7QUFDdEI7QUFDQSxRQUFJYSxTQUFTTSxNQUFiLEVBQXFCO0FBQ25CO0FBQ0EsVUFBSXRCLFNBQVNnQyxLQUFULENBQWVDLElBQWYsS0FBd0IsQ0FBNUIsRUFBK0I7QUFDN0J2QyxlQUFPd0MsUUFBUDtBQUNEO0FBQ0R4QyxhQUFPa0MsZUFBUCxDQUF1QlosU0FBU1AsR0FBaEMsRUFBcUNsQixJQUFyQztBQUNBO0FBQ0Q7QUFDRDtBQUNBLFFBQU1tQixRQUFRUixXQUFkO0FBQ0EsUUFBTVMsU0FBU1AsWUFBWUYsV0FBM0I7QUFDQVIsV0FBT3lDLGVBQVAsQ0FBdUJsQyxRQUF2QixFQUFpQ1MsS0FBakMsRUFBd0NDLE1BQXhDLEVBQWdELEVBQUVuQixvQkFBRixFQUFoRDtBQUNBO0FBQ0Q7O0FBRUQ7QUFDQU8sVUFBUUwsT0FBT0ssS0FBZjtBQUNBQyxhQUFXRCxNQUFNQyxRQUFqQjtBQUNBZ0IsYUFBV2hCLFNBQVNpQixpQkFBVCxDQUEyQmhCLFFBQTNCLEVBQXFDRSxNQUFyQyxDQUFYO0FBQ0FlLGVBQWFGLFNBQVNHLG1CQUFULENBQTZCbEIsUUFBN0IsQ0FBYjtBQUNBbUIsYUFBV0osU0FBU0csbUJBQVQsQ0FBNkJoQixNQUE3QixDQUFYOztBQUVBLE1BQUllLFdBQVdrQixJQUFYLElBQW1CLE1BQXZCLEVBQStCO0FBQzdCMUMsV0FBTzJDLGNBQVAsQ0FBc0JuQixXQUFXVCxHQUFqQyxFQUFzQ1AsV0FBdEMsRUFBbURYLElBQW5EO0FBQ0QsR0FGRCxNQUVPO0FBQ0xHLFdBQU80QyxxQkFBUCxDQUE2QnBCLFdBQVdULEdBQXhDLEVBQTZDUixRQUE3QyxFQUF1REMsV0FBdkQsRUFBb0VYLElBQXBFO0FBQ0Q7O0FBRUQsTUFBSTZCLFNBQVNnQixJQUFULElBQWlCLE1BQXJCLEVBQTZCO0FBQzNCMUMsV0FBTzJDLGNBQVAsQ0FBc0JqQixTQUFTWCxHQUEvQixFQUFvQ0wsU0FBcEMsRUFBK0NiLElBQS9DO0FBQ0QsR0FGRCxNQUVPO0FBQ0xHLFdBQU80QyxxQkFBUCxDQUE2QmxCLFNBQVNYLEdBQXRDLEVBQTJDTixNQUEzQyxFQUFtREMsU0FBbkQsRUFBOERiLElBQTlEO0FBQ0Q7O0FBRUQ7QUFDQVEsVUFBUUwsT0FBT0ssS0FBZjtBQUNBQyxhQUFXRCxNQUFNQyxRQUFqQjtBQUNBZ0IsYUFBV2hCLFNBQVNpQixpQkFBVCxDQUEyQmhCLFFBQTNCLEVBQXFDRSxNQUFyQyxDQUFYO0FBQ0FlLGVBQWFGLFNBQVNHLG1CQUFULENBQTZCbEIsUUFBN0IsQ0FBYjtBQUNBbUIsYUFBV0osU0FBU0csbUJBQVQsQ0FBNkJoQixNQUE3QixDQUFYO0FBQ0EsTUFBTW9DLGFBQWF2QixTQUFTZ0IsS0FBVCxDQUFlUSxPQUFmLENBQXVCdEIsVUFBdkIsQ0FBbkI7QUFDQSxNQUFNdUIsV0FBV3pCLFNBQVNnQixLQUFULENBQWVRLE9BQWYsQ0FBdUJwQixRQUF2QixDQUFqQjtBQUNBLE1BQU1zQixVQUFVMUIsU0FBU2dCLEtBQVQsQ0FBZVcsS0FBZixDQUFxQkosYUFBYSxDQUFsQyxFQUFxQ0UsV0FBVyxDQUFoRCxDQUFoQjtBQUNBLE1BQU1HLE9BQU81QyxTQUFTNkMsV0FBVCxDQUFxQjFDLE1BQXJCLENBQWI7O0FBRUE7QUFDQSxNQUFJdUMsUUFBUVQsSUFBWixFQUFrQjtBQUNoQlMsWUFBUW5DLE9BQVIsQ0FBZ0IsVUFBQ3VDLEtBQUQsRUFBVztBQUN6QnBELGFBQU9rQyxlQUFQLENBQXVCa0IsTUFBTXJDLEdBQTdCLEVBQWtDbEIsSUFBbEM7QUFDRCxLQUZEO0FBR0Q7O0FBRUQ7QUFDQTtBQUNBUSxVQUFRTCxPQUFPSyxLQUFmO0FBQ0FDLGFBQVdELE1BQU1DLFFBQWpCO0FBQ0EsTUFBTStDLGFBQWEvQyxTQUFTZ0QsZUFBVCxDQUF5Qi9DLFFBQXpCLENBQW5CO0FBQ0EsTUFBTWdELFdBQVdqRCxTQUFTZ0QsZUFBVCxDQUF5QkosS0FBS25DLEdBQTlCLENBQWpCOztBQUVBO0FBQ0EsTUFBSXdDLFNBQVMzQixNQUFiLEVBQXFCO0FBQ25CNUIsV0FBT2tDLGVBQVAsQ0FBdUJtQixXQUFXdEMsR0FBbEM7QUFDQTtBQUNEOztBQUVEO0FBQ0E7QUFDQSxNQUFJc0MsV0FBV3RDLEdBQVgsS0FBbUJ3QyxTQUFTeEMsR0FBaEMsRUFBcUM7QUFDbkN3QyxhQUFTakIsS0FBVCxDQUFlekIsT0FBZixDQUF1QixVQUFDdUMsS0FBRCxFQUFRSSxDQUFSLEVBQWM7QUFDbkMsVUFBTUMsU0FBU0osV0FBV3RDLEdBQTFCO0FBQ0EsVUFBTTJDLFdBQVdMLFdBQVdmLEtBQVgsQ0FBaUJDLElBQWpCLEdBQXdCaUIsQ0FBekM7QUFDQXhELGFBQU8yRCxhQUFQLENBQXFCUCxNQUFNckMsR0FBM0IsRUFBZ0MwQyxNQUFoQyxFQUF3Q0MsUUFBeEMsRUFBa0Q3RCxJQUFsRDtBQUNELEtBSkQ7O0FBTUE7QUFDQSxRQUFNK0QsU0FBU3RELFNBQVN1RCw0QkFBVCxDQUFzQ04sU0FBU3hDLEdBQS9DLEtBQXVEd0MsUUFBdEU7QUFDQXZELFdBQU9rQyxlQUFQLENBQXVCMEIsT0FBTzdDLEdBQTlCLEVBQW1DbEIsSUFBbkM7QUFDRDs7QUFFRCxNQUFJQyxTQUFKLEVBQWU7QUFDYkUsV0FBTzhELGtCQUFQLENBQTBCeEMsU0FBU1AsR0FBbkM7QUFDRDtBQUNGLENBMUpEOztBQTRKQTs7Ozs7Ozs7O0FBU0FuQixRQUFRbUUseUJBQVIsR0FBb0MsVUFBQy9ELE1BQUQsRUFBU0MsS0FBVCxFQUFnQkUsT0FBaEIsRUFBNEI7QUFBQSxNQUN0REUsS0FEc0QsR0FDNUNMLE1BRDRDLENBQ3RESyxLQURzRDtBQUFBLE1BRXREQyxRQUZzRCxHQUV6Q0QsS0FGeUMsQ0FFdERDLFFBRnNEO0FBQUEsTUFHdERDLFFBSHNELEdBRzVCTixLQUg0QixDQUd0RE0sUUFIc0Q7QUFBQSxNQUc1Q0MsV0FINEMsR0FHNUJQLEtBSDRCLENBRzVDTyxXQUg0Qzs7QUFJOUQsTUFBTTZDLGFBQWEvQyxTQUFTZ0QsZUFBVCxDQUF5Qi9DLFFBQXpCLENBQW5CO0FBQ0EsTUFBTXlELFNBQVNYLFdBQVdZLFNBQVgsQ0FBcUIxRCxRQUFyQixDQUFmO0FBQ0EsTUFBTTJELElBQUlGLFNBQVN4RCxXQUFuQjtBQU44RCxNQU90RFUsSUFQc0QsR0FPN0NtQyxVQVA2QyxDQU90RG5DLElBUHNEOztBQVE5RCxNQUFNaUQsSUFBSSxpQkFBT0MscUJBQVAsQ0FBNkJsRCxJQUE3QixFQUFtQ2dELENBQW5DLENBQVY7QUFDQWxFLFNBQU9xRSxxQkFBUCxDQUE2QnBFLEtBQTdCLEVBQW9Da0UsQ0FBcEMsRUFBdUNoRSxPQUF2QztBQUNELENBVkQ7O0FBWUE7Ozs7Ozs7OztBQVNBUCxRQUFRMEUseUJBQVIsR0FBb0MsVUFBQ3RFLE1BQUQsRUFBU0MsS0FBVCxFQUFnQkUsT0FBaEIsRUFBNEI7QUFBQSxNQUN0REUsS0FEc0QsR0FDNUNMLE1BRDRDLENBQ3RESyxLQURzRDtBQUFBLE1BRXREQyxRQUZzRCxHQUV6Q0QsS0FGeUMsQ0FFdERDLFFBRnNEO0FBQUEsTUFHdERDLFFBSHNELEdBRzVCTixLQUg0QixDQUd0RE0sUUFIc0Q7QUFBQSxNQUc1Q0MsV0FINEMsR0FHNUJQLEtBSDRCLENBRzVDTyxXQUg0Qzs7QUFJOUQsTUFBTTZDLGFBQWEvQyxTQUFTZ0QsZUFBVCxDQUF5Qi9DLFFBQXpCLENBQW5CO0FBQ0EsTUFBTXlELFNBQVNYLFdBQVdZLFNBQVgsQ0FBcUIxRCxRQUFyQixDQUFmO0FBQ0EsTUFBTTJELElBQUlGLFNBQVN4RCxXQUFuQjtBQUNBUixTQUFPcUUscUJBQVAsQ0FBNkJwRSxLQUE3QixFQUFvQ2lFLENBQXBDLEVBQXVDL0QsT0FBdkM7QUFDRCxDQVJEOztBQVVBOzs7Ozs7Ozs7QUFTQVAsUUFBUTJFLHlCQUFSLEdBQW9DLFVBQUN2RSxNQUFELEVBQVNDLEtBQVQsRUFBZ0JFLE9BQWhCLEVBQTRCO0FBQUEsTUFDdERFLEtBRHNELEdBQzVDTCxNQUQ0QyxDQUN0REssS0FEc0Q7QUFBQSxNQUV0REMsUUFGc0QsR0FFekNELEtBRnlDLENBRXREQyxRQUZzRDtBQUFBLE1BR3REQyxRQUhzRCxHQUc1Qk4sS0FINEIsQ0FHdERNLFFBSHNEO0FBQUEsTUFHNUNDLFdBSDRDLEdBRzVCUCxLQUg0QixDQUc1Q08sV0FINEM7O0FBSTlELE1BQU02QyxhQUFhL0MsU0FBU2dELGVBQVQsQ0FBeUIvQyxRQUF6QixDQUFuQjtBQUNBLE1BQU15RCxTQUFTWCxXQUFXWSxTQUFYLENBQXFCMUQsUUFBckIsQ0FBZjtBQUNBLE1BQU0yRCxJQUFJRixTQUFTeEQsV0FBbkI7QUFOOEQsTUFPdERVLElBUHNELEdBTzdDbUMsVUFQNkMsQ0FPdERuQyxJQVBzRDs7QUFROUQsTUFBTWlELElBQUksaUJBQU9LLHFCQUFQLENBQTZCdEQsSUFBN0IsRUFBbUNnRCxDQUFuQyxDQUFWO0FBQ0FsRSxTQUFPcUUscUJBQVAsQ0FBNkJwRSxLQUE3QixFQUFvQ2tFLENBQXBDLEVBQXVDaEUsT0FBdkM7QUFDRCxDQVZEOztBQVlBOzs7Ozs7Ozs7O0FBVUFQLFFBQVF5RSxxQkFBUixHQUFnQyxVQUFDckUsTUFBRCxFQUFTQyxLQUFULEVBQXdDO0FBQUEsTUFBeEJrRSxDQUF3Qix1RUFBcEIsQ0FBb0I7QUFBQSxNQUFqQmhFLE9BQWlCLHVFQUFQLEVBQU87QUFBQSw0QkFDekNBLE9BRHlDLENBQzlETCxTQUQ4RDtBQUFBLE1BQzlEQSxTQUQ4RCx1Q0FDbEQsSUFEa0Q7QUFBQSxNQUU5RE8sS0FGOEQsR0FFcERMLE1BRm9ELENBRTlESyxLQUY4RDtBQUFBLE1BRzlEQyxRQUg4RCxHQUdqREQsS0FIaUQsQ0FHOURDLFFBSDhEO0FBQUEsZUFJcENMLEtBSm9DO0FBQUEsTUFJOURNLFFBSjhELFVBSTlEQSxRQUo4RDtBQUFBLE1BSXBEd0IsV0FKb0QsVUFJcERBLFdBSm9EOztBQU10RTs7QUFDQSxNQUFJOUIsTUFBTXdFLFVBQVYsRUFBc0I7QUFDcEJ6RSxXQUFPb0IsYUFBUCxDQUFxQm5CLEtBQXJCLEVBQTRCLEVBQUVILG9CQUFGLEVBQTVCO0FBQ0E7QUFDRDs7QUFFRCxNQUFNNEUsUUFBUXBFLFNBQVNnRCxlQUFULENBQXlCL0MsUUFBekIsQ0FBZDtBQUNBO0FBQ0EsTUFBSW1FLFNBQVNBLE1BQU05QyxNQUFuQixFQUEyQjtBQUN6QjVCLFdBQU9rQyxlQUFQLENBQXVCd0MsTUFBTTNELEdBQTdCLEVBQWtDLEVBQUVqQixvQkFBRixFQUFsQztBQUNBO0FBQ0Q7QUFDRDtBQUNBLE1BQUk0RSxTQUFTLENBQUNBLE1BQU05QyxNQUFoQixJQUEwQjhDLE1BQU1DLE9BQWhDLElBQTJDckUsU0FBU2dDLEtBQVQsQ0FBZUMsSUFBZixLQUF3QixDQUF2RSxFQUEwRTtBQUN4RXZDLFdBQU9rQyxlQUFQLENBQXVCd0MsTUFBTTNELEdBQTdCLEVBQWtDLEVBQUVqQixvQkFBRixFQUFsQztBQUNBO0FBQ0Q7O0FBRUQ7QUFDQSxNQUFNOEUsU0FBU3RFLFNBQVN1RSxnQkFBVCxDQUEwQnRFLFFBQTFCLENBQWY7QUFDQSxNQUFJcUUsVUFBVUEsT0FBT2hELE1BQXJCLEVBQTZCO0FBQzNCNUIsV0FBT2tDLGVBQVAsQ0FBdUIwQyxPQUFPN0QsR0FBOUIsRUFBbUMsRUFBRWpCLG9CQUFGLEVBQW5DO0FBQ0E7QUFDRDs7QUFFRDtBQUNBLE1BQUlHLE1BQU02RSxXQUFOLENBQWtCeEUsUUFBbEIsQ0FBSixFQUFpQztBQUMvQjtBQUNEOztBQUVEO0FBQ0E7QUFDQSxNQUFNWSxPQUFPWixTQUFTeUUsYUFBVCxDQUF1QnhFLFFBQXZCLENBQWI7QUFDQSxNQUFJTixNQUFNNkUsV0FBTixDQUFrQjVELElBQWxCLENBQUosRUFBNkI7QUFDM0IsUUFBTThELE9BQU8xRSxTQUFTMkUsZUFBVCxDQUF5Qi9ELEtBQUtILEdBQTlCLENBQWI7QUFDQSxRQUFNbUUsWUFBWTVFLFNBQVNnRCxlQUFULENBQXlCMEIsS0FBS2pFLEdBQTlCLENBQWxCO0FBQ0EsUUFBTW9FLGFBQWE3RSxTQUFTdUUsZ0JBQVQsQ0FBMEJHLEtBQUtqRSxHQUEvQixDQUFuQjs7QUFFQTtBQUNBLFFBQUltRSxhQUFhQSxVQUFVdEQsTUFBM0IsRUFBbUM7QUFDakM1QixhQUFPa0MsZUFBUCxDQUF1QmdELFVBQVVuRSxHQUFqQyxFQUFzQyxFQUFFakIsb0JBQUYsRUFBdEM7QUFDQTtBQUNEOztBQUVEO0FBQ0EsUUFBSXFGLGNBQWNBLFdBQVd2RCxNQUE3QixFQUFxQztBQUNuQzVCLGFBQU9rQyxlQUFQLENBQXVCaUQsV0FBV3BFLEdBQWxDLEVBQXVDLEVBQUVqQixvQkFBRixFQUF2QztBQUNBO0FBQ0Q7O0FBRUQ7QUFDQTtBQUNBLFFBQUlxRSxLQUFLLENBQUwsSUFBVWUsYUFBYVIsS0FBM0IsRUFBa0M7QUFDaEN6RSxjQUFRQSxNQUFNbUYsS0FBTixDQUFZO0FBQ2xCQyxtQkFBV0wsS0FBS2pFLEdBREU7QUFFbEJjLHNCQUFjbUQsS0FBSzlELElBQUwsQ0FBVUQ7QUFGTixPQUFaLENBQVI7O0FBS0FqQixhQUFPb0IsYUFBUCxDQUFxQm5CLEtBQXJCLEVBQTRCLEVBQUVILG9CQUFGLEVBQTVCO0FBQ0E7QUFDRDtBQUNGOztBQUVEO0FBQ0E7QUFDQSxNQUFJcUUsSUFBSXBDLFdBQVIsRUFBcUI7QUFDbkI5QixZQUFRQSxNQUFNbUYsS0FBTixDQUFZO0FBQ2xCckQsbUJBQWFBLGNBQWNvQyxDQURUO0FBRWxCckMsa0JBQVk7QUFGTSxLQUFaLENBQVI7O0FBS0E5QixXQUFPb0IsYUFBUCxDQUFxQm5CLEtBQXJCLEVBQTRCLEVBQUVILG9CQUFGLEVBQTVCO0FBQ0E7QUFDRDs7QUFFRDtBQUNBLE1BQUlnQixPQUFPSSxJQUFYO0FBQ0EsTUFBSThDLFNBQVMsQ0FBYjtBQUNBLE1BQUlzQixZQUFZdkQsV0FBaEI7O0FBRUEsU0FBT29DLElBQUltQixTQUFYLEVBQXNCO0FBQ3BCeEUsV0FBT1IsU0FBUzJFLGVBQVQsQ0FBeUJuRSxLQUFLQyxHQUE5QixDQUFQO0FBQ0EsUUFBTW1DLE9BQU9vQyxZQUFZeEUsS0FBS0ksSUFBTCxDQUFVRCxNQUFuQztBQUNBLFFBQUlrRCxLQUFLakIsSUFBVCxFQUFlO0FBQ2JjLGVBQVNkLE9BQU9pQixDQUFoQjtBQUNBO0FBQ0QsS0FIRCxNQUdPO0FBQ0xtQixrQkFBWXBDLElBQVo7QUFDRDtBQUNGOztBQUVEO0FBQ0EsTUFBSTVDLFNBQVNpRixhQUFULENBQXVCekUsS0FBS0MsR0FBNUIsQ0FBSixFQUFzQztBQUNwQyxRQUFNeUUsU0FBU2xGLFNBQVNtRixjQUFULENBQXdCM0UsS0FBS0MsR0FBN0IsQ0FBZjtBQUNBRCxXQUFPUixTQUFTNkMsV0FBVCxDQUFxQnFDLE9BQU96RSxHQUE1QixDQUFQO0FBQ0FpRCxhQUFTLENBQVQ7QUFDRDs7QUFFRC9ELFVBQVFBLE1BQU1tRixLQUFOLENBQVk7QUFDbEJNLGNBQVU1RSxLQUFLQyxHQURHO0FBRWxCZ0IsaUJBQWFpQyxNQUZLO0FBR2xCbEMsZ0JBQVk7QUFITSxHQUFaLENBQVI7O0FBTUE5QixTQUFPb0IsYUFBUCxDQUFxQm5CLEtBQXJCLEVBQTRCLEVBQUVILG9CQUFGLEVBQTVCO0FBQ0QsQ0EvR0Q7O0FBaUhBOzs7Ozs7Ozs7QUFTQUYsUUFBUStGLHdCQUFSLEdBQW1DLFVBQUMzRixNQUFELEVBQVNDLEtBQVQsRUFBZ0JFLE9BQWhCLEVBQTRCO0FBQUEsTUFDckRFLEtBRHFELEdBQzNDTCxNQUQyQyxDQUNyREssS0FEcUQ7QUFBQSxNQUVyREMsUUFGcUQsR0FFeENELEtBRndDLENBRXJEQyxRQUZxRDtBQUFBLE1BR3JEQyxRQUhxRCxHQUczQk4sS0FIMkIsQ0FHckRNLFFBSHFEO0FBQUEsTUFHM0NDLFdBSDJDLEdBRzNCUCxLQUgyQixDQUczQ08sV0FIMkM7O0FBSTdELE1BQU02QyxhQUFhL0MsU0FBU2dELGVBQVQsQ0FBeUIvQyxRQUF6QixDQUFuQjtBQUNBLE1BQU15RCxTQUFTWCxXQUFXWSxTQUFYLENBQXFCMUQsUUFBckIsQ0FBZjtBQUNBLE1BQU0yRCxJQUFJRixTQUFTeEQsV0FBbkI7QUFONkQsTUFPckRVLElBUHFELEdBTzVDbUMsVUFQNEMsQ0FPckRuQyxJQVBxRDs7QUFRN0QsTUFBTWlELElBQUksaUJBQU95QixvQkFBUCxDQUE0QjFFLElBQTVCLEVBQWtDZ0QsQ0FBbEMsQ0FBVjtBQUNBbEUsU0FBTzZGLG9CQUFQLENBQTRCNUYsS0FBNUIsRUFBbUNrRSxDQUFuQyxFQUFzQ2hFLE9BQXRDO0FBQ0QsQ0FWRDs7QUFZQTs7Ozs7Ozs7O0FBU0FQLFFBQVFrRyx3QkFBUixHQUFtQyxVQUFDOUYsTUFBRCxFQUFTQyxLQUFULEVBQWdCRSxPQUFoQixFQUE0QjtBQUFBLE1BQ3JERSxLQURxRCxHQUMzQ0wsTUFEMkMsQ0FDckRLLEtBRHFEO0FBQUEsTUFFckRDLFFBRnFELEdBRXhDRCxLQUZ3QyxDQUVyREMsUUFGcUQ7QUFBQSxNQUdyREMsUUFIcUQsR0FHM0JOLEtBSDJCLENBR3JETSxRQUhxRDtBQUFBLE1BRzNDQyxXQUgyQyxHQUczQlAsS0FIMkIsQ0FHM0NPLFdBSDJDOztBQUk3RCxNQUFNNkMsYUFBYS9DLFNBQVNnRCxlQUFULENBQXlCL0MsUUFBekIsQ0FBbkI7QUFDQSxNQUFNeUQsU0FBU1gsV0FBV1ksU0FBWCxDQUFxQjFELFFBQXJCLENBQWY7QUFDQSxNQUFNMkQsSUFBSUYsU0FBU3hELFdBQW5CO0FBQ0FSLFNBQU82RixvQkFBUCxDQUE0QjVGLEtBQTVCLEVBQW1DaUUsQ0FBbkMsRUFBc0MvRCxPQUF0QztBQUNELENBUkQ7O0FBVUE7Ozs7Ozs7OztBQVNBUCxRQUFRbUcsd0JBQVIsR0FBbUMsVUFBQy9GLE1BQUQsRUFBU0MsS0FBVCxFQUFnQkUsT0FBaEIsRUFBNEI7QUFBQSxNQUNyREUsS0FEcUQsR0FDM0NMLE1BRDJDLENBQ3JESyxLQURxRDtBQUFBLE1BRXJEQyxRQUZxRCxHQUV4Q0QsS0FGd0MsQ0FFckRDLFFBRnFEO0FBQUEsTUFHckRDLFFBSHFELEdBRzNCTixLQUgyQixDQUdyRE0sUUFIcUQ7QUFBQSxNQUczQ0MsV0FIMkMsR0FHM0JQLEtBSDJCLENBRzNDTyxXQUgyQzs7QUFJN0QsTUFBTTZDLGFBQWEvQyxTQUFTZ0QsZUFBVCxDQUF5Qi9DLFFBQXpCLENBQW5CO0FBQ0EsTUFBTXlELFNBQVNYLFdBQVdZLFNBQVgsQ0FBcUIxRCxRQUFyQixDQUFmO0FBQ0EsTUFBTTJELElBQUlGLFNBQVN4RCxXQUFuQjtBQU42RCxNQU9yRFUsSUFQcUQsR0FPNUNtQyxVQVA0QyxDQU9yRG5DLElBUHFEOztBQVE3RCxNQUFNaUQsSUFBSSxpQkFBTzZCLG9CQUFQLENBQTRCOUUsSUFBNUIsRUFBa0NnRCxDQUFsQyxDQUFWO0FBQ0FsRSxTQUFPNkYsb0JBQVAsQ0FBNEI1RixLQUE1QixFQUFtQ2tFLENBQW5DLEVBQXNDaEUsT0FBdEM7QUFDRCxDQVZEOztBQVlBOzs7Ozs7Ozs7O0FBVUFQLFFBQVFpRyxvQkFBUixHQUErQixVQUFDN0YsTUFBRCxFQUFTQyxLQUFULEVBQXdDO0FBQUEsTUFBeEJrRSxDQUF3Qix1RUFBcEIsQ0FBb0I7QUFBQSxNQUFqQmhFLE9BQWlCLHVFQUFQLEVBQU87QUFBQSw0QkFDeENBLE9BRHdDLENBQzdETCxTQUQ2RDtBQUFBLE1BQzdEQSxTQUQ2RCx1Q0FDakQsSUFEaUQ7QUFBQSxNQUU3RE8sS0FGNkQsR0FFbkRMLE1BRm1ELENBRTdESyxLQUY2RDtBQUFBLE1BRzdEQyxRQUg2RCxHQUdoREQsS0FIZ0QsQ0FHN0RDLFFBSDZEO0FBQUEsZ0JBSW5DTCxLQUptQztBQUFBLE1BSTdETSxRQUo2RCxXQUk3REEsUUFKNkQ7QUFBQSxNQUluRHdCLFdBSm1ELFdBSW5EQSxXQUptRDs7QUFNckU7O0FBQ0EsTUFBSTlCLE1BQU13RSxVQUFWLEVBQXNCO0FBQ3BCekUsV0FBT29CLGFBQVAsQ0FBcUJuQixLQUFyQixFQUE0QixFQUFFSCxvQkFBRixFQUE1QjtBQUNBO0FBQ0Q7O0FBRUQsTUFBTTRFLFFBQVFwRSxTQUFTZ0QsZUFBVCxDQUF5Qi9DLFFBQXpCLENBQWQ7QUFDQTtBQUNBLE1BQUltRSxTQUFTQSxNQUFNOUMsTUFBbkIsRUFBMkI7QUFDekI1QixXQUFPa0MsZUFBUCxDQUF1QndDLE1BQU0zRCxHQUE3QixFQUFrQyxFQUFFakIsb0JBQUYsRUFBbEM7QUFDQTtBQUNEO0FBQ0Q7QUFDQSxNQUFJNEUsU0FBUyxDQUFDQSxNQUFNOUMsTUFBaEIsSUFBMEI4QyxNQUFNQyxPQUFoQyxJQUEyQ3JFLFNBQVNnQyxLQUFULENBQWVDLElBQWYsS0FBd0IsQ0FBdkUsRUFBMEU7QUFDeEV2QyxXQUFPa0MsZUFBUCxDQUF1QndDLE1BQU0zRCxHQUE3QixFQUFrQyxFQUFFakIsb0JBQUYsRUFBbEM7QUFDQTtBQUNEOztBQUVEO0FBQ0EsTUFBTThFLFNBQVN0RSxTQUFTdUUsZ0JBQVQsQ0FBMEJ0RSxRQUExQixDQUFmO0FBQ0EsTUFBSXFFLFVBQVVBLE9BQU9oRCxNQUFyQixFQUE2QjtBQUMzQjVCLFdBQU9rQyxlQUFQLENBQXVCMEMsT0FBTzdELEdBQTlCLEVBQW1DLEVBQUVqQixvQkFBRixFQUFuQztBQUNBO0FBQ0Q7O0FBRUQ7QUFDQSxNQUFJRyxNQUFNZ0csU0FBTixDQUFnQjNGLFFBQWhCLENBQUosRUFBK0I7QUFDN0I7QUFDRDs7QUFFRDtBQUNBO0FBQ0EsTUFBTVksT0FBT1osU0FBU3lFLGFBQVQsQ0FBdUJ4RSxRQUF2QixDQUFiO0FBQ0EsTUFBSU4sTUFBTWdHLFNBQU4sQ0FBZ0IvRSxJQUFoQixDQUFKLEVBQTJCO0FBQ3pCLFFBQU1nQyxPQUFPNUMsU0FBUzZDLFdBQVQsQ0FBcUJqQyxLQUFLSCxHQUExQixDQUFiO0FBQ0EsUUFBTW1GLFlBQVk1RixTQUFTZ0QsZUFBVCxDQUF5QkosS0FBS25DLEdBQTlCLENBQWxCO0FBQ0EsUUFBTW9GLGFBQWE3RixTQUFTdUUsZ0JBQVQsQ0FBMEIzQixLQUFLbkMsR0FBL0IsQ0FBbkI7O0FBRUE7QUFDQSxRQUFJbUYsYUFBYUEsVUFBVXRFLE1BQTNCLEVBQW1DO0FBQ2pDNUIsYUFBT2tDLGVBQVAsQ0FBdUJnRSxVQUFVbkYsR0FBakMsRUFBc0MsRUFBRWpCLG9CQUFGLEVBQXRDO0FBQ0E7QUFDRDs7QUFFRDtBQUNBLFFBQUlxRyxjQUFjQSxXQUFXdkUsTUFBN0IsRUFBcUM7QUFDbkM1QixhQUFPa0MsZUFBUCxDQUF1QmlFLFdBQVdwRixHQUFsQyxFQUF1QyxFQUFFakIsb0JBQUYsRUFBdkM7QUFDQTtBQUNEOztBQUVEO0FBQ0E7QUFDQSxRQUFJcUUsS0FBSyxDQUFMLElBQVUrQixhQUFheEIsS0FBM0IsRUFBa0M7QUFDaEN6RSxjQUFRQSxNQUFNbUYsS0FBTixDQUFZO0FBQ2xCTSxrQkFBVXhDLEtBQUtuQyxHQURHO0FBRWxCZ0IscUJBQWE7QUFGSyxPQUFaLENBQVI7O0FBS0EvQixhQUFPb0IsYUFBUCxDQUFxQm5CLEtBQXJCLEVBQTRCLEVBQUVILG9CQUFGLEVBQTVCO0FBQ0E7QUFDRDtBQUNGOztBQUVEO0FBQ0E7QUFDQTtBQUNBLE1BQUlxRSxLQUFNakQsS0FBS0EsSUFBTCxDQUFVRCxNQUFWLEdBQW1CYyxXQUE3QixFQUEyQztBQUN6QzlCLFlBQVFBLE1BQU1tRixLQUFOLENBQVk7QUFDbEJyRCxtQkFBYUEsY0FBY29DO0FBRFQsS0FBWixDQUFSOztBQUlBbkUsV0FBT29CLGFBQVAsQ0FBcUJuQixLQUFyQixFQUE0QixFQUFFSCxvQkFBRixFQUE1QjtBQUNBO0FBQ0Q7O0FBRUQ7QUFDQSxNQUFJZ0IsT0FBT0ksSUFBWDtBQUNBLE1BQUk4QyxTQUFTakMsV0FBYjtBQUNBLE1BQUl1RCxZQUFZcEUsS0FBS0EsSUFBTCxDQUFVRCxNQUFWLEdBQW1CYyxXQUFuQzs7QUFFQSxTQUFPb0MsSUFBSW1CLFNBQVgsRUFBc0I7QUFDcEJ4RSxXQUFPUixTQUFTNkMsV0FBVCxDQUFxQnJDLEtBQUtDLEdBQTFCLENBQVA7QUFDQSxRQUFNbUMsUUFBT29DLFlBQVl4RSxLQUFLSSxJQUFMLENBQVVELE1BQW5DO0FBQ0EsUUFBSWtELEtBQUtqQixLQUFULEVBQWU7QUFDYmMsZUFBU0csSUFBSW1CLFNBQWI7QUFDQTtBQUNELEtBSEQsTUFHTztBQUNMQSxrQkFBWXBDLEtBQVo7QUFDRDtBQUNGOztBQUVEO0FBQ0EsTUFBSTVDLFNBQVNpRixhQUFULENBQXVCekUsS0FBS0MsR0FBNUIsQ0FBSixFQUFzQztBQUNwQyxRQUFNeUUsU0FBU2xGLFNBQVNtRixjQUFULENBQXdCM0UsS0FBS0MsR0FBN0IsQ0FBZjtBQUNBRCxXQUFPUixTQUFTMkUsZUFBVCxDQUF5Qk8sT0FBT3pFLEdBQWhDLENBQVA7QUFDQWlELGFBQVNsRCxLQUFLSSxJQUFMLENBQVVELE1BQW5CO0FBQ0Q7O0FBRURoQixVQUFRQSxNQUFNbUYsS0FBTixDQUFZO0FBQ2xCTSxjQUFVNUUsS0FBS0MsR0FERztBQUVsQmdCLGlCQUFhaUM7QUFGSyxHQUFaLENBQVI7O0FBS0FoRSxTQUFPb0IsYUFBUCxDQUFxQm5CLEtBQXJCLEVBQTRCLEVBQUVILG9CQUFGLEVBQTVCO0FBQ0QsQ0E5R0Q7O0FBZ0hBOzs7Ozs7Ozs7O0FBVUFGLFFBQVF3RyxrQkFBUixHQUE2QixVQUFDcEcsTUFBRCxFQUFTQyxLQUFULEVBQWdCeUUsS0FBaEIsRUFBd0M7QUFBQSxNQUFqQnZFLE9BQWlCLHVFQUFQLEVBQU87O0FBQ25FdUUsVUFBUSxvQkFBVUEsS0FBVixDQUFnQkEsS0FBaEIsQ0FBUjtBQURtRSw0QkFFdEN2RSxPQUZzQyxDQUUzREwsU0FGMkQ7QUFBQSxNQUUzREEsU0FGMkQsdUNBRS9DLElBRitDOzs7QUFJbkUsTUFBSUcsTUFBTXdFLFVBQVYsRUFBc0I7QUFDcEJ6RSxXQUFPb0IsYUFBUCxDQUFxQm5CLEtBQXJCO0FBQ0FBLFlBQVFBLE1BQU1vRyxlQUFOLEVBQVI7QUFDRDs7QUFQa0UsTUFTM0RoRyxLQVQyRCxHQVNqREwsTUFUaUQsQ0FTM0RLLEtBVDJEO0FBQUEsTUFVM0RDLFFBVjJELEdBVTlDRCxLQVY4QyxDQVUzREMsUUFWMkQ7QUFBQSxnQkFXakNMLEtBWGlDO0FBQUEsTUFXM0RNLFFBWDJELFdBVzNEQSxRQVgyRDtBQUFBLE1BV2pEQyxXQVhpRCxXQVdqREEsV0FYaUQ7O0FBWW5FLE1BQU02QyxhQUFhL0MsU0FBU2dELGVBQVQsQ0FBeUIvQyxRQUF6QixDQUFuQjtBQUNBLE1BQU1pRixTQUFTbEYsU0FBU2dHLFNBQVQsQ0FBbUJqRCxXQUFXdEMsR0FBOUIsQ0FBZjtBQUNBLE1BQU1DLFFBQVF3RSxPQUFPbEQsS0FBUCxDQUFhUSxPQUFiLENBQXFCTyxVQUFyQixDQUFkOztBQUVBLE1BQUlBLFdBQVd6QixNQUFmLEVBQXVCO0FBQ3JCLFFBQU0yRSxRQUFRdEcsTUFBTWdHLFNBQU4sQ0FBZ0I1QyxVQUFoQixJQUE4QixDQUE5QixHQUFrQyxDQUFoRDtBQUNBckQsV0FBT3dHLGVBQVAsQ0FBdUJoQixPQUFPekUsR0FBOUIsRUFBbUNDLFFBQVF1RixLQUEzQyxFQUFrRDdCLEtBQWxELEVBQXlELEVBQUU1RSxvQkFBRixFQUF6RDtBQUNELEdBSEQsTUFLSyxJQUFJdUQsV0FBV3NCLE9BQWYsRUFBd0I7QUFDM0IzRSxXQUFPa0MsZUFBUCxDQUF1Qm1CLFdBQVd0QyxHQUFsQztBQUNBZixXQUFPd0csZUFBUCxDQUF1QmhCLE9BQU96RSxHQUE5QixFQUFtQ0MsS0FBbkMsRUFBMEMwRCxLQUExQyxFQUFpRCxFQUFFNUUsb0JBQUYsRUFBakQ7QUFDRCxHQUhJLE1BS0EsSUFBSUcsTUFBTTZFLFdBQU4sQ0FBa0J6QixVQUFsQixDQUFKLEVBQW1DO0FBQ3RDckQsV0FBT3dHLGVBQVAsQ0FBdUJoQixPQUFPekUsR0FBOUIsRUFBbUNDLEtBQW5DLEVBQTBDMEQsS0FBMUMsRUFBaUQsRUFBRTVFLG9CQUFGLEVBQWpEO0FBQ0QsR0FGSSxNQUlBLElBQUlHLE1BQU1nRyxTQUFOLENBQWdCNUMsVUFBaEIsQ0FBSixFQUFpQztBQUNwQ3JELFdBQU93RyxlQUFQLENBQXVCaEIsT0FBT3pFLEdBQTlCLEVBQW1DQyxRQUFRLENBQTNDLEVBQThDMEQsS0FBOUMsRUFBcUQsRUFBRTVFLG9CQUFGLEVBQXJEO0FBQ0QsR0FGSSxNQUlBO0FBQ0hFLFdBQU80QyxxQkFBUCxDQUE2QlMsV0FBV3RDLEdBQXhDLEVBQTZDUixRQUE3QyxFQUF1REMsV0FBdkQsRUFBb0VYLElBQXBFO0FBQ0FHLFdBQU93RyxlQUFQLENBQXVCaEIsT0FBT3pFLEdBQTlCLEVBQW1DQyxRQUFRLENBQTNDLEVBQThDMEQsS0FBOUMsRUFBcUQsRUFBRTVFLG9CQUFGLEVBQXJEO0FBQ0Q7O0FBRUQsTUFBSUEsU0FBSixFQUFlO0FBQ2JFLFdBQU84RCxrQkFBUCxDQUEwQjBCLE9BQU96RSxHQUFqQztBQUNEO0FBQ0YsQ0ExQ0Q7O0FBNENBOzs7Ozs7Ozs7O0FBVUFuQixRQUFRNkcscUJBQVIsR0FBZ0MsVUFBQ3pHLE1BQUQsRUFBU0MsS0FBVCxFQUFnQnlHLFFBQWhCLEVBQTJDO0FBQUEsTUFBakJ2RyxPQUFpQix1RUFBUCxFQUFPO0FBQUEsNEJBQzVDQSxPQUQ0QyxDQUNqRUwsU0FEaUU7QUFBQSxNQUNqRUEsU0FEaUUsdUNBQ3JELElBRHFEOztBQUd6RTs7QUFDQSxNQUFJRyxNQUFNd0UsVUFBVixFQUFzQjtBQUNwQnpFLFdBQU9vQixhQUFQLENBQXFCbkIsS0FBckIsRUFBNEJKLElBQTVCO0FBQ0FJLFlBQVFBLE1BQU1vRyxlQUFOLEVBQVI7QUFDRDs7QUFFRDtBQUNBLE1BQUksQ0FBQ0ssU0FBU3BFLEtBQVQsQ0FBZUMsSUFBcEIsRUFBMEI7O0FBRTFCO0FBQ0E7QUFDQTtBQUNBO0FBQ0FtRSxhQUFXQSxTQUFTQyxjQUFULENBQXdCO0FBQUEsV0FBU3ZELE1BQU13RCxhQUFOLEVBQVQ7QUFBQSxHQUF4QixDQUFYOztBQUVBO0FBbEJ5RSxnQkFtQnZDM0csS0FuQnVDO0FBQUEsTUFtQmpFTSxRQW5CaUUsV0FtQmpFQSxRQW5CaUU7QUFBQSxNQW1CdkRDLFdBbkJ1RCxXQW1CdkRBLFdBbkJ1RDtBQUFBLE1Bb0JuRUgsS0FwQm1FLEdBb0J6REwsTUFwQnlELENBb0JuRUssS0FwQm1FO0FBQUEsZ0JBcUJ0REEsS0FyQnNEO0FBQUEsTUFxQm5FQyxRQXJCbUUsV0FxQm5FQSxRQXJCbUU7O0FBc0J6RSxNQUFJdUcsWUFBWXZHLFNBQVN5RSxhQUFULENBQXVCeEUsUUFBdkIsQ0FBaEI7QUFDQSxNQUFJOEMsYUFBYS9DLFNBQVNnRCxlQUFULENBQXlCdUQsVUFBVTlGLEdBQW5DLENBQWpCO0FBQ0EsTUFBSVMsYUFBYTZCLFdBQVc1QixtQkFBWCxDQUErQm9GLFVBQVU5RixHQUF6QyxDQUFqQjtBQUNBLE1BQU0rRixZQUFZN0csTUFBTTZFLFdBQU4sQ0FBa0J6QixVQUFsQixDQUFsQjtBQUNBLE1BQU1tQyxTQUFTbEYsU0FBU2dHLFNBQVQsQ0FBbUJqRCxXQUFXdEMsR0FBOUIsQ0FBZjtBQUNBLE1BQU1DLFFBQVF3RSxPQUFPbEQsS0FBUCxDQUFhUSxPQUFiLENBQXFCTyxVQUFyQixDQUFkO0FBQ0EsTUFBTTBELFNBQVNMLFNBQVNNLFNBQVQsRUFBZjtBQUNBLE1BQU1DLGFBQWFGLE9BQU8zRSxLQUFQLEVBQW5CO0FBQ0EsTUFBTThFLFlBQVlILE9BQU9JLElBQVAsRUFBbEI7O0FBRUE7QUFDQSxNQUFJRixjQUFjQyxTQUFkLElBQTJCRCxXQUFXckYsTUFBMUMsRUFBa0Q7QUFDaEQ1QixXQUFPb0csa0JBQVAsQ0FBMEJuRyxLQUExQixFQUFpQ2dILFVBQWpDLEVBQTZDOUcsT0FBN0M7QUFDQTtBQUNEOztBQUVEO0FBQ0E7QUFDQSxNQUFJOEcsY0FBY0MsU0FBbEIsRUFBNkI7QUFDM0IsUUFBTUUsZUFBZVYsU0FBU1csV0FBVCxDQUFxQkosV0FBV2xHLEdBQWhDLEVBQXFDO0FBQUEsYUFBS3VHLEVBQUVoRixLQUFGLENBQVFDLElBQVIsSUFBZ0IsQ0FBckI7QUFBQSxLQUFyQyxDQUFyQjtBQUNBLFFBQU1nRixjQUFjSCxnQkFBZ0JILFVBQXBDO0FBQ0EsUUFBTXBFLGFBQWEyQyxPQUFPbEQsS0FBUCxDQUFhUSxPQUFiLENBQXFCTyxVQUFyQixDQUFuQjtBQUNBcUQsZUFBV0EsU0FBU2MsZ0JBQVQsQ0FBMEJELFlBQVl4RyxHQUF0QyxDQUFYOztBQUVBMkYsYUFBU3BFLEtBQVQsQ0FBZXpCLE9BQWYsQ0FBdUIsVUFBQ0MsSUFBRCxFQUFPMEMsQ0FBUCxFQUFhO0FBQ2xDLFVBQU1FLFdBQVdiLGFBQWFXLENBQWIsR0FBaUIsQ0FBbEM7QUFDQXhELGFBQU93RyxlQUFQLENBQXVCaEIsT0FBT3pFLEdBQTlCLEVBQW1DMkMsUUFBbkMsRUFBNkM1QyxJQUE3QyxFQUFtRGpCLElBQW5EO0FBQ0QsS0FIRDtBQUlEOztBQUVEO0FBQ0EsTUFBSVcsZUFBZSxDQUFuQixFQUFzQjtBQUNwQlIsV0FBTzRDLHFCQUFQLENBQTZCcEIsV0FBV1QsR0FBeEMsRUFBNkNSLFFBQTdDLEVBQXVEQyxXQUF2RCxFQUFvRVgsSUFBcEU7QUFDRDs7QUFFRDtBQUNBUSxVQUFRTCxPQUFPSyxLQUFmO0FBQ0FDLGFBQVdELE1BQU1DLFFBQWpCO0FBQ0F1RyxjQUFZdkcsU0FBU3lFLGFBQVQsQ0FBdUJ4RSxRQUF2QixDQUFaO0FBQ0E4QyxlQUFhL0MsU0FBU2dELGVBQVQsQ0FBeUIvQyxRQUF6QixDQUFiO0FBQ0FpQixlQUFhNkIsV0FBVzVCLG1CQUFYLENBQStCb0YsVUFBVTlGLEdBQXpDLENBQWI7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsTUFBSWtHLGNBQWNDLFNBQWxCLEVBQTZCO0FBQzNCLFFBQU1PLFlBQVlYLFlBQVl0RixVQUFaLEdBQXlCNkIsV0FBV3BCLGNBQVgsQ0FBMEJULFdBQVdULEdBQXJDLENBQTNDO0FBQ0EsUUFBTTJHLFlBQVlELFlBQVlwRSxXQUFXZixLQUFYLENBQWlCcUYsU0FBakIsQ0FBMkI7QUFBQSxhQUFLeEQsRUFBRXBELEdBQUYsSUFBUzBHLFVBQVUxRyxHQUF4QjtBQUFBLEtBQTNCLENBQVosR0FBc0Usc0JBQXhGO0FBQ0EsUUFBTTZHLFlBQVlWLFVBQVU1RSxLQUFWLENBQWdCQyxJQUFsQzs7QUFFQW1GLGNBQVU3RyxPQUFWLENBQWtCLFVBQUNDLElBQUQsRUFBTzBDLENBQVAsRUFBYTtBQUM3QixVQUFNRSxXQUFXa0UsWUFBWXBFLENBQTdCO0FBQ0F4RCxhQUFPMkQsYUFBUCxDQUFxQjdDLEtBQUtDLEdBQTFCLEVBQStCbUcsVUFBVW5HLEdBQXpDLEVBQThDMkMsUUFBOUMsRUFBd0Q3RCxJQUF4RDtBQUNELEtBSEQ7QUFJRDs7QUFFRDtBQUNBO0FBQ0EsTUFBSXdELFdBQVdzQixPQUFmLEVBQXdCO0FBQ3RCM0UsV0FBT2tDLGVBQVAsQ0FBdUJtQixXQUFXdEMsR0FBbEMsRUFBdUNsQixJQUF2QztBQUNBRyxXQUFPd0csZUFBUCxDQUF1QmhCLE9BQU96RSxHQUE5QixFQUFtQ0MsS0FBbkMsRUFBMENpRyxVQUExQyxFQUFzRHBILElBQXREO0FBQ0Q7O0FBRUQ7QUFDQTtBQU5BLE9BT0s7QUFDSCxVQUFNZ0ksY0FBY3hFLFdBQVc1QixtQkFBWCxDQUErQm9GLFVBQVU5RixHQUF6QyxDQUFwQjtBQUNBLFVBQU0rRyxjQUFjekUsV0FBV2YsS0FBWCxDQUFpQlEsT0FBakIsQ0FBeUIrRSxXQUF6QixDQUFwQjs7QUFFQVosaUJBQVczRSxLQUFYLENBQWlCekIsT0FBakIsQ0FBeUIsVUFBQytELE1BQUQsRUFBU3BCLENBQVQsRUFBZTtBQUN0QyxZQUFNVSxJQUFJMUQsZUFBZSxDQUFmLEdBQW1CLENBQW5CLEdBQXVCLENBQWpDO0FBQ0EsWUFBTWtELFdBQVdvRSxjQUFjdEUsQ0FBZCxHQUFrQlUsQ0FBbkM7QUFDQWxFLGVBQU93RyxlQUFQLENBQXVCbkQsV0FBV3RDLEdBQWxDLEVBQXVDMkMsUUFBdkMsRUFBaURrQixNQUFqRCxFQUF5RC9FLElBQXpEO0FBQ0QsT0FKRDtBQUtEOztBQUVEO0FBQ0EsTUFBSUMsU0FBSixFQUFlO0FBQ2JFLFdBQU84RCxrQkFBUCxDQUEwQjBCLE9BQU96RSxHQUFqQztBQUNEO0FBQ0YsQ0F0R0Q7O0FBd0dBOzs7Ozs7Ozs7O0FBVUFuQixRQUFRbUksbUJBQVIsR0FBOEIsVUFBQy9ILE1BQUQsRUFBU0MsS0FBVCxFQUFnQjJFLE1BQWhCLEVBQXlDO0FBQUEsTUFBakJ6RSxPQUFpQix1RUFBUCxFQUFPO0FBQUEsNEJBQ3hDQSxPQUR3QyxDQUM3REwsU0FENkQ7QUFBQSxNQUM3REEsU0FENkQsdUNBQ2pELElBRGlEOztBQUVyRThFLFdBQVMsb0JBQVVBLE1BQVYsQ0FBaUJBLE1BQWpCLENBQVQ7O0FBRUEsTUFBSTNFLE1BQU13RSxVQUFWLEVBQXNCO0FBQ3BCekUsV0FBT29CLGFBQVAsQ0FBcUJuQixLQUFyQixFQUE0QkosSUFBNUI7QUFDQUksWUFBUUEsTUFBTW9HLGVBQU4sRUFBUjtBQUNEOztBQVBvRSxNQVM3RGhHLEtBVDZELEdBU25ETCxNQVRtRCxDQVM3REssS0FUNkQ7QUFBQSxNQVU3REMsUUFWNkQsR0FVaERELEtBVmdELENBVTdEQyxRQVY2RDtBQUFBLGdCQVduQ0wsS0FYbUM7QUFBQSxNQVc3RE0sUUFYNkQsV0FXN0RBLFFBWDZEO0FBQUEsTUFXbkRDLFdBWG1ELFdBV25EQSxXQVhtRDs7QUFZckUsTUFBTWdGLFNBQVNsRixTQUFTZ0csU0FBVCxDQUFtQi9GLFFBQW5CLENBQWY7QUFDQSxNQUFNc0csWUFBWXZHLFNBQVMwSCxnQkFBVCxDQUEwQnpILFFBQTFCLENBQWxCO0FBQ0EsTUFBTVMsUUFBUXdFLE9BQU9sRCxLQUFQLENBQWFRLE9BQWIsQ0FBcUIrRCxTQUFyQixDQUFkOztBQUVBLE1BQUlyQixPQUFPNUQsTUFBWCxFQUFtQjs7QUFFbkI1QixTQUFPMkMsY0FBUCxDQUFzQnBDLFFBQXRCLEVBQWdDQyxXQUFoQyxFQUE2Q1gsSUFBN0M7QUFDQUcsU0FBT3dHLGVBQVAsQ0FBdUJoQixPQUFPekUsR0FBOUIsRUFBbUNDLFFBQVEsQ0FBM0MsRUFBOEM0RCxNQUE5QyxFQUFzRC9FLElBQXREOztBQUVBLE1BQUlDLFNBQUosRUFBZTtBQUNiRSxXQUFPOEQsa0JBQVAsQ0FBMEIwQixPQUFPekUsR0FBakM7QUFDRDtBQUNGLENBeEJEOztBQTBCQTs7Ozs7Ozs7Ozs7QUFXQW5CLFFBQVFxSSxpQkFBUixHQUE0QixVQUFDakksTUFBRCxFQUFTQyxLQUFULEVBQWdCaUIsSUFBaEIsRUFBc0JnSCxLQUF0QixFQUE4QztBQUFBLE1BQWpCL0gsT0FBaUIsdUVBQVAsRUFBTztBQUFBLE1BQ2xFTCxTQURrRSxHQUNwREssT0FEb0QsQ0FDbEVMLFNBRGtFO0FBQUEsTUFFaEVPLEtBRmdFLEdBRXRETCxNQUZzRCxDQUVoRUssS0FGZ0U7QUFBQSxNQUdoRUMsUUFIZ0UsR0FHbkRELEtBSG1ELENBR2hFQyxRQUhnRTtBQUFBLE1BSWhFQyxRQUpnRSxHQUl0Q04sS0FKc0MsQ0FJaEVNLFFBSmdFO0FBQUEsTUFJdERDLFdBSnNELEdBSXRDUCxLQUpzQyxDQUl0RE8sV0FKc0Q7O0FBS3hFLE1BQU1nRixTQUFTbEYsU0FBU2dHLFNBQVQsQ0FBbUIvRixRQUFuQixDQUFmOztBQUVBLE1BQUlpRixPQUFPNUQsTUFBWCxFQUFtQjs7QUFFbkIsTUFBSTNCLE1BQU13RSxVQUFWLEVBQXNCO0FBQ3BCekUsV0FBT29CLGFBQVAsQ0FBcUJuQixLQUFyQixFQUE0QkosSUFBNUI7QUFDRDs7QUFFRDtBQUNBLE1BQUlDLGNBQWNxSSxTQUFsQixFQUE2QjtBQUMzQnJJLGdCQUFZRyxNQUFNd0UsVUFBbEI7QUFDRDs7QUFFRHpFLFNBQU9vSSxlQUFQLENBQXVCN0gsUUFBdkIsRUFBaUNDLFdBQWpDLEVBQThDVSxJQUE5QyxFQUFvRGdILEtBQXBELEVBQTJELEVBQUVwSSxvQkFBRixFQUEzRDtBQUNELENBbkJEOztBQXFCQTs7Ozs7Ozs7OztBQVVBRixRQUFReUksaUJBQVIsR0FBNEIsVUFBQ3JJLE1BQUQsRUFBU0MsS0FBVCxFQUFnQkMsSUFBaEIsRUFBdUM7QUFBQSxNQUFqQkMsT0FBaUIsdUVBQVAsRUFBTzs7QUFDakUsTUFBSUYsTUFBTUcsV0FBVixFQUF1Qjs7QUFEMEMsNEJBR3BDRCxPQUhvQyxDQUd6REwsU0FIeUQ7QUFBQSxNQUd6REEsU0FIeUQsdUNBRzdDLElBSDZDO0FBQUEsTUFJekRPLEtBSnlELEdBSS9DTCxNQUorQyxDQUl6REssS0FKeUQ7QUFBQSxNQUt6REMsUUFMeUQsR0FLNUNELEtBTDRDLENBS3pEQyxRQUx5RDs7QUFNakUsTUFBTUssUUFBUUwsU0FBU00sZUFBVCxDQUF5QlgsS0FBekIsQ0FBZDtBQU5pRSxNQU96RE0sUUFQeUQsR0FPWk4sS0FQWSxDQU96RE0sUUFQeUQ7QUFBQSxNQU8vQ0MsV0FQK0MsR0FPWlAsS0FQWSxDQU8vQ08sV0FQK0M7QUFBQSxNQU9sQ0MsTUFQa0MsR0FPWlIsS0FQWSxDQU9sQ1EsTUFQa0M7QUFBQSxNQU8xQkMsU0FQMEIsR0FPWlQsS0FQWSxDQU8xQlMsU0FQMEI7OztBQVNqRUMsUUFBTUUsT0FBTixDQUFjLFVBQUNDLElBQUQsRUFBVTtBQUFBLFFBQ2RDLEdBRGMsR0FDTkQsSUFETSxDQUNkQyxHQURjOztBQUV0QixRQUFJQyxRQUFRLENBQVo7QUFDQSxRQUFJQyxTQUFTSCxLQUFLSSxJQUFMLENBQVVELE1BQXZCOztBQUVBLFFBQUlGLE9BQU9SLFFBQVgsRUFBcUJTLFFBQVFSLFdBQVI7QUFDckIsUUFBSU8sT0FBT04sTUFBWCxFQUFtQlEsU0FBU1AsU0FBVDtBQUNuQixRQUFJSyxPQUFPUixRQUFQLElBQW1CUSxPQUFPTixNQUE5QixFQUFzQ1EsU0FBU1AsWUFBWUYsV0FBckI7O0FBRXRDUixXQUFPc0ksZUFBUCxDQUF1QnZILEdBQXZCLEVBQTRCQyxLQUE1QixFQUFtQ0MsTUFBbkMsRUFBMkNmLElBQTNDLEVBQWlELEVBQUVKLG9CQUFGLEVBQWpEO0FBQ0QsR0FWRDtBQVdELENBcEJEOztBQXNCQTs7Ozs7Ozs7OztBQVVBRixRQUFRMkksZUFBUixHQUEwQixVQUFDdkksTUFBRCxFQUFTQyxLQUFULEVBQWdCdUksVUFBaEIsRUFBNkM7QUFBQSxNQUFqQnJJLE9BQWlCLHVFQUFQLEVBQU87QUFBQSw0QkFDeENBLE9BRHdDLENBQzdETCxTQUQ2RDtBQUFBLE1BQzdEQSxTQUQ2RCx1Q0FDakQsSUFEaUQ7QUFBQSxNQUU3RE8sS0FGNkQsR0FFbkRMLE1BRm1ELENBRTdESyxLQUY2RDtBQUFBLE1BRzdEQyxRQUg2RCxHQUdoREQsS0FIZ0QsQ0FHN0RDLFFBSDZEOztBQUlyRSxNQUFNeUcsU0FBU3pHLFNBQVNtSSxnQkFBVCxDQUEwQnhJLEtBQTFCLENBQWY7O0FBRUE4RyxTQUFPbEcsT0FBUCxDQUFlLFVBQUM2RCxLQUFELEVBQVc7QUFDeEIxRSxXQUFPMEksWUFBUCxDQUFvQmhFLE1BQU0zRCxHQUExQixFQUErQnlILFVBQS9CLEVBQTJDLEVBQUUxSSxvQkFBRixFQUEzQztBQUNELEdBRkQ7QUFHRCxDQVREOztBQVdBOzs7Ozs7Ozs7O0FBVUFGLFFBQVErSSxnQkFBUixHQUEyQixVQUFDM0ksTUFBRCxFQUFTQyxLQUFULEVBQWdCdUksVUFBaEIsRUFBNkM7QUFBQSxNQUFqQnJJLE9BQWlCLHVFQUFQLEVBQU87QUFBQSw2QkFDekNBLE9BRHlDLENBQzlETCxTQUQ4RDtBQUFBLE1BQzlEQSxTQUQ4RCx3Q0FDbEQsSUFEa0Q7QUFBQSxNQUU5RE8sS0FGOEQsR0FFcERMLE1BRm9ELENBRTlESyxLQUY4RDtBQUFBLE1BRzlEQyxRQUg4RCxHQUdqREQsS0FIaUQsQ0FHOURDLFFBSDhEOztBQUl0RSxNQUFNc0ksVUFBVXRJLFNBQVN1SSxpQkFBVCxDQUEyQjVJLEtBQTNCLENBQWhCOztBQUVBMkksVUFBUS9ILE9BQVIsQ0FBZ0IsVUFBQytELE1BQUQsRUFBWTtBQUMxQjVFLFdBQU8wSSxZQUFQLENBQW9COUQsT0FBTzdELEdBQTNCLEVBQWdDeUgsVUFBaEMsRUFBNEMsRUFBRTFJLG9CQUFGLEVBQTVDO0FBQ0QsR0FGRDtBQUdELENBVEQ7O0FBV0E7Ozs7Ozs7Ozs7QUFVQUYsUUFBUWtKLGlCQUFSLEdBQTRCLFVBQUM5SSxNQUFELEVBQVNDLEtBQVQsRUFBNkM7QUFBQSxNQUE3QjhJLE1BQTZCLHVFQUFwQixDQUFvQjtBQUFBLE1BQWpCNUksT0FBaUIsdUVBQVAsRUFBTztBQUFBLDZCQUMxQ0EsT0FEMEMsQ0FDL0RMLFNBRCtEO0FBQUEsTUFDL0RBLFNBRCtELHdDQUNuRCxJQURtRDs7O0FBR3ZFLE1BQUlHLE1BQU13RSxVQUFWLEVBQXNCO0FBQ3BCekUsV0FBT29CLGFBQVAsQ0FBcUJuQixLQUFyQixFQUE0QixFQUFFSCxvQkFBRixFQUE1QjtBQUNBRyxZQUFRQSxNQUFNb0csZUFBTixFQUFSO0FBQ0Q7O0FBTnNFLGdCQVFyQ3BHLEtBUnFDO0FBQUEsTUFRL0RNLFFBUitELFdBUS9EQSxRQVIrRDtBQUFBLE1BUXJEQyxXQVJxRCxXQVFyREEsV0FScUQ7QUFBQSxNQVMvREgsS0FUK0QsR0FTckRMLE1BVHFELENBUy9ESyxLQVQrRDtBQUFBLE1BVS9EQyxRQVYrRCxHQVVsREQsS0FWa0QsQ0FVL0RDLFFBVitEOztBQVd2RSxNQUFJUSxPQUFPUixTQUFTMEgsZ0JBQVQsQ0FBMEJ6SCxRQUExQixDQUFYO0FBQ0EsTUFBSWlGLFNBQVNsRixTQUFTZ0QsZUFBVCxDQUF5QnhDLEtBQUtDLEdBQTlCLENBQWI7QUFDQSxNQUFJaUksSUFBSSxDQUFSOztBQUVBLFNBQU94RCxVQUFVQSxPQUFPOUMsSUFBUCxJQUFlLE9BQXpCLElBQW9Dc0csSUFBSUQsTUFBL0MsRUFBdUQ7QUFDckRqSSxXQUFPMEUsTUFBUDtBQUNBQSxhQUFTbEYsU0FBU2dELGVBQVQsQ0FBeUJrQyxPQUFPekUsR0FBaEMsQ0FBVDtBQUNBaUk7QUFDRDs7QUFFRGhKLFNBQU80QyxxQkFBUCxDQUE2QjlCLEtBQUtDLEdBQWxDLEVBQXVDUixRQUF2QyxFQUFpREMsV0FBakQsRUFBOEQsRUFBRVYsb0JBQUYsRUFBOUQ7QUFDRCxDQXRCRDs7QUF3QkE7Ozs7Ozs7Ozs7QUFVQUYsUUFBUXFKLGtCQUFSLEdBQTZCLFVBQUNqSixNQUFELEVBQVNDLEtBQVQsRUFBb0Q7QUFBQSxNQUFwQzhJLE1BQW9DLHVFQUEzQkcsUUFBMkI7QUFBQSxNQUFqQi9JLE9BQWlCLHVFQUFQLEVBQU87QUFBQSw2QkFDbERBLE9BRGtELENBQ3ZFTCxTQUR1RTtBQUFBLE1BQ3ZFQSxTQUR1RSx3Q0FDM0QsSUFEMkQ7OztBQUcvRSxNQUFJRyxNQUFNd0UsVUFBVixFQUFzQjtBQUNwQnpFLFdBQU9vQixhQUFQLENBQXFCbkIsS0FBckIsRUFBNEIsRUFBRUgsb0JBQUYsRUFBNUI7QUFDQUcsWUFBUUEsTUFBTW9HLGVBQU4sRUFBUjtBQUNEOztBQU44RSxnQkFRN0NwRyxLQVI2QztBQUFBLE1BUXZFTSxRQVJ1RSxXQVF2RUEsUUFSdUU7QUFBQSxNQVE3REMsV0FSNkQsV0FRN0RBLFdBUjZEO0FBQUEsTUFTdkVILEtBVHVFLEdBUzdETCxNQVQ2RCxDQVN2RUssS0FUdUU7QUFBQSxNQVV2RUMsUUFWdUUsR0FVMURELEtBVjBELENBVXZFQyxRQVZ1RTs7QUFXL0UsTUFBSVEsT0FBT1IsU0FBUzBILGdCQUFULENBQTBCekgsUUFBMUIsQ0FBWDtBQUNBLE1BQUlpRixTQUFTbEYsU0FBU3VFLGdCQUFULENBQTBCL0QsS0FBS0MsR0FBL0IsQ0FBYjtBQUNBLE1BQUlpSSxJQUFJLENBQVI7O0FBRUEsU0FBT3hELFVBQVVBLE9BQU85QyxJQUFQLElBQWUsUUFBekIsSUFBcUNzRyxJQUFJRCxNQUFoRCxFQUF3RDtBQUN0RGpJLFdBQU8wRSxNQUFQO0FBQ0FBLGFBQVNsRixTQUFTdUUsZ0JBQVQsQ0FBMEJXLE9BQU96RSxHQUFqQyxDQUFUO0FBQ0FpSTtBQUNEOztBQUVEaEosU0FBTzRDLHFCQUFQLENBQTZCOUIsS0FBS0MsR0FBbEMsRUFBdUNSLFFBQXZDLEVBQWlEQyxXQUFqRCxFQUE4RCxFQUFFVixvQkFBRixFQUE5RDtBQUNELENBdEJEOztBQXdCQTs7Ozs7Ozs7Ozs7QUFXQUYsUUFBUXVKLGlCQUFSLEdBQTRCLFVBQUNuSixNQUFELEVBQVNDLEtBQVQsRUFBZ0JDLElBQWhCLEVBQXVDO0FBQUEsTUFBakJDLE9BQWlCLHVFQUFQLEVBQU87O0FBQ2pFLE1BQUlGLE1BQU1HLFdBQVYsRUFBdUI7O0FBRXZCRixTQUFPLG9CQUFVQSxJQUFWLENBQWVBLElBQWYsQ0FBUDs7QUFIaUUsNkJBS3BDQyxPQUxvQyxDQUt6REwsU0FMeUQ7QUFBQSxNQUt6REEsU0FMeUQsd0NBSzdDLElBTDZDO0FBQUEsTUFNekRPLEtBTnlELEdBTS9DTCxNQU4rQyxDQU16REssS0FOeUQ7QUFBQSxNQU96REMsUUFQeUQsR0FPNUNELEtBUDRDLENBT3pEQyxRQVB5RDs7QUFRakUsTUFBTTRILFFBQVE1SCxTQUFTOEksZUFBVCxDQUF5Qm5KLEtBQXpCLENBQWQ7QUFDQSxNQUFNb0osU0FBU25CLE1BQU1vQixJQUFOLENBQVc7QUFBQSxXQUFLQyxFQUFFQyxNQUFGLENBQVN0SixJQUFULENBQUw7QUFBQSxHQUFYLENBQWY7O0FBRUEsTUFBSW1KLE1BQUosRUFBWTtBQUNWckosV0FBT3FJLGlCQUFQLENBQXlCcEksS0FBekIsRUFBZ0NDLElBQWhDLEVBQXNDLEVBQUVKLG9CQUFGLEVBQXRDO0FBQ0QsR0FGRCxNQUVPO0FBQ0xFLFdBQU9ELGNBQVAsQ0FBc0JFLEtBQXRCLEVBQTZCQyxJQUE3QixFQUFtQyxFQUFFSixvQkFBRixFQUFuQztBQUNEO0FBQ0YsQ0FoQkQ7O0FBa0JBOzs7Ozs7Ozs7O0FBVUFGLFFBQVE2SixrQkFBUixHQUE2QixVQUFDekosTUFBRCxFQUFTQyxLQUFULEVBQWdCdUksVUFBaEIsRUFBNkM7QUFBQSxNQUFqQnJJLE9BQWlCLHVFQUFQLEVBQU87O0FBQ3hFcUksZUFBYSxvQkFBVWtCLGNBQVYsQ0FBeUJsQixVQUF6QixDQUFiOztBQUR3RSw2QkFHM0NySSxPQUgyQyxDQUdoRUwsU0FIZ0U7QUFBQSxNQUdoRUEsU0FIZ0Usd0NBR3BELElBSG9EO0FBQUEsTUFJbEVPLEtBSmtFLEdBSXhETCxNQUp3RCxDQUlsRUssS0FKa0U7QUFBQSxnQkFLckRBLEtBTHFEO0FBQUEsTUFLbEVDLFFBTGtFLFdBS2xFQSxRQUxrRTs7QUFNeEUsTUFBTXlHLFNBQVN6RyxTQUFTbUksZ0JBQVQsQ0FBMEJ4SSxLQUExQixDQUFmO0FBQ0EsTUFBTTBKLFdBQVc1QyxPQUNkNkMsR0FEYyxDQUNWLFVBQUNsRixLQUFELEVBQVc7QUFDZCxXQUFPcEUsU0FBU3VKLFVBQVQsQ0FBb0JuRixNQUFNM0QsR0FBMUIsRUFBK0IsVUFBQ3lFLE1BQUQsRUFBWTtBQUNoRCxVQUFJQSxPQUFPOUMsSUFBUCxJQUFlLE9BQW5CLEVBQTRCLE9BQU8sS0FBUDtBQUM1QixVQUFJOEYsV0FBV3NCLElBQVgsSUFBbUIsSUFBbkIsSUFBMkJ0RSxPQUFPc0UsSUFBUCxJQUFldEIsV0FBV3NCLElBQXpELEVBQStELE9BQU8sS0FBUDtBQUMvRCxVQUFJdEIsV0FBVzVHLE1BQVgsSUFBcUIsSUFBckIsSUFBNkI0RCxPQUFPNUQsTUFBUCxJQUFpQjRHLFdBQVc1RyxNQUE3RCxFQUFxRSxPQUFPLEtBQVA7QUFDckUsVUFBSTRHLFdBQVd1QixJQUFYLElBQW1CLElBQW5CLElBQTJCLENBQUN2RSxPQUFPdUUsSUFBUCxDQUFZQyxVQUFaLENBQXVCeEIsV0FBV3VCLElBQWxDLENBQWhDLEVBQXlFLE9BQU8sS0FBUDtBQUN6RSxhQUFPLElBQVA7QUFDRCxLQU5NLENBQVA7QUFPRCxHQVRjLEVBVWRFLE1BVmMsQ0FVUDtBQUFBLFdBQVVaLE1BQVY7QUFBQSxHQVZPLEVBV2RhLFlBWGMsR0FZZEMsTUFaYyxFQUFqQjs7QUFjQVIsV0FBUzlJLE9BQVQsQ0FBaUIsVUFBQzZELEtBQUQsRUFBVztBQUMxQixRQUFNdEMsUUFBUXNDLE1BQU1wQyxLQUFOLENBQVlGLEtBQVosRUFBZDtBQUNBLFFBQU0rRSxPQUFPekMsTUFBTXBDLEtBQU4sQ0FBWTZFLElBQVosRUFBYjtBQUNBLFFBQU0zQixTQUFTbEYsU0FBU2dHLFNBQVQsQ0FBbUI1QixNQUFNM0QsR0FBekIsQ0FBZjtBQUNBLFFBQU1DLFFBQVF3RSxPQUFPbEQsS0FBUCxDQUFhUSxPQUFiLENBQXFCNEIsS0FBckIsQ0FBZDs7QUFFQSxRQUFNMEYsV0FBVzFGLE1BQU1wQyxLQUFOLENBQVkySCxNQUFaLENBQW1CLFVBQUM3RyxLQUFELEVBQVc7QUFDN0MsYUFBTzJELE9BQU91QyxJQUFQLENBQVk7QUFBQSxlQUFLbEcsU0FBU2lILENBQVQsSUFBY2pILE1BQU1rSCxhQUFOLENBQW9CRCxFQUFFdEosR0FBdEIsQ0FBbkI7QUFBQSxPQUFaLENBQVA7QUFDRCxLQUZnQixDQUFqQjs7QUFJQSxRQUFNd0osYUFBYUgsU0FBU2hJLEtBQVQsRUFBbkI7QUFDQSxRQUFNb0ksWUFBWUosU0FBU2pELElBQVQsRUFBbEI7O0FBRUEsUUFBSS9FLFNBQVNtSSxVQUFULElBQXVCcEQsUUFBUXFELFNBQW5DLEVBQThDO0FBQzVDOUYsWUFBTXBDLEtBQU4sQ0FBWXpCLE9BQVosQ0FBb0IsVUFBQ3VDLEtBQUQsRUFBUUksQ0FBUixFQUFjO0FBQ2hDeEQsZUFBTzJELGFBQVAsQ0FBcUJQLE1BQU1yQyxHQUEzQixFQUFnQ3lFLE9BQU96RSxHQUF2QyxFQUE0Q0MsUUFBUXdDLENBQXBELEVBQXVEM0QsSUFBdkQ7QUFDRCxPQUZEOztBQUlBRyxhQUFPa0MsZUFBUCxDQUF1QndDLE1BQU0zRCxHQUE3QixFQUFrQ2xCLElBQWxDO0FBQ0QsS0FORCxNQVFLLElBQUlzSCxRQUFRcUQsU0FBWixFQUF1QjtBQUMxQjlGLFlBQU1wQyxLQUFOLENBQ0dxRixTQURILENBQ2E7QUFBQSxlQUFLeEQsS0FBS29HLFVBQVY7QUFBQSxPQURiLEVBRUcxSixPQUZILENBRVcsVUFBQ3VDLEtBQUQsRUFBUUksQ0FBUixFQUFjO0FBQ3JCeEQsZUFBTzJELGFBQVAsQ0FBcUJQLE1BQU1yQyxHQUEzQixFQUFnQ3lFLE9BQU96RSxHQUF2QyxFQUE0Q0MsUUFBUSxDQUFSLEdBQVl3QyxDQUF4RCxFQUEyRDNELElBQTNEO0FBQ0QsT0FKSDtBQUtELEtBTkksTUFRQSxJQUFJdUMsU0FBU21JLFVBQWIsRUFBeUI7QUFDNUI3RixZQUFNcEMsS0FBTixDQUNHbUksU0FESCxDQUNhO0FBQUEsZUFBS3RHLEtBQUtxRyxTQUFWO0FBQUEsT0FEYixFQUVHRSxJQUZILENBRVFGLFNBRlIsRUFHRzNKLE9BSEgsQ0FHVyxVQUFDdUMsS0FBRCxFQUFRSSxDQUFSLEVBQWM7QUFDckJ4RCxlQUFPMkQsYUFBUCxDQUFxQlAsTUFBTXJDLEdBQTNCLEVBQWdDeUUsT0FBT3pFLEdBQXZDLEVBQTRDQyxRQUFRd0MsQ0FBcEQsRUFBdUQzRCxJQUF2RDtBQUNELE9BTEg7QUFNRCxLQVBJLE1BU0E7QUFDSCxVQUFNOEssWUFBWUosV0FBV0ssWUFBWCxFQUFsQjtBQUNBNUssYUFBTzRDLHFCQUFQLENBQTZCOEIsTUFBTTNELEdBQW5DLEVBQXdDNEosVUFBVTVKLEdBQWxELEVBQXVELENBQXZELEVBQTBEbEIsSUFBMUQ7QUFDQVEsY0FBUUwsT0FBT0ssS0FBZjtBQUNBQyxpQkFBV0QsTUFBTUMsUUFBakI7O0FBRUE4SixlQUFTdkosT0FBVCxDQUFpQixVQUFDdUMsS0FBRCxFQUFRSSxDQUFSLEVBQWM7QUFDN0IsWUFBSUEsS0FBSyxDQUFULEVBQVk7QUFDVixjQUFNK0MsUUFBUW5ELEtBQWQ7QUFDQUEsa0JBQVE5QyxTQUFTdUssWUFBVCxDQUFzQnpILE1BQU1yQyxHQUE1QixDQUFSO0FBQ0FmLGlCQUFPa0MsZUFBUCxDQUF1QnFFLE1BQU14RixHQUE3QixFQUFrQ2xCLElBQWxDO0FBQ0Q7O0FBRURHLGVBQU8yRCxhQUFQLENBQXFCUCxNQUFNckMsR0FBM0IsRUFBZ0N5RSxPQUFPekUsR0FBdkMsRUFBNENDLFFBQVEsQ0FBUixHQUFZd0MsQ0FBeEQsRUFBMkQzRCxJQUEzRDtBQUNELE9BUkQ7QUFTRDtBQUNGLEdBdEREOztBQXdEQTtBQUNBLE1BQUlDLFNBQUosRUFBZTtBQUNiRSxXQUFPOEssaUJBQVA7QUFDRDtBQUNGLENBakZEOztBQW1GQTs7Ozs7Ozs7OztBQVVBbEwsUUFBUW1MLG1CQUFSLEdBQThCLFVBQUMvSyxNQUFELEVBQVNDLEtBQVQsRUFBZ0J1SSxVQUFoQixFQUE2QztBQUFBLE1BQWpCckksT0FBaUIsdUVBQVAsRUFBTzs7QUFDekVxSSxlQUFhLG9CQUFVa0IsY0FBVixDQUF5QmxCLFVBQXpCLENBQWI7O0FBRHlFLDZCQUc1Q3JJLE9BSDRDLENBR2pFTCxTQUhpRTtBQUFBLE1BR2pFQSxTQUhpRSx3Q0FHckQsSUFIcUQ7QUFBQSxNQUlqRU8sS0FKaUUsR0FJdkRMLE1BSnVELENBSWpFSyxLQUppRTtBQUFBLE1BS2pFQyxRQUxpRSxHQUtwREQsS0FMb0QsQ0FLakVDLFFBTGlFOztBQU16RSxNQUFNSyxRQUFRTCxTQUFTTSxlQUFULENBQXlCWCxLQUF6QixDQUFkO0FBQ0EsTUFBTTJJLFVBQVVqSSxNQUNiaUosR0FEYSxDQUNULFVBQUMxSSxJQUFELEVBQVU7QUFDYixXQUFPWixTQUFTdUosVUFBVCxDQUFvQjNJLEtBQUtILEdBQXpCLEVBQThCLFVBQUN5RSxNQUFELEVBQVk7QUFDL0MsVUFBSUEsT0FBTzlDLElBQVAsSUFBZSxRQUFuQixFQUE2QixPQUFPLEtBQVA7QUFDN0IsVUFBSThGLFdBQVdzQixJQUFYLElBQW1CLElBQW5CLElBQTJCdEUsT0FBT3NFLElBQVAsSUFBZXRCLFdBQVdzQixJQUF6RCxFQUErRCxPQUFPLEtBQVA7QUFDL0QsVUFBSXRCLFdBQVc1RyxNQUFYLElBQXFCLElBQXJCLElBQTZCNEQsT0FBTzVELE1BQVAsSUFBaUI0RyxXQUFXNUcsTUFBN0QsRUFBcUUsT0FBTyxLQUFQO0FBQ3JFLFVBQUk0RyxXQUFXdUIsSUFBWCxJQUFtQixJQUFuQixJQUEyQixDQUFDdkUsT0FBT3VFLElBQVAsQ0FBWUMsVUFBWixDQUF1QnhCLFdBQVd1QixJQUFsQyxDQUFoQyxFQUF5RSxPQUFPLEtBQVA7QUFDekUsYUFBTyxJQUFQO0FBQ0QsS0FOTSxDQUFQO0FBT0QsR0FUYSxFQVViRSxNQVZhLENBVU47QUFBQSxXQUFVWixNQUFWO0FBQUEsR0FWTSxFQVdiYSxZQVhhLEdBWWJDLE1BWmEsRUFBaEI7O0FBY0F2QixVQUFRL0gsT0FBUixDQUFnQixVQUFDK0QsTUFBRCxFQUFZO0FBQzFCLFFBQU1ZLFNBQVN4RixPQUFPSyxLQUFQLENBQWFDLFFBQWIsQ0FBc0JnRyxTQUF0QixDQUFnQzFCLE9BQU83RCxHQUF2QyxDQUFmO0FBQ0EsUUFBTUMsUUFBUXdFLE9BQU9sRCxLQUFQLENBQWFRLE9BQWIsQ0FBcUI4QixNQUFyQixDQUFkOztBQUVBQSxXQUFPdEMsS0FBUCxDQUFhekIsT0FBYixDQUFxQixVQUFDdUMsS0FBRCxFQUFRSSxDQUFSLEVBQWM7QUFDakN4RCxhQUFPMkQsYUFBUCxDQUFxQlAsTUFBTXJDLEdBQTNCLEVBQWdDeUUsT0FBT3pFLEdBQXZDLEVBQTRDQyxRQUFRd0MsQ0FBcEQsRUFBdUQzRCxJQUF2RDtBQUNELEtBRkQ7QUFHRCxHQVBEOztBQVNBO0FBQ0EsTUFBSUMsU0FBSixFQUFlO0FBQ2JFLFdBQU84SyxpQkFBUDtBQUNEO0FBQ0YsQ0FsQ0Q7O0FBb0NBOzs7Ozs7Ozs7O0FBVUFsTCxRQUFRb0wsZ0JBQVIsR0FBMkIsVUFBQ2hMLE1BQUQsRUFBU0MsS0FBVCxFQUFnQnlFLEtBQWhCLEVBQXdDO0FBQUEsTUFBakJ2RSxPQUFpQix1RUFBUCxFQUFPOztBQUNqRXVFLFVBQVEsb0JBQVVBLEtBQVYsQ0FBZ0JBLEtBQWhCLENBQVI7QUFDQUEsVUFBUUEsTUFBTXVHLEdBQU4sQ0FBVSxPQUFWLEVBQW1CdkcsTUFBTXBDLEtBQU4sQ0FBWTRJLEtBQVosRUFBbkIsQ0FBUjs7QUFGaUUsNkJBSXBDL0ssT0FKb0MsQ0FJekRMLFNBSnlEO0FBQUEsTUFJekRBLFNBSnlELHdDQUk3QyxJQUo2QztBQUFBLE1BS3pETyxLQUx5RCxHQUsvQ0wsTUFMK0MsQ0FLekRLLEtBTHlEO0FBQUEsTUFNekRDLFFBTnlELEdBTTVDRCxLQU40QyxDQU16REMsUUFOeUQ7OztBQVFqRSxNQUFNeUcsU0FBU3pHLFNBQVNtSSxnQkFBVCxDQUEwQnhJLEtBQTFCLENBQWY7QUFDQSxNQUFNa0wsYUFBYXBFLE9BQU8zRSxLQUFQLEVBQW5CO0FBQ0EsTUFBTWdKLFlBQVlyRSxPQUFPSSxJQUFQLEVBQWxCO0FBQ0EsTUFBSTNCLGVBQUo7QUFBQSxNQUFZNkYsaUJBQVo7QUFBQSxNQUFzQnJLLGNBQXRCOztBQUVBO0FBQ0E7QUFDQSxNQUFJK0YsT0FBTzlGLE1BQVAsS0FBa0IsQ0FBdEIsRUFBeUI7QUFDdkJ1RSxhQUFTbEYsU0FBU2dHLFNBQVQsQ0FBbUI2RSxXQUFXcEssR0FBOUIsQ0FBVDtBQUNBc0ssZUFBV3RFLE1BQVg7QUFDRDs7QUFFRDtBQUxBLE9BTUs7QUFDSHZCLGVBQVNsRixTQUFTdUosVUFBVCxDQUFvQnNCLFdBQVdwSyxHQUEvQixFQUFvQyxVQUFDdUssRUFBRCxFQUFRO0FBQ25ELGVBQU8sQ0FBQyxDQUFDaEwsU0FBU3VKLFVBQVQsQ0FBb0J1QixVQUFVckssR0FBOUIsRUFBbUM7QUFBQSxpQkFBTXVLLE1BQU1DLEVBQVo7QUFBQSxTQUFuQyxDQUFUO0FBQ0QsT0FGUSxDQUFUO0FBR0Q7O0FBRUQ7QUFDQSxNQUFJL0YsVUFBVSxJQUFkLEVBQW9CQSxTQUFTbEYsUUFBVDs7QUFFcEI7QUFDQTtBQUNBLE1BQUkrSyxZQUFZLElBQWhCLEVBQXNCO0FBQ3BCLFFBQU1HLFVBQVVoRyxPQUFPbEQsS0FBUCxDQUFhbUosTUFBYixDQUFvQixVQUFDQyxHQUFELEVBQU01SyxJQUFOLEVBQVkwQyxDQUFaLEVBQWtCO0FBQ3BELFVBQUkxQyxRQUFRcUssVUFBUixJQUFzQnJLLEtBQUt3SixhQUFMLENBQW1CYSxXQUFXcEssR0FBOUIsQ0FBMUIsRUFBOEQySyxJQUFJLENBQUosSUFBU2xJLENBQVQ7QUFDOUQsVUFBSTFDLFFBQVFzSyxTQUFSLElBQXFCdEssS0FBS3dKLGFBQUwsQ0FBbUJjLFVBQVVySyxHQUE3QixDQUF6QixFQUE0RDJLLElBQUksQ0FBSixJQUFTbEksQ0FBVDtBQUM1RCxhQUFPa0ksR0FBUDtBQUNELEtBSmUsRUFJYixFQUphLENBQWhCOztBQU1BMUssWUFBUXdLLFFBQVEsQ0FBUixDQUFSO0FBQ0FILGVBQVc3RixPQUFPbEQsS0FBUCxDQUFhVyxLQUFiLENBQW1CdUksUUFBUSxDQUFSLENBQW5CLEVBQStCQSxRQUFRLENBQVIsSUFBYSxDQUE1QyxDQUFYO0FBQ0Q7O0FBRUQ7QUFDQSxNQUFJeEssU0FBUyxJQUFiLEVBQW1CO0FBQ2pCQSxZQUFRd0UsT0FBT2xELEtBQVAsQ0FBYVEsT0FBYixDQUFxQnVJLFNBQVNqSixLQUFULEVBQXJCLENBQVI7QUFDRDs7QUFFRDtBQUNBcEMsU0FBT3dHLGVBQVAsQ0FBdUJoQixPQUFPekUsR0FBOUIsRUFBbUNDLEtBQW5DLEVBQTBDMEQsS0FBMUMsRUFBaUQ3RSxJQUFqRDs7QUFFQTtBQUNBd0wsV0FBU3hLLE9BQVQsQ0FBaUIsVUFBQ0MsSUFBRCxFQUFPMEMsQ0FBUCxFQUFhO0FBQzVCeEQsV0FBTzJELGFBQVAsQ0FBcUI3QyxLQUFLQyxHQUExQixFQUErQjJELE1BQU0zRCxHQUFyQyxFQUEwQ3lDLENBQTFDLEVBQTZDM0QsSUFBN0M7QUFDRCxHQUZEOztBQUlBLE1BQUlDLFNBQUosRUFBZTtBQUNiRSxXQUFPOEQsa0JBQVAsQ0FBMEIwQixPQUFPekUsR0FBakM7QUFDRDtBQUNGLENBM0REOztBQTZEQTs7Ozs7Ozs7OztBQVVBbkIsUUFBUStMLGlCQUFSLEdBQTRCLFVBQUMzTCxNQUFELEVBQVNDLEtBQVQsRUFBZ0IyRSxNQUFoQixFQUF5QztBQUFBLE1BQWpCekUsT0FBaUIsdUVBQVAsRUFBTztBQUFBLE1BQzdERSxLQUQ2RCxHQUNuREwsTUFEbUQsQ0FDN0RLLEtBRDZEO0FBQUEsZ0JBRWhEQSxLQUZnRDtBQUFBLE1BRTdEQyxRQUY2RCxXQUU3REEsUUFGNkQ7QUFBQSw2QkFHdENILE9BSHNDLENBRzNETCxTQUgyRDtBQUFBLE1BRzNEQSxTQUgyRCx3Q0FHL0MsSUFIK0M7QUFBQSxNQUkzRFMsUUFKMkQsR0FJZE4sS0FKYyxDQUkzRE0sUUFKMkQ7QUFBQSxNQUlqREMsV0FKaUQsR0FJZFAsS0FKYyxDQUlqRE8sV0FKaUQ7QUFBQSxNQUlwQ0MsTUFKb0MsR0FJZFIsS0FKYyxDQUlwQ1EsTUFKb0M7QUFBQSxNQUk1QkMsU0FKNEIsR0FJZFQsS0FKYyxDQUk1QlMsU0FKNEI7OztBQU1uRSxNQUFJVCxNQUFNRyxXQUFWLEVBQXVCO0FBQ3JCO0FBQ0EsUUFBTXdMLGVBQWV0TCxTQUFTdUUsZ0JBQVQsQ0FBMEJ0RSxRQUExQixDQUFyQjtBQUNBLFFBQUksQ0FBQ3FMLGFBQWFoSyxNQUFsQixFQUEwQjtBQUN4QjtBQUNEOztBQUVELFdBQU81QixPQUFPNkwsZUFBUCxDQUF1QkQsYUFBYTdLLEdBQXBDLEVBQXlDNkQsTUFBekMsRUFBaUR6RSxPQUFqRCxDQUFQO0FBQ0Q7O0FBRUR5RSxXQUFTLG9CQUFVQSxNQUFWLENBQWlCQSxNQUFqQixDQUFUO0FBQ0FBLFdBQVNBLE9BQU9xRyxHQUFQLENBQVcsT0FBWCxFQUFvQnJHLE9BQU90QyxLQUFQLENBQWE0SSxLQUFiLEVBQXBCLENBQVQ7O0FBRUEsTUFBTW5FLFNBQVN6RyxTQUFTbUksZ0JBQVQsQ0FBMEJ4SSxLQUExQixDQUFmO0FBQ0EsTUFBSW9ELGFBQWEvQyxTQUFTZ0QsZUFBVCxDQUF5Qi9DLFFBQXpCLENBQWpCO0FBQ0EsTUFBSWdELFdBQVdqRCxTQUFTZ0QsZUFBVCxDQUF5QjdDLE1BQXpCLENBQWY7QUFDQSxNQUFJZSxhQUFhNkIsV0FBVzVCLG1CQUFYLENBQStCbEIsUUFBL0IsQ0FBakI7QUFDQSxNQUFJbUIsV0FBVzZCLFNBQVM5QixtQkFBVCxDQUE2QmhCLE1BQTdCLENBQWY7O0FBRUFULFNBQU80QyxxQkFBUCxDQUE2QmxCLFNBQVNYLEdBQXRDLEVBQTJDTixNQUEzQyxFQUFtREMsU0FBbkQsRUFBOERiLElBQTlEO0FBQ0FHLFNBQU80QyxxQkFBUCxDQUE2QnBCLFdBQVdULEdBQXhDLEVBQTZDUixRQUE3QyxFQUF1REMsV0FBdkQsRUFBb0VYLElBQXBFOztBQUVBUSxVQUFRTCxPQUFPSyxLQUFmO0FBQ0FDLGFBQVdELE1BQU1DLFFBQWpCO0FBQ0ErQyxlQUFhL0MsU0FBU3lFLGFBQVQsQ0FBdUIxQixXQUFXdEMsR0FBbEMsQ0FBYjtBQUNBd0MsYUFBV2pELFNBQVN5RSxhQUFULENBQXVCeEIsU0FBU3hDLEdBQWhDLENBQVg7QUFDQVMsZUFBYTZCLFdBQVc1QixtQkFBWCxDQUErQmxCLFFBQS9CLENBQWI7QUFDQW1CLGFBQVc2QixTQUFTOUIsbUJBQVQsQ0FBNkJoQixNQUE3QixDQUFYO0FBQ0EsTUFBTW9DLGFBQWFRLFdBQVdmLEtBQVgsQ0FBaUJRLE9BQWpCLENBQXlCdEIsVUFBekIsQ0FBbkI7QUFDQSxNQUFNdUIsV0FBV1EsU0FBU2pCLEtBQVQsQ0FBZVEsT0FBZixDQUF1QnBCLFFBQXZCLENBQWpCOztBQUVBLE1BQUkyQixjQUFjRSxRQUFsQixFQUE0QjtBQUMxQmxELFlBQVFMLE9BQU9LLEtBQWY7QUFDQUMsZUFBV0QsTUFBTUMsUUFBakI7QUFDQStDLGlCQUFhL0MsU0FBU2dELGVBQVQsQ0FBeUIvQyxRQUF6QixDQUFiO0FBQ0FpQixpQkFBYTZCLFdBQVc1QixtQkFBWCxDQUErQmxCLFFBQS9CLENBQWI7O0FBRUEsUUFBTXVMLGFBQWF4TCxTQUFTMkIsY0FBVCxDQUF3QlQsV0FBV1QsR0FBbkMsQ0FBbkI7QUFDQSxRQUFNZ0wsa0JBQWtCMUksV0FBV2YsS0FBWCxDQUFpQlEsT0FBakIsQ0FBeUJnSixVQUF6QixDQUF4QjtBQUNBLFFBQU1FLFdBQVd6TCxZQUFZRSxNQUFaLEdBQXFCcUwsVUFBckIsR0FBa0N6SSxXQUFXNUIsbUJBQVgsQ0FBK0JoQixNQUEvQixDQUFuRDtBQUNBLFFBQU1tSSxVQUFVdkYsV0FBV2YsS0FBWCxDQUNicUYsU0FEYSxDQUNIO0FBQUEsYUFBS3hELEtBQUsySCxVQUFWO0FBQUEsS0FERyxFQUVickIsU0FGYSxDQUVIO0FBQUEsYUFBS3RHLEtBQUs2SCxRQUFWO0FBQUEsS0FGRyxFQUdidEIsSUFIYSxDQUdSc0IsUUFIUSxDQUFoQjs7QUFLQSxRQUFNbEwsT0FBTzhELE9BQU9nQyxhQUFQLEVBQWI7O0FBRUE1RyxXQUFPd0csZUFBUCxDQUF1Qm5ELFdBQVd0QyxHQUFsQyxFQUF1Q2dMLGVBQXZDLEVBQXdEakwsSUFBeEQsRUFBOERqQixJQUE5RDs7QUFFQStJLFlBQVEvSCxPQUFSLENBQWdCLFVBQUN1QyxLQUFELEVBQVFJLENBQVIsRUFBYztBQUM1QnhELGFBQU8yRCxhQUFQLENBQXFCUCxNQUFNckMsR0FBM0IsRUFBZ0NELEtBQUtDLEdBQXJDLEVBQTBDeUMsQ0FBMUMsRUFBNkMzRCxJQUE3QztBQUNELEtBRkQ7O0FBSUEsUUFBSUMsU0FBSixFQUFlO0FBQ2JFLGFBQU84RCxrQkFBUCxDQUEwQlQsV0FBV3RDLEdBQXJDO0FBQ0Q7QUFDRixHQXpCRCxNQTJCSztBQUNILFFBQU1rTCxlQUFlNUksV0FBV2YsS0FBWCxDQUFpQlcsS0FBakIsQ0FBdUJKLGFBQWEsQ0FBcEMsQ0FBckI7QUFDQSxRQUFNcUosYUFBYTNJLFNBQVNqQixLQUFULENBQWVXLEtBQWYsQ0FBcUIsQ0FBckIsRUFBd0JGLFdBQVcsQ0FBbkMsQ0FBbkI7QUFDQSxRQUFNb0osWUFBWXZILE9BQU9nQyxhQUFQLEVBQWxCO0FBQ0EsUUFBTXdGLFVBQVV4SCxPQUFPZ0MsYUFBUCxFQUFoQjs7QUFFQTVHLFdBQU93RyxlQUFQLENBQXVCbkQsV0FBV3RDLEdBQWxDLEVBQXVDOEIsYUFBYSxDQUFwRCxFQUF1RHNKLFNBQXZELEVBQWtFdE0sSUFBbEU7QUFDQUcsV0FBT3dHLGVBQVAsQ0FBdUJqRCxTQUFTeEMsR0FBaEMsRUFBcUNnQyxRQUFyQyxFQUErQ3FKLE9BQS9DLEVBQXdEdk0sSUFBeEQ7O0FBRUFvTSxpQkFBYXBMLE9BQWIsQ0FBcUIsVUFBQ3VDLEtBQUQsRUFBUUksQ0FBUixFQUFjO0FBQ2pDeEQsYUFBTzJELGFBQVAsQ0FBcUJQLE1BQU1yQyxHQUEzQixFQUFnQ29MLFVBQVVwTCxHQUExQyxFQUErQ3lDLENBQS9DLEVBQWtEM0QsSUFBbEQ7QUFDRCxLQUZEOztBQUlBcU0sZUFBV3JMLE9BQVgsQ0FBbUIsVUFBQ3VDLEtBQUQsRUFBUUksQ0FBUixFQUFjO0FBQy9CeEQsYUFBTzJELGFBQVAsQ0FBcUJQLE1BQU1yQyxHQUEzQixFQUFnQ3FMLFFBQVFyTCxHQUF4QyxFQUE2Q3lDLENBQTdDLEVBQWdEM0QsSUFBaEQ7QUFDRCxLQUZEOztBQUlBLFFBQUlDLFNBQUosRUFBZTtBQUNiRSxhQUNHOEQsa0JBREgsQ0FDc0JULFdBQVd0QyxHQURqQyxrQkFFRytDLGtCQUZILENBRXNCUCxTQUFTeEMsR0FGL0I7QUFHRDs7QUFFRGdHLFdBQU85RCxLQUFQLENBQWEsQ0FBYixFQUFnQixDQUFDLENBQWpCLEVBQW9CcEMsT0FBcEIsQ0FBNEIsVUFBQzZELEtBQUQsRUFBVztBQUNyQyxVQUFNNUQsT0FBTzhELE9BQU9nQyxhQUFQLEVBQWI7QUFDQTVHLGFBQU93RyxlQUFQLENBQXVCOUIsTUFBTTNELEdBQTdCLEVBQWtDLENBQWxDLEVBQXFDRCxJQUFyQyxFQUEyQ2pCLElBQTNDOztBQUVBNkUsWUFBTXBDLEtBQU4sQ0FBWXpCLE9BQVosQ0FBb0IsVUFBQ3VDLEtBQUQsRUFBUUksQ0FBUixFQUFjO0FBQ2hDeEQsZUFBTzJELGFBQVAsQ0FBcUJQLE1BQU1yQyxHQUEzQixFQUFnQ0QsS0FBS0MsR0FBckMsRUFBMEN5QyxDQUExQyxFQUE2QzNELElBQTdDO0FBQ0QsT0FGRDs7QUFJQSxVQUFJQyxTQUFKLEVBQWU7QUFDYkUsZUFBTzhELGtCQUFQLENBQTBCWSxNQUFNM0QsR0FBaEM7QUFDRDtBQUNGLEtBWEQ7QUFZRDtBQUNGLENBcEdEOztBQXNHQTs7Ozs7Ozs7Ozs7QUFXQW5CLFFBQVF5TSxlQUFSLEdBQTBCLFVBQUNyTSxNQUFELEVBQVNDLEtBQVQsRUFBZ0JxTSxNQUFoQixFQUEwRDtBQUFBLE1BQWxDQyxNQUFrQyx1RUFBekJELE1BQXlCO0FBQUEsTUFBakJuTSxPQUFpQix1RUFBUCxFQUFPO0FBQUEsNkJBQ3JEQSxPQURxRCxDQUMxRUwsU0FEMEU7QUFBQSxNQUMxRUEsU0FEMEUsd0NBQzlELElBRDhEO0FBQUEsTUFFMUVTLFFBRjBFLEdBRXJETixLQUZxRCxDQUUxRU0sUUFGMEU7QUFBQSxNQUVoRUUsTUFGZ0UsR0FFckRSLEtBRnFELENBRWhFUSxNQUZnRTs7QUFHbEYsTUFBTStMLFFBQVF2TSxNQUFNb0csZUFBTixFQUFkO0FBQ0EsTUFBSW9HLE1BQU14TSxNQUFNeU0sYUFBTixFQUFWOztBQUVBLE1BQUluTSxZQUFZRSxNQUFoQixFQUF3QjtBQUN0QmdNLFVBQU1BLElBQUlFLElBQUosQ0FBU0wsT0FBT3JMLE1BQWhCLENBQU47QUFDRDs7QUFFRGpCLFNBQU9pSSxpQkFBUCxDQUF5QnVFLEtBQXpCLEVBQWdDRixNQUFoQyxFQUF3QyxFQUF4QyxFQUE0QyxFQUFFeE0sb0JBQUYsRUFBNUM7QUFDQUUsU0FBT2lJLGlCQUFQLENBQXlCd0UsR0FBekIsRUFBOEJGLE1BQTlCLEVBQXNDLEVBQXRDLEVBQTBDLEVBQUV6TSxvQkFBRixFQUExQztBQUNELENBWkQ7O0FBY0E7Ozs7OztrQkFNZUYsTyIsImZpbGUiOiJhdC1yYW5nZS5qcyIsInNvdXJjZXNDb250ZW50IjpbIlxuaW1wb3J0IE5vcm1hbGl6ZSBmcm9tICcuLi91dGlscy9ub3JtYWxpemUnXG5pbXBvcnQgU3RyaW5nIGZyb20gJy4uL3V0aWxzL3N0cmluZydcbmltcG9ydCBTQ0hFTUEgZnJvbSAnLi4vc2NoZW1hcy9jb3JlJ1xuaW1wb3J0IHsgTGlzdCB9IGZyb20gJ2ltbXV0YWJsZSdcblxuLyoqXG4gKiBDaGFuZ2VzLlxuICpcbiAqIEB0eXBlIHtPYmplY3R9XG4gKi9cblxuY29uc3QgQ2hhbmdlcyA9IHt9XG5cbi8qKlxuICogQW4gb3B0aW9ucyBvYmplY3Qgd2l0aCBub3JtYWxpemUgc2V0IHRvIGBmYWxzZWAuXG4gKlxuICogQHR5cGUge09iamVjdH1cbiAqL1xuXG5jb25zdCBPUFRTID0ge1xuICBub3JtYWxpemU6IGZhbHNlXG59XG5cbi8qKlxuICogQWRkIGEgbmV3IGBtYXJrYCB0byB0aGUgY2hhcmFjdGVycyBhdCBgcmFuZ2VgLlxuICpcbiAqIEBwYXJhbSB7Q2hhbmdlfSBjaGFuZ2VcbiAqIEBwYXJhbSB7U2VsZWN0aW9ufSByYW5nZVxuICogQHBhcmFtIHtNaXhlZH0gbWFya1xuICogQHBhcmFtIHtPYmplY3R9IG9wdGlvbnNcbiAqICAgQHByb3BlcnR5IHtCb29sZWFufSBub3JtYWxpemVcbiAqL1xuXG5DaGFuZ2VzLmFkZE1hcmtBdFJhbmdlID0gKGNoYW5nZSwgcmFuZ2UsIG1hcmssIG9wdGlvbnMgPSB7fSkgPT4ge1xuICBpZiAocmFuZ2UuaXNDb2xsYXBzZWQpIHJldHVyblxuXG4gIGNvbnN0IHsgbm9ybWFsaXplID0gdHJ1ZSB9ID0gb3B0aW9uc1xuICBjb25zdCB7IHN0YXRlIH0gPSBjaGFuZ2VcbiAgY29uc3QgeyBkb2N1bWVudCB9ID0gc3RhdGVcbiAgY29uc3QgeyBzdGFydEtleSwgc3RhcnRPZmZzZXQsIGVuZEtleSwgZW5kT2Zmc2V0IH0gPSByYW5nZVxuICBjb25zdCB0ZXh0cyA9IGRvY3VtZW50LmdldFRleHRzQXRSYW5nZShyYW5nZSlcblxuICB0ZXh0cy5mb3JFYWNoKChub2RlKSA9PiB7XG4gICAgY29uc3QgeyBrZXkgfSA9IG5vZGVcbiAgICBsZXQgaW5kZXggPSAwXG4gICAgbGV0IGxlbmd0aCA9IG5vZGUudGV4dC5sZW5ndGhcblxuICAgIGlmIChrZXkgPT0gc3RhcnRLZXkpIGluZGV4ID0gc3RhcnRPZmZzZXRcbiAgICBpZiAoa2V5ID09IGVuZEtleSkgbGVuZ3RoID0gZW5kT2Zmc2V0XG4gICAgaWYgKGtleSA9PSBzdGFydEtleSAmJiBrZXkgPT0gZW5kS2V5KSBsZW5ndGggPSBlbmRPZmZzZXQgLSBzdGFydE9mZnNldFxuXG4gICAgY2hhbmdlLmFkZE1hcmtCeUtleShrZXksIGluZGV4LCBsZW5ndGgsIG1hcmssIHsgbm9ybWFsaXplIH0pXG4gIH0pXG59XG5cbi8qKlxuICogRGVsZXRlIGV2ZXJ5dGhpbmcgaW4gYSBgcmFuZ2VgLlxuICpcbiAqIEBwYXJhbSB7Q2hhbmdlfSBjaGFuZ2VcbiAqIEBwYXJhbSB7U2VsZWN0aW9ufSByYW5nZVxuICogQHBhcmFtIHtPYmplY3R9IG9wdGlvbnNcbiAqICAgQHByb3BlcnR5IHtCb29sZWFufSBub3JtYWxpemVcbiAqL1xuXG5DaGFuZ2VzLmRlbGV0ZUF0UmFuZ2UgPSAoY2hhbmdlLCByYW5nZSwgb3B0aW9ucyA9IHt9KSA9PiB7XG4gIGlmIChyYW5nZS5pc0NvbGxhcHNlZCkgcmV0dXJuXG5cbiAgY2hhbmdlLnNuYXBzaG90U2VsZWN0aW9uKClcblxuICBjb25zdCB7IG5vcm1hbGl6ZSA9IHRydWUgfSA9IG9wdGlvbnNcbiAgbGV0IHsgc3RhcnRLZXksIHN0YXJ0T2Zmc2V0LCBlbmRLZXksIGVuZE9mZnNldCB9ID0gcmFuZ2VcblxuICAvLyBTcGxpdCBhdCB0aGUgcmFuZ2UgZWRnZXMgd2l0aGluIGEgY29tbW9uIGFuY2VzdG9yLCB3aXRob3V0IG5vcm1hbGl6aW5nLlxuICBsZXQgeyBzdGF0ZSB9ID0gY2hhbmdlXG4gIGxldCB7IGRvY3VtZW50IH0gPSBzdGF0ZVxuICBsZXQgYW5jZXN0b3IgPSBkb2N1bWVudC5nZXRDb21tb25BbmNlc3RvcihzdGFydEtleSwgZW5kS2V5KVxuICBsZXQgc3RhcnRDaGlsZCA9IGFuY2VzdG9yLmdldEZ1cnRoZXN0QW5jZXN0b3Ioc3RhcnRLZXkpXG4gIGxldCBlbmRDaGlsZCA9IGFuY2VzdG9yLmdldEZ1cnRoZXN0QW5jZXN0b3IoZW5kS2V5KVxuXG4gIC8vIElmIHRoZSBzdGFydCBjaGlsZCBpcyBhIHZvaWQgbm9kZSwgYW5kIHRoZSByYW5nZSBiZWdpbnMgb3JcbiAgLy8gZW5kcyAod2hlbiByYW5nZSBpcyBiYWNrd2FyZCkgYXQgdGhlIHN0YXJ0IG9mIGl0LCByZW1vdmUgaXRcbiAgLy8gYW5kIHNldCBuZXh0U2libGluZyBhcyBzdGFydENoaWxkIHVudGlsIHRoZXJlIGlzIG5vIHN0YXJ0Q2hpbGRcbiAgLy8gdGhhdCBpcyBhIHZvaWQgbm9kZSBhbmQgaW5jbHVkZWQgaW4gdGhlIHNlbGVjdGlvbiByYW5nZVxuICBsZXQgc3RhcnRDaGlsZEluY2x1ZGVzVm9pZCA9IHN0YXJ0Q2hpbGQuaXNWb2lkICYmIChcbiAgICByYW5nZS5hbmNob3JPZmZzZXQgPT09IDAgJiYgIXJhbmdlLmlzQmFja3dhcmQgfHxcbiAgICByYW5nZS5mb2N1c09mZnNldCA9PT0gMCAmJiByYW5nZS5pc0JhY2t3YXJkXG4gIClcblxuICB3aGlsZSAoc3RhcnRDaGlsZEluY2x1ZGVzVm9pZCkge1xuICAgIGNvbnN0IG5leHRTaWJsaW5nID0gZG9jdW1lbnQuZ2V0TmV4dFNpYmxpbmcoc3RhcnRDaGlsZC5rZXkpXG4gICAgY2hhbmdlLnJlbW92ZU5vZGVCeUtleShzdGFydENoaWxkLmtleSwgT1BUUylcbiAgICAvLyBBYm9ydCBpZiBubyBuZXh0U2libGluZyBvciB3ZSBhcmUgYWJvdXQgdG8gcHJvY2VzcyB0aGUgZW5kQ2hpbGQgd2hpY2ggaXMgYXNsbyBhIHZvaWQgbm9kZVxuICAgIGlmICghbmV4dFNpYmxpbmcgfHwgZW5kQ2hpbGQua2V5ID09PSBuZXh0U2libGluZy5rZXkgJiYgbmV4dFNpYmxpbmcuaXNWb2lkKSB7XG4gICAgICBzdGFydENoaWxkSW5jbHVkZXNWb2lkID0gZmFsc2VcbiAgICAgIHJldHVyblxuICAgIH1cbiAgICAvLyBQcm9jZXNzIHRoZSBuZXh0IHZvaWRcbiAgICBpZiAobmV4dFNpYmxpbmcuaXNWb2lkKSB7XG4gICAgICBzdGFydENoaWxkID0gbmV4dFNpYmxpbmdcbiAgICB9XG4gICAgLy8gU2V0IHRoZSBzdGFydENoaWxkLCBzdGFydEtleSBhbmQgc3RhcnRPZmZzZXQgaW4gdGhlIGJlZ2lubmluZyBvZiB0aGUgbmV4dCBub24gdm9pZCBzaWJsaW5nXG4gICAgaWYgKCFuZXh0U2libGluZy5pc1ZvaWQpIHtcbiAgICAgIHN0YXJ0Q2hpbGQgPSBuZXh0U2libGluZ1xuICAgICAgaWYgKHN0YXJ0Q2hpbGQuZ2V0VGV4dHMpIHtcbiAgICAgICAgc3RhcnRLZXkgPSBzdGFydENoaWxkLmdldFRleHRzKCkuZmlyc3QoKS5rZXlcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHN0YXJ0S2V5ID0gc3RhcnRDaGlsZC5rZXlcbiAgICAgIH1cbiAgICAgIHN0YXJ0T2Zmc2V0ID0gMFxuICAgICAgc3RhcnRDaGlsZEluY2x1ZGVzVm9pZCA9IGZhbHNlXG4gICAgfVxuICB9XG5cbiAgLy8gSWYgdGhlIHN0YXJ0IGNoaWxkIGlzIGEgdm9pZCBub2RlLCBhbmQgdGhlIHJhbmdlIGVuZHMgb3JcbiAgLy8gYmVnaW5zICh3aGVuIHJhbmdlIGlzIGJhY2t3YXJkKSBhdCB0aGUgZW5kIG9mIGl0IG1vdmUgdG8gbmV4dFNpYmxpbmdcbiAgY29uc3Qgc3RhcnRDaGlsZEVuZE9mVm9pZCA9IHN0YXJ0Q2hpbGQuaXNWb2lkICYmIChcbiAgICByYW5nZS5hbmNob3JPZmZzZXQgPT09IDEgJiYgIXJhbmdlLmlzQmFja3dhcmQgfHxcbiAgICByYW5nZS5mb2N1c09mZnNldCA9PT0gMSAmJiByYW5nZS5pc0JhY2t3YXJkXG4gIClcblxuICBpZiAoc3RhcnRDaGlsZEVuZE9mVm9pZCkge1xuICAgIGNvbnN0IG5leHRTaWJsaW5nID0gZG9jdW1lbnQuZ2V0TmV4dFNpYmxpbmcoc3RhcnRDaGlsZC5rZXkpXG4gICAgaWYgKG5leHRTaWJsaW5nKSB7XG4gICAgICBzdGFydENoaWxkID0gbmV4dFNpYmxpbmdcbiAgICAgIGlmIChzdGFydENoaWxkLmdldFRleHRzKSB7XG4gICAgICAgIHN0YXJ0S2V5ID0gc3RhcnRDaGlsZC5nZXRUZXh0cygpLmZpcnN0KCkua2V5XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBzdGFydEtleSA9IHN0YXJ0Q2hpbGQua2V5XG4gICAgICB9XG4gICAgICBzdGFydE9mZnNldCA9IDBcbiAgICB9XG4gIH1cblxuICAvLyBJZiB0aGUgc3RhcnQgYW5kIGVuZCBrZXkgYXJlIHRoZSBzYW1lLCB3ZSBjYW4ganVzdCByZW1vdmUgaXQuXG4gIGlmIChzdGFydEtleSA9PSBlbmRLZXkpIHtcbiAgICAvLyBJZiBpdCBpcyBhIHZvaWQgbm9kZSwgcmVtb3ZlIHRoZSB3aG9sZSBub2RlXG4gICAgaWYgKGFuY2VzdG9yLmlzVm9pZCkge1xuICAgICAgLy8gRGVzZWxlY3QgaWYgdGhpcyBpcyB0aGUgb25seSBub2RlIGxlZnQgaW4gZG9jdW1lbnRcbiAgICAgIGlmIChkb2N1bWVudC5ub2Rlcy5zaXplID09PSAxKSB7XG4gICAgICAgIGNoYW5nZS5kZXNlbGVjdCgpXG4gICAgICB9XG4gICAgICBjaGFuZ2UucmVtb3ZlTm9kZUJ5S2V5KGFuY2VzdG9yLmtleSwgT1BUUylcbiAgICAgIHJldHVyblxuICAgIH1cbiAgICAvLyBSZW1vdmUgdGhlIHRleHRcbiAgICBjb25zdCBpbmRleCA9IHN0YXJ0T2Zmc2V0XG4gICAgY29uc3QgbGVuZ3RoID0gZW5kT2Zmc2V0IC0gc3RhcnRPZmZzZXRcbiAgICBjaGFuZ2UucmVtb3ZlVGV4dEJ5S2V5KHN0YXJ0S2V5LCBpbmRleCwgbGVuZ3RoLCB7IG5vcm1hbGl6ZSB9KVxuICAgIHJldHVyblxuICB9XG5cbiAgLy8gU3BsaXQgYXQgdGhlIHJhbmdlIGVkZ2VzIHdpdGhpbiBhIGNvbW1vbiBhbmNlc3Rvciwgd2l0aG91dCBub3JtYWxpemluZy5cbiAgc3RhdGUgPSBjaGFuZ2Uuc3RhdGVcbiAgZG9jdW1lbnQgPSBzdGF0ZS5kb2N1bWVudFxuICBhbmNlc3RvciA9IGRvY3VtZW50LmdldENvbW1vbkFuY2VzdG9yKHN0YXJ0S2V5LCBlbmRLZXkpXG4gIHN0YXJ0Q2hpbGQgPSBhbmNlc3Rvci5nZXRGdXJ0aGVzdEFuY2VzdG9yKHN0YXJ0S2V5KVxuICBlbmRDaGlsZCA9IGFuY2VzdG9yLmdldEZ1cnRoZXN0QW5jZXN0b3IoZW5kS2V5KVxuXG4gIGlmIChzdGFydENoaWxkLmtpbmQgPT0gJ3RleHQnKSB7XG4gICAgY2hhbmdlLnNwbGl0Tm9kZUJ5S2V5KHN0YXJ0Q2hpbGQua2V5LCBzdGFydE9mZnNldCwgT1BUUylcbiAgfSBlbHNlIHtcbiAgICBjaGFuZ2Uuc3BsaXREZXNjZW5kYW50c0J5S2V5KHN0YXJ0Q2hpbGQua2V5LCBzdGFydEtleSwgc3RhcnRPZmZzZXQsIE9QVFMpXG4gIH1cblxuICBpZiAoZW5kQ2hpbGQua2luZCA9PSAndGV4dCcpIHtcbiAgICBjaGFuZ2Uuc3BsaXROb2RlQnlLZXkoZW5kQ2hpbGQua2V5LCBlbmRPZmZzZXQsIE9QVFMpXG4gIH0gZWxzZSB7XG4gICAgY2hhbmdlLnNwbGl0RGVzY2VuZGFudHNCeUtleShlbmRDaGlsZC5rZXksIGVuZEtleSwgZW5kT2Zmc2V0LCBPUFRTKVxuICB9XG5cbiAgLy8gUmVmcmVzaCB2YXJpYWJsZXMuXG4gIHN0YXRlID0gY2hhbmdlLnN0YXRlXG4gIGRvY3VtZW50ID0gc3RhdGUuZG9jdW1lbnRcbiAgYW5jZXN0b3IgPSBkb2N1bWVudC5nZXRDb21tb25BbmNlc3RvcihzdGFydEtleSwgZW5kS2V5KVxuICBzdGFydENoaWxkID0gYW5jZXN0b3IuZ2V0RnVydGhlc3RBbmNlc3RvcihzdGFydEtleSlcbiAgZW5kQ2hpbGQgPSBhbmNlc3Rvci5nZXRGdXJ0aGVzdEFuY2VzdG9yKGVuZEtleSlcbiAgY29uc3Qgc3RhcnRJbmRleCA9IGFuY2VzdG9yLm5vZGVzLmluZGV4T2Yoc3RhcnRDaGlsZClcbiAgY29uc3QgZW5kSW5kZXggPSBhbmNlc3Rvci5ub2Rlcy5pbmRleE9mKGVuZENoaWxkKVxuICBjb25zdCBtaWRkbGVzID0gYW5jZXN0b3Iubm9kZXMuc2xpY2Uoc3RhcnRJbmRleCArIDEsIGVuZEluZGV4ICsgMSlcbiAgY29uc3QgbmV4dCA9IGRvY3VtZW50LmdldE5leHRUZXh0KGVuZEtleSlcblxuICAvLyBSZW1vdmUgYWxsIG9mIHRoZSBtaWRkbGUgbm9kZXMsIGJldHdlZW4gdGhlIHNwbGl0cy5cbiAgaWYgKG1pZGRsZXMuc2l6ZSkge1xuICAgIG1pZGRsZXMuZm9yRWFjaCgoY2hpbGQpID0+IHtcbiAgICAgIGNoYW5nZS5yZW1vdmVOb2RlQnlLZXkoY2hpbGQua2V5LCBPUFRTKVxuICAgIH0pXG4gIH1cblxuICAvLyBJZiB0aGUgc3RhcnQgYW5kIGVuZCBibG9jayBhcmUgZGlmZmVyZW50LCBtb3ZlIGFsbCBvZiB0aGUgbm9kZXMgZnJvbSB0aGVcbiAgLy8gZW5kIGJsb2NrIGludG8gdGhlIHN0YXJ0IGJsb2NrLlxuICBzdGF0ZSA9IGNoYW5nZS5zdGF0ZVxuICBkb2N1bWVudCA9IHN0YXRlLmRvY3VtZW50XG4gIGNvbnN0IHN0YXJ0QmxvY2sgPSBkb2N1bWVudC5nZXRDbG9zZXN0QmxvY2soc3RhcnRLZXkpXG4gIGNvbnN0IGVuZEJsb2NrID0gZG9jdW1lbnQuZ2V0Q2xvc2VzdEJsb2NrKG5leHQua2V5KVxuXG4gIC8vIElmIHRoZSBlbmRCbG9jayBpcyB2b2lkLCBqdXN0IHJlbW92ZSB0aGUgc3RhcnRCbG9ja1xuICBpZiAoZW5kQmxvY2suaXNWb2lkKSB7XG4gICAgY2hhbmdlLnJlbW92ZU5vZGVCeUtleShzdGFydEJsb2NrLmtleSlcbiAgICByZXR1cm5cbiAgfVxuXG4gIC8vIElmIHRoZSBzdGFydCBhbmQgZW5kIGJsb2NrIGFyZSBkaWZmZXJlbnQsIG1vdmUgYWxsIG9mIHRoZSBub2RlcyBmcm9tIHRoZVxuICAvLyBlbmQgYmxvY2sgaW50byB0aGUgc3RhcnQgYmxvY2tcbiAgaWYgKHN0YXJ0QmxvY2sua2V5ICE9PSBlbmRCbG9jay5rZXkpIHtcbiAgICBlbmRCbG9jay5ub2Rlcy5mb3JFYWNoKChjaGlsZCwgaSkgPT4ge1xuICAgICAgY29uc3QgbmV3S2V5ID0gc3RhcnRCbG9jay5rZXlcbiAgICAgIGNvbnN0IG5ld0luZGV4ID0gc3RhcnRCbG9jay5ub2Rlcy5zaXplICsgaVxuICAgICAgY2hhbmdlLm1vdmVOb2RlQnlLZXkoY2hpbGQua2V5LCBuZXdLZXksIG5ld0luZGV4LCBPUFRTKVxuICAgIH0pXG5cbiAgICAvLyBSZW1vdmUgcGFyZW50cyBvZiBlbmRCbG9jayBhcyBsb25nIGFzIHRoZXkgaGF2ZSBhIHNpbmdsZSBjaGlsZFxuICAgIGNvbnN0IGxvbmVseSA9IGRvY3VtZW50LmdldEZ1cnRoZXN0T25seUNoaWxkQW5jZXN0b3IoZW5kQmxvY2sua2V5KSB8fCBlbmRCbG9ja1xuICAgIGNoYW5nZS5yZW1vdmVOb2RlQnlLZXkobG9uZWx5LmtleSwgT1BUUylcbiAgfVxuXG4gIGlmIChub3JtYWxpemUpIHtcbiAgICBjaGFuZ2Uubm9ybWFsaXplTm9kZUJ5S2V5KGFuY2VzdG9yLmtleSwgU0NIRU1BKVxuICB9XG59XG5cbi8qKlxuICogRGVsZXRlIGJhY2t3YXJkIHVudGlsIHRoZSBjaGFyYWN0ZXIgYm91bmRhcnkgYXQgYSBgcmFuZ2VgLlxuICpcbiAqIEBwYXJhbSB7Q2hhbmdlfSBjaGFuZ2VcbiAqIEBwYXJhbSB7U2VsZWN0aW9ufSByYW5nZVxuICogQHBhcmFtIHtPYmplY3R9IG9wdGlvbnNcbiAqICAgQHByb3BlcnR5IHtCb29sZWFufSBub3JtYWxpemVcbiAqL1xuXG5DaGFuZ2VzLmRlbGV0ZUNoYXJCYWNrd2FyZEF0UmFuZ2UgPSAoY2hhbmdlLCByYW5nZSwgb3B0aW9ucykgPT4ge1xuICBjb25zdCB7IHN0YXRlIH0gPSBjaGFuZ2VcbiAgY29uc3QgeyBkb2N1bWVudCB9ID0gc3RhdGVcbiAgY29uc3QgeyBzdGFydEtleSwgc3RhcnRPZmZzZXQgfSA9IHJhbmdlXG4gIGNvbnN0IHN0YXJ0QmxvY2sgPSBkb2N1bWVudC5nZXRDbG9zZXN0QmxvY2soc3RhcnRLZXkpXG4gIGNvbnN0IG9mZnNldCA9IHN0YXJ0QmxvY2suZ2V0T2Zmc2V0KHN0YXJ0S2V5KVxuICBjb25zdCBvID0gb2Zmc2V0ICsgc3RhcnRPZmZzZXRcbiAgY29uc3QgeyB0ZXh0IH0gPSBzdGFydEJsb2NrXG4gIGNvbnN0IG4gPSBTdHJpbmcuZ2V0Q2hhck9mZnNldEJhY2t3YXJkKHRleHQsIG8pXG4gIGNoYW5nZS5kZWxldGVCYWNrd2FyZEF0UmFuZ2UocmFuZ2UsIG4sIG9wdGlvbnMpXG59XG5cbi8qKlxuICogRGVsZXRlIGJhY2t3YXJkIHVudGlsIHRoZSBsaW5lIGJvdW5kYXJ5IGF0IGEgYHJhbmdlYC5cbiAqXG4gKiBAcGFyYW0ge0NoYW5nZX0gY2hhbmdlXG4gKiBAcGFyYW0ge1NlbGVjdGlvbn0gcmFuZ2VcbiAqIEBwYXJhbSB7T2JqZWN0fSBvcHRpb25zXG4gKiAgIEBwcm9wZXJ0eSB7Qm9vbGVhbn0gbm9ybWFsaXplXG4gKi9cblxuQ2hhbmdlcy5kZWxldGVMaW5lQmFja3dhcmRBdFJhbmdlID0gKGNoYW5nZSwgcmFuZ2UsIG9wdGlvbnMpID0+IHtcbiAgY29uc3QgeyBzdGF0ZSB9ID0gY2hhbmdlXG4gIGNvbnN0IHsgZG9jdW1lbnQgfSA9IHN0YXRlXG4gIGNvbnN0IHsgc3RhcnRLZXksIHN0YXJ0T2Zmc2V0IH0gPSByYW5nZVxuICBjb25zdCBzdGFydEJsb2NrID0gZG9jdW1lbnQuZ2V0Q2xvc2VzdEJsb2NrKHN0YXJ0S2V5KVxuICBjb25zdCBvZmZzZXQgPSBzdGFydEJsb2NrLmdldE9mZnNldChzdGFydEtleSlcbiAgY29uc3QgbyA9IG9mZnNldCArIHN0YXJ0T2Zmc2V0XG4gIGNoYW5nZS5kZWxldGVCYWNrd2FyZEF0UmFuZ2UocmFuZ2UsIG8sIG9wdGlvbnMpXG59XG5cbi8qKlxuICogRGVsZXRlIGJhY2t3YXJkIHVudGlsIHRoZSB3b3JkIGJvdW5kYXJ5IGF0IGEgYHJhbmdlYC5cbiAqXG4gKiBAcGFyYW0ge0NoYW5nZX0gY2hhbmdlXG4gKiBAcGFyYW0ge1NlbGVjdGlvbn0gcmFuZ2VcbiAqIEBwYXJhbSB7T2JqZWN0fSBvcHRpb25zXG4gKiAgIEBwcm9wZXJ0eSB7Qm9vbGVhbn0gbm9ybWFsaXplXG4gKi9cblxuQ2hhbmdlcy5kZWxldGVXb3JkQmFja3dhcmRBdFJhbmdlID0gKGNoYW5nZSwgcmFuZ2UsIG9wdGlvbnMpID0+IHtcbiAgY29uc3QgeyBzdGF0ZSB9ID0gY2hhbmdlXG4gIGNvbnN0IHsgZG9jdW1lbnQgfSA9IHN0YXRlXG4gIGNvbnN0IHsgc3RhcnRLZXksIHN0YXJ0T2Zmc2V0IH0gPSByYW5nZVxuICBjb25zdCBzdGFydEJsb2NrID0gZG9jdW1lbnQuZ2V0Q2xvc2VzdEJsb2NrKHN0YXJ0S2V5KVxuICBjb25zdCBvZmZzZXQgPSBzdGFydEJsb2NrLmdldE9mZnNldChzdGFydEtleSlcbiAgY29uc3QgbyA9IG9mZnNldCArIHN0YXJ0T2Zmc2V0XG4gIGNvbnN0IHsgdGV4dCB9ID0gc3RhcnRCbG9ja1xuICBjb25zdCBuID0gU3RyaW5nLmdldFdvcmRPZmZzZXRCYWNrd2FyZCh0ZXh0LCBvKVxuICBjaGFuZ2UuZGVsZXRlQmFja3dhcmRBdFJhbmdlKHJhbmdlLCBuLCBvcHRpb25zKVxufVxuXG4vKipcbiAqIERlbGV0ZSBiYWNrd2FyZCBgbmAgY2hhcmFjdGVycyBhdCBhIGByYW5nZWAuXG4gKlxuICogQHBhcmFtIHtDaGFuZ2V9IGNoYW5nZVxuICogQHBhcmFtIHtTZWxlY3Rpb259IHJhbmdlXG4gKiBAcGFyYW0ge051bWJlcn0gbiAob3B0aW9uYWwpXG4gKiBAcGFyYW0ge09iamVjdH0gb3B0aW9uc1xuICogICBAcHJvcGVydHkge0Jvb2xlYW59IG5vcm1hbGl6ZVxuICovXG5cbkNoYW5nZXMuZGVsZXRlQmFja3dhcmRBdFJhbmdlID0gKGNoYW5nZSwgcmFuZ2UsIG4gPSAxLCBvcHRpb25zID0ge30pID0+IHtcbiAgY29uc3QgeyBub3JtYWxpemUgPSB0cnVlIH0gPSBvcHRpb25zXG4gIGNvbnN0IHsgc3RhdGUgfSA9IGNoYW5nZVxuICBjb25zdCB7IGRvY3VtZW50IH0gPSBzdGF0ZVxuICBjb25zdCB7IHN0YXJ0S2V5LCBmb2N1c09mZnNldCB9ID0gcmFuZ2VcblxuICAvLyBJZiB0aGUgcmFuZ2UgaXMgZXhwYW5kZWQsIHBlcmZvcm0gYSByZWd1bGFyIGRlbGV0ZSBpbnN0ZWFkLlxuICBpZiAocmFuZ2UuaXNFeHBhbmRlZCkge1xuICAgIGNoYW5nZS5kZWxldGVBdFJhbmdlKHJhbmdlLCB7IG5vcm1hbGl6ZSB9KVxuICAgIHJldHVyblxuICB9XG5cbiAgY29uc3QgYmxvY2sgPSBkb2N1bWVudC5nZXRDbG9zZXN0QmxvY2soc3RhcnRLZXkpXG4gIC8vIElmIHRoZSBjbG9zZXN0IGJsb2NrIGlzIHZvaWQsIGRlbGV0ZSBpdC5cbiAgaWYgKGJsb2NrICYmIGJsb2NrLmlzVm9pZCkge1xuICAgIGNoYW5nZS5yZW1vdmVOb2RlQnlLZXkoYmxvY2sua2V5LCB7IG5vcm1hbGl6ZSB9KVxuICAgIHJldHVyblxuICB9XG4gIC8vIElmIHRoZSBjbG9zZXN0IGlzIG5vdCB2b2lkLCBidXQgZW1wdHksIHJlbW92ZSBpdFxuICBpZiAoYmxvY2sgJiYgIWJsb2NrLmlzVm9pZCAmJiBibG9jay5pc0VtcHR5ICYmIGRvY3VtZW50Lm5vZGVzLnNpemUgIT09IDEpIHtcbiAgICBjaGFuZ2UucmVtb3ZlTm9kZUJ5S2V5KGJsb2NrLmtleSwgeyBub3JtYWxpemUgfSlcbiAgICByZXR1cm5cbiAgfVxuXG4gIC8vIElmIHRoZSBjbG9zZXN0IGlubGluZSBpcyB2b2lkLCBkZWxldGUgaXQuXG4gIGNvbnN0IGlubGluZSA9IGRvY3VtZW50LmdldENsb3Nlc3RJbmxpbmUoc3RhcnRLZXkpXG4gIGlmIChpbmxpbmUgJiYgaW5saW5lLmlzVm9pZCkge1xuICAgIGNoYW5nZS5yZW1vdmVOb2RlQnlLZXkoaW5saW5lLmtleSwgeyBub3JtYWxpemUgfSlcbiAgICByZXR1cm5cbiAgfVxuXG4gIC8vIElmIHRoZSByYW5nZSBpcyBhdCB0aGUgc3RhcnQgb2YgdGhlIGRvY3VtZW50LCBhYm9ydC5cbiAgaWYgKHJhbmdlLmlzQXRTdGFydE9mKGRvY3VtZW50KSkge1xuICAgIHJldHVyblxuICB9XG5cbiAgLy8gSWYgdGhlIHJhbmdlIGlzIGF0IHRoZSBzdGFydCBvZiB0aGUgdGV4dCBub2RlLCB3ZSBuZWVkIHRvIGZpZ3VyZSBvdXQgd2hhdFxuICAvLyBpcyBiZWhpbmQgaXQgdG8ga25vdyBob3cgdG8gZGVsZXRlLi4uXG4gIGNvbnN0IHRleHQgPSBkb2N1bWVudC5nZXREZXNjZW5kYW50KHN0YXJ0S2V5KVxuICBpZiAocmFuZ2UuaXNBdFN0YXJ0T2YodGV4dCkpIHtcbiAgICBjb25zdCBwcmV2ID0gZG9jdW1lbnQuZ2V0UHJldmlvdXNUZXh0KHRleHQua2V5KVxuICAgIGNvbnN0IHByZXZCbG9jayA9IGRvY3VtZW50LmdldENsb3Nlc3RCbG9jayhwcmV2LmtleSlcbiAgICBjb25zdCBwcmV2SW5saW5lID0gZG9jdW1lbnQuZ2V0Q2xvc2VzdElubGluZShwcmV2LmtleSlcblxuICAgIC8vIElmIHRoZSBwcmV2aW91cyBibG9jayBpcyB2b2lkLCByZW1vdmUgaXQuXG4gICAgaWYgKHByZXZCbG9jayAmJiBwcmV2QmxvY2suaXNWb2lkKSB7XG4gICAgICBjaGFuZ2UucmVtb3ZlTm9kZUJ5S2V5KHByZXZCbG9jay5rZXksIHsgbm9ybWFsaXplIH0pXG4gICAgICByZXR1cm5cbiAgICB9XG5cbiAgICAvLyBJZiB0aGUgcHJldmlvdXMgaW5saW5lIGlzIHZvaWQsIHJlbW92ZSBpdC5cbiAgICBpZiAocHJldklubGluZSAmJiBwcmV2SW5saW5lLmlzVm9pZCkge1xuICAgICAgY2hhbmdlLnJlbW92ZU5vZGVCeUtleShwcmV2SW5saW5lLmtleSwgeyBub3JtYWxpemUgfSlcbiAgICAgIHJldHVyblxuICAgIH1cblxuICAgIC8vIElmIHdlJ3JlIGRlbGV0aW5nIGJ5IG9uZSBjaGFyYWN0ZXIgYW5kIHRoZSBwcmV2aW91cyB0ZXh0IG5vZGUgaXMgbm90XG4gICAgLy8gaW5zaWRlIHRoZSBjdXJyZW50IGJsb2NrLCB3ZSBuZWVkIHRvIG1lcmdlIHRoZSB0d28gYmxvY2tzIHRvZ2V0aGVyLlxuICAgIGlmIChuID09IDEgJiYgcHJldkJsb2NrICE9IGJsb2NrKSB7XG4gICAgICByYW5nZSA9IHJhbmdlLm1lcmdlKHtcbiAgICAgICAgYW5jaG9yS2V5OiBwcmV2LmtleSxcbiAgICAgICAgYW5jaG9yT2Zmc2V0OiBwcmV2LnRleHQubGVuZ3RoLFxuICAgICAgfSlcblxuICAgICAgY2hhbmdlLmRlbGV0ZUF0UmFuZ2UocmFuZ2UsIHsgbm9ybWFsaXplIH0pXG4gICAgICByZXR1cm5cbiAgICB9XG4gIH1cblxuICAvLyBJZiB0aGUgZm9jdXMgb2Zmc2V0IGlzIGZhcnRoZXIgdGhhbiB0aGUgbnVtYmVyIG9mIGNoYXJhY3RlcnMgdG8gZGVsZXRlLFxuICAvLyBqdXN0IHJlbW92ZSB0aGUgY2hhcmFjdGVycyBiYWNrd2FyZHMgaW5zaWRlIHRoZSBjdXJyZW50IG5vZGUuXG4gIGlmIChuIDwgZm9jdXNPZmZzZXQpIHtcbiAgICByYW5nZSA9IHJhbmdlLm1lcmdlKHtcbiAgICAgIGZvY3VzT2Zmc2V0OiBmb2N1c09mZnNldCAtIG4sXG4gICAgICBpc0JhY2t3YXJkOiB0cnVlLFxuICAgIH0pXG5cbiAgICBjaGFuZ2UuZGVsZXRlQXRSYW5nZShyYW5nZSwgeyBub3JtYWxpemUgfSlcbiAgICByZXR1cm5cbiAgfVxuXG4gIC8vIE90aGVyd2lzZSwgd2UgbmVlZCB0byBzZWUgaG93IG1hbnkgbm9kZXMgYmFja3dhcmRzIHRvIGdvLlxuICBsZXQgbm9kZSA9IHRleHRcbiAgbGV0IG9mZnNldCA9IDBcbiAgbGV0IHRyYXZlcnNlZCA9IGZvY3VzT2Zmc2V0XG5cbiAgd2hpbGUgKG4gPiB0cmF2ZXJzZWQpIHtcbiAgICBub2RlID0gZG9jdW1lbnQuZ2V0UHJldmlvdXNUZXh0KG5vZGUua2V5KVxuICAgIGNvbnN0IG5leHQgPSB0cmF2ZXJzZWQgKyBub2RlLnRleHQubGVuZ3RoXG4gICAgaWYgKG4gPD0gbmV4dCkge1xuICAgICAgb2Zmc2V0ID0gbmV4dCAtIG5cbiAgICAgIGJyZWFrXG4gICAgfSBlbHNlIHtcbiAgICAgIHRyYXZlcnNlZCA9IG5leHRcbiAgICB9XG4gIH1cblxuICAvLyBJZiB0aGUgZm9jdXMgbm9kZSBpcyBpbnNpZGUgYSB2b2lkLCBnbyB1cCB1bnRpbCByaWdodCBhZnRlciBpdC5cbiAgaWYgKGRvY3VtZW50Lmhhc1ZvaWRQYXJlbnQobm9kZS5rZXkpKSB7XG4gICAgY29uc3QgcGFyZW50ID0gZG9jdW1lbnQuZ2V0Q2xvc2VzdFZvaWQobm9kZS5rZXkpXG4gICAgbm9kZSA9IGRvY3VtZW50LmdldE5leHRUZXh0KHBhcmVudC5rZXkpXG4gICAgb2Zmc2V0ID0gMFxuICB9XG5cbiAgcmFuZ2UgPSByYW5nZS5tZXJnZSh7XG4gICAgZm9jdXNLZXk6IG5vZGUua2V5LFxuICAgIGZvY3VzT2Zmc2V0OiBvZmZzZXQsXG4gICAgaXNCYWNrd2FyZDogdHJ1ZVxuICB9KVxuXG4gIGNoYW5nZS5kZWxldGVBdFJhbmdlKHJhbmdlLCB7IG5vcm1hbGl6ZSB9KVxufVxuXG4vKipcbiAqIERlbGV0ZSBmb3J3YXJkIHVudGlsIHRoZSBjaGFyYWN0ZXIgYm91bmRhcnkgYXQgYSBgcmFuZ2VgLlxuICpcbiAqIEBwYXJhbSB7Q2hhbmdlfSBjaGFuZ2VcbiAqIEBwYXJhbSB7U2VsZWN0aW9ufSByYW5nZVxuICogQHBhcmFtIHtPYmplY3R9IG9wdGlvbnNcbiAqICAgQHByb3BlcnR5IHtCb29sZWFufSBub3JtYWxpemVcbiAqL1xuXG5DaGFuZ2VzLmRlbGV0ZUNoYXJGb3J3YXJkQXRSYW5nZSA9IChjaGFuZ2UsIHJhbmdlLCBvcHRpb25zKSA9PiB7XG4gIGNvbnN0IHsgc3RhdGUgfSA9IGNoYW5nZVxuICBjb25zdCB7IGRvY3VtZW50IH0gPSBzdGF0ZVxuICBjb25zdCB7IHN0YXJ0S2V5LCBzdGFydE9mZnNldCB9ID0gcmFuZ2VcbiAgY29uc3Qgc3RhcnRCbG9jayA9IGRvY3VtZW50LmdldENsb3Nlc3RCbG9jayhzdGFydEtleSlcbiAgY29uc3Qgb2Zmc2V0ID0gc3RhcnRCbG9jay5nZXRPZmZzZXQoc3RhcnRLZXkpXG4gIGNvbnN0IG8gPSBvZmZzZXQgKyBzdGFydE9mZnNldFxuICBjb25zdCB7IHRleHQgfSA9IHN0YXJ0QmxvY2tcbiAgY29uc3QgbiA9IFN0cmluZy5nZXRDaGFyT2Zmc2V0Rm9yd2FyZCh0ZXh0LCBvKVxuICBjaGFuZ2UuZGVsZXRlRm9yd2FyZEF0UmFuZ2UocmFuZ2UsIG4sIG9wdGlvbnMpXG59XG5cbi8qKlxuICogRGVsZXRlIGZvcndhcmQgdW50aWwgdGhlIGxpbmUgYm91bmRhcnkgYXQgYSBgcmFuZ2VgLlxuICpcbiAqIEBwYXJhbSB7Q2hhbmdlfSBjaGFuZ2VcbiAqIEBwYXJhbSB7U2VsZWN0aW9ufSByYW5nZVxuICogQHBhcmFtIHtPYmplY3R9IG9wdGlvbnNcbiAqICAgQHByb3BlcnR5IHtCb29sZWFufSBub3JtYWxpemVcbiAqL1xuXG5DaGFuZ2VzLmRlbGV0ZUxpbmVGb3J3YXJkQXRSYW5nZSA9IChjaGFuZ2UsIHJhbmdlLCBvcHRpb25zKSA9PiB7XG4gIGNvbnN0IHsgc3RhdGUgfSA9IGNoYW5nZVxuICBjb25zdCB7IGRvY3VtZW50IH0gPSBzdGF0ZVxuICBjb25zdCB7IHN0YXJ0S2V5LCBzdGFydE9mZnNldCB9ID0gcmFuZ2VcbiAgY29uc3Qgc3RhcnRCbG9jayA9IGRvY3VtZW50LmdldENsb3Nlc3RCbG9jayhzdGFydEtleSlcbiAgY29uc3Qgb2Zmc2V0ID0gc3RhcnRCbG9jay5nZXRPZmZzZXQoc3RhcnRLZXkpXG4gIGNvbnN0IG8gPSBvZmZzZXQgKyBzdGFydE9mZnNldFxuICBjaGFuZ2UuZGVsZXRlRm9yd2FyZEF0UmFuZ2UocmFuZ2UsIG8sIG9wdGlvbnMpXG59XG5cbi8qKlxuICogRGVsZXRlIGZvcndhcmQgdW50aWwgdGhlIHdvcmQgYm91bmRhcnkgYXQgYSBgcmFuZ2VgLlxuICpcbiAqIEBwYXJhbSB7Q2hhbmdlfSBjaGFuZ2VcbiAqIEBwYXJhbSB7U2VsZWN0aW9ufSByYW5nZVxuICogQHBhcmFtIHtPYmplY3R9IG9wdGlvbnNcbiAqICAgQHByb3BlcnR5IHtCb29sZWFufSBub3JtYWxpemVcbiAqL1xuXG5DaGFuZ2VzLmRlbGV0ZVdvcmRGb3J3YXJkQXRSYW5nZSA9IChjaGFuZ2UsIHJhbmdlLCBvcHRpb25zKSA9PiB7XG4gIGNvbnN0IHsgc3RhdGUgfSA9IGNoYW5nZVxuICBjb25zdCB7IGRvY3VtZW50IH0gPSBzdGF0ZVxuICBjb25zdCB7IHN0YXJ0S2V5LCBzdGFydE9mZnNldCB9ID0gcmFuZ2VcbiAgY29uc3Qgc3RhcnRCbG9jayA9IGRvY3VtZW50LmdldENsb3Nlc3RCbG9jayhzdGFydEtleSlcbiAgY29uc3Qgb2Zmc2V0ID0gc3RhcnRCbG9jay5nZXRPZmZzZXQoc3RhcnRLZXkpXG4gIGNvbnN0IG8gPSBvZmZzZXQgKyBzdGFydE9mZnNldFxuICBjb25zdCB7IHRleHQgfSA9IHN0YXJ0QmxvY2tcbiAgY29uc3QgbiA9IFN0cmluZy5nZXRXb3JkT2Zmc2V0Rm9yd2FyZCh0ZXh0LCBvKVxuICBjaGFuZ2UuZGVsZXRlRm9yd2FyZEF0UmFuZ2UocmFuZ2UsIG4sIG9wdGlvbnMpXG59XG5cbi8qKlxuICogRGVsZXRlIGZvcndhcmQgYG5gIGNoYXJhY3RlcnMgYXQgYSBgcmFuZ2VgLlxuICpcbiAqIEBwYXJhbSB7Q2hhbmdlfSBjaGFuZ2VcbiAqIEBwYXJhbSB7U2VsZWN0aW9ufSByYW5nZVxuICogQHBhcmFtIHtOdW1iZXJ9IG4gKG9wdGlvbmFsKVxuICogQHBhcmFtIHtPYmplY3R9IG9wdGlvbnNcbiAqICAgQHByb3BlcnR5IHtCb29sZWFufSBub3JtYWxpemVcbiAqL1xuXG5DaGFuZ2VzLmRlbGV0ZUZvcndhcmRBdFJhbmdlID0gKGNoYW5nZSwgcmFuZ2UsIG4gPSAxLCBvcHRpb25zID0ge30pID0+IHtcbiAgY29uc3QgeyBub3JtYWxpemUgPSB0cnVlIH0gPSBvcHRpb25zXG4gIGNvbnN0IHsgc3RhdGUgfSA9IGNoYW5nZVxuICBjb25zdCB7IGRvY3VtZW50IH0gPSBzdGF0ZVxuICBjb25zdCB7IHN0YXJ0S2V5LCBmb2N1c09mZnNldCB9ID0gcmFuZ2VcblxuICAvLyBJZiB0aGUgcmFuZ2UgaXMgZXhwYW5kZWQsIHBlcmZvcm0gYSByZWd1bGFyIGRlbGV0ZSBpbnN0ZWFkLlxuICBpZiAocmFuZ2UuaXNFeHBhbmRlZCkge1xuICAgIGNoYW5nZS5kZWxldGVBdFJhbmdlKHJhbmdlLCB7IG5vcm1hbGl6ZSB9KVxuICAgIHJldHVyblxuICB9XG5cbiAgY29uc3QgYmxvY2sgPSBkb2N1bWVudC5nZXRDbG9zZXN0QmxvY2soc3RhcnRLZXkpXG4gIC8vIElmIHRoZSBjbG9zZXN0IGJsb2NrIGlzIHZvaWQsIGRlbGV0ZSBpdC5cbiAgaWYgKGJsb2NrICYmIGJsb2NrLmlzVm9pZCkge1xuICAgIGNoYW5nZS5yZW1vdmVOb2RlQnlLZXkoYmxvY2sua2V5LCB7IG5vcm1hbGl6ZSB9KVxuICAgIHJldHVyblxuICB9XG4gIC8vIElmIHRoZSBjbG9zZXN0IGlzIG5vdCB2b2lkLCBidXQgZW1wdHksIHJlbW92ZSBpdFxuICBpZiAoYmxvY2sgJiYgIWJsb2NrLmlzVm9pZCAmJiBibG9jay5pc0VtcHR5ICYmIGRvY3VtZW50Lm5vZGVzLnNpemUgIT09IDEpIHtcbiAgICBjaGFuZ2UucmVtb3ZlTm9kZUJ5S2V5KGJsb2NrLmtleSwgeyBub3JtYWxpemUgfSlcbiAgICByZXR1cm5cbiAgfVxuXG4gIC8vIElmIHRoZSBjbG9zZXN0IGlubGluZSBpcyB2b2lkLCBkZWxldGUgaXQuXG4gIGNvbnN0IGlubGluZSA9IGRvY3VtZW50LmdldENsb3Nlc3RJbmxpbmUoc3RhcnRLZXkpXG4gIGlmIChpbmxpbmUgJiYgaW5saW5lLmlzVm9pZCkge1xuICAgIGNoYW5nZS5yZW1vdmVOb2RlQnlLZXkoaW5saW5lLmtleSwgeyBub3JtYWxpemUgfSlcbiAgICByZXR1cm5cbiAgfVxuXG4gIC8vIElmIHRoZSByYW5nZSBpcyBhdCB0aGUgc3RhcnQgb2YgdGhlIGRvY3VtZW50LCBhYm9ydC5cbiAgaWYgKHJhbmdlLmlzQXRFbmRPZihkb2N1bWVudCkpIHtcbiAgICByZXR1cm5cbiAgfVxuXG4gIC8vIElmIHRoZSByYW5nZSBpcyBhdCB0aGUgc3RhcnQgb2YgdGhlIHRleHQgbm9kZSwgd2UgbmVlZCB0byBmaWd1cmUgb3V0IHdoYXRcbiAgLy8gaXMgYmVoaW5kIGl0IHRvIGtub3cgaG93IHRvIGRlbGV0ZS4uLlxuICBjb25zdCB0ZXh0ID0gZG9jdW1lbnQuZ2V0RGVzY2VuZGFudChzdGFydEtleSlcbiAgaWYgKHJhbmdlLmlzQXRFbmRPZih0ZXh0KSkge1xuICAgIGNvbnN0IG5leHQgPSBkb2N1bWVudC5nZXROZXh0VGV4dCh0ZXh0LmtleSlcbiAgICBjb25zdCBuZXh0QmxvY2sgPSBkb2N1bWVudC5nZXRDbG9zZXN0QmxvY2sobmV4dC5rZXkpXG4gICAgY29uc3QgbmV4dElubGluZSA9IGRvY3VtZW50LmdldENsb3Nlc3RJbmxpbmUobmV4dC5rZXkpXG5cbiAgICAvLyBJZiB0aGUgcHJldmlvdXMgYmxvY2sgaXMgdm9pZCwgcmVtb3ZlIGl0LlxuICAgIGlmIChuZXh0QmxvY2sgJiYgbmV4dEJsb2NrLmlzVm9pZCkge1xuICAgICAgY2hhbmdlLnJlbW92ZU5vZGVCeUtleShuZXh0QmxvY2sua2V5LCB7IG5vcm1hbGl6ZSB9KVxuICAgICAgcmV0dXJuXG4gICAgfVxuXG4gICAgLy8gSWYgdGhlIHByZXZpb3VzIGlubGluZSBpcyB2b2lkLCByZW1vdmUgaXQuXG4gICAgaWYgKG5leHRJbmxpbmUgJiYgbmV4dElubGluZS5pc1ZvaWQpIHtcbiAgICAgIGNoYW5nZS5yZW1vdmVOb2RlQnlLZXkobmV4dElubGluZS5rZXksIHsgbm9ybWFsaXplIH0pXG4gICAgICByZXR1cm5cbiAgICB9XG5cbiAgICAvLyBJZiB3ZSdyZSBkZWxldGluZyBieSBvbmUgY2hhcmFjdGVyIGFuZCB0aGUgcHJldmlvdXMgdGV4dCBub2RlIGlzIG5vdFxuICAgIC8vIGluc2lkZSB0aGUgY3VycmVudCBibG9jaywgd2UgbmVlZCB0byBtZXJnZSB0aGUgdHdvIGJsb2NrcyB0b2dldGhlci5cbiAgICBpZiAobiA9PSAxICYmIG5leHRCbG9jayAhPSBibG9jaykge1xuICAgICAgcmFuZ2UgPSByYW5nZS5tZXJnZSh7XG4gICAgICAgIGZvY3VzS2V5OiBuZXh0LmtleSxcbiAgICAgICAgZm9jdXNPZmZzZXQ6IDBcbiAgICAgIH0pXG5cbiAgICAgIGNoYW5nZS5kZWxldGVBdFJhbmdlKHJhbmdlLCB7IG5vcm1hbGl6ZSB9KVxuICAgICAgcmV0dXJuXG4gICAgfVxuICB9XG5cbiAgLy8gSWYgdGhlIHJlbWFpbmluZyBjaGFyYWN0ZXJzIHRvIHRoZSBlbmQgb2YgdGhlIG5vZGUgaXMgZ3JlYXRlciB0aGFuIG9yIGVxdWFsXG4gIC8vIHRvIHRoZSBudW1iZXIgb2YgY2hhcmFjdGVycyB0byBkZWxldGUsIGp1c3QgcmVtb3ZlIHRoZSBjaGFyYWN0ZXJzIGZvcndhcmRzXG4gIC8vIGluc2lkZSB0aGUgY3VycmVudCBub2RlLlxuICBpZiAobiA8PSAodGV4dC50ZXh0Lmxlbmd0aCAtIGZvY3VzT2Zmc2V0KSkge1xuICAgIHJhbmdlID0gcmFuZ2UubWVyZ2Uoe1xuICAgICAgZm9jdXNPZmZzZXQ6IGZvY3VzT2Zmc2V0ICsgblxuICAgIH0pXG5cbiAgICBjaGFuZ2UuZGVsZXRlQXRSYW5nZShyYW5nZSwgeyBub3JtYWxpemUgfSlcbiAgICByZXR1cm5cbiAgfVxuXG4gIC8vIE90aGVyd2lzZSwgd2UgbmVlZCB0byBzZWUgaG93IG1hbnkgbm9kZXMgZm9yd2FyZHMgdG8gZ28uXG4gIGxldCBub2RlID0gdGV4dFxuICBsZXQgb2Zmc2V0ID0gZm9jdXNPZmZzZXRcbiAgbGV0IHRyYXZlcnNlZCA9IHRleHQudGV4dC5sZW5ndGggLSBmb2N1c09mZnNldFxuXG4gIHdoaWxlIChuID4gdHJhdmVyc2VkKSB7XG4gICAgbm9kZSA9IGRvY3VtZW50LmdldE5leHRUZXh0KG5vZGUua2V5KVxuICAgIGNvbnN0IG5leHQgPSB0cmF2ZXJzZWQgKyBub2RlLnRleHQubGVuZ3RoXG4gICAgaWYgKG4gPD0gbmV4dCkge1xuICAgICAgb2Zmc2V0ID0gbiAtIHRyYXZlcnNlZFxuICAgICAgYnJlYWtcbiAgICB9IGVsc2Uge1xuICAgICAgdHJhdmVyc2VkID0gbmV4dFxuICAgIH1cbiAgfVxuXG4gIC8vIElmIHRoZSBmb2N1cyBub2RlIGlzIGluc2lkZSBhIHZvaWQsIGdvIHVwIHVudGlsIHJpZ2h0IGJlZm9yZSBpdC5cbiAgaWYgKGRvY3VtZW50Lmhhc1ZvaWRQYXJlbnQobm9kZS5rZXkpKSB7XG4gICAgY29uc3QgcGFyZW50ID0gZG9jdW1lbnQuZ2V0Q2xvc2VzdFZvaWQobm9kZS5rZXkpXG4gICAgbm9kZSA9IGRvY3VtZW50LmdldFByZXZpb3VzVGV4dChwYXJlbnQua2V5KVxuICAgIG9mZnNldCA9IG5vZGUudGV4dC5sZW5ndGhcbiAgfVxuXG4gIHJhbmdlID0gcmFuZ2UubWVyZ2Uoe1xuICAgIGZvY3VzS2V5OiBub2RlLmtleSxcbiAgICBmb2N1c09mZnNldDogb2Zmc2V0LFxuICB9KVxuXG4gIGNoYW5nZS5kZWxldGVBdFJhbmdlKHJhbmdlLCB7IG5vcm1hbGl6ZSB9KVxufVxuXG4vKipcbiAqIEluc2VydCBhIGBibG9ja2Agbm9kZSBhdCBgcmFuZ2VgLlxuICpcbiAqIEBwYXJhbSB7Q2hhbmdlfSBjaGFuZ2VcbiAqIEBwYXJhbSB7U2VsZWN0aW9ufSByYW5nZVxuICogQHBhcmFtIHtCbG9ja3xTdHJpbmd8T2JqZWN0fSBibG9ja1xuICogQHBhcmFtIHtPYmplY3R9IG9wdGlvbnNcbiAqICAgQHByb3BlcnR5IHtCb29sZWFufSBub3JtYWxpemVcbiAqL1xuXG5DaGFuZ2VzLmluc2VydEJsb2NrQXRSYW5nZSA9IChjaGFuZ2UsIHJhbmdlLCBibG9jaywgb3B0aW9ucyA9IHt9KSA9PiB7XG4gIGJsb2NrID0gTm9ybWFsaXplLmJsb2NrKGJsb2NrKVxuICBjb25zdCB7IG5vcm1hbGl6ZSA9IHRydWUgfSA9IG9wdGlvbnNcblxuICBpZiAocmFuZ2UuaXNFeHBhbmRlZCkge1xuICAgIGNoYW5nZS5kZWxldGVBdFJhbmdlKHJhbmdlKVxuICAgIHJhbmdlID0gcmFuZ2UuY29sbGFwc2VUb1N0YXJ0KClcbiAgfVxuXG4gIGNvbnN0IHsgc3RhdGUgfSA9IGNoYW5nZVxuICBjb25zdCB7IGRvY3VtZW50IH0gPSBzdGF0ZVxuICBjb25zdCB7IHN0YXJ0S2V5LCBzdGFydE9mZnNldCB9ID0gcmFuZ2VcbiAgY29uc3Qgc3RhcnRCbG9jayA9IGRvY3VtZW50LmdldENsb3Nlc3RCbG9jayhzdGFydEtleSlcbiAgY29uc3QgcGFyZW50ID0gZG9jdW1lbnQuZ2V0UGFyZW50KHN0YXJ0QmxvY2sua2V5KVxuICBjb25zdCBpbmRleCA9IHBhcmVudC5ub2Rlcy5pbmRleE9mKHN0YXJ0QmxvY2spXG5cbiAgaWYgKHN0YXJ0QmxvY2suaXNWb2lkKSB7XG4gICAgY29uc3QgZXh0cmEgPSByYW5nZS5pc0F0RW5kT2Yoc3RhcnRCbG9jaykgPyAxIDogMFxuICAgIGNoYW5nZS5pbnNlcnROb2RlQnlLZXkocGFyZW50LmtleSwgaW5kZXggKyBleHRyYSwgYmxvY2ssIHsgbm9ybWFsaXplIH0pXG4gIH1cblxuICBlbHNlIGlmIChzdGFydEJsb2NrLmlzRW1wdHkpIHtcbiAgICBjaGFuZ2UucmVtb3ZlTm9kZUJ5S2V5KHN0YXJ0QmxvY2sua2V5KVxuICAgIGNoYW5nZS5pbnNlcnROb2RlQnlLZXkocGFyZW50LmtleSwgaW5kZXgsIGJsb2NrLCB7IG5vcm1hbGl6ZSB9KVxuICB9XG5cbiAgZWxzZSBpZiAocmFuZ2UuaXNBdFN0YXJ0T2Yoc3RhcnRCbG9jaykpIHtcbiAgICBjaGFuZ2UuaW5zZXJ0Tm9kZUJ5S2V5KHBhcmVudC5rZXksIGluZGV4LCBibG9jaywgeyBub3JtYWxpemUgfSlcbiAgfVxuXG4gIGVsc2UgaWYgKHJhbmdlLmlzQXRFbmRPZihzdGFydEJsb2NrKSkge1xuICAgIGNoYW5nZS5pbnNlcnROb2RlQnlLZXkocGFyZW50LmtleSwgaW5kZXggKyAxLCBibG9jaywgeyBub3JtYWxpemUgfSlcbiAgfVxuXG4gIGVsc2Uge1xuICAgIGNoYW5nZS5zcGxpdERlc2NlbmRhbnRzQnlLZXkoc3RhcnRCbG9jay5rZXksIHN0YXJ0S2V5LCBzdGFydE9mZnNldCwgT1BUUylcbiAgICBjaGFuZ2UuaW5zZXJ0Tm9kZUJ5S2V5KHBhcmVudC5rZXksIGluZGV4ICsgMSwgYmxvY2ssIHsgbm9ybWFsaXplIH0pXG4gIH1cblxuICBpZiAobm9ybWFsaXplKSB7XG4gICAgY2hhbmdlLm5vcm1hbGl6ZU5vZGVCeUtleShwYXJlbnQua2V5LCBTQ0hFTUEpXG4gIH1cbn1cblxuLyoqXG4gKiBJbnNlcnQgYSBgZnJhZ21lbnRgIGF0IGEgYHJhbmdlYC5cbiAqXG4gKiBAcGFyYW0ge0NoYW5nZX0gY2hhbmdlXG4gKiBAcGFyYW0ge1NlbGVjdGlvbn0gcmFuZ2VcbiAqIEBwYXJhbSB7RG9jdW1lbnR9IGZyYWdtZW50XG4gKiBAcGFyYW0ge09iamVjdH0gb3B0aW9uc1xuICogICBAcHJvcGVydHkge0Jvb2xlYW59IG5vcm1hbGl6ZVxuICovXG5cbkNoYW5nZXMuaW5zZXJ0RnJhZ21lbnRBdFJhbmdlID0gKGNoYW5nZSwgcmFuZ2UsIGZyYWdtZW50LCBvcHRpb25zID0ge30pID0+IHtcbiAgY29uc3QgeyBub3JtYWxpemUgPSB0cnVlIH0gPSBvcHRpb25zXG5cbiAgLy8gSWYgdGhlIHJhbmdlIGlzIGV4cGFuZGVkLCBkZWxldGUgaXQgZmlyc3QuXG4gIGlmIChyYW5nZS5pc0V4cGFuZGVkKSB7XG4gICAgY2hhbmdlLmRlbGV0ZUF0UmFuZ2UocmFuZ2UsIE9QVFMpXG4gICAgcmFuZ2UgPSByYW5nZS5jb2xsYXBzZVRvU3RhcnQoKVxuICB9XG5cbiAgLy8gSWYgdGhlIGZyYWdtZW50IGlzIGVtcHR5LCB0aGVyZSdzIG5vdGhpbmcgdG8gZG8gYWZ0ZXIgZGVsZXRpbmcuXG4gIGlmICghZnJhZ21lbnQubm9kZXMuc2l6ZSkgcmV0dXJuXG5cbiAgLy8gUmVnZW5lcmF0ZSB0aGUga2V5cyBmb3IgYWxsIG9mIHRoZSBmcmFnbWVudHMgbm9kZXMsIHNvIHRoYXQgdGhleSdyZVxuICAvLyBndWFyYW50ZWVkIG5vdCB0byBjb2xsaWRlIHdpdGggdGhlIGV4aXN0aW5nIGtleXMgaW4gdGhlIGRvY3VtZW50LiBPdGhlcndpc2VcbiAgLy8gdGhleSB3aWxsIGJlIHJlbmdlcmF0ZWQgYXV0b21hdGljYWxseSBhbmQgd2Ugd29uJ3QgaGF2ZSBhbiBlYXN5IHdheSB0b1xuICAvLyByZWZlcmVuY2UgdGhlbS5cbiAgZnJhZ21lbnQgPSBmcmFnbWVudC5tYXBEZXNjZW5kYW50cyhjaGlsZCA9PiBjaGlsZC5yZWdlbmVyYXRlS2V5KCkpXG5cbiAgLy8gQ2FsY3VsYXRlIGEgZmV3IHRoaW5ncy4uLlxuICBjb25zdCB7IHN0YXJ0S2V5LCBzdGFydE9mZnNldCB9ID0gcmFuZ2VcbiAgbGV0IHsgc3RhdGUgfSA9IGNoYW5nZVxuICBsZXQgeyBkb2N1bWVudCB9ID0gc3RhdGVcbiAgbGV0IHN0YXJ0VGV4dCA9IGRvY3VtZW50LmdldERlc2NlbmRhbnQoc3RhcnRLZXkpXG4gIGxldCBzdGFydEJsb2NrID0gZG9jdW1lbnQuZ2V0Q2xvc2VzdEJsb2NrKHN0YXJ0VGV4dC5rZXkpXG4gIGxldCBzdGFydENoaWxkID0gc3RhcnRCbG9jay5nZXRGdXJ0aGVzdEFuY2VzdG9yKHN0YXJ0VGV4dC5rZXkpXG4gIGNvbnN0IGlzQXRTdGFydCA9IHJhbmdlLmlzQXRTdGFydE9mKHN0YXJ0QmxvY2spXG4gIGNvbnN0IHBhcmVudCA9IGRvY3VtZW50LmdldFBhcmVudChzdGFydEJsb2NrLmtleSlcbiAgY29uc3QgaW5kZXggPSBwYXJlbnQubm9kZXMuaW5kZXhPZihzdGFydEJsb2NrKVxuICBjb25zdCBibG9ja3MgPSBmcmFnbWVudC5nZXRCbG9ja3MoKVxuICBjb25zdCBmaXJzdEJsb2NrID0gYmxvY2tzLmZpcnN0KClcbiAgY29uc3QgbGFzdEJsb2NrID0gYmxvY2tzLmxhc3QoKVxuXG4gIC8vIElmIHRoZSBmcmFnbWVudCBvbmx5IGNvbnRhaW5zIGEgdm9pZCBibG9jaywgdXNlIGBpbnNlcnRCbG9ja2AgaW5zdGVhZC5cbiAgaWYgKGZpcnN0QmxvY2sgPT0gbGFzdEJsb2NrICYmIGZpcnN0QmxvY2suaXNWb2lkKSB7XG4gICAgY2hhbmdlLmluc2VydEJsb2NrQXRSYW5nZShyYW5nZSwgZmlyc3RCbG9jaywgb3B0aW9ucylcbiAgICByZXR1cm5cbiAgfVxuXG4gIC8vIElmIHRoZSBmaXJzdCBhbmQgbGFzdCBibG9jayBhcmVuJ3QgdGhlIHNhbWUsIHdlIG5lZWQgdG8gaW5zZXJ0IGFsbCBvZiB0aGVcbiAgLy8gbm9kZXMgYWZ0ZXIgdGhlIGZyYWdtZW50J3MgZmlyc3QgYmxvY2sgYXQgdGhlIGluZGV4LlxuICBpZiAoZmlyc3RCbG9jayAhPSBsYXN0QmxvY2spIHtcbiAgICBjb25zdCBsb25lbHlQYXJlbnQgPSBmcmFnbWVudC5nZXRGdXJ0aGVzdChmaXJzdEJsb2NrLmtleSwgcCA9PiBwLm5vZGVzLnNpemUgPT0gMSlcbiAgICBjb25zdCBsb25lbHlDaGlsZCA9IGxvbmVseVBhcmVudCB8fCBmaXJzdEJsb2NrXG4gICAgY29uc3Qgc3RhcnRJbmRleCA9IHBhcmVudC5ub2Rlcy5pbmRleE9mKHN0YXJ0QmxvY2spXG4gICAgZnJhZ21lbnQgPSBmcmFnbWVudC5yZW1vdmVEZXNjZW5kYW50KGxvbmVseUNoaWxkLmtleSlcblxuICAgIGZyYWdtZW50Lm5vZGVzLmZvckVhY2goKG5vZGUsIGkpID0+IHtcbiAgICAgIGNvbnN0IG5ld0luZGV4ID0gc3RhcnRJbmRleCArIGkgKyAxXG4gICAgICBjaGFuZ2UuaW5zZXJ0Tm9kZUJ5S2V5KHBhcmVudC5rZXksIG5ld0luZGV4LCBub2RlLCBPUFRTKVxuICAgIH0pXG4gIH1cblxuICAvLyBDaGVjayBpZiB3ZSBuZWVkIHRvIHNwbGl0IHRoZSBub2RlLlxuICBpZiAoc3RhcnRPZmZzZXQgIT0gMCkge1xuICAgIGNoYW5nZS5zcGxpdERlc2NlbmRhbnRzQnlLZXkoc3RhcnRDaGlsZC5rZXksIHN0YXJ0S2V5LCBzdGFydE9mZnNldCwgT1BUUylcbiAgfVxuXG4gIC8vIFVwZGF0ZSBvdXIgdmFyaWFibGVzIHdpdGggdGhlIG5ldyBzdGF0ZS5cbiAgc3RhdGUgPSBjaGFuZ2Uuc3RhdGVcbiAgZG9jdW1lbnQgPSBzdGF0ZS5kb2N1bWVudFxuICBzdGFydFRleHQgPSBkb2N1bWVudC5nZXREZXNjZW5kYW50KHN0YXJ0S2V5KVxuICBzdGFydEJsb2NrID0gZG9jdW1lbnQuZ2V0Q2xvc2VzdEJsb2NrKHN0YXJ0S2V5KVxuICBzdGFydENoaWxkID0gc3RhcnRCbG9jay5nZXRGdXJ0aGVzdEFuY2VzdG9yKHN0YXJ0VGV4dC5rZXkpXG5cbiAgLy8gSWYgdGhlIGZpcnN0IGFuZCBsYXN0IGJsb2NrIGFyZW4ndCB0aGUgc2FtZSwgd2UgbmVlZCB0byBtb3ZlIGFueSBvZiB0aGVcbiAgLy8gc3RhcnRpbmcgYmxvY2sncyBjaGlsZHJlbiBhZnRlciB0aGUgc3BsaXQgaW50byB0aGUgbGFzdCBibG9jayBvZiB0aGVcbiAgLy8gZnJhZ21lbnQsIHdoaWNoIGhhcyBhbHJlYWR5IGJlZW4gaW5zZXJ0ZWQuXG4gIGlmIChmaXJzdEJsb2NrICE9IGxhc3RCbG9jaykge1xuICAgIGNvbnN0IG5leHRDaGlsZCA9IGlzQXRTdGFydCA/IHN0YXJ0Q2hpbGQgOiBzdGFydEJsb2NrLmdldE5leHRTaWJsaW5nKHN0YXJ0Q2hpbGQua2V5KVxuICAgIGNvbnN0IG5leHROb2RlcyA9IG5leHRDaGlsZCA/IHN0YXJ0QmxvY2subm9kZXMuc2tpcFVudGlsKG4gPT4gbi5rZXkgPT0gbmV4dENoaWxkLmtleSkgOiBMaXN0KClcbiAgICBjb25zdCBsYXN0SW5kZXggPSBsYXN0QmxvY2subm9kZXMuc2l6ZVxuXG4gICAgbmV4dE5vZGVzLmZvckVhY2goKG5vZGUsIGkpID0+IHtcbiAgICAgIGNvbnN0IG5ld0luZGV4ID0gbGFzdEluZGV4ICsgaVxuICAgICAgY2hhbmdlLm1vdmVOb2RlQnlLZXkobm9kZS5rZXksIGxhc3RCbG9jay5rZXksIG5ld0luZGV4LCBPUFRTKVxuICAgIH0pXG4gIH1cblxuICAvLyBJZiB0aGUgc3RhcnRpbmcgYmxvY2sgaXMgZW1wdHksIHdlIHJlcGxhY2UgaXQgZW50aXJlbHkgd2l0aCB0aGUgZmlyc3QgYmxvY2tcbiAgLy8gb2YgdGhlIGZyYWdtZW50LCBzaW5jZSB0aGlzIGxlYWRzIHRvIGEgbW9yZSBleHBlY3RlZCBiZWhhdmlvciBmb3IgdGhlIHVzZXIuXG4gIGlmIChzdGFydEJsb2NrLmlzRW1wdHkpIHtcbiAgICBjaGFuZ2UucmVtb3ZlTm9kZUJ5S2V5KHN0YXJ0QmxvY2sua2V5LCBPUFRTKVxuICAgIGNoYW5nZS5pbnNlcnROb2RlQnlLZXkocGFyZW50LmtleSwgaW5kZXgsIGZpcnN0QmxvY2ssIE9QVFMpXG4gIH1cblxuICAvLyBPdGhlcndpc2UsIHdlIG1haW50YWluIHRoZSBzdGFydGluZyBibG9jaywgYW5kIGluc2VydCBhbGwgb2YgdGhlIGZpcnN0XG4gIC8vIGJsb2NrJ3MgaW5saW5lIG5vZGVzIGludG8gaXQgYXQgdGhlIHNwbGl0IHBvaW50LlxuICBlbHNlIHtcbiAgICBjb25zdCBpbmxpbmVDaGlsZCA9IHN0YXJ0QmxvY2suZ2V0RnVydGhlc3RBbmNlc3RvcihzdGFydFRleHQua2V5KVxuICAgIGNvbnN0IGlubGluZUluZGV4ID0gc3RhcnRCbG9jay5ub2Rlcy5pbmRleE9mKGlubGluZUNoaWxkKVxuXG4gICAgZmlyc3RCbG9jay5ub2Rlcy5mb3JFYWNoKChpbmxpbmUsIGkpID0+IHtcbiAgICAgIGNvbnN0IG8gPSBzdGFydE9mZnNldCA9PSAwID8gMCA6IDFcbiAgICAgIGNvbnN0IG5ld0luZGV4ID0gaW5saW5lSW5kZXggKyBpICsgb1xuICAgICAgY2hhbmdlLmluc2VydE5vZGVCeUtleShzdGFydEJsb2NrLmtleSwgbmV3SW5kZXgsIGlubGluZSwgT1BUUylcbiAgICB9KVxuICB9XG5cbiAgLy8gTm9ybWFsaXplIGlmIHJlcXVlc3RlZC5cbiAgaWYgKG5vcm1hbGl6ZSkge1xuICAgIGNoYW5nZS5ub3JtYWxpemVOb2RlQnlLZXkocGFyZW50LmtleSwgU0NIRU1BKVxuICB9XG59XG5cbi8qKlxuICogSW5zZXJ0IGFuIGBpbmxpbmVgIG5vZGUgYXQgYHJhbmdlYC5cbiAqXG4gKiBAcGFyYW0ge0NoYW5nZX0gY2hhbmdlXG4gKiBAcGFyYW0ge1NlbGVjdGlvbn0gcmFuZ2VcbiAqIEBwYXJhbSB7SW5saW5lfFN0cmluZ3xPYmplY3R9IGlubGluZVxuICogQHBhcmFtIHtPYmplY3R9IG9wdGlvbnNcbiAqICAgQHByb3BlcnR5IHtCb29sZWFufSBub3JtYWxpemVcbiAqL1xuXG5DaGFuZ2VzLmluc2VydElubGluZUF0UmFuZ2UgPSAoY2hhbmdlLCByYW5nZSwgaW5saW5lLCBvcHRpb25zID0ge30pID0+IHtcbiAgY29uc3QgeyBub3JtYWxpemUgPSB0cnVlIH0gPSBvcHRpb25zXG4gIGlubGluZSA9IE5vcm1hbGl6ZS5pbmxpbmUoaW5saW5lKVxuXG4gIGlmIChyYW5nZS5pc0V4cGFuZGVkKSB7XG4gICAgY2hhbmdlLmRlbGV0ZUF0UmFuZ2UocmFuZ2UsIE9QVFMpXG4gICAgcmFuZ2UgPSByYW5nZS5jb2xsYXBzZVRvU3RhcnQoKVxuICB9XG5cbiAgY29uc3QgeyBzdGF0ZSB9ID0gY2hhbmdlXG4gIGNvbnN0IHsgZG9jdW1lbnQgfSA9IHN0YXRlXG4gIGNvbnN0IHsgc3RhcnRLZXksIHN0YXJ0T2Zmc2V0IH0gPSByYW5nZVxuICBjb25zdCBwYXJlbnQgPSBkb2N1bWVudC5nZXRQYXJlbnQoc3RhcnRLZXkpXG4gIGNvbnN0IHN0YXJ0VGV4dCA9IGRvY3VtZW50LmFzc2VydERlc2NlbmRhbnQoc3RhcnRLZXkpXG4gIGNvbnN0IGluZGV4ID0gcGFyZW50Lm5vZGVzLmluZGV4T2Yoc3RhcnRUZXh0KVxuXG4gIGlmIChwYXJlbnQuaXNWb2lkKSByZXR1cm5cblxuICBjaGFuZ2Uuc3BsaXROb2RlQnlLZXkoc3RhcnRLZXksIHN0YXJ0T2Zmc2V0LCBPUFRTKVxuICBjaGFuZ2UuaW5zZXJ0Tm9kZUJ5S2V5KHBhcmVudC5rZXksIGluZGV4ICsgMSwgaW5saW5lLCBPUFRTKVxuXG4gIGlmIChub3JtYWxpemUpIHtcbiAgICBjaGFuZ2Uubm9ybWFsaXplTm9kZUJ5S2V5KHBhcmVudC5rZXksIFNDSEVNQSlcbiAgfVxufVxuXG4vKipcbiAqIEluc2VydCBgdGV4dGAgYXQgYSBgcmFuZ2VgLCB3aXRoIG9wdGlvbmFsIGBtYXJrc2AuXG4gKlxuICogQHBhcmFtIHtDaGFuZ2V9IGNoYW5nZVxuICogQHBhcmFtIHtTZWxlY3Rpb259IHJhbmdlXG4gKiBAcGFyYW0ge1N0cmluZ30gdGV4dFxuICogQHBhcmFtIHtTZXQ8TWFyaz59IG1hcmtzIChvcHRpb25hbClcbiAqIEBwYXJhbSB7T2JqZWN0fSBvcHRpb25zXG4gKiAgIEBwcm9wZXJ0eSB7Qm9vbGVhbn0gbm9ybWFsaXplXG4gKi9cblxuQ2hhbmdlcy5pbnNlcnRUZXh0QXRSYW5nZSA9IChjaGFuZ2UsIHJhbmdlLCB0ZXh0LCBtYXJrcywgb3B0aW9ucyA9IHt9KSA9PiB7XG4gIGxldCB7IG5vcm1hbGl6ZSB9ID0gb3B0aW9uc1xuICBjb25zdCB7IHN0YXRlIH0gPSBjaGFuZ2VcbiAgY29uc3QgeyBkb2N1bWVudCB9ID0gc3RhdGVcbiAgY29uc3QgeyBzdGFydEtleSwgc3RhcnRPZmZzZXQgfSA9IHJhbmdlXG4gIGNvbnN0IHBhcmVudCA9IGRvY3VtZW50LmdldFBhcmVudChzdGFydEtleSlcblxuICBpZiAocGFyZW50LmlzVm9pZCkgcmV0dXJuXG5cbiAgaWYgKHJhbmdlLmlzRXhwYW5kZWQpIHtcbiAgICBjaGFuZ2UuZGVsZXRlQXRSYW5nZShyYW5nZSwgT1BUUylcbiAgfVxuXG4gIC8vIFBFUkY6IFVubGVzcyBzcGVjaWZpZWQsIGRvbid0IG5vcm1hbGl6ZSBpZiBvbmx5IGluc2VydGluZyB0ZXh0LlxuICBpZiAobm9ybWFsaXplICE9PSB1bmRlZmluZWQpIHtcbiAgICBub3JtYWxpemUgPSByYW5nZS5pc0V4cGFuZGVkXG4gIH1cblxuICBjaGFuZ2UuaW5zZXJ0VGV4dEJ5S2V5KHN0YXJ0S2V5LCBzdGFydE9mZnNldCwgdGV4dCwgbWFya3MsIHsgbm9ybWFsaXplIH0pXG59XG5cbi8qKlxuICogUmVtb3ZlIGFuIGV4aXN0aW5nIGBtYXJrYCB0byB0aGUgY2hhcmFjdGVycyBhdCBgcmFuZ2VgLlxuICpcbiAqIEBwYXJhbSB7Q2hhbmdlfSBjaGFuZ2VcbiAqIEBwYXJhbSB7U2VsZWN0aW9ufSByYW5nZVxuICogQHBhcmFtIHtNYXJrfFN0cmluZ30gbWFyayAob3B0aW9uYWwpXG4gKiBAcGFyYW0ge09iamVjdH0gb3B0aW9uc1xuICogICBAcHJvcGVydHkge0Jvb2xlYW59IG5vcm1hbGl6ZVxuICovXG5cbkNoYW5nZXMucmVtb3ZlTWFya0F0UmFuZ2UgPSAoY2hhbmdlLCByYW5nZSwgbWFyaywgb3B0aW9ucyA9IHt9KSA9PiB7XG4gIGlmIChyYW5nZS5pc0NvbGxhcHNlZCkgcmV0dXJuXG5cbiAgY29uc3QgeyBub3JtYWxpemUgPSB0cnVlIH0gPSBvcHRpb25zXG4gIGNvbnN0IHsgc3RhdGUgfSA9IGNoYW5nZVxuICBjb25zdCB7IGRvY3VtZW50IH0gPSBzdGF0ZVxuICBjb25zdCB0ZXh0cyA9IGRvY3VtZW50LmdldFRleHRzQXRSYW5nZShyYW5nZSlcbiAgY29uc3QgeyBzdGFydEtleSwgc3RhcnRPZmZzZXQsIGVuZEtleSwgZW5kT2Zmc2V0IH0gPSByYW5nZVxuXG4gIHRleHRzLmZvckVhY2goKG5vZGUpID0+IHtcbiAgICBjb25zdCB7IGtleSB9ID0gbm9kZVxuICAgIGxldCBpbmRleCA9IDBcbiAgICBsZXQgbGVuZ3RoID0gbm9kZS50ZXh0Lmxlbmd0aFxuXG4gICAgaWYgKGtleSA9PSBzdGFydEtleSkgaW5kZXggPSBzdGFydE9mZnNldFxuICAgIGlmIChrZXkgPT0gZW5kS2V5KSBsZW5ndGggPSBlbmRPZmZzZXRcbiAgICBpZiAoa2V5ID09IHN0YXJ0S2V5ICYmIGtleSA9PSBlbmRLZXkpIGxlbmd0aCA9IGVuZE9mZnNldCAtIHN0YXJ0T2Zmc2V0XG5cbiAgICBjaGFuZ2UucmVtb3ZlTWFya0J5S2V5KGtleSwgaW5kZXgsIGxlbmd0aCwgbWFyaywgeyBub3JtYWxpemUgfSlcbiAgfSlcbn1cblxuLyoqXG4gKiBTZXQgdGhlIGBwcm9wZXJ0aWVzYCBvZiBibG9jayBub2RlcyBpbiBhIGByYW5nZWAuXG4gKlxuICogQHBhcmFtIHtDaGFuZ2V9IGNoYW5nZVxuICogQHBhcmFtIHtTZWxlY3Rpb259IHJhbmdlXG4gKiBAcGFyYW0ge09iamVjdHxTdHJpbmd9IHByb3BlcnRpZXNcbiAqIEBwYXJhbSB7T2JqZWN0fSBvcHRpb25zXG4gKiAgIEBwcm9wZXJ0eSB7Qm9vbGVhbn0gbm9ybWFsaXplXG4gKi9cblxuQ2hhbmdlcy5zZXRCbG9ja0F0UmFuZ2UgPSAoY2hhbmdlLCByYW5nZSwgcHJvcGVydGllcywgb3B0aW9ucyA9IHt9KSA9PiB7XG4gIGNvbnN0IHsgbm9ybWFsaXplID0gdHJ1ZSB9ID0gb3B0aW9uc1xuICBjb25zdCB7IHN0YXRlIH0gPSBjaGFuZ2VcbiAgY29uc3QgeyBkb2N1bWVudCB9ID0gc3RhdGVcbiAgY29uc3QgYmxvY2tzID0gZG9jdW1lbnQuZ2V0QmxvY2tzQXRSYW5nZShyYW5nZSlcblxuICBibG9ja3MuZm9yRWFjaCgoYmxvY2spID0+IHtcbiAgICBjaGFuZ2Uuc2V0Tm9kZUJ5S2V5KGJsb2NrLmtleSwgcHJvcGVydGllcywgeyBub3JtYWxpemUgfSlcbiAgfSlcbn1cblxuLyoqXG4gKiBTZXQgdGhlIGBwcm9wZXJ0aWVzYCBvZiBpbmxpbmUgbm9kZXMgaW4gYSBgcmFuZ2VgLlxuICpcbiAqIEBwYXJhbSB7Q2hhbmdlfSBjaGFuZ2VcbiAqIEBwYXJhbSB7U2VsZWN0aW9ufSByYW5nZVxuICogQHBhcmFtIHtPYmplY3R8U3RyaW5nfSBwcm9wZXJ0aWVzXG4gKiBAcGFyYW0ge09iamVjdH0gb3B0aW9uc1xuICogICBAcHJvcGVydHkge0Jvb2xlYW59IG5vcm1hbGl6ZVxuICovXG5cbkNoYW5nZXMuc2V0SW5saW5lQXRSYW5nZSA9IChjaGFuZ2UsIHJhbmdlLCBwcm9wZXJ0aWVzLCBvcHRpb25zID0ge30pID0+IHtcbiAgY29uc3QgeyBub3JtYWxpemUgPSB0cnVlIH0gPSBvcHRpb25zXG4gIGNvbnN0IHsgc3RhdGUgfSA9IGNoYW5nZVxuICBjb25zdCB7IGRvY3VtZW50IH0gPSBzdGF0ZVxuICBjb25zdCBpbmxpbmVzID0gZG9jdW1lbnQuZ2V0SW5saW5lc0F0UmFuZ2UocmFuZ2UpXG5cbiAgaW5saW5lcy5mb3JFYWNoKChpbmxpbmUpID0+IHtcbiAgICBjaGFuZ2Uuc2V0Tm9kZUJ5S2V5KGlubGluZS5rZXksIHByb3BlcnRpZXMsIHsgbm9ybWFsaXplIH0pXG4gIH0pXG59XG5cbi8qKlxuICogU3BsaXQgdGhlIGJsb2NrIG5vZGVzIGF0IGEgYHJhbmdlYCwgdG8gb3B0aW9uYWwgYGhlaWdodGAuXG4gKlxuICogQHBhcmFtIHtDaGFuZ2V9IGNoYW5nZVxuICogQHBhcmFtIHtTZWxlY3Rpb259IHJhbmdlXG4gKiBAcGFyYW0ge051bWJlcn0gaGVpZ2h0IChvcHRpb25hbClcbiAqIEBwYXJhbSB7T2JqZWN0fSBvcHRpb25zXG4gKiAgIEBwcm9wZXJ0eSB7Qm9vbGVhbn0gbm9ybWFsaXplXG4gKi9cblxuQ2hhbmdlcy5zcGxpdEJsb2NrQXRSYW5nZSA9IChjaGFuZ2UsIHJhbmdlLCBoZWlnaHQgPSAxLCBvcHRpb25zID0ge30pID0+IHtcbiAgY29uc3QgeyBub3JtYWxpemUgPSB0cnVlIH0gPSBvcHRpb25zXG5cbiAgaWYgKHJhbmdlLmlzRXhwYW5kZWQpIHtcbiAgICBjaGFuZ2UuZGVsZXRlQXRSYW5nZShyYW5nZSwgeyBub3JtYWxpemUgfSlcbiAgICByYW5nZSA9IHJhbmdlLmNvbGxhcHNlVG9TdGFydCgpXG4gIH1cblxuICBjb25zdCB7IHN0YXJ0S2V5LCBzdGFydE9mZnNldCB9ID0gcmFuZ2VcbiAgY29uc3QgeyBzdGF0ZSB9ID0gY2hhbmdlXG4gIGNvbnN0IHsgZG9jdW1lbnQgfSA9IHN0YXRlXG4gIGxldCBub2RlID0gZG9jdW1lbnQuYXNzZXJ0RGVzY2VuZGFudChzdGFydEtleSlcbiAgbGV0IHBhcmVudCA9IGRvY3VtZW50LmdldENsb3Nlc3RCbG9jayhub2RlLmtleSlcbiAgbGV0IGggPSAwXG5cbiAgd2hpbGUgKHBhcmVudCAmJiBwYXJlbnQua2luZCA9PSAnYmxvY2snICYmIGggPCBoZWlnaHQpIHtcbiAgICBub2RlID0gcGFyZW50XG4gICAgcGFyZW50ID0gZG9jdW1lbnQuZ2V0Q2xvc2VzdEJsb2NrKHBhcmVudC5rZXkpXG4gICAgaCsrXG4gIH1cblxuICBjaGFuZ2Uuc3BsaXREZXNjZW5kYW50c0J5S2V5KG5vZGUua2V5LCBzdGFydEtleSwgc3RhcnRPZmZzZXQsIHsgbm9ybWFsaXplIH0pXG59XG5cbi8qKlxuICogU3BsaXQgdGhlIGlubGluZSBub2RlcyBhdCBhIGByYW5nZWAsIHRvIG9wdGlvbmFsIGBoZWlnaHRgLlxuICpcbiAqIEBwYXJhbSB7Q2hhbmdlfSBjaGFuZ2VcbiAqIEBwYXJhbSB7U2VsZWN0aW9ufSByYW5nZVxuICogQHBhcmFtIHtOdW1iZXJ9IGhlaWdodCAob3B0aW9uYWwpXG4gKiBAcGFyYW0ge09iamVjdH0gb3B0aW9uc1xuICogICBAcHJvcGVydHkge0Jvb2xlYW59IG5vcm1hbGl6ZVxuICovXG5cbkNoYW5nZXMuc3BsaXRJbmxpbmVBdFJhbmdlID0gKGNoYW5nZSwgcmFuZ2UsIGhlaWdodCA9IEluZmluaXR5LCBvcHRpb25zID0ge30pID0+IHtcbiAgY29uc3QgeyBub3JtYWxpemUgPSB0cnVlIH0gPSBvcHRpb25zXG5cbiAgaWYgKHJhbmdlLmlzRXhwYW5kZWQpIHtcbiAgICBjaGFuZ2UuZGVsZXRlQXRSYW5nZShyYW5nZSwgeyBub3JtYWxpemUgfSlcbiAgICByYW5nZSA9IHJhbmdlLmNvbGxhcHNlVG9TdGFydCgpXG4gIH1cblxuICBjb25zdCB7IHN0YXJ0S2V5LCBzdGFydE9mZnNldCB9ID0gcmFuZ2VcbiAgY29uc3QgeyBzdGF0ZSB9ID0gY2hhbmdlXG4gIGNvbnN0IHsgZG9jdW1lbnQgfSA9IHN0YXRlXG4gIGxldCBub2RlID0gZG9jdW1lbnQuYXNzZXJ0RGVzY2VuZGFudChzdGFydEtleSlcbiAgbGV0IHBhcmVudCA9IGRvY3VtZW50LmdldENsb3Nlc3RJbmxpbmUobm9kZS5rZXkpXG4gIGxldCBoID0gMFxuXG4gIHdoaWxlIChwYXJlbnQgJiYgcGFyZW50LmtpbmQgPT0gJ2lubGluZScgJiYgaCA8IGhlaWdodCkge1xuICAgIG5vZGUgPSBwYXJlbnRcbiAgICBwYXJlbnQgPSBkb2N1bWVudC5nZXRDbG9zZXN0SW5saW5lKHBhcmVudC5rZXkpXG4gICAgaCsrXG4gIH1cblxuICBjaGFuZ2Uuc3BsaXREZXNjZW5kYW50c0J5S2V5KG5vZGUua2V5LCBzdGFydEtleSwgc3RhcnRPZmZzZXQsIHsgbm9ybWFsaXplIH0pXG59XG5cbi8qKlxuICogQWRkIG9yIHJlbW92ZSBhIGBtYXJrYCBmcm9tIHRoZSBjaGFyYWN0ZXJzIGF0IGByYW5nZWAsIGRlcGVuZGluZyBvbiB3aGV0aGVyXG4gKiBpdCdzIGFscmVhZHkgdGhlcmUuXG4gKlxuICogQHBhcmFtIHtDaGFuZ2V9IGNoYW5nZVxuICogQHBhcmFtIHtTZWxlY3Rpb259IHJhbmdlXG4gKiBAcGFyYW0ge01peGVkfSBtYXJrXG4gKiBAcGFyYW0ge09iamVjdH0gb3B0aW9uc1xuICogICBAcHJvcGVydHkge0Jvb2xlYW59IG5vcm1hbGl6ZVxuICovXG5cbkNoYW5nZXMudG9nZ2xlTWFya0F0UmFuZ2UgPSAoY2hhbmdlLCByYW5nZSwgbWFyaywgb3B0aW9ucyA9IHt9KSA9PiB7XG4gIGlmIChyYW5nZS5pc0NvbGxhcHNlZCkgcmV0dXJuXG5cbiAgbWFyayA9IE5vcm1hbGl6ZS5tYXJrKG1hcmspXG5cbiAgY29uc3QgeyBub3JtYWxpemUgPSB0cnVlIH0gPSBvcHRpb25zXG4gIGNvbnN0IHsgc3RhdGUgfSA9IGNoYW5nZVxuICBjb25zdCB7IGRvY3VtZW50IH0gPSBzdGF0ZVxuICBjb25zdCBtYXJrcyA9IGRvY3VtZW50LmdldE1hcmtzQXRSYW5nZShyYW5nZSlcbiAgY29uc3QgZXhpc3RzID0gbWFya3Muc29tZShtID0+IG0uZXF1YWxzKG1hcmspKVxuXG4gIGlmIChleGlzdHMpIHtcbiAgICBjaGFuZ2UucmVtb3ZlTWFya0F0UmFuZ2UocmFuZ2UsIG1hcmssIHsgbm9ybWFsaXplIH0pXG4gIH0gZWxzZSB7XG4gICAgY2hhbmdlLmFkZE1hcmtBdFJhbmdlKHJhbmdlLCBtYXJrLCB7IG5vcm1hbGl6ZSB9KVxuICB9XG59XG5cbi8qKlxuICogVW53cmFwIGFsbCBvZiB0aGUgYmxvY2sgbm9kZXMgaW4gYSBgcmFuZ2VgIGZyb20gYSBibG9jayB3aXRoIGBwcm9wZXJ0aWVzYC5cbiAqXG4gKiBAcGFyYW0ge0NoYW5nZX0gY2hhbmdlXG4gKiBAcGFyYW0ge1NlbGVjdGlvbn0gcmFuZ2VcbiAqIEBwYXJhbSB7U3RyaW5nfE9iamVjdH0gcHJvcGVydGllc1xuICogQHBhcmFtIHtPYmplY3R9IG9wdGlvbnNcbiAqICAgQHByb3BlcnR5IHtCb29sZWFufSBub3JtYWxpemVcbiAqL1xuXG5DaGFuZ2VzLnVud3JhcEJsb2NrQXRSYW5nZSA9IChjaGFuZ2UsIHJhbmdlLCBwcm9wZXJ0aWVzLCBvcHRpb25zID0ge30pID0+IHtcbiAgcHJvcGVydGllcyA9IE5vcm1hbGl6ZS5ub2RlUHJvcGVydGllcyhwcm9wZXJ0aWVzKVxuXG4gIGNvbnN0IHsgbm9ybWFsaXplID0gdHJ1ZSB9ID0gb3B0aW9uc1xuICBsZXQgeyBzdGF0ZSB9ID0gY2hhbmdlXG4gIGxldCB7IGRvY3VtZW50IH0gPSBzdGF0ZVxuICBjb25zdCBibG9ja3MgPSBkb2N1bWVudC5nZXRCbG9ja3NBdFJhbmdlKHJhbmdlKVxuICBjb25zdCB3cmFwcGVycyA9IGJsb2Nrc1xuICAgIC5tYXAoKGJsb2NrKSA9PiB7XG4gICAgICByZXR1cm4gZG9jdW1lbnQuZ2V0Q2xvc2VzdChibG9jay5rZXksIChwYXJlbnQpID0+IHtcbiAgICAgICAgaWYgKHBhcmVudC5raW5kICE9ICdibG9jaycpIHJldHVybiBmYWxzZVxuICAgICAgICBpZiAocHJvcGVydGllcy50eXBlICE9IG51bGwgJiYgcGFyZW50LnR5cGUgIT0gcHJvcGVydGllcy50eXBlKSByZXR1cm4gZmFsc2VcbiAgICAgICAgaWYgKHByb3BlcnRpZXMuaXNWb2lkICE9IG51bGwgJiYgcGFyZW50LmlzVm9pZCAhPSBwcm9wZXJ0aWVzLmlzVm9pZCkgcmV0dXJuIGZhbHNlXG4gICAgICAgIGlmIChwcm9wZXJ0aWVzLmRhdGEgIT0gbnVsbCAmJiAhcGFyZW50LmRhdGEuaXNTdXBlcnNldChwcm9wZXJ0aWVzLmRhdGEpKSByZXR1cm4gZmFsc2VcbiAgICAgICAgcmV0dXJuIHRydWVcbiAgICAgIH0pXG4gICAgfSlcbiAgICAuZmlsdGVyKGV4aXN0cyA9PiBleGlzdHMpXG4gICAgLnRvT3JkZXJlZFNldCgpXG4gICAgLnRvTGlzdCgpXG5cbiAgd3JhcHBlcnMuZm9yRWFjaCgoYmxvY2spID0+IHtcbiAgICBjb25zdCBmaXJzdCA9IGJsb2NrLm5vZGVzLmZpcnN0KClcbiAgICBjb25zdCBsYXN0ID0gYmxvY2subm9kZXMubGFzdCgpXG4gICAgY29uc3QgcGFyZW50ID0gZG9jdW1lbnQuZ2V0UGFyZW50KGJsb2NrLmtleSlcbiAgICBjb25zdCBpbmRleCA9IHBhcmVudC5ub2Rlcy5pbmRleE9mKGJsb2NrKVxuXG4gICAgY29uc3QgY2hpbGRyZW4gPSBibG9jay5ub2Rlcy5maWx0ZXIoKGNoaWxkKSA9PiB7XG4gICAgICByZXR1cm4gYmxvY2tzLnNvbWUoYiA9PiBjaGlsZCA9PSBiIHx8IGNoaWxkLmhhc0Rlc2NlbmRhbnQoYi5rZXkpKVxuICAgIH0pXG5cbiAgICBjb25zdCBmaXJzdE1hdGNoID0gY2hpbGRyZW4uZmlyc3QoKVxuICAgIGNvbnN0IGxhc3RNYXRjaCA9IGNoaWxkcmVuLmxhc3QoKVxuXG4gICAgaWYgKGZpcnN0ID09IGZpcnN0TWF0Y2ggJiYgbGFzdCA9PSBsYXN0TWF0Y2gpIHtcbiAgICAgIGJsb2NrLm5vZGVzLmZvckVhY2goKGNoaWxkLCBpKSA9PiB7XG4gICAgICAgIGNoYW5nZS5tb3ZlTm9kZUJ5S2V5KGNoaWxkLmtleSwgcGFyZW50LmtleSwgaW5kZXggKyBpLCBPUFRTKVxuICAgICAgfSlcblxuICAgICAgY2hhbmdlLnJlbW92ZU5vZGVCeUtleShibG9jay5rZXksIE9QVFMpXG4gICAgfVxuXG4gICAgZWxzZSBpZiAobGFzdCA9PSBsYXN0TWF0Y2gpIHtcbiAgICAgIGJsb2NrLm5vZGVzXG4gICAgICAgIC5za2lwVW50aWwobiA9PiBuID09IGZpcnN0TWF0Y2gpXG4gICAgICAgIC5mb3JFYWNoKChjaGlsZCwgaSkgPT4ge1xuICAgICAgICAgIGNoYW5nZS5tb3ZlTm9kZUJ5S2V5KGNoaWxkLmtleSwgcGFyZW50LmtleSwgaW5kZXggKyAxICsgaSwgT1BUUylcbiAgICAgICAgfSlcbiAgICB9XG5cbiAgICBlbHNlIGlmIChmaXJzdCA9PSBmaXJzdE1hdGNoKSB7XG4gICAgICBibG9jay5ub2Rlc1xuICAgICAgICAudGFrZVVudGlsKG4gPT4gbiA9PSBsYXN0TWF0Y2gpXG4gICAgICAgIC5wdXNoKGxhc3RNYXRjaClcbiAgICAgICAgLmZvckVhY2goKGNoaWxkLCBpKSA9PiB7XG4gICAgICAgICAgY2hhbmdlLm1vdmVOb2RlQnlLZXkoY2hpbGQua2V5LCBwYXJlbnQua2V5LCBpbmRleCArIGksIE9QVFMpXG4gICAgICAgIH0pXG4gICAgfVxuXG4gICAgZWxzZSB7XG4gICAgICBjb25zdCBmaXJzdFRleHQgPSBmaXJzdE1hdGNoLmdldEZpcnN0VGV4dCgpXG4gICAgICBjaGFuZ2Uuc3BsaXREZXNjZW5kYW50c0J5S2V5KGJsb2NrLmtleSwgZmlyc3RUZXh0LmtleSwgMCwgT1BUUylcbiAgICAgIHN0YXRlID0gY2hhbmdlLnN0YXRlXG4gICAgICBkb2N1bWVudCA9IHN0YXRlLmRvY3VtZW50XG5cbiAgICAgIGNoaWxkcmVuLmZvckVhY2goKGNoaWxkLCBpKSA9PiB7XG4gICAgICAgIGlmIChpID09IDApIHtcbiAgICAgICAgICBjb25zdCBleHRyYSA9IGNoaWxkXG4gICAgICAgICAgY2hpbGQgPSBkb2N1bWVudC5nZXROZXh0QmxvY2soY2hpbGQua2V5KVxuICAgICAgICAgIGNoYW5nZS5yZW1vdmVOb2RlQnlLZXkoZXh0cmEua2V5LCBPUFRTKVxuICAgICAgICB9XG5cbiAgICAgICAgY2hhbmdlLm1vdmVOb2RlQnlLZXkoY2hpbGQua2V5LCBwYXJlbnQua2V5LCBpbmRleCArIDEgKyBpLCBPUFRTKVxuICAgICAgfSlcbiAgICB9XG4gIH0pXG5cbiAgLy8gVE9ETzogb3B0bWl6ZSB0byBvbmx5IG5vcm1hbGl6ZSB0aGUgcmlnaHQgYmxvY2tcbiAgaWYgKG5vcm1hbGl6ZSkge1xuICAgIGNoYW5nZS5ub3JtYWxpemVEb2N1bWVudChTQ0hFTUEpXG4gIH1cbn1cblxuLyoqXG4gKiBVbndyYXAgdGhlIGlubGluZSBub2RlcyBpbiBhIGByYW5nZWAgZnJvbSBhbiBpbmxpbmUgd2l0aCBgcHJvcGVydGllc2AuXG4gKlxuICogQHBhcmFtIHtDaGFuZ2V9IGNoYW5nZVxuICogQHBhcmFtIHtTZWxlY3Rpb259IHJhbmdlXG4gKiBAcGFyYW0ge1N0cmluZ3xPYmplY3R9IHByb3BlcnRpZXNcbiAqIEBwYXJhbSB7T2JqZWN0fSBvcHRpb25zXG4gKiAgIEBwcm9wZXJ0eSB7Qm9vbGVhbn0gbm9ybWFsaXplXG4gKi9cblxuQ2hhbmdlcy51bndyYXBJbmxpbmVBdFJhbmdlID0gKGNoYW5nZSwgcmFuZ2UsIHByb3BlcnRpZXMsIG9wdGlvbnMgPSB7fSkgPT4ge1xuICBwcm9wZXJ0aWVzID0gTm9ybWFsaXplLm5vZGVQcm9wZXJ0aWVzKHByb3BlcnRpZXMpXG5cbiAgY29uc3QgeyBub3JtYWxpemUgPSB0cnVlIH0gPSBvcHRpb25zXG4gIGNvbnN0IHsgc3RhdGUgfSA9IGNoYW5nZVxuICBjb25zdCB7IGRvY3VtZW50IH0gPSBzdGF0ZVxuICBjb25zdCB0ZXh0cyA9IGRvY3VtZW50LmdldFRleHRzQXRSYW5nZShyYW5nZSlcbiAgY29uc3QgaW5saW5lcyA9IHRleHRzXG4gICAgLm1hcCgodGV4dCkgPT4ge1xuICAgICAgcmV0dXJuIGRvY3VtZW50LmdldENsb3Nlc3QodGV4dC5rZXksIChwYXJlbnQpID0+IHtcbiAgICAgICAgaWYgKHBhcmVudC5raW5kICE9ICdpbmxpbmUnKSByZXR1cm4gZmFsc2VcbiAgICAgICAgaWYgKHByb3BlcnRpZXMudHlwZSAhPSBudWxsICYmIHBhcmVudC50eXBlICE9IHByb3BlcnRpZXMudHlwZSkgcmV0dXJuIGZhbHNlXG4gICAgICAgIGlmIChwcm9wZXJ0aWVzLmlzVm9pZCAhPSBudWxsICYmIHBhcmVudC5pc1ZvaWQgIT0gcHJvcGVydGllcy5pc1ZvaWQpIHJldHVybiBmYWxzZVxuICAgICAgICBpZiAocHJvcGVydGllcy5kYXRhICE9IG51bGwgJiYgIXBhcmVudC5kYXRhLmlzU3VwZXJzZXQocHJvcGVydGllcy5kYXRhKSkgcmV0dXJuIGZhbHNlXG4gICAgICAgIHJldHVybiB0cnVlXG4gICAgICB9KVxuICAgIH0pXG4gICAgLmZpbHRlcihleGlzdHMgPT4gZXhpc3RzKVxuICAgIC50b09yZGVyZWRTZXQoKVxuICAgIC50b0xpc3QoKVxuXG4gIGlubGluZXMuZm9yRWFjaCgoaW5saW5lKSA9PiB7XG4gICAgY29uc3QgcGFyZW50ID0gY2hhbmdlLnN0YXRlLmRvY3VtZW50LmdldFBhcmVudChpbmxpbmUua2V5KVxuICAgIGNvbnN0IGluZGV4ID0gcGFyZW50Lm5vZGVzLmluZGV4T2YoaW5saW5lKVxuXG4gICAgaW5saW5lLm5vZGVzLmZvckVhY2goKGNoaWxkLCBpKSA9PiB7XG4gICAgICBjaGFuZ2UubW92ZU5vZGVCeUtleShjaGlsZC5rZXksIHBhcmVudC5rZXksIGluZGV4ICsgaSwgT1BUUylcbiAgICB9KVxuICB9KVxuXG4gIC8vIFRPRE86IG9wdG1pemUgdG8gb25seSBub3JtYWxpemUgdGhlIHJpZ2h0IGJsb2NrXG4gIGlmIChub3JtYWxpemUpIHtcbiAgICBjaGFuZ2Uubm9ybWFsaXplRG9jdW1lbnQoU0NIRU1BKVxuICB9XG59XG5cbi8qKlxuICogV3JhcCBhbGwgb2YgdGhlIGJsb2NrcyBpbiBhIGByYW5nZWAgaW4gYSBuZXcgYGJsb2NrYC5cbiAqXG4gKiBAcGFyYW0ge0NoYW5nZX0gY2hhbmdlXG4gKiBAcGFyYW0ge1NlbGVjdGlvbn0gcmFuZ2VcbiAqIEBwYXJhbSB7QmxvY2t8T2JqZWN0fFN0cmluZ30gYmxvY2tcbiAqIEBwYXJhbSB7T2JqZWN0fSBvcHRpb25zXG4gKiAgIEBwcm9wZXJ0eSB7Qm9vbGVhbn0gbm9ybWFsaXplXG4gKi9cblxuQ2hhbmdlcy53cmFwQmxvY2tBdFJhbmdlID0gKGNoYW5nZSwgcmFuZ2UsIGJsb2NrLCBvcHRpb25zID0ge30pID0+IHtcbiAgYmxvY2sgPSBOb3JtYWxpemUuYmxvY2soYmxvY2spXG4gIGJsb2NrID0gYmxvY2suc2V0KCdub2RlcycsIGJsb2NrLm5vZGVzLmNsZWFyKCkpXG5cbiAgY29uc3QgeyBub3JtYWxpemUgPSB0cnVlIH0gPSBvcHRpb25zXG4gIGNvbnN0IHsgc3RhdGUgfSA9IGNoYW5nZVxuICBjb25zdCB7IGRvY3VtZW50IH0gPSBzdGF0ZVxuXG4gIGNvbnN0IGJsb2NrcyA9IGRvY3VtZW50LmdldEJsb2Nrc0F0UmFuZ2UocmFuZ2UpXG4gIGNvbnN0IGZpcnN0YmxvY2sgPSBibG9ja3MuZmlyc3QoKVxuICBjb25zdCBsYXN0YmxvY2sgPSBibG9ja3MubGFzdCgpXG4gIGxldCBwYXJlbnQsIHNpYmxpbmdzLCBpbmRleFxuXG4gIC8vIElmIHRoZXJlIGlzIG9ubHkgb25lIGJsb2NrIGluIHRoZSBzZWxlY3Rpb24gdGhlbiB3ZSBrbm93IHRoZSBwYXJlbnQgYW5kXG4gIC8vIHNpYmxpbmdzLlxuICBpZiAoYmxvY2tzLmxlbmd0aCA9PT0gMSkge1xuICAgIHBhcmVudCA9IGRvY3VtZW50LmdldFBhcmVudChmaXJzdGJsb2NrLmtleSlcbiAgICBzaWJsaW5ncyA9IGJsb2Nrc1xuICB9XG5cbiAgLy8gRGV0ZXJtaW5lIGNsb3Nlc3Qgc2hhcmVkIHBhcmVudCB0byBhbGwgYmxvY2tzIGluIHNlbGVjdGlvbi5cbiAgZWxzZSB7XG4gICAgcGFyZW50ID0gZG9jdW1lbnQuZ2V0Q2xvc2VzdChmaXJzdGJsb2NrLmtleSwgKHAxKSA9PiB7XG4gICAgICByZXR1cm4gISFkb2N1bWVudC5nZXRDbG9zZXN0KGxhc3RibG9jay5rZXksIHAyID0+IHAxID09IHAyKVxuICAgIH0pXG4gIH1cblxuICAvLyBJZiBubyBzaGFyZWQgcGFyZW50IGNvdWxkIGJlIGZvdW5kIHRoZW4gdGhlIHBhcmVudCBpcyB0aGUgZG9jdW1lbnQuXG4gIGlmIChwYXJlbnQgPT0gbnVsbCkgcGFyZW50ID0gZG9jdW1lbnRcblxuICAvLyBDcmVhdGUgYSBsaXN0IG9mIGRpcmVjdCBjaGlsZHJlbiBzaWJsaW5ncyBvZiBwYXJlbnQgdGhhdCBmYWxsIGluIHRoZVxuICAvLyBzZWxlY3Rpb24uXG4gIGlmIChzaWJsaW5ncyA9PSBudWxsKSB7XG4gICAgY29uc3QgaW5kZXhlcyA9IHBhcmVudC5ub2Rlcy5yZWR1Y2UoKGluZCwgbm9kZSwgaSkgPT4ge1xuICAgICAgaWYgKG5vZGUgPT0gZmlyc3RibG9jayB8fCBub2RlLmhhc0Rlc2NlbmRhbnQoZmlyc3RibG9jay5rZXkpKSBpbmRbMF0gPSBpXG4gICAgICBpZiAobm9kZSA9PSBsYXN0YmxvY2sgfHwgbm9kZS5oYXNEZXNjZW5kYW50KGxhc3RibG9jay5rZXkpKSBpbmRbMV0gPSBpXG4gICAgICByZXR1cm4gaW5kXG4gICAgfSwgW10pXG5cbiAgICBpbmRleCA9IGluZGV4ZXNbMF1cbiAgICBzaWJsaW5ncyA9IHBhcmVudC5ub2Rlcy5zbGljZShpbmRleGVzWzBdLCBpbmRleGVzWzFdICsgMSlcbiAgfVxuXG4gIC8vIEdldCB0aGUgaW5kZXggdG8gcGxhY2UgdGhlIG5ldyB3cmFwcGVkIG5vZGUgYXQuXG4gIGlmIChpbmRleCA9PSBudWxsKSB7XG4gICAgaW5kZXggPSBwYXJlbnQubm9kZXMuaW5kZXhPZihzaWJsaW5ncy5maXJzdCgpKVxuICB9XG5cbiAgLy8gSW5qZWN0IHRoZSBuZXcgYmxvY2sgbm9kZSBpbnRvIHRoZSBwYXJlbnQuXG4gIGNoYW5nZS5pbnNlcnROb2RlQnlLZXkocGFyZW50LmtleSwgaW5kZXgsIGJsb2NrLCBPUFRTKVxuXG4gIC8vIE1vdmUgdGhlIHNpYmxpbmcgbm9kZXMgaW50byB0aGUgbmV3IGJsb2NrIG5vZGUuXG4gIHNpYmxpbmdzLmZvckVhY2goKG5vZGUsIGkpID0+IHtcbiAgICBjaGFuZ2UubW92ZU5vZGVCeUtleShub2RlLmtleSwgYmxvY2sua2V5LCBpLCBPUFRTKVxuICB9KVxuXG4gIGlmIChub3JtYWxpemUpIHtcbiAgICBjaGFuZ2Uubm9ybWFsaXplTm9kZUJ5S2V5KHBhcmVudC5rZXksIFNDSEVNQSlcbiAgfVxufVxuXG4vKipcbiAqIFdyYXAgdGhlIHRleHQgYW5kIGlubGluZXMgaW4gYSBgcmFuZ2VgIGluIGEgbmV3IGBpbmxpbmVgLlxuICpcbiAqIEBwYXJhbSB7Q2hhbmdlfSBjaGFuZ2VcbiAqIEBwYXJhbSB7U2VsZWN0aW9ufSByYW5nZVxuICogQHBhcmFtIHtJbmxpbmV8T2JqZWN0fFN0cmluZ30gaW5saW5lXG4gKiBAcGFyYW0ge09iamVjdH0gb3B0aW9uc1xuICogICBAcHJvcGVydHkge0Jvb2xlYW59IG5vcm1hbGl6ZVxuICovXG5cbkNoYW5nZXMud3JhcElubGluZUF0UmFuZ2UgPSAoY2hhbmdlLCByYW5nZSwgaW5saW5lLCBvcHRpb25zID0ge30pID0+IHtcbiAgbGV0IHsgc3RhdGUgfSA9IGNoYW5nZVxuICBsZXQgeyBkb2N1bWVudCB9ID0gc3RhdGVcbiAgY29uc3QgeyBub3JtYWxpemUgPSB0cnVlIH0gPSBvcHRpb25zXG4gIGNvbnN0IHsgc3RhcnRLZXksIHN0YXJ0T2Zmc2V0LCBlbmRLZXksIGVuZE9mZnNldCB9ID0gcmFuZ2VcblxuICBpZiAocmFuZ2UuaXNDb2xsYXBzZWQpIHtcbiAgICAvLyBXcmFwcGluZyBhbiBpbmxpbmUgdm9pZFxuICAgIGNvbnN0IGlubGluZVBhcmVudCA9IGRvY3VtZW50LmdldENsb3Nlc3RJbmxpbmUoc3RhcnRLZXkpXG4gICAgaWYgKCFpbmxpbmVQYXJlbnQuaXNWb2lkKSB7XG4gICAgICByZXR1cm5cbiAgICB9XG5cbiAgICByZXR1cm4gY2hhbmdlLndyYXBJbmxpbmVCeUtleShpbmxpbmVQYXJlbnQua2V5LCBpbmxpbmUsIG9wdGlvbnMpXG4gIH1cblxuICBpbmxpbmUgPSBOb3JtYWxpemUuaW5saW5lKGlubGluZSlcbiAgaW5saW5lID0gaW5saW5lLnNldCgnbm9kZXMnLCBpbmxpbmUubm9kZXMuY2xlYXIoKSlcblxuICBjb25zdCBibG9ja3MgPSBkb2N1bWVudC5nZXRCbG9ja3NBdFJhbmdlKHJhbmdlKVxuICBsZXQgc3RhcnRCbG9jayA9IGRvY3VtZW50LmdldENsb3Nlc3RCbG9jayhzdGFydEtleSlcbiAgbGV0IGVuZEJsb2NrID0gZG9jdW1lbnQuZ2V0Q2xvc2VzdEJsb2NrKGVuZEtleSlcbiAgbGV0IHN0YXJ0Q2hpbGQgPSBzdGFydEJsb2NrLmdldEZ1cnRoZXN0QW5jZXN0b3Ioc3RhcnRLZXkpXG4gIGxldCBlbmRDaGlsZCA9IGVuZEJsb2NrLmdldEZ1cnRoZXN0QW5jZXN0b3IoZW5kS2V5KVxuXG4gIGNoYW5nZS5zcGxpdERlc2NlbmRhbnRzQnlLZXkoZW5kQ2hpbGQua2V5LCBlbmRLZXksIGVuZE9mZnNldCwgT1BUUylcbiAgY2hhbmdlLnNwbGl0RGVzY2VuZGFudHNCeUtleShzdGFydENoaWxkLmtleSwgc3RhcnRLZXksIHN0YXJ0T2Zmc2V0LCBPUFRTKVxuXG4gIHN0YXRlID0gY2hhbmdlLnN0YXRlXG4gIGRvY3VtZW50ID0gc3RhdGUuZG9jdW1lbnRcbiAgc3RhcnRCbG9jayA9IGRvY3VtZW50LmdldERlc2NlbmRhbnQoc3RhcnRCbG9jay5rZXkpXG4gIGVuZEJsb2NrID0gZG9jdW1lbnQuZ2V0RGVzY2VuZGFudChlbmRCbG9jay5rZXkpXG4gIHN0YXJ0Q2hpbGQgPSBzdGFydEJsb2NrLmdldEZ1cnRoZXN0QW5jZXN0b3Ioc3RhcnRLZXkpXG4gIGVuZENoaWxkID0gZW5kQmxvY2suZ2V0RnVydGhlc3RBbmNlc3RvcihlbmRLZXkpXG4gIGNvbnN0IHN0YXJ0SW5kZXggPSBzdGFydEJsb2NrLm5vZGVzLmluZGV4T2Yoc3RhcnRDaGlsZClcbiAgY29uc3QgZW5kSW5kZXggPSBlbmRCbG9jay5ub2Rlcy5pbmRleE9mKGVuZENoaWxkKVxuXG4gIGlmIChzdGFydEJsb2NrID09IGVuZEJsb2NrKSB7XG4gICAgc3RhdGUgPSBjaGFuZ2Uuc3RhdGVcbiAgICBkb2N1bWVudCA9IHN0YXRlLmRvY3VtZW50XG4gICAgc3RhcnRCbG9jayA9IGRvY3VtZW50LmdldENsb3Nlc3RCbG9jayhzdGFydEtleSlcbiAgICBzdGFydENoaWxkID0gc3RhcnRCbG9jay5nZXRGdXJ0aGVzdEFuY2VzdG9yKHN0YXJ0S2V5KVxuXG4gICAgY29uc3Qgc3RhcnRJbm5lciA9IGRvY3VtZW50LmdldE5leHRTaWJsaW5nKHN0YXJ0Q2hpbGQua2V5KVxuICAgIGNvbnN0IHN0YXJ0SW5uZXJJbmRleCA9IHN0YXJ0QmxvY2subm9kZXMuaW5kZXhPZihzdGFydElubmVyKVxuICAgIGNvbnN0IGVuZElubmVyID0gc3RhcnRLZXkgPT0gZW5kS2V5ID8gc3RhcnRJbm5lciA6IHN0YXJ0QmxvY2suZ2V0RnVydGhlc3RBbmNlc3RvcihlbmRLZXkpXG4gICAgY29uc3QgaW5saW5lcyA9IHN0YXJ0QmxvY2subm9kZXNcbiAgICAgIC5za2lwVW50aWwobiA9PiBuID09IHN0YXJ0SW5uZXIpXG4gICAgICAudGFrZVVudGlsKG4gPT4gbiA9PSBlbmRJbm5lcilcbiAgICAgIC5wdXNoKGVuZElubmVyKVxuXG4gICAgY29uc3Qgbm9kZSA9IGlubGluZS5yZWdlbmVyYXRlS2V5KClcblxuICAgIGNoYW5nZS5pbnNlcnROb2RlQnlLZXkoc3RhcnRCbG9jay5rZXksIHN0YXJ0SW5uZXJJbmRleCwgbm9kZSwgT1BUUylcblxuICAgIGlubGluZXMuZm9yRWFjaCgoY2hpbGQsIGkpID0+IHtcbiAgICAgIGNoYW5nZS5tb3ZlTm9kZUJ5S2V5KGNoaWxkLmtleSwgbm9kZS5rZXksIGksIE9QVFMpXG4gICAgfSlcblxuICAgIGlmIChub3JtYWxpemUpIHtcbiAgICAgIGNoYW5nZS5ub3JtYWxpemVOb2RlQnlLZXkoc3RhcnRCbG9jay5rZXksIFNDSEVNQSlcbiAgICB9XG4gIH1cblxuICBlbHNlIHtcbiAgICBjb25zdCBzdGFydElubGluZXMgPSBzdGFydEJsb2NrLm5vZGVzLnNsaWNlKHN0YXJ0SW5kZXggKyAxKVxuICAgIGNvbnN0IGVuZElubGluZXMgPSBlbmRCbG9jay5ub2Rlcy5zbGljZSgwLCBlbmRJbmRleCArIDEpXG4gICAgY29uc3Qgc3RhcnROb2RlID0gaW5saW5lLnJlZ2VuZXJhdGVLZXkoKVxuICAgIGNvbnN0IGVuZE5vZGUgPSBpbmxpbmUucmVnZW5lcmF0ZUtleSgpXG5cbiAgICBjaGFuZ2UuaW5zZXJ0Tm9kZUJ5S2V5KHN0YXJ0QmxvY2sua2V5LCBzdGFydEluZGV4IC0gMSwgc3RhcnROb2RlLCBPUFRTKVxuICAgIGNoYW5nZS5pbnNlcnROb2RlQnlLZXkoZW5kQmxvY2sua2V5LCBlbmRJbmRleCwgZW5kTm9kZSwgT1BUUylcblxuICAgIHN0YXJ0SW5saW5lcy5mb3JFYWNoKChjaGlsZCwgaSkgPT4ge1xuICAgICAgY2hhbmdlLm1vdmVOb2RlQnlLZXkoY2hpbGQua2V5LCBzdGFydE5vZGUua2V5LCBpLCBPUFRTKVxuICAgIH0pXG5cbiAgICBlbmRJbmxpbmVzLmZvckVhY2goKGNoaWxkLCBpKSA9PiB7XG4gICAgICBjaGFuZ2UubW92ZU5vZGVCeUtleShjaGlsZC5rZXksIGVuZE5vZGUua2V5LCBpLCBPUFRTKVxuICAgIH0pXG5cbiAgICBpZiAobm9ybWFsaXplKSB7XG4gICAgICBjaGFuZ2VcbiAgICAgICAgLm5vcm1hbGl6ZU5vZGVCeUtleShzdGFydEJsb2NrLmtleSwgU0NIRU1BKVxuICAgICAgICAubm9ybWFsaXplTm9kZUJ5S2V5KGVuZEJsb2NrLmtleSwgU0NIRU1BKVxuICAgIH1cblxuICAgIGJsb2Nrcy5zbGljZSgxLCAtMSkuZm9yRWFjaCgoYmxvY2spID0+IHtcbiAgICAgIGNvbnN0IG5vZGUgPSBpbmxpbmUucmVnZW5lcmF0ZUtleSgpXG4gICAgICBjaGFuZ2UuaW5zZXJ0Tm9kZUJ5S2V5KGJsb2NrLmtleSwgMCwgbm9kZSwgT1BUUylcblxuICAgICAgYmxvY2subm9kZXMuZm9yRWFjaCgoY2hpbGQsIGkpID0+IHtcbiAgICAgICAgY2hhbmdlLm1vdmVOb2RlQnlLZXkoY2hpbGQua2V5LCBub2RlLmtleSwgaSwgT1BUUylcbiAgICAgIH0pXG5cbiAgICAgIGlmIChub3JtYWxpemUpIHtcbiAgICAgICAgY2hhbmdlLm5vcm1hbGl6ZU5vZGVCeUtleShibG9jay5rZXksIFNDSEVNQSlcbiAgICAgIH1cbiAgICB9KVxuICB9XG59XG5cbi8qKlxuICogV3JhcCB0aGUgdGV4dCBpbiBhIGByYW5nZWAgaW4gYSBwcmVmaXgvc3VmZml4LlxuICpcbiAqIEBwYXJhbSB7Q2hhbmdlfSBjaGFuZ2VcbiAqIEBwYXJhbSB7U2VsZWN0aW9ufSByYW5nZVxuICogQHBhcmFtIHtTdHJpbmd9IHByZWZpeFxuICogQHBhcmFtIHtTdHJpbmd9IHN1ZmZpeCAob3B0aW9uYWwpXG4gKiBAcGFyYW0ge09iamVjdH0gb3B0aW9uc1xuICogICBAcHJvcGVydHkge0Jvb2xlYW59IG5vcm1hbGl6ZVxuICovXG5cbkNoYW5nZXMud3JhcFRleHRBdFJhbmdlID0gKGNoYW5nZSwgcmFuZ2UsIHByZWZpeCwgc3VmZml4ID0gcHJlZml4LCBvcHRpb25zID0ge30pID0+IHtcbiAgY29uc3QgeyBub3JtYWxpemUgPSB0cnVlIH0gPSBvcHRpb25zXG4gIGNvbnN0IHsgc3RhcnRLZXksIGVuZEtleSB9ID0gcmFuZ2VcbiAgY29uc3Qgc3RhcnQgPSByYW5nZS5jb2xsYXBzZVRvU3RhcnQoKVxuICBsZXQgZW5kID0gcmFuZ2UuY29sbGFwc2VUb0VuZCgpXG5cbiAgaWYgKHN0YXJ0S2V5ID09IGVuZEtleSkge1xuICAgIGVuZCA9IGVuZC5tb3ZlKHByZWZpeC5sZW5ndGgpXG4gIH1cblxuICBjaGFuZ2UuaW5zZXJ0VGV4dEF0UmFuZ2Uoc3RhcnQsIHByZWZpeCwgW10sIHsgbm9ybWFsaXplIH0pXG4gIGNoYW5nZS5pbnNlcnRUZXh0QXRSYW5nZShlbmQsIHN1ZmZpeCwgW10sIHsgbm9ybWFsaXplIH0pXG59XG5cbi8qKlxuICogRXhwb3J0LlxuICpcbiAqIEB0eXBlIHtPYmplY3R9XG4gKi9cblxuZXhwb3J0IGRlZmF1bHQgQ2hhbmdlc1xuIl19