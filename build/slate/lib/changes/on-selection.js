'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

var _normalize = require('../utils/normalize');

var _normalize2 = _interopRequireDefault(_normalize);

var _isEmpty = require('is-empty');

var _isEmpty2 = _interopRequireDefault(_isEmpty);

var _logger = require('../utils/logger');

var _logger2 = _interopRequireDefault(_logger);

var _pick = require('lodash/pick');

var _pick2 = _interopRequireDefault(_pick);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Changes.
 *
 * @type {Object}
 */

var Changes = {};

/**
 * Set `properties` on the selection.
 *
 * @param {Change} change
 * @param {Object} properties
 */

Changes.select = function (change, properties) {
  var options = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};

  properties = _normalize2.default.selectionProperties(properties);

  var _options$snapshot = options.snapshot,
      snapshot = _options$snapshot === undefined ? false : _options$snapshot;
  var state = change.state;
  var document = state.document,
      selection = state.selection;

  var props = {};
  var sel = selection.toJSON();
  var next = selection.merge(properties).normalize(document);
  properties = (0, _pick2.default)(next, Object.keys(properties));

  // Remove any properties that are already equal to the current selection. And
  // create a dictionary of the previous values for all of the properties that
  // are being changed, for the inverse operation.
  for (var k in properties) {
    if (snapshot == false && properties[k] == sel[k]) continue;
    props[k] = properties[k];
  }

  // Resolve the selection keys into paths.
  sel.anchorPath = sel.anchorKey == null ? null : document.getPath(sel.anchorKey);
  delete sel.anchorKey;

  if (props.anchorKey) {
    props.anchorPath = props.anchorKey == null ? null : document.getPath(props.anchorKey);
    delete props.anchorKey;
  }

  sel.focusPath = sel.focusKey == null ? null : document.getPath(sel.focusKey);
  delete sel.focusKey;

  if (props.focusKey) {
    props.focusPath = props.focusKey == null ? null : document.getPath(props.focusKey);
    delete props.focusKey;
  }

  // If the selection moves, clear any marks, unless the new selection
  // properties change the marks in some way.
  var moved = ['anchorPath', 'anchorOffset', 'focusPath', 'focusOffset'].some(function (p) {
    return props.hasOwnProperty(p);
  });

  if (sel.marks && properties.marks == sel.marks && moved) {
    props.marks = null;
  }

  // If there are no new properties to set, abort.
  if ((0, _isEmpty2.default)(props)) {
    return;
  }

  // Apply the operation.
  change.applyOperation({
    type: 'set_selection',
    properties: props,
    selection: sel
  }, snapshot ? { skip: false, merge: false } : {});
};

/**
 * Select the whole document.
 *
 * @param {Change} change
 */

Changes.selectAll = function (change) {
  var state = change.state;
  var document = state.document,
      selection = state.selection;

  var next = selection.moveToRangeOf(document);
  change.select(next);
};

/**
 * Snapshot the current selection.
 *
 * @param {Change} change
 */

Changes.snapshotSelection = function (change) {
  var state = change.state;
  var selection = state.selection;

  change.select(selection, { snapshot: true });
};

/**
 * Set `properties` on the selection.
 *
 * @param {Mixed} ...args
 * @param {Change} change
 */

Changes.moveTo = function (change, properties) {
  _logger2.default.deprecate('0.17.0', 'The `moveTo()` change is deprecated, please use `select()` instead.');
  change.select(properties);
};

/**
 * Unset the selection's marks.
 *
 * @param {Change} change
 */

Changes.unsetMarks = function (change) {
  _logger2.default.deprecate('0.17.0', 'The `unsetMarks()` change is deprecated.');
  change.select({ marks: null });
};

/**
 * Unset the selection, removing an association to a node.
 *
 * @param {Change} change
 */

Changes.unsetSelection = function (change) {
  _logger2.default.deprecate('0.17.0', 'The `unsetSelection()` change is deprecated, please use `deselect()` instead.');
  change.select({
    anchorKey: null,
    anchorOffset: 0,
    focusKey: null,
    focusOffset: 0,
    isFocused: false,
    isBackward: false
  });
};

/**
 * Mix in selection changes that are just a proxy for the selection method.
 */

var PROXY_TRANSFORMS = ['blur', 'collapseTo', 'collapseToAnchor', 'collapseToEnd', 'collapseToEndOf', 'collapseToFocus', 'collapseToStart', 'collapseToStartOf', 'extend', 'extendTo', 'extendToEndOf', 'extendToStartOf', 'flip', 'focus', 'move', 'moveAnchor', 'moveAnchorOffsetTo', 'moveAnchorTo', 'moveAnchorToEndOf', 'moveAnchorToStartOf', 'moveEnd', 'moveEndOffsetTo', 'moveEndTo', 'moveFocus', 'moveFocusOffsetTo', 'moveFocusTo', 'moveFocusToEndOf', 'moveFocusToStartOf', 'moveOffsetsTo', 'moveStart', 'moveStartOffsetTo', 'moveStartTo',
// 'moveTo', Commented out for now, since it conflicts with a deprecated one.
'moveToEnd', 'moveToEndOf', 'moveToRangeOf', 'moveToStart', 'moveToStartOf', 'deselect'];

PROXY_TRANSFORMS.forEach(function (method) {
  Changes[method] = function (change) {
    for (var _len = arguments.length, args = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
      args[_key - 1] = arguments[_key];
    }

    var normalize = method != 'deselect';
    var state = change.state;
    var document = state.document,
        selection = state.selection;

    var next = selection[method].apply(selection, args);
    if (normalize) next = next.normalize(document);
    change.select(next);
  };
});

/**
 * Mix in node-related changes.
 */

var PREFIXES = ['moveTo', 'collapseTo', 'extendTo'];

var DIRECTIONS = ['Next', 'Previous'];

var KINDS = ['Block', 'Inline', 'Text'];

PREFIXES.forEach(function (prefix) {
  var edges = ['Start', 'End'];

  if (prefix == 'moveTo') {
    edges.push('Range');
  }

  edges.forEach(function (edge) {
    DIRECTIONS.forEach(function (direction) {
      KINDS.forEach(function (kind) {
        var get = 'get' + direction + kind;
        var getAtRange = 'get' + kind + 'sAtRange';
        var index = direction == 'Next' ? 'last' : 'first';
        var method = '' + prefix + edge + 'Of';
        var name = '' + method + direction + kind;

        Changes[name] = function (change) {
          var state = change.state;
          var document = state.document,
              selection = state.selection;

          var nodes = document[getAtRange](selection);
          var node = nodes[index]();
          var target = document[get](node.key);
          if (!target) return;
          var next = selection[method](target);
          change.select(next);
        };
      });
    });
  });
});

/**
 * Mix in deprecated changes with a warning.
 */

var DEPRECATED_TRANSFORMS = [['extendBackward', 'extend', 'The `extendBackward(n)` change is deprecated, please use `extend(n)` instead with a negative offset.'], ['extendForward', 'extend', 'The `extendForward(n)` change is deprecated, please use `extend(n)` instead.'], ['moveBackward', 'move', 'The `moveBackward(n)` change is deprecated, please use `move(n)` instead with a negative offset.'], ['moveForward', 'move', 'The `moveForward(n)` change is deprecated, please use `move(n)` instead.'], ['moveStartOffset', 'moveStart', 'The `moveStartOffset(n)` change is deprecated, please use `moveStart(n)` instead.'], ['moveEndOffset', 'moveEnd', 'The `moveEndOffset(n)` change is deprecated, please use `moveEnd()` instead.'], ['moveToOffsets', 'moveOffsetsTo', 'The `moveToOffsets()` change is deprecated, please use `moveOffsetsTo()` instead.'], ['flipSelection', 'flip', 'The `flipSelection()` change is deprecated, please use `flip()` instead.']];

DEPRECATED_TRANSFORMS.forEach(function (_ref) {
  var _ref2 = _slicedToArray(_ref, 3),
      old = _ref2[0],
      current = _ref2[1],
      warning = _ref2[2];

  Changes[old] = function (change) {
    for (var _len2 = arguments.length, args = Array(_len2 > 1 ? _len2 - 1 : 0), _key2 = 1; _key2 < _len2; _key2++) {
      args[_key2 - 1] = arguments[_key2];
    }

    _logger2.default.deprecate('0.17.0', warning);
    var state = change.state;
    var document = state.document,
        selection = state.selection;

    var sel = selection[current].apply(selection, args).normalize(document);
    change.select(sel);
  };
});

/**
 * Export.
 *
 * @type {Object}
 */

exports.default = Changes;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9jaGFuZ2VzL29uLXNlbGVjdGlvbi5qcyJdLCJuYW1lcyI6WyJDaGFuZ2VzIiwic2VsZWN0IiwiY2hhbmdlIiwicHJvcGVydGllcyIsIm9wdGlvbnMiLCJzZWxlY3Rpb25Qcm9wZXJ0aWVzIiwic25hcHNob3QiLCJzdGF0ZSIsImRvY3VtZW50Iiwic2VsZWN0aW9uIiwicHJvcHMiLCJzZWwiLCJ0b0pTT04iLCJuZXh0IiwibWVyZ2UiLCJub3JtYWxpemUiLCJPYmplY3QiLCJrZXlzIiwiayIsImFuY2hvclBhdGgiLCJhbmNob3JLZXkiLCJnZXRQYXRoIiwiZm9jdXNQYXRoIiwiZm9jdXNLZXkiLCJtb3ZlZCIsInNvbWUiLCJoYXNPd25Qcm9wZXJ0eSIsInAiLCJtYXJrcyIsImFwcGx5T3BlcmF0aW9uIiwidHlwZSIsInNraXAiLCJzZWxlY3RBbGwiLCJtb3ZlVG9SYW5nZU9mIiwic25hcHNob3RTZWxlY3Rpb24iLCJtb3ZlVG8iLCJkZXByZWNhdGUiLCJ1bnNldE1hcmtzIiwidW5zZXRTZWxlY3Rpb24iLCJhbmNob3JPZmZzZXQiLCJmb2N1c09mZnNldCIsImlzRm9jdXNlZCIsImlzQmFja3dhcmQiLCJQUk9YWV9UUkFOU0ZPUk1TIiwiZm9yRWFjaCIsIm1ldGhvZCIsImFyZ3MiLCJQUkVGSVhFUyIsIkRJUkVDVElPTlMiLCJLSU5EUyIsInByZWZpeCIsImVkZ2VzIiwicHVzaCIsImVkZ2UiLCJkaXJlY3Rpb24iLCJraW5kIiwiZ2V0IiwiZ2V0QXRSYW5nZSIsImluZGV4IiwibmFtZSIsIm5vZGVzIiwibm9kZSIsInRhcmdldCIsImtleSIsIkRFUFJFQ0FURURfVFJBTlNGT1JNUyIsIm9sZCIsImN1cnJlbnQiLCJ3YXJuaW5nIl0sIm1hcHBpbmdzIjoiOzs7Ozs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUNBOzs7Ozs7QUFFQTs7Ozs7O0FBTUEsSUFBTUEsVUFBVSxFQUFoQjs7QUFFQTs7Ozs7OztBQU9BQSxRQUFRQyxNQUFSLEdBQWlCLFVBQUNDLE1BQUQsRUFBU0MsVUFBVCxFQUFzQztBQUFBLE1BQWpCQyxPQUFpQix1RUFBUCxFQUFPOztBQUNyREQsZUFBYSxvQkFBVUUsbUJBQVYsQ0FBOEJGLFVBQTlCLENBQWI7O0FBRHFELDBCQUd4QkMsT0FId0IsQ0FHN0NFLFFBSDZDO0FBQUEsTUFHN0NBLFFBSDZDLHFDQUdsQyxLQUhrQztBQUFBLE1BSTdDQyxLQUo2QyxHQUluQ0wsTUFKbUMsQ0FJN0NLLEtBSjZDO0FBQUEsTUFLN0NDLFFBTDZDLEdBS3JCRCxLQUxxQixDQUs3Q0MsUUFMNkM7QUFBQSxNQUtuQ0MsU0FMbUMsR0FLckJGLEtBTHFCLENBS25DRSxTQUxtQzs7QUFNckQsTUFBTUMsUUFBUSxFQUFkO0FBQ0EsTUFBTUMsTUFBTUYsVUFBVUcsTUFBVixFQUFaO0FBQ0EsTUFBTUMsT0FBT0osVUFBVUssS0FBVixDQUFnQlgsVUFBaEIsRUFBNEJZLFNBQTVCLENBQXNDUCxRQUF0QyxDQUFiO0FBQ0FMLGVBQWEsb0JBQUtVLElBQUwsRUFBV0csT0FBT0MsSUFBUCxDQUFZZCxVQUFaLENBQVgsQ0FBYjs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxPQUFLLElBQU1lLENBQVgsSUFBZ0JmLFVBQWhCLEVBQTRCO0FBQzFCLFFBQUlHLFlBQVksS0FBWixJQUFxQkgsV0FBV2UsQ0FBWCxLQUFpQlAsSUFBSU8sQ0FBSixDQUExQyxFQUFrRDtBQUNsRFIsVUFBTVEsQ0FBTixJQUFXZixXQUFXZSxDQUFYLENBQVg7QUFDRDs7QUFFRDtBQUNBUCxNQUFJUSxVQUFKLEdBQWlCUixJQUFJUyxTQUFKLElBQWlCLElBQWpCLEdBQXdCLElBQXhCLEdBQStCWixTQUFTYSxPQUFULENBQWlCVixJQUFJUyxTQUFyQixDQUFoRDtBQUNBLFNBQU9ULElBQUlTLFNBQVg7O0FBRUEsTUFBSVYsTUFBTVUsU0FBVixFQUFxQjtBQUNuQlYsVUFBTVMsVUFBTixHQUFtQlQsTUFBTVUsU0FBTixJQUFtQixJQUFuQixHQUEwQixJQUExQixHQUFpQ1osU0FBU2EsT0FBVCxDQUFpQlgsTUFBTVUsU0FBdkIsQ0FBcEQ7QUFDQSxXQUFPVixNQUFNVSxTQUFiO0FBQ0Q7O0FBRURULE1BQUlXLFNBQUosR0FBZ0JYLElBQUlZLFFBQUosSUFBZ0IsSUFBaEIsR0FBdUIsSUFBdkIsR0FBOEJmLFNBQVNhLE9BQVQsQ0FBaUJWLElBQUlZLFFBQXJCLENBQTlDO0FBQ0EsU0FBT1osSUFBSVksUUFBWDs7QUFFQSxNQUFJYixNQUFNYSxRQUFWLEVBQW9CO0FBQ2xCYixVQUFNWSxTQUFOLEdBQWtCWixNQUFNYSxRQUFOLElBQWtCLElBQWxCLEdBQXlCLElBQXpCLEdBQWdDZixTQUFTYSxPQUFULENBQWlCWCxNQUFNYSxRQUF2QixDQUFsRDtBQUNBLFdBQU9iLE1BQU1hLFFBQWI7QUFDRDs7QUFFRDtBQUNBO0FBQ0EsTUFBTUMsUUFBUSxDQUNaLFlBRFksRUFFWixjQUZZLEVBR1osV0FIWSxFQUlaLGFBSlksRUFLWkMsSUFMWSxDQUtQO0FBQUEsV0FBS2YsTUFBTWdCLGNBQU4sQ0FBcUJDLENBQXJCLENBQUw7QUFBQSxHQUxPLENBQWQ7O0FBT0EsTUFBSWhCLElBQUlpQixLQUFKLElBQWF6QixXQUFXeUIsS0FBWCxJQUFvQmpCLElBQUlpQixLQUFyQyxJQUE4Q0osS0FBbEQsRUFBeUQ7QUFDdkRkLFVBQU1rQixLQUFOLEdBQWMsSUFBZDtBQUNEOztBQUVEO0FBQ0EsTUFBSSx1QkFBUWxCLEtBQVIsQ0FBSixFQUFvQjtBQUNsQjtBQUNEOztBQUVEO0FBQ0FSLFNBQU8yQixjQUFQLENBQXNCO0FBQ3BCQyxVQUFNLGVBRGM7QUFFcEIzQixnQkFBWU8sS0FGUTtBQUdwQkQsZUFBV0U7QUFIUyxHQUF0QixFQUlHTCxXQUFXLEVBQUV5QixNQUFNLEtBQVIsRUFBZWpCLE9BQU8sS0FBdEIsRUFBWCxHQUEyQyxFQUo5QztBQUtELENBNUREOztBQThEQTs7Ozs7O0FBTUFkLFFBQVFnQyxTQUFSLEdBQW9CLFVBQUM5QixNQUFELEVBQVk7QUFBQSxNQUN0QkssS0FEc0IsR0FDWkwsTUFEWSxDQUN0QkssS0FEc0I7QUFBQSxNQUV0QkMsUUFGc0IsR0FFRUQsS0FGRixDQUV0QkMsUUFGc0I7QUFBQSxNQUVaQyxTQUZZLEdBRUVGLEtBRkYsQ0FFWkUsU0FGWTs7QUFHOUIsTUFBTUksT0FBT0osVUFBVXdCLGFBQVYsQ0FBd0J6QixRQUF4QixDQUFiO0FBQ0FOLFNBQU9ELE1BQVAsQ0FBY1ksSUFBZDtBQUNELENBTEQ7O0FBT0E7Ozs7OztBQU1BYixRQUFRa0MsaUJBQVIsR0FBNEIsVUFBQ2hDLE1BQUQsRUFBWTtBQUFBLE1BQzlCSyxLQUQ4QixHQUNwQkwsTUFEb0IsQ0FDOUJLLEtBRDhCO0FBQUEsTUFFOUJFLFNBRjhCLEdBRWhCRixLQUZnQixDQUU5QkUsU0FGOEI7O0FBR3RDUCxTQUFPRCxNQUFQLENBQWNRLFNBQWQsRUFBeUIsRUFBRUgsVUFBVSxJQUFaLEVBQXpCO0FBQ0QsQ0FKRDs7QUFNQTs7Ozs7OztBQU9BTixRQUFRbUMsTUFBUixHQUFpQixVQUFDakMsTUFBRCxFQUFTQyxVQUFULEVBQXdCO0FBQ3ZDLG1CQUFPaUMsU0FBUCxDQUFpQixRQUFqQixFQUEyQixxRUFBM0I7QUFDQWxDLFNBQU9ELE1BQVAsQ0FBY0UsVUFBZDtBQUNELENBSEQ7O0FBS0E7Ozs7OztBQU1BSCxRQUFRcUMsVUFBUixHQUFxQixVQUFDbkMsTUFBRCxFQUFZO0FBQy9CLG1CQUFPa0MsU0FBUCxDQUFpQixRQUFqQixFQUEyQiwwQ0FBM0I7QUFDQWxDLFNBQU9ELE1BQVAsQ0FBYyxFQUFFMkIsT0FBTyxJQUFULEVBQWQ7QUFDRCxDQUhEOztBQUtBOzs7Ozs7QUFNQTVCLFFBQVFzQyxjQUFSLEdBQXlCLFVBQUNwQyxNQUFELEVBQVk7QUFDbkMsbUJBQU9rQyxTQUFQLENBQWlCLFFBQWpCLEVBQTJCLCtFQUEzQjtBQUNBbEMsU0FBT0QsTUFBUCxDQUFjO0FBQ1ptQixlQUFXLElBREM7QUFFWm1CLGtCQUFjLENBRkY7QUFHWmhCLGNBQVUsSUFIRTtBQUlaaUIsaUJBQWEsQ0FKRDtBQUtaQyxlQUFXLEtBTEM7QUFNWkMsZ0JBQVk7QUFOQSxHQUFkO0FBUUQsQ0FWRDs7QUFZQTs7OztBQUlBLElBQU1DLG1CQUFtQixDQUN2QixNQUR1QixFQUV2QixZQUZ1QixFQUd2QixrQkFIdUIsRUFJdkIsZUFKdUIsRUFLdkIsaUJBTHVCLEVBTXZCLGlCQU51QixFQU92QixpQkFQdUIsRUFRdkIsbUJBUnVCLEVBU3ZCLFFBVHVCLEVBVXZCLFVBVnVCLEVBV3ZCLGVBWHVCLEVBWXZCLGlCQVp1QixFQWF2QixNQWJ1QixFQWN2QixPQWR1QixFQWV2QixNQWZ1QixFQWdCdkIsWUFoQnVCLEVBaUJ2QixvQkFqQnVCLEVBa0J2QixjQWxCdUIsRUFtQnZCLG1CQW5CdUIsRUFvQnZCLHFCQXBCdUIsRUFxQnZCLFNBckJ1QixFQXNCdkIsaUJBdEJ1QixFQXVCdkIsV0F2QnVCLEVBd0J2QixXQXhCdUIsRUF5QnZCLG1CQXpCdUIsRUEwQnZCLGFBMUJ1QixFQTJCdkIsa0JBM0J1QixFQTRCdkIsb0JBNUJ1QixFQTZCdkIsZUE3QnVCLEVBOEJ2QixXQTlCdUIsRUErQnZCLG1CQS9CdUIsRUFnQ3ZCLGFBaEN1QjtBQWlDdkI7QUFDQSxXQWxDdUIsRUFtQ3ZCLGFBbkN1QixFQW9DdkIsZUFwQ3VCLEVBcUN2QixhQXJDdUIsRUFzQ3ZCLGVBdEN1QixFQXVDdkIsVUF2Q3VCLENBQXpCOztBQTBDQUEsaUJBQWlCQyxPQUFqQixDQUF5QixVQUFDQyxNQUFELEVBQVk7QUFDbkM3QyxVQUFRNkMsTUFBUixJQUFrQixVQUFDM0MsTUFBRCxFQUFxQjtBQUFBLHNDQUFUNEMsSUFBUztBQUFUQSxVQUFTO0FBQUE7O0FBQ3JDLFFBQU0vQixZQUFZOEIsVUFBVSxVQUE1QjtBQURxQyxRQUU3QnRDLEtBRjZCLEdBRW5CTCxNQUZtQixDQUU3QkssS0FGNkI7QUFBQSxRQUc3QkMsUUFINkIsR0FHTEQsS0FISyxDQUc3QkMsUUFINkI7QUFBQSxRQUduQkMsU0FIbUIsR0FHTEYsS0FISyxDQUduQkUsU0FIbUI7O0FBSXJDLFFBQUlJLE9BQU9KLFVBQVVvQyxNQUFWLG1CQUFxQkMsSUFBckIsQ0FBWDtBQUNBLFFBQUkvQixTQUFKLEVBQWVGLE9BQU9BLEtBQUtFLFNBQUwsQ0FBZVAsUUFBZixDQUFQO0FBQ2ZOLFdBQU9ELE1BQVAsQ0FBY1ksSUFBZDtBQUNELEdBUEQ7QUFRRCxDQVREOztBQVdBOzs7O0FBSUEsSUFBTWtDLFdBQVcsQ0FDZixRQURlLEVBRWYsWUFGZSxFQUdmLFVBSGUsQ0FBakI7O0FBTUEsSUFBTUMsYUFBYSxDQUNqQixNQURpQixFQUVqQixVQUZpQixDQUFuQjs7QUFLQSxJQUFNQyxRQUFRLENBQ1osT0FEWSxFQUVaLFFBRlksRUFHWixNQUhZLENBQWQ7O0FBTUFGLFNBQVNILE9BQVQsQ0FBaUIsVUFBQ00sTUFBRCxFQUFZO0FBQzNCLE1BQU1DLFFBQVEsQ0FDWixPQURZLEVBRVosS0FGWSxDQUFkOztBQUtBLE1BQUlELFVBQVUsUUFBZCxFQUF3QjtBQUN0QkMsVUFBTUMsSUFBTixDQUFXLE9BQVg7QUFDRDs7QUFFREQsUUFBTVAsT0FBTixDQUFjLFVBQUNTLElBQUQsRUFBVTtBQUN0QkwsZUFBV0osT0FBWCxDQUFtQixVQUFDVSxTQUFELEVBQWU7QUFDaENMLFlBQU1MLE9BQU4sQ0FBYyxVQUFDVyxJQUFELEVBQVU7QUFDdEIsWUFBTUMsY0FBWUYsU0FBWixHQUF3QkMsSUFBOUI7QUFDQSxZQUFNRSxxQkFBbUJGLElBQW5CLGFBQU47QUFDQSxZQUFNRyxRQUFRSixhQUFhLE1BQWIsR0FBc0IsTUFBdEIsR0FBK0IsT0FBN0M7QUFDQSxZQUFNVCxjQUFZSyxNQUFaLEdBQXFCRyxJQUFyQixPQUFOO0FBQ0EsWUFBTU0sWUFBVWQsTUFBVixHQUFtQlMsU0FBbkIsR0FBK0JDLElBQXJDOztBQUVBdkQsZ0JBQVEyRCxJQUFSLElBQWdCLFVBQUN6RCxNQUFELEVBQVk7QUFBQSxjQUNsQkssS0FEa0IsR0FDUkwsTUFEUSxDQUNsQkssS0FEa0I7QUFBQSxjQUVsQkMsUUFGa0IsR0FFTUQsS0FGTixDQUVsQkMsUUFGa0I7QUFBQSxjQUVSQyxTQUZRLEdBRU1GLEtBRk4sQ0FFUkUsU0FGUTs7QUFHMUIsY0FBTW1ELFFBQVFwRCxTQUFTaUQsVUFBVCxFQUFxQmhELFNBQXJCLENBQWQ7QUFDQSxjQUFNb0QsT0FBT0QsTUFBTUYsS0FBTixHQUFiO0FBQ0EsY0FBTUksU0FBU3RELFNBQVNnRCxHQUFULEVBQWNLLEtBQUtFLEdBQW5CLENBQWY7QUFDQSxjQUFJLENBQUNELE1BQUwsRUFBYTtBQUNiLGNBQU1qRCxPQUFPSixVQUFVb0MsTUFBVixFQUFrQmlCLE1BQWxCLENBQWI7QUFDQTVELGlCQUFPRCxNQUFQLENBQWNZLElBQWQ7QUFDRCxTQVREO0FBVUQsT0FqQkQ7QUFrQkQsS0FuQkQ7QUFvQkQsR0FyQkQ7QUFzQkQsQ0FoQ0Q7O0FBa0NBOzs7O0FBSUEsSUFBTW1ELHdCQUF3QixDQUM1QixDQUFDLGdCQUFELEVBQW1CLFFBQW5CLEVBQTZCLHNHQUE3QixDQUQ0QixFQUU1QixDQUFDLGVBQUQsRUFBa0IsUUFBbEIsRUFBNEIsOEVBQTVCLENBRjRCLEVBRzVCLENBQUMsY0FBRCxFQUFpQixNQUFqQixFQUF5QixrR0FBekIsQ0FINEIsRUFJNUIsQ0FBQyxhQUFELEVBQWdCLE1BQWhCLEVBQXdCLDBFQUF4QixDQUo0QixFQUs1QixDQUFDLGlCQUFELEVBQW9CLFdBQXBCLEVBQWlDLG1GQUFqQyxDQUw0QixFQU01QixDQUFDLGVBQUQsRUFBa0IsU0FBbEIsRUFBNkIsOEVBQTdCLENBTjRCLEVBTzVCLENBQUMsZUFBRCxFQUFrQixlQUFsQixFQUFtQyxtRkFBbkMsQ0FQNEIsRUFRNUIsQ0FBQyxlQUFELEVBQWtCLE1BQWxCLEVBQTBCLDBFQUExQixDQVI0QixDQUE5Qjs7QUFXQUEsc0JBQXNCcEIsT0FBdEIsQ0FBOEIsZ0JBQStCO0FBQUE7QUFBQSxNQUE1QnFCLEdBQTRCO0FBQUEsTUFBdkJDLE9BQXVCO0FBQUEsTUFBZEMsT0FBYzs7QUFDM0RuRSxVQUFRaUUsR0FBUixJQUFlLFVBQUMvRCxNQUFELEVBQXFCO0FBQUEsdUNBQVQ0QyxJQUFTO0FBQVRBLFVBQVM7QUFBQTs7QUFDbEMscUJBQU9WLFNBQVAsQ0FBaUIsUUFBakIsRUFBMkIrQixPQUEzQjtBQURrQyxRQUUxQjVELEtBRjBCLEdBRWhCTCxNQUZnQixDQUUxQkssS0FGMEI7QUFBQSxRQUcxQkMsUUFIMEIsR0FHRkQsS0FIRSxDQUcxQkMsUUFIMEI7QUFBQSxRQUdoQkMsU0FIZ0IsR0FHRkYsS0FIRSxDQUdoQkUsU0FIZ0I7O0FBSWxDLFFBQU1FLE1BQU1GLFVBQVV5RCxPQUFWLG1CQUFzQnBCLElBQXRCLEVBQTRCL0IsU0FBNUIsQ0FBc0NQLFFBQXRDLENBQVo7QUFDQU4sV0FBT0QsTUFBUCxDQUFjVSxHQUFkO0FBQ0QsR0FORDtBQU9ELENBUkQ7O0FBVUE7Ozs7OztrQkFNZVgsTyIsImZpbGUiOiJvbi1zZWxlY3Rpb24uanMiLCJzb3VyY2VzQ29udGVudCI6WyJcbmltcG9ydCBOb3JtYWxpemUgZnJvbSAnLi4vdXRpbHMvbm9ybWFsaXplJ1xuaW1wb3J0IGlzRW1wdHkgZnJvbSAnaXMtZW1wdHknXG5pbXBvcnQgbG9nZ2VyIGZyb20gJy4uL3V0aWxzL2xvZ2dlcidcbmltcG9ydCBwaWNrIGZyb20gJ2xvZGFzaC9waWNrJ1xuXG4vKipcbiAqIENoYW5nZXMuXG4gKlxuICogQHR5cGUge09iamVjdH1cbiAqL1xuXG5jb25zdCBDaGFuZ2VzID0ge31cblxuLyoqXG4gKiBTZXQgYHByb3BlcnRpZXNgIG9uIHRoZSBzZWxlY3Rpb24uXG4gKlxuICogQHBhcmFtIHtDaGFuZ2V9IGNoYW5nZVxuICogQHBhcmFtIHtPYmplY3R9IHByb3BlcnRpZXNcbiAqL1xuXG5DaGFuZ2VzLnNlbGVjdCA9IChjaGFuZ2UsIHByb3BlcnRpZXMsIG9wdGlvbnMgPSB7fSkgPT4ge1xuICBwcm9wZXJ0aWVzID0gTm9ybWFsaXplLnNlbGVjdGlvblByb3BlcnRpZXMocHJvcGVydGllcylcblxuICBjb25zdCB7IHNuYXBzaG90ID0gZmFsc2UgfSA9IG9wdGlvbnNcbiAgY29uc3QgeyBzdGF0ZSB9ID0gY2hhbmdlXG4gIGNvbnN0IHsgZG9jdW1lbnQsIHNlbGVjdGlvbiB9ID0gc3RhdGVcbiAgY29uc3QgcHJvcHMgPSB7fVxuICBjb25zdCBzZWwgPSBzZWxlY3Rpb24udG9KU09OKClcbiAgY29uc3QgbmV4dCA9IHNlbGVjdGlvbi5tZXJnZShwcm9wZXJ0aWVzKS5ub3JtYWxpemUoZG9jdW1lbnQpXG4gIHByb3BlcnRpZXMgPSBwaWNrKG5leHQsIE9iamVjdC5rZXlzKHByb3BlcnRpZXMpKVxuXG4gIC8vIFJlbW92ZSBhbnkgcHJvcGVydGllcyB0aGF0IGFyZSBhbHJlYWR5IGVxdWFsIHRvIHRoZSBjdXJyZW50IHNlbGVjdGlvbi4gQW5kXG4gIC8vIGNyZWF0ZSBhIGRpY3Rpb25hcnkgb2YgdGhlIHByZXZpb3VzIHZhbHVlcyBmb3IgYWxsIG9mIHRoZSBwcm9wZXJ0aWVzIHRoYXRcbiAgLy8gYXJlIGJlaW5nIGNoYW5nZWQsIGZvciB0aGUgaW52ZXJzZSBvcGVyYXRpb24uXG4gIGZvciAoY29uc3QgayBpbiBwcm9wZXJ0aWVzKSB7XG4gICAgaWYgKHNuYXBzaG90ID09IGZhbHNlICYmIHByb3BlcnRpZXNba10gPT0gc2VsW2tdKSBjb250aW51ZVxuICAgIHByb3BzW2tdID0gcHJvcGVydGllc1trXVxuICB9XG5cbiAgLy8gUmVzb2x2ZSB0aGUgc2VsZWN0aW9uIGtleXMgaW50byBwYXRocy5cbiAgc2VsLmFuY2hvclBhdGggPSBzZWwuYW5jaG9yS2V5ID09IG51bGwgPyBudWxsIDogZG9jdW1lbnQuZ2V0UGF0aChzZWwuYW5jaG9yS2V5KVxuICBkZWxldGUgc2VsLmFuY2hvcktleVxuXG4gIGlmIChwcm9wcy5hbmNob3JLZXkpIHtcbiAgICBwcm9wcy5hbmNob3JQYXRoID0gcHJvcHMuYW5jaG9yS2V5ID09IG51bGwgPyBudWxsIDogZG9jdW1lbnQuZ2V0UGF0aChwcm9wcy5hbmNob3JLZXkpXG4gICAgZGVsZXRlIHByb3BzLmFuY2hvcktleVxuICB9XG5cbiAgc2VsLmZvY3VzUGF0aCA9IHNlbC5mb2N1c0tleSA9PSBudWxsID8gbnVsbCA6IGRvY3VtZW50LmdldFBhdGgoc2VsLmZvY3VzS2V5KVxuICBkZWxldGUgc2VsLmZvY3VzS2V5XG5cbiAgaWYgKHByb3BzLmZvY3VzS2V5KSB7XG4gICAgcHJvcHMuZm9jdXNQYXRoID0gcHJvcHMuZm9jdXNLZXkgPT0gbnVsbCA/IG51bGwgOiBkb2N1bWVudC5nZXRQYXRoKHByb3BzLmZvY3VzS2V5KVxuICAgIGRlbGV0ZSBwcm9wcy5mb2N1c0tleVxuICB9XG5cbiAgLy8gSWYgdGhlIHNlbGVjdGlvbiBtb3ZlcywgY2xlYXIgYW55IG1hcmtzLCB1bmxlc3MgdGhlIG5ldyBzZWxlY3Rpb25cbiAgLy8gcHJvcGVydGllcyBjaGFuZ2UgdGhlIG1hcmtzIGluIHNvbWUgd2F5LlxuICBjb25zdCBtb3ZlZCA9IFtcbiAgICAnYW5jaG9yUGF0aCcsXG4gICAgJ2FuY2hvck9mZnNldCcsXG4gICAgJ2ZvY3VzUGF0aCcsXG4gICAgJ2ZvY3VzT2Zmc2V0JyxcbiAgXS5zb21lKHAgPT4gcHJvcHMuaGFzT3duUHJvcGVydHkocCkpXG5cbiAgaWYgKHNlbC5tYXJrcyAmJiBwcm9wZXJ0aWVzLm1hcmtzID09IHNlbC5tYXJrcyAmJiBtb3ZlZCkge1xuICAgIHByb3BzLm1hcmtzID0gbnVsbFxuICB9XG5cbiAgLy8gSWYgdGhlcmUgYXJlIG5vIG5ldyBwcm9wZXJ0aWVzIHRvIHNldCwgYWJvcnQuXG4gIGlmIChpc0VtcHR5KHByb3BzKSkge1xuICAgIHJldHVyblxuICB9XG5cbiAgLy8gQXBwbHkgdGhlIG9wZXJhdGlvbi5cbiAgY2hhbmdlLmFwcGx5T3BlcmF0aW9uKHtcbiAgICB0eXBlOiAnc2V0X3NlbGVjdGlvbicsXG4gICAgcHJvcGVydGllczogcHJvcHMsXG4gICAgc2VsZWN0aW9uOiBzZWwsXG4gIH0sIHNuYXBzaG90ID8geyBza2lwOiBmYWxzZSwgbWVyZ2U6IGZhbHNlIH0gOiB7fSlcbn1cblxuLyoqXG4gKiBTZWxlY3QgdGhlIHdob2xlIGRvY3VtZW50LlxuICpcbiAqIEBwYXJhbSB7Q2hhbmdlfSBjaGFuZ2VcbiAqL1xuXG5DaGFuZ2VzLnNlbGVjdEFsbCA9IChjaGFuZ2UpID0+IHtcbiAgY29uc3QgeyBzdGF0ZSB9ID0gY2hhbmdlXG4gIGNvbnN0IHsgZG9jdW1lbnQsIHNlbGVjdGlvbiB9ID0gc3RhdGVcbiAgY29uc3QgbmV4dCA9IHNlbGVjdGlvbi5tb3ZlVG9SYW5nZU9mKGRvY3VtZW50KVxuICBjaGFuZ2Uuc2VsZWN0KG5leHQpXG59XG5cbi8qKlxuICogU25hcHNob3QgdGhlIGN1cnJlbnQgc2VsZWN0aW9uLlxuICpcbiAqIEBwYXJhbSB7Q2hhbmdlfSBjaGFuZ2VcbiAqL1xuXG5DaGFuZ2VzLnNuYXBzaG90U2VsZWN0aW9uID0gKGNoYW5nZSkgPT4ge1xuICBjb25zdCB7IHN0YXRlIH0gPSBjaGFuZ2VcbiAgY29uc3QgeyBzZWxlY3Rpb24gfSA9IHN0YXRlXG4gIGNoYW5nZS5zZWxlY3Qoc2VsZWN0aW9uLCB7IHNuYXBzaG90OiB0cnVlIH0pXG59XG5cbi8qKlxuICogU2V0IGBwcm9wZXJ0aWVzYCBvbiB0aGUgc2VsZWN0aW9uLlxuICpcbiAqIEBwYXJhbSB7TWl4ZWR9IC4uLmFyZ3NcbiAqIEBwYXJhbSB7Q2hhbmdlfSBjaGFuZ2VcbiAqL1xuXG5DaGFuZ2VzLm1vdmVUbyA9IChjaGFuZ2UsIHByb3BlcnRpZXMpID0+IHtcbiAgbG9nZ2VyLmRlcHJlY2F0ZSgnMC4xNy4wJywgJ1RoZSBgbW92ZVRvKClgIGNoYW5nZSBpcyBkZXByZWNhdGVkLCBwbGVhc2UgdXNlIGBzZWxlY3QoKWAgaW5zdGVhZC4nKVxuICBjaGFuZ2Uuc2VsZWN0KHByb3BlcnRpZXMpXG59XG5cbi8qKlxuICogVW5zZXQgdGhlIHNlbGVjdGlvbidzIG1hcmtzLlxuICpcbiAqIEBwYXJhbSB7Q2hhbmdlfSBjaGFuZ2VcbiAqL1xuXG5DaGFuZ2VzLnVuc2V0TWFya3MgPSAoY2hhbmdlKSA9PiB7XG4gIGxvZ2dlci5kZXByZWNhdGUoJzAuMTcuMCcsICdUaGUgYHVuc2V0TWFya3MoKWAgY2hhbmdlIGlzIGRlcHJlY2F0ZWQuJylcbiAgY2hhbmdlLnNlbGVjdCh7IG1hcmtzOiBudWxsIH0pXG59XG5cbi8qKlxuICogVW5zZXQgdGhlIHNlbGVjdGlvbiwgcmVtb3ZpbmcgYW4gYXNzb2NpYXRpb24gdG8gYSBub2RlLlxuICpcbiAqIEBwYXJhbSB7Q2hhbmdlfSBjaGFuZ2VcbiAqL1xuXG5DaGFuZ2VzLnVuc2V0U2VsZWN0aW9uID0gKGNoYW5nZSkgPT4ge1xuICBsb2dnZXIuZGVwcmVjYXRlKCcwLjE3LjAnLCAnVGhlIGB1bnNldFNlbGVjdGlvbigpYCBjaGFuZ2UgaXMgZGVwcmVjYXRlZCwgcGxlYXNlIHVzZSBgZGVzZWxlY3QoKWAgaW5zdGVhZC4nKVxuICBjaGFuZ2Uuc2VsZWN0KHtcbiAgICBhbmNob3JLZXk6IG51bGwsXG4gICAgYW5jaG9yT2Zmc2V0OiAwLFxuICAgIGZvY3VzS2V5OiBudWxsLFxuICAgIGZvY3VzT2Zmc2V0OiAwLFxuICAgIGlzRm9jdXNlZDogZmFsc2UsXG4gICAgaXNCYWNrd2FyZDogZmFsc2VcbiAgfSlcbn1cblxuLyoqXG4gKiBNaXggaW4gc2VsZWN0aW9uIGNoYW5nZXMgdGhhdCBhcmUganVzdCBhIHByb3h5IGZvciB0aGUgc2VsZWN0aW9uIG1ldGhvZC5cbiAqL1xuXG5jb25zdCBQUk9YWV9UUkFOU0ZPUk1TID0gW1xuICAnYmx1cicsXG4gICdjb2xsYXBzZVRvJyxcbiAgJ2NvbGxhcHNlVG9BbmNob3InLFxuICAnY29sbGFwc2VUb0VuZCcsXG4gICdjb2xsYXBzZVRvRW5kT2YnLFxuICAnY29sbGFwc2VUb0ZvY3VzJyxcbiAgJ2NvbGxhcHNlVG9TdGFydCcsXG4gICdjb2xsYXBzZVRvU3RhcnRPZicsXG4gICdleHRlbmQnLFxuICAnZXh0ZW5kVG8nLFxuICAnZXh0ZW5kVG9FbmRPZicsXG4gICdleHRlbmRUb1N0YXJ0T2YnLFxuICAnZmxpcCcsXG4gICdmb2N1cycsXG4gICdtb3ZlJyxcbiAgJ21vdmVBbmNob3InLFxuICAnbW92ZUFuY2hvck9mZnNldFRvJyxcbiAgJ21vdmVBbmNob3JUbycsXG4gICdtb3ZlQW5jaG9yVG9FbmRPZicsXG4gICdtb3ZlQW5jaG9yVG9TdGFydE9mJyxcbiAgJ21vdmVFbmQnLFxuICAnbW92ZUVuZE9mZnNldFRvJyxcbiAgJ21vdmVFbmRUbycsXG4gICdtb3ZlRm9jdXMnLFxuICAnbW92ZUZvY3VzT2Zmc2V0VG8nLFxuICAnbW92ZUZvY3VzVG8nLFxuICAnbW92ZUZvY3VzVG9FbmRPZicsXG4gICdtb3ZlRm9jdXNUb1N0YXJ0T2YnLFxuICAnbW92ZU9mZnNldHNUbycsXG4gICdtb3ZlU3RhcnQnLFxuICAnbW92ZVN0YXJ0T2Zmc2V0VG8nLFxuICAnbW92ZVN0YXJ0VG8nLFxuICAvLyAnbW92ZVRvJywgQ29tbWVudGVkIG91dCBmb3Igbm93LCBzaW5jZSBpdCBjb25mbGljdHMgd2l0aCBhIGRlcHJlY2F0ZWQgb25lLlxuICAnbW92ZVRvRW5kJyxcbiAgJ21vdmVUb0VuZE9mJyxcbiAgJ21vdmVUb1JhbmdlT2YnLFxuICAnbW92ZVRvU3RhcnQnLFxuICAnbW92ZVRvU3RhcnRPZicsXG4gICdkZXNlbGVjdCcsXG5dXG5cblBST1hZX1RSQU5TRk9STVMuZm9yRWFjaCgobWV0aG9kKSA9PiB7XG4gIENoYW5nZXNbbWV0aG9kXSA9IChjaGFuZ2UsIC4uLmFyZ3MpID0+IHtcbiAgICBjb25zdCBub3JtYWxpemUgPSBtZXRob2QgIT0gJ2Rlc2VsZWN0J1xuICAgIGNvbnN0IHsgc3RhdGUgfSA9IGNoYW5nZVxuICAgIGNvbnN0IHsgZG9jdW1lbnQsIHNlbGVjdGlvbiB9ID0gc3RhdGVcbiAgICBsZXQgbmV4dCA9IHNlbGVjdGlvblttZXRob2RdKC4uLmFyZ3MpXG4gICAgaWYgKG5vcm1hbGl6ZSkgbmV4dCA9IG5leHQubm9ybWFsaXplKGRvY3VtZW50KVxuICAgIGNoYW5nZS5zZWxlY3QobmV4dClcbiAgfVxufSlcblxuLyoqXG4gKiBNaXggaW4gbm9kZS1yZWxhdGVkIGNoYW5nZXMuXG4gKi9cblxuY29uc3QgUFJFRklYRVMgPSBbXG4gICdtb3ZlVG8nLFxuICAnY29sbGFwc2VUbycsXG4gICdleHRlbmRUbycsXG5dXG5cbmNvbnN0IERJUkVDVElPTlMgPSBbXG4gICdOZXh0JyxcbiAgJ1ByZXZpb3VzJyxcbl1cblxuY29uc3QgS0lORFMgPSBbXG4gICdCbG9jaycsXG4gICdJbmxpbmUnLFxuICAnVGV4dCcsXG5dXG5cblBSRUZJWEVTLmZvckVhY2goKHByZWZpeCkgPT4ge1xuICBjb25zdCBlZGdlcyA9IFtcbiAgICAnU3RhcnQnLFxuICAgICdFbmQnLFxuICBdXG5cbiAgaWYgKHByZWZpeCA9PSAnbW92ZVRvJykge1xuICAgIGVkZ2VzLnB1c2goJ1JhbmdlJylcbiAgfVxuXG4gIGVkZ2VzLmZvckVhY2goKGVkZ2UpID0+IHtcbiAgICBESVJFQ1RJT05TLmZvckVhY2goKGRpcmVjdGlvbikgPT4ge1xuICAgICAgS0lORFMuZm9yRWFjaCgoa2luZCkgPT4ge1xuICAgICAgICBjb25zdCBnZXQgPSBgZ2V0JHtkaXJlY3Rpb259JHtraW5kfWBcbiAgICAgICAgY29uc3QgZ2V0QXRSYW5nZSA9IGBnZXQke2tpbmR9c0F0UmFuZ2VgXG4gICAgICAgIGNvbnN0IGluZGV4ID0gZGlyZWN0aW9uID09ICdOZXh0JyA/ICdsYXN0JyA6ICdmaXJzdCdcbiAgICAgICAgY29uc3QgbWV0aG9kID0gYCR7cHJlZml4fSR7ZWRnZX1PZmBcbiAgICAgICAgY29uc3QgbmFtZSA9IGAke21ldGhvZH0ke2RpcmVjdGlvbn0ke2tpbmR9YFxuXG4gICAgICAgIENoYW5nZXNbbmFtZV0gPSAoY2hhbmdlKSA9PiB7XG4gICAgICAgICAgY29uc3QgeyBzdGF0ZSB9ID0gY2hhbmdlXG4gICAgICAgICAgY29uc3QgeyBkb2N1bWVudCwgc2VsZWN0aW9uIH0gPSBzdGF0ZVxuICAgICAgICAgIGNvbnN0IG5vZGVzID0gZG9jdW1lbnRbZ2V0QXRSYW5nZV0oc2VsZWN0aW9uKVxuICAgICAgICAgIGNvbnN0IG5vZGUgPSBub2Rlc1tpbmRleF0oKVxuICAgICAgICAgIGNvbnN0IHRhcmdldCA9IGRvY3VtZW50W2dldF0obm9kZS5rZXkpXG4gICAgICAgICAgaWYgKCF0YXJnZXQpIHJldHVyblxuICAgICAgICAgIGNvbnN0IG5leHQgPSBzZWxlY3Rpb25bbWV0aG9kXSh0YXJnZXQpXG4gICAgICAgICAgY2hhbmdlLnNlbGVjdChuZXh0KVxuICAgICAgICB9XG4gICAgICB9KVxuICAgIH0pXG4gIH0pXG59KVxuXG4vKipcbiAqIE1peCBpbiBkZXByZWNhdGVkIGNoYW5nZXMgd2l0aCBhIHdhcm5pbmcuXG4gKi9cblxuY29uc3QgREVQUkVDQVRFRF9UUkFOU0ZPUk1TID0gW1xuICBbJ2V4dGVuZEJhY2t3YXJkJywgJ2V4dGVuZCcsICdUaGUgYGV4dGVuZEJhY2t3YXJkKG4pYCBjaGFuZ2UgaXMgZGVwcmVjYXRlZCwgcGxlYXNlIHVzZSBgZXh0ZW5kKG4pYCBpbnN0ZWFkIHdpdGggYSBuZWdhdGl2ZSBvZmZzZXQuJ10sXG4gIFsnZXh0ZW5kRm9yd2FyZCcsICdleHRlbmQnLCAnVGhlIGBleHRlbmRGb3J3YXJkKG4pYCBjaGFuZ2UgaXMgZGVwcmVjYXRlZCwgcGxlYXNlIHVzZSBgZXh0ZW5kKG4pYCBpbnN0ZWFkLiddLFxuICBbJ21vdmVCYWNrd2FyZCcsICdtb3ZlJywgJ1RoZSBgbW92ZUJhY2t3YXJkKG4pYCBjaGFuZ2UgaXMgZGVwcmVjYXRlZCwgcGxlYXNlIHVzZSBgbW92ZShuKWAgaW5zdGVhZCB3aXRoIGEgbmVnYXRpdmUgb2Zmc2V0LiddLFxuICBbJ21vdmVGb3J3YXJkJywgJ21vdmUnLCAnVGhlIGBtb3ZlRm9yd2FyZChuKWAgY2hhbmdlIGlzIGRlcHJlY2F0ZWQsIHBsZWFzZSB1c2UgYG1vdmUobilgIGluc3RlYWQuJ10sXG4gIFsnbW92ZVN0YXJ0T2Zmc2V0JywgJ21vdmVTdGFydCcsICdUaGUgYG1vdmVTdGFydE9mZnNldChuKWAgY2hhbmdlIGlzIGRlcHJlY2F0ZWQsIHBsZWFzZSB1c2UgYG1vdmVTdGFydChuKWAgaW5zdGVhZC4nXSxcbiAgWydtb3ZlRW5kT2Zmc2V0JywgJ21vdmVFbmQnLCAnVGhlIGBtb3ZlRW5kT2Zmc2V0KG4pYCBjaGFuZ2UgaXMgZGVwcmVjYXRlZCwgcGxlYXNlIHVzZSBgbW92ZUVuZCgpYCBpbnN0ZWFkLiddLFxuICBbJ21vdmVUb09mZnNldHMnLCAnbW92ZU9mZnNldHNUbycsICdUaGUgYG1vdmVUb09mZnNldHMoKWAgY2hhbmdlIGlzIGRlcHJlY2F0ZWQsIHBsZWFzZSB1c2UgYG1vdmVPZmZzZXRzVG8oKWAgaW5zdGVhZC4nXSxcbiAgWydmbGlwU2VsZWN0aW9uJywgJ2ZsaXAnLCAnVGhlIGBmbGlwU2VsZWN0aW9uKClgIGNoYW5nZSBpcyBkZXByZWNhdGVkLCBwbGVhc2UgdXNlIGBmbGlwKClgIGluc3RlYWQuJ10sXG5dXG5cbkRFUFJFQ0FURURfVFJBTlNGT1JNUy5mb3JFYWNoKChbIG9sZCwgY3VycmVudCwgd2FybmluZyBdKSA9PiB7XG4gIENoYW5nZXNbb2xkXSA9IChjaGFuZ2UsIC4uLmFyZ3MpID0+IHtcbiAgICBsb2dnZXIuZGVwcmVjYXRlKCcwLjE3LjAnLCB3YXJuaW5nKVxuICAgIGNvbnN0IHsgc3RhdGUgfSA9IGNoYW5nZVxuICAgIGNvbnN0IHsgZG9jdW1lbnQsIHNlbGVjdGlvbiB9ID0gc3RhdGVcbiAgICBjb25zdCBzZWwgPSBzZWxlY3Rpb25bY3VycmVudF0oLi4uYXJncykubm9ybWFsaXplKGRvY3VtZW50KVxuICAgIGNoYW5nZS5zZWxlY3Qoc2VsKVxuICB9XG59KVxuXG4vKipcbiAqIEV4cG9ydC5cbiAqXG4gKiBAdHlwZSB7T2JqZWN0fVxuICovXG5cbmV4cG9ydCBkZWZhdWx0IENoYW5nZXNcbiJdfQ==