import { decorate, injectable } from 'inversify';
import { RouterStore as BaseRouterStore } from 'mobx-router5';

decorate(injectable(), BaseRouterStore);

@injectable()
export class RouterStore extends BaseRouterStore {

}



