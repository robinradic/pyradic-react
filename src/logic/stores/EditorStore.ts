import { Store, Unsubscribe } from 'redux'
import { action, observable } from 'mobx';
import { injectable } from 'inversify';
import { Actions, DisplayState, Editables, Editor, State } from '../../interfaces';

@injectable()
export class EditorStore {
    private _editor: Editor
    private store: Store<State>
    private unsubscribe: Unsubscribe;
    private _actions: Actions

    public get editor(): Editor { return this._editor; }

    public get actions(): Actions { return this._actions; }

    @observable public editables: Editables;
    @observable public display: DisplayState;
    @observable public focus: any;
    @observable public settings: { [key: string]: any };

    public setEditor(editor: { store: Store<State>, [key: string]: any }) {
        if ( this.unsubscribe !== undefined ) {
            this.unsubscribe();
        }
        this._editor = editor as any;
        this.store   = editor.store;

        this.setState(editor.store.getState())
        this.unsubscribe = editor.store.subscribe(() => {
            this.setState(editor.store.getState())
        })
    }

    @action
    protected setState(state: State) {
        this.editables = state.editables;
        this.display   = state.display;
        this.focus     = state.focus;
        this.settings  = state.settings;
    }

    public setActions(actions: any) { this._actions = actions;}
}
