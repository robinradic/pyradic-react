import { injectable } from 'inversify';
import { inject, Symbols } from '../ioc';
import { AuthStore, BreakpointStore, EditorStore, RouterStore, UserStore } from '#/stores';
import { action, toJS } from 'mobx';


@injectable()
export class RootStore {
    @inject(Symbols.AuthStore) auth: AuthStore;
    @inject(Symbols.BreakpointStore) breakpoint: BreakpointStore;
    @inject(Symbols.EditorStore) editor: EditorStore;
    @inject(Symbols.RouterStore) router: RouterStore;
    @inject(Symbols.UserStore) user: UserStore;

    @action toJS(...args) {
        if ( args.length === 0 ) {
            let js = {};
            [ 'auth', 'breakpoint', 'editor', 'router', 'user' ].forEach(key => {
                js[ key ] = toJS(this[ key ]);
            })
            return js;
        }
        if ( args.length === 1 ) {
            return toJS(args[ 0 ])
        }
        return args.map(arg => toJS(arg));
    }

    // @inject(Symbols.GameStore) game: GameStore;
    // @inject(Symbols.RoomStore) room: RoomStore;
    //
    // get auth(): AuthStore {return container.get<AuthStore>(Symbols.AuthStore) }
    //
    // get breakpoint(): BreakpointStore {return container.get<BreakpointStore>(Symbols.BreakpointStore) }
    //
    // get game(): GameStore {return container.get<GameStore>(Symbols.GameStore) }
    //
    // get room(): RoomStore {return container.get<RoomStore>(Symbols.RoomStore) }
    //
    // get router(): RouterStore {return container.get<RouterStore>(Symbols.RouterStore) }
    //
    // get user(): UserStore {return container.get<UserStore>(Symbols.UserStore) }
}
