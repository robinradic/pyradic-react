import { PluginFactory } from 'router5/core/plugins';
import { Route } from '#/router';
import { Params, Router } from 'router5/create-router';
import { NavigationOptions } from 'router5/core/navigation';


export const mobxPlugin: (store:RouterStore) => PluginFactory
export type RouteType = 'route' | 'previousRoute' | 'transitionRoute'

export class RouterStore {
    route: Route;
    previousRoute: Route
    transitionRoute: Route
    transitionError: any
    intersectionNode: string
    toActivate: []
    toDeactivate: []
    router: Router;

    setRouter(router: Router)

    updateRoute(routeType: RouteType, route: Route)

    resetRoute(routeType: RouteType)

    // Just an alias
    navigate(routeName: string,
             routeParams: Params,
             options: NavigationOptions)
}
