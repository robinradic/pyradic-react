import React, { Component } from 'react';
import { Route } from '#/router';
import { State } from 'router5/create-router';

export const routeNode    = (name: string) => (RouteComponent: Component) => Component
export const withRoute    = () => (RouteComponent: Component) => Component
export const withLink     = () => (RouteComponent: Component) => Component
export const getComponent = (route: Route | string, routeNodeName: string, routesConfig) => Component

interface RouteViewProps {
    routes: Route[],
    routeNodeName: string
    route: State
}

/**
 * RouteView component: it should be used inside a routeNode component
 *
 * It select and render the correct component associated with the current route for the given routeNodeName
 *
 * @return {object|null}
 */
export class RouteView extends Component<RouteViewProps> {}


interface BaseLinkProps {
    routeParams?: object,
    routeOptions?: object,
    routeName: string,  // not required because onClick could be passed instead
    onClick?: Function,
    className?: string,  // could be passed directly or forwarded (computed) from withRoute/withLink
    children?: React.ReactNode,
    dangerouslySetInnerHTML?: object,
    route?: Route
    isActive?: boolean
}

/**
 * BaseLink component: it generates an anchor element with href computed from props.routeName.
 *
 * This component won't re-render on route change
 *
 * The props `router` or `routerStore` (if decorated with @inject) are required only if props `routeName` is also passed.
 * If props `onClick` is passed then the navigation won't happen and the callback will be executed instead.
 *
 * Usage:
 * `<BaseLink
 *    router={routerInstance}         // optional/required: when we don't inject the routerStore then we need to pass the router explicitly.
 *                                    // If we don't use navigation then it's not required.
 *    routerStore={routerStore}       // optional/required: as above but could be @inject-ed or passed as prop
 *    onClick={onClickCB}             // optional, when passed the navigation will be prevented and the onClickCB will be executed instead
 *    routeName="home"                // optional/required: route to navigate to. When onClick is passed we don't need it
 *    routeParams={routeParamsObj}    // optional, default {}
 *    routeOptions={routeOptionsObj}  // optional, default {}
 */
export class BaseLink extends Component<BaseLinkProps> {
    buildUrl(routeName, routeParams): string

    clickHandler(evt)
}

export class Link extends BaseLink {}

export class NavLink extends BaseLink {}
