import React from 'react'
import OEditor, { createEmptyState, Editable } from 'ory-editor-core'
import 'ory-editor-core/lib/index.css' // we also want to load the stylesheets
import slate from 'ory-editor-plugins-slate' // The rich text area plugin
import 'ory-editor-plugins-slate/lib/index.css' // Stylesheets for the rich text area plugin
import * as displayActions from 'ory-editor-core/lib/actions/display'
import { actions as actionsFn } from 'ory-editor-core/lib/actions'
import tapEventPlugin from 'react-tap-event-plugin';
import { EditorStore } from '#/stores';
import { container, Symbols } from '#/ioc';
import { ContentPlugin, Editor } from 'interfaces';


const SlatePlugin:ContentPlugin = slate();

import { FontAwesomeIcon as Icon, Props as IconProps } from '@fortawesome/react-fontawesome'
import { faICursor } from '@fortawesome/free-solid-svg-icons'
import BlackBorderPlugin from '../plugins/BlackBorderPlugin';
import InputTextFieldPlugin from '../plugins/content/InputTextFieldPlugin';

SlatePlugin.IconComponent = <Icon icon={faICursor} size="3x" />

tapEventPlugin()

const editable = createEmptyState();
const editor:Editor  = new OEditor({
    plugins      : {
        //InputTextFieldPlugin
        content: [ SlatePlugin ], // Define plugins for content cells. To import multiple plugins, use [slate(), image, spacer, divider]
        //layout: [parallax({ defaultPlugin: slate() })] // Define plugins for layout cells
        layout: [BlackBorderPlugin]
    },
    defaultPlugin: SlatePlugin,
    editables    : [ editable ]
})
const actions = actionsFn(editor.store.dispatch)

export const store:EditorStore = new EditorStore();

store.setEditor(editor);
store.setActions(actions);

container.bind(Symbols.EditorStore).toConstantValue(store);
