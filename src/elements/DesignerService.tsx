import { action, computed, observable } from 'mobx';
import { guid, injectable } from 'inversify';
import { ComponentClass } from 'react';

export interface ConfiguratorOptions {
    id: string
    name: string
    props?: any
}

export interface ConfiguratorConfig extends ConfiguratorOptions {
    Component: ComponentClass<{configurator: ConfiguratorOptions, state: ElementState }>
}

export interface ElementOptions {
    id: string
    name: string
    description?: string
    configurator?:string[]
    options?: any
}

export interface ElementConfig extends ElementOptions {
    Component: ComponentClass<{state: ElementState}>
}

export interface ElementState {
    guid: string
    element: ElementConfig
    props: any
    children: Array<ElementState>
}


@injectable()
export class DesignerService {
    elements: Array<ElementConfig> = []

    registerElement(config: ElementConfig) {
        if ( this.elements.find(element => element.id === config.id) === undefined ) {
            this.elements.push(config);
        }
    }

    configurators: Array<ConfiguratorConfig> = []

    registerConfigurator(config: ConfiguratorConfig) {
        if ( this.configurators.find(configurator => configurator.id === config.id) === undefined ) {
            this.configurators.push(config)
        }
    }

    getConfigurator(id):ConfiguratorConfig { return this.configurators.find(configurator => configurator.id === id) }

    @observable state: Array<ElementState> = [];

    @action addElementToState(id, parent?) {
        let element = this.elements.find(element => element.id === id);
        if ( element === undefined ) throw new Error('Element not found ini addElementToState')
        let es = {
            guid    : guid(),
            element,
            props   : {},
            children: []
        }
        this.state.push(es)
    }

    protected findElementState(guid: string, current?: ElementState): ElementState | false {
        if ( current && guid === current.guid ) return current;
        let children = current ? current.children : this.state;
        for ( let i = 0; i < children.length; i ++ ) {
            let result = this.findElementState(guid, children[ i ]);
            if ( result !== false ) return result;
        }
        return false;
    }

    @observable configurator: { element: ElementState }

    @action showConfigurator(guid: string) {
        let element = this.findElementState(guid) as ElementState
        if ( element === false as any ) return;
        this.configurator = {
            element
        }
    }

    @action hideConfigurator() {
        this.configurator = undefined
    }

    @computed get configuratorVisible(): boolean { return this.configurator !== undefined }
}
