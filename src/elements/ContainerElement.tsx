import React, { Component } from 'react';
import { observer } from 'mobx-react';
import { CSSModules, Hot } from 'decorators';
import { classes, style, types } from 'typestyle'
import styles from 'styles/styles.module.scss'
import element from './elementWrapper'

const log = require('debug')('components:ContainerElement')

export interface ContainerElementProps {
    /** Optional CSSProperties with nesting support (using typestyle) */
    style?: types.CSSProperties
    /** Optional className */
    className?: string;
}

/**
 * ContainerElement component
 */

@element({
    id: 'container',
    name: 'Container',
    description: 'A container element',
    configurator: [
        'offsets'
    ],
    options: {

    }
})
@Hot(module)
@observer
export default class ContainerElement extends Component<ContainerElementProps> {
    static displayName: string                 = 'ContainerElement'
    static defaultProps: ContainerElementProps = {}

    render() {
        // const {} = this.props;
        return (
            <div className={this.getClassName()}>
                Hello ContainerElement !
            </div>
        )
    }

    getClassName() { return classes(style(this.props.style), this.props.className); }
}
