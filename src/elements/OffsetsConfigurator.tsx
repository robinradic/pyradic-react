import React, { Component } from 'react';
import { observer } from 'mobx-react';
import { CSSModules, Hot } from 'decorators';
import { classes, style, types } from 'typestyle'
import configurator from './configuratorWrapper'
import * as t from 'io-ts'

const log = require('debug')('components:OffsetsConfigurator')

export interface OffsetsConfiguratorProps {
    /** Optional CSSProperties with nesting support (using typestyle) */
    style?: types.CSSProperties
    /** Optional className */
    className?: string;

}


/**
 * OffsetsConfigurator component
 */
@Hot(module)
@configurator({
    id   : 'offsets',
    name : 'Offsets',
    props: {
        margin : t.type({
            top   : t.tuple([ t.string, t.number ]),
            right : t.tuple([ t.string, t.number ]),
            bottom: t.tuple([ t.string, t.number ]),
            left  : t.tuple([ t.string, t.number ])
        }),
        padding: t.type({
            top   : t.tuple([ t.string, t.number ]),
            right : t.tuple([ t.string, t.number ]),
            bottom: t.tuple([ t.string, t.number ]),
            left  : t.tuple([ t.string, t.number ])
        })
    }
})
@observer
export default class OffsetsConfigurator extends Component<OffsetsConfiguratorProps> {
    static displayName: string                    = 'OffsetsConfigurator'
    static defaultProps: OffsetsConfiguratorProps = {}

    render() {
        // const {} = this.props;
        return (
            <div className={this.getClassName()}>
                Hello OffsetsConfigurator !
            </div>
        )
    }

    getClassName() { return classes(style(this.props.style), this.props.className); }
}
