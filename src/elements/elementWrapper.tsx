import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { observable,computed } from 'mobx';
import {classes} from 'typestyle'
import { observer } from 'mobx-react';
import { container, Symbols } from '#/ioc';
import { DesignerService, ElementState } from './DesignerService';

const log = require('debug')('components:elementWrapper')

export const elementWrapper = (config: any = {}) => {

    return ElementComponent => {

        const ds = container.get<DesignerService>(Symbols.DesignerService);

        @observer
        class Wrapper extends Component<{state:ElementState}> {

            onMouseOver  = (e: React.MouseEvent<any>) => {
                // log('onMouseOver', e)
            }
            onMouseLeave = (e: React.MouseEvent<any>) => {
                // log('onMouseLeave', e)
            }
            onMouseDown = (e: React.MouseEvent<any>) => {
                // log('onMouseDown', e);
                if(!this.selected) {
                    ds.showConfigurator(this.props.state.guid);
                }
            }

            @computed get selected():boolean { return ds.configuratorVisible && ds.configurator.element.guid === this.props.state.guid }

            render() {
                let classNames = ['ds-element-wrapper']
                if(this.selected) classNames.push('active')
                return (
                    <div
                        className={classes(...classNames)}
                        onMouseOver={this.onMouseOver}
                        onMouseLeave={this.onMouseLeave}
                        onMouseOut={this.onMouseLeave}
                        onMouseDown={this.onMouseDown}
                    >
                        <ElementComponent>{this.props.children}</ElementComponent>
                    </div>
                )
            }
        }

        ds.registerElement({
            ...config,
            Component: Wrapper
        })

        return ElementComponent as any;
    }
}

export default elementWrapper;
