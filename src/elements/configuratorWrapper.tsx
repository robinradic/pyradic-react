import React, { Component } from 'react';
import { ConfiguratorOptions, DesignerService, ElementState } from './DesignerService';
import { container, Symbols } from '#/ioc';

const log = require('debug')('components:configuratorWrapper')


export const configuratorWrapper = (config: ConfiguratorOptions) => {

    return ConfiguratorComponent => {
        const ds = container.get<DesignerService>(Symbols.DesignerService);

        class Wrapper extends Component<{configurator: ConfiguratorOptions, state: ElementState }> {
            render() {
                log('render', { me: this })
                return (
                    <ConfiguratorComponent>{this.props.children}</ConfiguratorComponent>
                )
            }
        }

        ds.registerConfigurator({
            ...config,
            Component: Wrapper
        })

        return ConfiguratorComponent as any;
    }
}

export default configuratorWrapper;
