import React, { Component } from 'react';
import { observer } from 'mobx-react';
import { CSSModules, Hot } from 'decorators';
import { classes, style, types } from 'typestyle'
import styles from 'styles/styles.module.scss'

const log = require('debug')('components:GLTest')

export interface GLTestProps {
    /** Optional CSSProperties with nesting support (using typestyle) */
    style?: types.CSSProperties
    /** Optional className */
    className?: string;
}

/**
 * GLTest component
 */
@Hot(module)
@CSSModules(styles)
@observer
export default class GLTest extends Component<GLTestProps & CSSModules.InjectedCSSModuleProps> {
    static displayName: string       = 'GLTest'
    static defaultProps: GLTestProps = {}

    render() {
        // const {} = this.props;
        return (
            <div className={this.getClassName()}>
                Hello GLTest !
            </div>
        )
    }

    getClassName() { return classes(style(this.props.style), this.props.className); }
}
