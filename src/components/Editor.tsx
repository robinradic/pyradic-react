import React, { Component } from 'react';
import { observer } from 'mobx-react';
import { inject, Symbols } from '#/ioc';
import { EditorStore } from '#/stores';
import { Hot, WithStyles } from 'decorators';
import { Layout, List } from 'antd'
import { Menu, MenuItemProps } from 'semantic-ui-react'
import 'semantic-ui-less/definitions/collections/menu.less'

import { FontAwesomeIcon as Icon, Props as IconProps } from '@fortawesome/react-fontawesome'
import { faArrowsAlt, faDesktop, faMobileAlt, faPencilAlt, faPlus, faTable, faTabletAlt } from '@fortawesome/free-solid-svg-icons'
import { Editable } from 'ory-editor-core'
import { ContentPlugin } from '../interfaces';

import dragDropContext from 'ory-editor-core/lib/components/DragDropContext/index'
import draggable from 'ory-editor-ui/lib/Toolbar/Draggable/index'
import 'ory-editor-ui/lib/index.css'

const log                                = require('debug')('component:Layout')
const { Content, Footer, Header, Sider } = Layout;

interface Props {}

@Hot(module)
@WithStyles()
@observer
export default class Editor extends Component<Props & WithStyles.Props> {
    static displayName               = 'Editor'
    static styles: WithStyles.Styles = {
        layout  : { minHeight: '100vh' },
        // sidebar
        list    : {},
        listItem: {
            $nest: {
                '.ory-toolbar-draggable': {
                    display: 'block', background: 'black',
                    height : '20px', width: '20px'

                }
            }
        },

        // header
        header      : { padding: 0, height: '65px' },
        menu        : { height: '100%' },
        menuItem    : { height: '100%' },
        menuItemIcon: { fontSize: 20 },


        content: { border: '1px solid #c7c2c2' }

    }

    @inject(Symbols.EditorStore) store: EditorStore

    render() {
        window[ 'editor' ]   = this;
        const { classNames } = this.props
        return (
            <Layout className={classNames.layout}>
                {this.renderSidebar()}
                <Layout>
                    {this.renderHeader()}
                    <Content className={classNames.content}>
                        <Editable editor={this.store.editor} id={this.store.editables.present[ 0 ].id}/>
                    </Content>
                </Layout>
            </Layout>
        );
    }

    renderSidebar() {
        const { classNames }  = this.props
        const contentPlugins  = this.store.editor.plugins.plugins.content;
        const DragDropContext = dragDropContext(this.store.editor.dragDropContext)
        return (
            <Sider width={250}>
                <h4>Components</h4>
                <DragDropContext>
                    <List
                        itemLayout="horizontal"
                        className={classNames.list}
                        dataSource={contentPlugins}
                        renderItem={(plugin: ContentPlugin) => {
                            const initialState = plugin.createInitialState()
                            log('renderSidebar renderItem', { plugin, initialState })
                            const Draggable = draggable(plugin.name)
                            return (
                                <List.Item key={plugin.name} className={classNames.listItem}>
                                    <List.Item.Meta
                                        title={plugin.text}
                                        description={plugin.description}
                                        avatar={plugin.IconComponent}
                                    />
                                    <span
                                        className="ory-toolbar-item-drag-handle-button">
                                    <Draggable
                                        insert={{ content: { plugin, state: initialState } }}>
                                        <div className="ory-toolbar-item-drag-handle" />
                                    </Draggable>
                                        </span>
                                </List.Item>
                            )
                        }}
                    >
                    </List>
                </DragDropContext>
            </Sider>
        );
    }

    private renderHeader() {
        const { classNames }             = this.props
        let icon: Partial<IconProps>     = { className: classNames.menuItemIcon }
        let item: Partial<MenuItemProps> = {
            className: classNames.menuItem,
            link     : true,
            onClick  : (event: React.MouseEvent<HTMLAnchorElement>, data: MenuItemProps) => {
                if ( Object.keys(this.store.actions.mode).includes(data.name) ) {
                    let mode = this.store.display.mode.replace('resizing', 'resize');
                    if ( mode === data.name ) {
                        return this.store.actions.mode.preview();
                    }
                    this.store.actions.mode[ data.name ]();
                }
            }
        }

        let iname = (name: string): Partial<MenuItemProps> => ({ name, active: this.store.display.mode.replace('resizing', 'resize') === name })

        return (
            <Header className={classNames.header}>
                <Menu
                    pointing
                    className={classNames.menu}
                    icon='labeled'
                    size="mini"
                >
                    <Menu.Item {...item} {...iname('insert')}>
                        <Icon {...icon} icon={faPlus}/>
                        Add
                    </Menu.Item>

                    <Menu.Item {...item} {...iname('edit')}>
                        <Icon {...icon} icon={faPencilAlt}/>
                        Modify
                    </Menu.Item>

                    <Menu.Item {...item} {...iname('layout')}>
                        <Icon {...icon} icon={faTable}/>
                        Arrange
                    </Menu.Item>

                    <Menu.Item {...item} {...iname('resize')}>
                        <Icon {...icon} icon={faArrowsAlt}/>
                        Resize
                    </Menu.Item>

                    <Menu.Menu position="right">
                        <Menu.Item {...item} name='video play'>
                            <Icon {...icon} icon={faDesktop}/>
                            Desktop
                        </Menu.Item>

                        <Menu.Item {...item} name='video play'>
                            <Icon {...icon} icon={faMobileAlt}/>
                            Phone
                        </Menu.Item>

                        <Menu.Item {...item} name='video play'>
                            <Icon {...icon} icon={faTabletAlt}/>
                            Tablet
                        </Menu.Item>
                    </Menu.Menu>
                </Menu>
            </Header>
        );
    }

}
