import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { observer } from 'mobx-react';
import { inject, Symbols } from '#/ioc';
import { EditorStore } from '#/stores';
import { Hot, WithStyles } from 'decorators';
import { Button, Layout, Modal } from 'antd'
import { Menu, MenuItemProps } from 'semantic-ui-react'
import 'semantic-ui-less/definitions/collections/menu.less'

import { FontAwesomeIcon as Icon, Props as IconProps } from '@fortawesome/react-fontawesome'
import { faArrowsAlt, faDesktop, faMobileAlt, faPencilAlt, faPlus, faTable, faTabletAlt } from '@fortawesome/free-solid-svg-icons'
import './designer.scss'
import { action, computed, observable } from 'mobx';
import '../elements'
import { ConfiguratorConfig, DesignerService, ElementState } from '../elements/DesignerService';

const log                                = require('debug')('component:Layout')
const { Content, Footer, Header, Sider } = Layout;
window[ 'ReactDOM' ]                     = ReactDOM;


interface Props {}

@Hot(module)
@WithStyles()
@observer
export default class Designer extends Component<Props & WithStyles.Props> {
    static displayName               = 'Designer'
    static styles: WithStyles.Styles = {
        layout      : { minHeight: '100vh' },
        // sidebar
        list        : {},
        listItem    : {
            $nest: {
                '.ory-toolbar-draggable': {
                    display: 'block', background: 'black',
                    height : '20px', width: '20px'
                }
            }
        },
        // header
        header      : { padding: 0, height: '65px' },
        menu        : { height: '100%' },
        menuItem    : { height: '100%' },
        menuItemIcon: { fontSize: 20 },
        content     : {
            border  : '1px solid #c7c2c2',
            position: 'relative'
        }
    }

    @inject(Symbols.EditorStore) store: EditorStore
    @inject(Symbols.DesignerService) ds: DesignerService


    render() {
        window[ 'editor' ] = this;
        log('render', { me: this })
        const { classNames } = this.props
        return (
            <Layout className={classNames.layout}>
                {this.renderSidebar()}
                <Layout>
                    {this.renderHeader()}
                    <Content className={classNames.content}>
                        {this.ds.state.map(state => <state.element.Component state={state} key={state.guid}/>)}
                    </Content>
                </Layout>
            </Layout>
        );
    }

    @computed get collapsed(): boolean { return this.ds.configuratorVisible === false }

    renderSidebar() {
        const { classNames } = this.props

        return (
            <Sider
                width={250}
                collapsedWidth={0}
                collapsed={this.collapsed}
            >
                {this.ds.configuratorVisible ? this.renderConfigurator() : null}
            </Sider>
        );
    }

    renderConfigurator() {
        if ( ! this.ds.configuratorVisible ) return null;
        let configurators: ConfiguratorConfig[] = [];
        let es: ElementState                    = this.ds.configurator.element
        if ( es.element.configurator ) {
            configurators = es.element.configurator.map(id => this.ds.getConfigurator(id))
        }
        return (
            <div>
                <h4>{es.element.name}</h4>
                {configurators.map(configurator => <configurator.Component configurator={configurator} state={es} key={configurator.id}/>)}
            </div>
        )
    }


    @observable addModalVisible: boolean = false;

    @action showAddModal() { this.addModalVisible = true}

    @action hideAddModal() { this.addModalVisible = false}

    private renderAddModal() {
        return (
            <Modal
                title="Add Element"
                visible={this.addModalVisible}
                onCancel={() => this.hideAddModal()}
                footer={null}
            >
                {this.ds.elements.map(element => <Button key={element.id} onClick={() => {
                    log('add element button click', { element })
                    this.ds.addElementToState(element.id)
                    this.hideAddModal();
                }}>{element.name}</Button>)}

            </Modal>
        )
    }

    private renderHeader() {
        const { classNames }             = this.props
        let icon: Partial<IconProps>     = { className: classNames.menuItemIcon }
        let item: Partial<MenuItemProps> = {
            className: classNames.menuItem,
            link     : true,
            onClick  : (event: React.MouseEvent<HTMLAnchorElement>, data: MenuItemProps) => {

            }
        }

        let iname = (name: string): Partial<MenuItemProps> => ({ name, active: false })

        return (
            <Header className={classNames.header}>
                {this.renderAddModal()}
                <Menu
                    pointing
                    className={classNames.menu}
                    icon='labeled'
                    size="mini"
                >
                    {/*<Menu.Item {...item} name="sidebar_toggle" onClick={() => this.setCollapsed(! this.collapsed)}>
                        <Icon {...icon} icon={this.collapsed ? faAngleDoubleRight : faAngleDoubleLeft}/>
                        {this.collapsed ? 'Open' : 'Close'}
                    </Menu.Item>*/}

                    <Menu.Item {...item} name="add_element" onClick={() => this.showAddModal()}>
                        <Icon {...icon} icon={faPlus}/>
                        Add
                    </Menu.Item>

                    <Menu.Item {...item} {...iname('edit')}>
                        <Icon {...icon} icon={faPencilAlt}/>
                        Modify
                    </Menu.Item>

                    <Menu.Item {...item} {...iname('layout')}>
                        <Icon {...icon} icon={faTable}/>
                        Arrange
                    </Menu.Item>

                    <Menu.Item {...item} {...iname('resize')}>
                        <Icon {...icon} icon={faArrowsAlt}/>
                        Resize
                    </Menu.Item>

                    <Menu.Menu position="right">
                        <Menu.Item {...item} name='video play'>
                            <Icon {...icon} icon={faDesktop}/>
                            Desktop
                        </Menu.Item>

                        <Menu.Item {...item} name='video play'>
                            <Icon {...icon} icon={faMobileAlt}/>
                            Phone
                        </Menu.Item>

                        <Menu.Item {...item} name='video play'>
                            <Icon {...icon} icon={faTabletAlt}/>
                            Tablet
                        </Menu.Item>
                    </Menu.Menu>
                </Menu>
            </Header>
        );
    }

}
