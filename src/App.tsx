import * as React from 'react';
import { observer } from 'mobx-react';
import { inject, Symbols } from '#/ioc';
import { injectable } from 'inversify';

import { Hot } from 'decorators';
// import { Provider, connect } from 'react-redux'
// import Editor from '@/Editor';
import { EditorStore } from '#/stores';
import Designer from '@/Designer';

// import 'utils/ory.tsx'

@Hot(module)
@observer
@injectable()
export default class App extends React.Component {
    @inject(Symbols.EditorStore) store:EditorStore

    render(){
        return (
            <Designer/>
        )
    }
    // render2() {
    //     return (
    //         <Provider store={this.store.editor.store}>
    //             <Editor />
    //         </Provider>
    //     )
    // }
}
